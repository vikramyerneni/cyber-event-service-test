# Time-based counts

- Get statistics for all incidents, unassigned incidents, assigned incidents, and closed incidents
    - GET `/api/v1/incidents/observed-system/{system-id}/statistics/summary`
    - Response content type: `application/json`
    - Example response:

            {
              "requestTimestampMillis": 1586201149939,
              "all": {
                "count": 25,
                "recentDiff": 2 # the difference between the total number of incidents in the past 24 hours and the 24 hours prior to that
              },
              "unassigned": {   # unassigned incidents with a database status of "NEW" or "OPEN"
                "count": 5,
                "recentDiff": 0
              },
              "assigned": {     # assigned incidents with a database status of "NEW" or "OPEN"
                "count": 20,
                "recentDiff": 0
              },
              "closed": {       # incidents with a database status of "CLOSED"
                "count": 3,
                "recentDiff": 1
              }
            }

- Get statistics for the past `numDays` days for all incidents, unassigned incidents, assigned incidents, and closed incidents 
    - GET `/api/v1/incidents/observed-system/{system-id}/statistics`
    - Parameters:
        - `status` (required): acceptable values are `all`, `unassigned`, `assigned`, and `closed`
        - `numDays` (required): acceptable values are `1`, `3`, `7`, and `30`
    - Response content type: `application/json`
    - Example response:

            {
              "requestTimestampMillis": 1586201149939,
              "status": "assigned",
              "numDays": 1,
              "granularity": "3hrs",
              "counts": [ 4, 2, 1, 1, 0, 0, 2, 1 ] # there are eight three-hour intervals in a 24-hour period
            }

    - Response notes:
        - `requestTimestampMillis` is the timestamp, in milliseconds, of when the service received the request (basically the current time)
        - `status` is the same as the identically named request parameter
        - `numDays` is the same as the identically named request parameter
        - `granularity` depends on `numDays` (possible values are `3hrs`, `1day`, and `6days`):

            | `numDays` | `granularity` |
            | --------- | ------------- |
            | `1`       | `3hrs`        |
            | `3`       | `1day`        |
            | `7`       | `1day`        |
            | `30`      | `6days`       |

        - `counts` are the per-`granularity` counts over the past `numDays` days (specifically, `requestTimestampMillis` minus `numDays` days). For example, 30 `numDays` corresponds to a `granularity` of `6days`, meaning that `counts` will be an array that contains `30 / 6 = 5` numbers (we chose 6 days instead of 7 because 6 is a factor of 30). The array elements are sorted in ascending order by time (i.e., the first element is the oldest).

- Get severity-related statistics for the past `numDays` days for all incidents, unassigned incidents, assigned incidents, and closed incidents
    - GET `/api/v1/incidents/observed-system/{system-id}/statistics/severity`
    - Parameters:
        - `status` (required): acceptable values are `all`, `unassigned`, `assigned`, and `closed`
        - `numDays` (required): acceptable values are `1`, `3`, `7`, and `30`
    - Response content type: `application/json`
    - Example response:

            {
              "requestTimestampMillis": 1586201149939,
              "status": "assigned",
              "numDays": 1,
              "severity": {
                "high": 1,
                "medium": 6,
                "low": 4
              }
            }

    - Response notes:
        - `requestTimestampMillis` is the timestamp, in milliseconds, of when the service received the request (basically the current time)
        - `status` is the same as the identically named request parameter
        - `numDays` is the same as the identically named request parameter
        - `severity`: the number of incidents with the given `status` over the past `numDays` days (specifically, `requestTimestampMillis` minus `numDays` days), partitioned by severity level (high, medium, low)

# Time-based filters and exports

- Get all incidents, unassigned incidents, assigned incidents, and closed incidents for the past `numDays` days. This filter involves adding two parameters to an existing endpoint. TODO: Move to INCIDENTS_README.md
    - GET `/api/v1/incidents/observed-system/{system-id}` (already exists)
    - New parameters:
        - `status` (optional): acceptable values are `all`, `unassigned`, `assigned`, and `closed`
        - `numDays` (optional): acceptable values are `1`, `3`, `7`, and `30`
    - The response remains unchanged

- Export all incidents, unassigned incidents, assigned incidents, and closed incidents for the past `numDays` days in a CSV format. This capability involves adding two parameters to an existing endpoint. TODO: Move to INCIDENTS_README.md
    - GET `/api/v1/incidents/observed-system/{system-id}/export?format=csv` (already exists)
    - New parameters:
        - `status` (optional): acceptable values are `all`, `unassigned`, `assigned`, and `closed`
        - `numDays` (optional): acceptable values are `1`, `3`, `7`, and `30`
    - The response remains unchanged
