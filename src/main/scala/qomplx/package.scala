package qomplx

import java.time.Instant

package object event {
  type ObservedSystemId = String
  def now = Instant.now().toEpochMilli
  val UniqueViolationErrorCode = "23505" // https://www.postgresql.org/docs/current/errcodes-appendix.html
}
