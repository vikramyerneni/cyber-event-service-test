package qomplx.event.model

import qomplx.event.directives.UserDetail
import qomplx.event.now
import qomplx.event.routes.{ AttachNoteReq, IncidentNoteDTO }
import qomplx.event.util.Utils

final case class IncidentNote(
  id: String = Utils.generateId(),
  note: String,
  incidentId: String,
  createdOn: Long = now,
  createdBy: Long,
  lastModifiedOn: Long = now,
  lastModifiedBy: Long) {

  def toDTO = IncidentNoteDTO(id, note, createdBy, createdOn)
}

object IncidentNote {
  def map(id: String, note: String, incidentId: String,
    createdOn: Long,
    createdBy: Long,
    lastModifiedOn: Long,
    lastModifiedBy: Long): IncidentNote = {
    IncidentNote(id, note, incidentId, createdOn, createdBy, lastModifiedOn, lastModifiedBy)
  }

  def apply(attachNoteReq: AttachNoteReq, incidentId: String, userDetail: UserDetail): IncidentNote = {
    IncidentNote(note = attachNoteReq.note, incidentId = incidentId, createdBy = userDetail.userId, lastModifiedBy = userDetail.userId)
  }
}