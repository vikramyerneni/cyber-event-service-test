package qomplx.event.model

import com.github.tminglei.slickpg.TsVector
import qomplx.event.model
import qomplx.event.model.IncidentReporterSource.IncidentReporterSource
import qomplx.event.model.IncidentStatus.IncidentStatus
import qomplx.event.model.PropertyType.PropertyType
import qomplx.event.thirdpartyintegrations.model.{ ThirdPartySystem, ThirdPartySystemConfig, TicketingSystemMail }
import spray.json.JsValue

object Tables extends {
  val profile = qomplx.event.util.SlickPostgresProfile
} with Tables

// Slick data model trait for extension
trait Tables {
  val profile: qomplx.event.util.SlickPostgresProfile
  import profile.api._

  class Events(_tableTag: Tag) extends profile.api.Table[EventTable](_tableTag, Some("cyber_event_service"), "amp_events") {
    def * = {
      (eventId, observedSystemId, name, description, `type`, severity,
        timestamp, source, sourceId, categories, tactics, extraInfo,
        tokens_simple, incIdx, agentId, ruleId, customerAccountId) <> (EventTable.tupled, EventTable.unapply)
    }

    val eventId: Rep[String] = column[String]("event_id", O.PrimaryKey)
    val observedSystemId: Rep[Option[String]] = column[Option[String]]("observed_system_id")
    val name: Rep[String] = column[String]("name")
    val description: Rep[String] = column[String]("description")
    val `type`: Rep[String] = column[String]("type")
    val severity: Rep[Int] = column[Int]("severity")
    val timestamp: Rep[Long] = column[Long]("timestamp")
    val source: Rep[String] = column[String]("source")
    val sourceId: Rep[String] = column[String]("source_id")
    val categories: Rep[List[String]] = column[List[String]]("categories")
    val tactics: Rep[List[String]] = column[List[String]]("tactics")
    val extraInfo: Rep[Option[JsValue]] = column[JsValue]("extra_info")
    val tokens_simple: Rep[TsVector] = column[TsVector]("tokens_simple")
    val incIdx: Rep[Option[Long]] = column[Long]("inc_idx", O.AutoInc)
    val agentId: Rep[Option[String]] = column[Option[String]]("agent_id")
    val ruleId: Rep[Option[String]] = column[Option[String]]("rule_id")
    val customerAccountId: Rep[Option[String]] = column[Option[String]]("customer_account_id")

    val index1 = index("idx_obs_sys_id", observedSystemId)
    val index2 = index("idx_timestamp", timestamp)
    val index3 = index("amp_events_textsearch_simple_idx", tokens_simple)
    val index4 = index("idx_inc_idx", incIdx)
  }

  lazy val Events = TableQuery[Events]

  case class EventTable(
    eventId: String,
    observedSystemId: Option[String],
    name: String,
    description: String,
    `type`: String,
    severity: Int,
    timestamp: Long,
    source: String,
    sourceId: String,
    categories: List[String],
    tactics: List[String],
    extraInfo: Option[JsValue],
    tokens_simple: TsVector,
    incIdx: Option[Long],
    agentId: Option[String],
    ruleId: Option[String],
    customerAccountId: Option[String])

  class Incidents(_tableTag: Tag) extends profile.api.Table[Incident](_tableTag, Some("cyber_event_service"), "amp_incidents") {
    def * = (id.?, title, description, categories, tactics, resolution, resolutionTimestamp, status, severity, reportTimestamp,
      reporterId, reporterSource, tagId, assignee, assigneeEmail, createdOn, createdBy, lastModifiedOn, lastModifiedBy,
      reporterIdUnique.?, createdByUnique.?, lastModifiedByUnique.?) <> ((Incident.map _).tupled, Incident.unapply)

    val id: Rep[String] = column[String]("id", O.PrimaryKey)
    val title: Rep[String] = column[String]("title")
    val description: Rep[Option[String]] = column[String]("description")
    val categories = column[Option[List[String]]]("categories")
    val tactics = column[Option[List[String]]]("tactics")
    val resolution: Rep[Option[String]] = column[Option[String]]("resolution", O.Default(None))
    val resolutionTimestamp: Rep[Option[Long]] = column[Option[Long]]("resolution_timestamp", O.Default(None))
    val status: Rep[IncidentStatus] = column[IncidentStatus]("status")
    val severity: Rep[Int] = column[Int]("severity", O.Default(0))
    val reportTimestamp: Rep[Long] = column[Long]("report_timestamp")
    val reporterId: Rep[Long] = column[Long]("reporter_id")
    val reporterSource: Rep[IncidentReporterSource] = column[IncidentReporterSource]("reporter_source")
    val tagId: Rep[String] = column[String]("tag_id")
    val assignee: Rep[Option[Long]] = column[Option[Long]]("assignee", O.Default(None))
    val assigneeEmail: Rep[Option[String]] = column[Option[String]]("assignee_email", O.Default(None))
    val createdOn: Rep[Long] = column[Long]("created_on")
    val createdBy: Rep[Long] = column[Long]("created_by")
    val lastModifiedOn: Rep[Long] = column[Long]("last_modified_on")
    val lastModifiedBy: Rep[Long] = column[Long]("last_modified_by")
    // temporary columns
    val reporterIdUnique: Rep[String] = column[String]("reporter_id_unique")
    val createdByUnique: Rep[String] = column[String]("created_by_unique")
    val lastModifiedByUnique: Rep[String] = column[String]("last_modified_by_unique")
  }

  lazy val Incidents = TableQuery[Incidents]

  class EventIncidentJoinTable(_tableTag: Tag) extends profile.api.Table[(String, String)](_tableTag, Some("cyber_event_service"), "amp_event_incident") {
    def * = (eventId, incidentId)

    val eventId: Rep[String] = column[String]("amp_event_id")
    val incidentId: Rep[String] = column[String]("amp_incident_id")

    val pk = primaryKey("pk_amp_event_incident", (eventId, incidentId))

    lazy val eventsFk = foreignKey("fk_amp_event_incident_amp_event_id", eventId, Events)(r => r.eventId)
    lazy val incidentsFk = foreignKey("fk_amp_event_incident_amp_incident_id", incidentId, Incidents)(r => r.id)
  }

  lazy val EventsInIncident = TableQuery[EventIncidentJoinTable]

  class IncidentFiles(_tableTag: Tag) extends profile.api.Table[IncidentFile](_tableTag, Some("cyber_event_service"), "amp_incident_files") {
    def * = (id, name, description, s3Key, incidentId, createdOn, createdBy, lastModifiedOn, lastModifiedBy) <> ((IncidentFile.map _).tupled, IncidentFile.unapply)

    val id: Rep[String] = column[String]("id", O.PrimaryKey)
    val name: Rep[String] = column[String]("name")
    val description: Rep[Option[String]] = column[Option[String]]("description", O.Default(None))
    val s3Key: Rep[String] = column[String]("s3_key")
    val incidentId: Rep[String] = column[String]("incident_id")
    val createdOn: Rep[Long] = column[Long]("created_on")
    val createdBy: Rep[Long] = column[Long]("created_by")
    val lastModifiedOn: Rep[Long] = column[Long]("last_modified_on")
    val lastModifiedBy: Rep[Long] = column[Long]("last_modified_by")

    lazy val incidentsFk = foreignKey("fk_amp_incident_files_incident_id", incidentId, Incidents)(r => r.id)
  }

  lazy val IncidentFiles = TableQuery[IncidentFiles]

  class IncidentURLs(_tableTag: Tag) extends profile.api.Table[IncidentURL](_tableTag, Some("cyber_event_service"), "amp_incident_urls") {
    def * = (id, url, incidentId, createdOn, createdBy, lastModifiedOn, lastModifiedBy) <> ((IncidentURL.map _).tupled, IncidentURL.unapply)

    val id: Rep[String] = column[String]("id", O.PrimaryKey)
    val url: Rep[String] = column[String]("url")
    val incidentId: Rep[String] = column[String]("incident_id")
    val createdOn: Rep[Long] = column[Long]("created_on")
    val createdBy: Rep[Long] = column[Long]("created_by")
    val lastModifiedOn: Rep[Long] = column[Long]("last_modified_on")
    val lastModifiedBy: Rep[Long] = column[Long]("last_modified_by")

    lazy val incidentsFk = foreignKey("fk_amp_incident_urls_incident_id", incidentId, Incidents)(r => r.id)
  }

  lazy val IncidentURLs = TableQuery[IncidentURLs]

  class IncidentNotes(_tableTag: Tag) extends profile.api.Table[IncidentNote](_tableTag, Some("cyber_event_service"), "amp_incident_notes") {
    def * = (id, note, incidentId, createdOn, createdBy, lastModifiedOn, lastModifiedBy) <> ((IncidentNote.map _).tupled, IncidentNote.unapply)

    val id: Rep[String] = column[String]("id", O.PrimaryKey)
    val note: Rep[String] = column[String]("note")
    val incidentId: Rep[String] = column[String]("incident_id")
    val createdOn: Rep[Long] = column[Long]("created_on")
    val createdBy: Rep[Long] = column[Long]("created_by")
    val lastModifiedOn: Rep[Long] = column[Long]("last_modified_on")
    val lastModifiedBy: Rep[Long] = column[Long]("last_modified_by")

    lazy val incidentsFk = foreignKey("fk_amp_incident_notes_incident_id", incidentId, Incidents)(r => r.id)
  }

  lazy val IncidentNotes = TableQuery[IncidentNotes]

  class Categories(_tableTag: Tag) extends profile.api.Table[Category](_tableTag, Some("cyber_event_service"), "amp_categories") {
    def * = name <> (Category, Category.unapply)

    val name: Rep[String] = column[String]("name")
  }

  lazy val Categories = TableQuery[Categories]

  class Tactics(_tableTag: Tag) extends profile.api.Table[Tactic](_tableTag, Some("cyber_event_service"), "amp_tactics") {
    def * = name <> (Tactic, Tactic.unapply)

    val name: Rep[String] = column[String]("name")
  }

  lazy val Tactics = TableQuery[Tactics]

  class TicketingSystemConfigs(_tableTag: Tag) extends profile.api.Table[ThirdPartySystemConfig](_tableTag, Some("cyber_event_service"), "ns_third_party_integration_configs") {
    def * = (id.?, tagId, thirdPartySystem, name, description, emails, encryptedToken, enabled, createdOn, createdBy, lastModifiedOn, lastModifiedBy, recentlyEnabled) <> ((ThirdPartySystemConfig.apply _).tupled, ThirdPartySystemConfig.unapply)

    val id: Rep[String] = column[String]("id", O.PrimaryKey)
    val tagId: Rep[String] = column[String]("tag_id")
    val thirdPartySystem: Rep[ThirdPartySystem.ThirdPartySystem] = column[ThirdPartySystem.ThirdPartySystem]("third_party_system")
    val name: Rep[String] = column[String]("name")
    val description: Rep[Option[String]] = column[Option[String]]("description")
    val emails: Rep[Option[List[String]]] = column[Option[List[String]]]("emails")
    val encryptedToken: Rep[Array[Byte]] = column[Array[Byte]]("encrypted_token")
    val enabled: Rep[Boolean] = column[Boolean]("enabled")
    val createdOn: Rep[Long] = column[Long]("created_on")
    val createdBy: Rep[String] = column[String]("created_by")
    val lastModifiedOn: Rep[Long] = column[Long]("last_modified_on")
    val lastModifiedBy: Rep[String] = column[String]("last_modified_by")
    val recentlyEnabled: Rep[Boolean] = column[Boolean]("recently_enabled")

    val index1 = index("ns_third_party_integration_configs_pkey", id)
    val index2 = index("third_party_system_name_unique", (thirdPartySystem, name), true)
    val index3 = index("idx_ns_ts_configurations_observed_system_id", tagId)
  }

  lazy val TicketingSystemConfigs = TableQuery[TicketingSystemConfigs]

  class TicketingSystemMails(_tableTag: Tag) extends profile.api.Table[TicketingSystemMail](_tableTag, Some("cyber_event_service"), "ns_ticketing_system_mails") {
    def * = (id.?, configId, incId, incTitle, incStatus, incCategories, incTactics, incEventIds, incCreatedOn, delivered,
      createdOn, lastModifiedOn, thirdPartySystem, emails, incSeverity) <> ((TicketingSystemMail.apply _).tupled, TicketingSystemMail.unapply)

    val id: Rep[String] = column[String]("id", O.PrimaryKey)
    val configId: Rep[String] = column[String]("config_id")
    val incId: Rep[String] = column[String]("inc_id")
    val incTitle: Rep[String] = column[String]("inc_title")
    val incStatus: Rep[IncidentStatus] = column[IncidentStatus]("inc_status")
    val incCategories: Rep[List[String]] = column[List[String]]("inc_categories")
    val incTactics: Rep[List[String]] = column[List[String]]("inc_tactics")
    val incEventIds: Rep[List[String]] = column[List[String]]("inc_event_ids")
    val incCreatedOn: Rep[Long] = column[Long]("inc_created_on")
    val delivered: Rep[Boolean] = column[Boolean]("delivered")
    val createdOn: Rep[Long] = column[Long]("created_on")
    val lastModifiedOn: Rep[Long] = column[Long]("last_modified_on")
    val thirdPartySystem: Rep[ThirdPartySystem.ThirdPartySystem] = column[ThirdPartySystem.ThirdPartySystem]("third_party_system")
    val emails: Rep[List[String]] = column[List[String]]("emails")
    val incSeverity: Rep[Int] = column[Int]("inc_severity")

    val index1 = index("ns_ticketing_system_mails_pkey", id)
    val index2 = index("idx_ns_mails_integration_id", configId)
  }

  lazy val TicketingSystemMails = TableQuery[TicketingSystemMails]

  class Tags(_tableTag: Tag) extends profile.api.Table[model.Tag](_tableTag, Some("cyber_event_service"), "amp_tags") {
    def * = (kind, propertyId, tagId) <> (model.Tag.tupled, model.Tag.unapply)

    val kind: Rep[PropertyType] = column[PropertyType]("kind")
    val propertyId: Rep[String] = column[String]("property_id")
    val tagId: Rep[String] = column[String]("tag_id")

    val index1 = index("amp_tags_kind_property_tag_key", (kind, propertyId, tagId), true)
  }

  lazy val Tags = TableQuery[Tags]
}
