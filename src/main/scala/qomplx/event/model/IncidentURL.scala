package qomplx.event.model

import qomplx.event.directives.UserDetail
import qomplx.event.now
import qomplx.event.routes.{ AttachURLReq, IncidentUrlDTO }
import qomplx.event.util.Utils

final case class IncidentURL(
  id: String = Utils.generateId(),
  url: String,
  incidentId: String,
  createdOn: Long = now,
  createdBy: Long,
  lastModifiedOn: Long = now,
  lastModifiedBy: Long) {

  def toDTO = IncidentUrlDTO(id, url, createdBy, createdOn)
}

object IncidentURL {
  def map(id: String, url: String, incidentId: String,
    createdOn: Long,
    createdBy: Long,
    lastModifiedOn: Long,
    lastModifiedBy: Long): IncidentURL = {
    IncidentURL(id, url, incidentId, createdOn, createdBy, lastModifiedOn, lastModifiedBy)
  }

  def apply(attachURLReq: AttachURLReq, incidentId: String, userDetail: UserDetail): IncidentURL = {
    IncidentURL(url = attachURLReq.url, incidentId = incidentId, createdBy = userDetail.userId, lastModifiedBy = userDetail.userId)
  }
}