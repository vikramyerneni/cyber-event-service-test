package qomplx.event.model

final case class Category(name: String)
final case class Tactic(name: String)
