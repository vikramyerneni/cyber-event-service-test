package qomplx.event.model

object PropertyType extends Enumeration {
  type PropertyType = Value
  val AGENT, RULE = Value
}

final case class Tag(kind: PropertyType.PropertyType, propertyId: String, tagId: String)
