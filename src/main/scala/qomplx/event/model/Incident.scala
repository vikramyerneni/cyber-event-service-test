package qomplx.event.model

import qomplx.event.directives.UserDetail
import qomplx.event.model.IncidentReporterSource.IncidentReporterSource
import qomplx.event.model.IncidentStatus.IncidentStatus
import qomplx.event.routes.{ CreateIncidentReq, IncidentDTO, UpdateIncidentReq }
import qomplx.event.{ ObservedSystemId, now }

final case class Incident(
  id: Option[String] = None, // generated in this service, if not provided
  title: String,
  description: Option[String] = None,
  categories: Option[List[String]] = None,
  tactics: Option[List[String]] = None,
  resolution: Option[String] = None,
  resolutionTimestamp: Option[Long] = None,
  status: IncidentStatus = IncidentStatus.NEW,
  severity: Int,
  reportTimestamp: Long = now,
  reporterId: Long,
  reporterSource: IncidentReporterSource = IncidentReporterSource.USER,
  tagId: String,
  assigneeId: Option[Long] = None,
  assigneeEmail: Option[String] = None,
  createdOn: Long = now,
  createdBy: Long,
  lastModifiedOn: Long = now,
  lastModifiedBy: Long,
  reporterIdUnique: Option[String] = None,
  createdByUnique: Option[String] = None,
  lastModifiedByUnique: Option[String] = None) {

  def toIncidentDTO = IncidentDTO(id, title, description, categories, tactics, resolution, resolutionTimestamp, status, severity, reportTimestamp, reporterId, reporterSource, tagId, assigneeId, assigneeEmail, Nil)
}

object Incident {

  def map(id: Option[String] = None, title: String, description: Option[String] = None, categories: Option[List[String]], tactics: Option[List[String]], resolution: Option[String] = None, resolutionTimestamp: Option[Long] = None, status: IncidentStatus = IncidentStatus.NEW,
    severity: Int, reportTimestamp: Long = now, reporterId: Long, reporterSource: IncidentReporterSource = IncidentReporterSource.USER, observedSystemId: String,
    assigneeId: Option[Long] = None, assigneeEmail: Option[String] = None, createdOn: Long = now, createdBy: Long, lastModifiedOn: Long = now, lastModifiedBy: Long,
    reporterIdUnique: Option[String], createdByUnique: Option[String], lastModifiedByUnique: Option[String]): Incident = {
    apply(id, title, description, categories, tactics, resolution, resolutionTimestamp, status, severity, reportTimestamp, reporterId, reporterSource,
      observedSystemId, assigneeId, assigneeEmail, createdOn, createdBy, lastModifiedBy, lastModifiedOn, reporterIdUnique, createdByUnique, lastModifiedByUnique)
  }

  def apply(req: CreateIncidentReq, userDetail: UserDetail, observedSystemId: ObservedSystemId): Incident = {
    val incident =
      Incident(title = req.title, description = req.description, categories = req.categories, tactics = req.tactics, severity = req.severity,
        reporterId = userDetail.userId, tagId = observedSystemId, createdBy = userDetail.userId, lastModifiedBy = userDetail.userId,
        reporterIdUnique = Some(userDetail.userUniqueId), createdByUnique = Some(userDetail.userUniqueId), lastModifiedByUnique = Some(userDetail.userUniqueId))
    req.reporterSource match {
      case Some(source) => incident.copy(reporterSource = source)
      case None => incident
    }
  }

  def apply(id: String, req: UpdateIncidentReq, userDetail: UserDetail, observedSystemId: ObservedSystemId): Incident = {
    Incident(id = Some(id), title = req.title, description = req.description, categories = req.categories, tactics = req.tactics, resolution = req.resolution, resolutionTimestamp = req.resolutionTimestamp, status = req.status, reporterId = userDetail.userId,
      severity = req.severity, reporterSource = req.reporterSource, tagId = observedSystemId, assigneeId = req.assigneeId, assigneeEmail = req.assigneeEmail, createdBy = userDetail.userId, lastModifiedBy = userDetail.userId,
      reporterIdUnique = Some(userDetail.userUniqueId), createdByUnique = Some(userDetail.userUniqueId), lastModifiedByUnique = Some(userDetail.userUniqueId))
  }
}

object IncidentStatus extends Enumeration {
  type IncidentStatus = Value
  val NEW, OPEN, CLOSED = Value
}

object IncidentReporterSource extends Enumeration {
  type IncidentReporterSource = Value
  val USER, SYSTEM = Value
}
