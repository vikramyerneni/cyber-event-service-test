package qomplx.event.model

import qomplx.event.directives.UserDetail
import qomplx.event.now
import qomplx.event.routes.{ AttachFileReq, IncidentFileDTO }
import qomplx.event.util.Utils

final case class IncidentFile(
  id: String = Utils.generateId(),
  name: String,
  description: Option[String],
  s3Key: String,
  incidentId: String,
  createdOn: Long = now,
  createdBy: Long,
  lastModifiedOn: Long = now,
  lastModifiedBy: Long) {

  def toDTO = IncidentFileDTO(id, name, description, createdBy, createdOn)
}

object IncidentFile {
  def map(id: String, name: String, description: Option[String], s3Key: String,
    incidentId: String,
    createdOn: Long,
    createdBy: Long,
    lastModifiedOn: Long,
    lastModifiedBy: Long): IncidentFile = {
    IncidentFile(id, name, description, s3Key, incidentId, createdOn, createdBy, lastModifiedOn, lastModifiedBy)
  }

  def apply(attachFileReq: AttachFileReq, incidentId: String, userDetail: UserDetail): IncidentFile = {
    IncidentFile(name = attachFileReq.name, description = attachFileReq.description.orElse(Some(attachFileReq.name)), s3Key = attachFileReq.s3Key, incidentId = incidentId, createdBy = userDetail.userId, lastModifiedBy = userDetail.userId)
  }
}
