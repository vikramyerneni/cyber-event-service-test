package qomplx.event.routes

import java.time.Instant
import java.util.UUID

import akka.http.scaladsl.common.ToNameReceptacleEnhancements._string2NR
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.model.Multipart.BodyPart
import akka.http.scaladsl.model.{ ContentTypes, HttpEntity, HttpResponse, Multipart, StatusCodes }
import akka.http.scaladsl.server.Directives.{ entity, _ }
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.Materializer
import akka.stream.alpakka.csv.scaladsl.CsvFormatting
import akka.stream.alpakka.s3.MultipartUploadResult
import akka.stream.scaladsl.Source
import akka.util.ByteString
import com.fractal.auth.client.deprecated.User
import com.typesafe.scalalogging.StrictLogging
import enumeratum._
import enumeratum.EnumEntry.Lowercase
import qomplx.event.JsonSupport
import qomplx.event.directives.{ CustomSecurityDirectives, SecurityDirectivesForInternalRoutes, UserAuthenticatorDirectives }
import qomplx.event.model.IncidentReporterSource.IncidentReporterSource
import qomplx.event.model.IncidentStatus.IncidentStatus
import qomplx.event.model.{ Incident, IncidentFile, IncidentNote, IncidentURL }
import qomplx.event.repository.IncidentRepository
import qomplx.event.service.S3BucketHandlerService
import qomplx.event.thirdpartyintegrations.templates.Template
import qomplx.event.util.{ CommonRejectionHandler, QomplxError }

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.control.NonFatal
import scala.util.{ Failure, Success }

class IncidentRoutes(val incidentRepo: IncidentRepository, securityDirection: SecurityDirectivesForInternalRoutes, s3BucketHandlerService: S3BucketHandlerService, userAuthenticator: UserAuthenticatorDirectives)(implicit val ec: ExecutionContext, materializer: Materializer) extends StrictLogging with JsonSupport {
  import securityDirection.authenticateWithUserId

  // format: OFF
  // These routes are utilized directly by the UI, as opposed to being called via qweb-cyber. Eventually,
  // all routes needed by the UI will be called directly, instead of going via qweb-cyber hop.
  def publicIncidentRoutes(user: User): Route =
    pathPrefix("api" / "v1" / "incidents") {
      handleRejections(CommonRejectionHandler.rejectionHandler) {
        pathPrefix("observed-system" / Segment) { obsSysId =>
          exportRoutes(obsSysId) ~
          statisticsRoutes(obsSysId) ~
          incidentRoutesPublic(obsSysId)
        }
      }
    }
  
  private def exportRoutes(obsSysId: String): Route =
    (path("export") & get) {
      parameters('assignee.as[Long].?, 'search.?, 'status.?, 'numDays.as[Int].?, "format" ! "csv").as(ExportIncidentsParams) { params =>
        onComplete(incidentRepo.getIncidentsAndEventIdsByObservedSystem(obsSysId, params, 1000)) {
          case Success(incidentsAndEventIds) =>
            logger.debug(s"Exporting to CSV format the incidents for observed system [$obsSysId].")

            val headerSource = Source.single(IncidentDTO.FieldNames)

            val csvSource =
              headerSource
                .concat(Source(incidentsAndEventIds.map(IncidentDTO.convertToFieldVector)))
                .via(CsvFormatting.format())

            respondWithHeader(`Content-Disposition`(ContentDispositionTypes.attachment, Map("filename" -> s"incidents-$obsSysId.csv"))) {
              complete(HttpEntity(ContentTypes.`text/csv(UTF-8)`, csvSource))
            }
          case Failure(ex) =>
            logger.warn(s"Error occurred when exporting to CSV format the incidents for observed system [$obsSysId]: ", ex)
            complete(StatusCodes.InternalServerError)
        }
      }
    }

  private def statisticsRoutes(obsSysId: String): Route =
    (pathPrefix("statistics") & get) {
      concat(
        pathEnd {
          parameters("status".as[String], "numDays".as[Int]).as(StatisticsParams) { params =>
            val timeOfRequest = Instant.now().toEpochMilli
            onComplete(incidentRepo.getStatisticsDetails(obsSysId, timeOfRequest, IncidentStatusForStatistics.withName(params.status), params.numDays)) {
              case Success(details) =>
                logger.debug(s"Successfully retrieved the statistics details for timestamp ${timeOfRequest}: $details")
                complete(details)
              case Failure(t) =>
                logger.warn(s"An error occurred when trying to get the statistics details for timestamp $timeOfRequest", t)
                complete(StatusCodes.InternalServerError)
            }
          }
        },
        path("severity") {
          parameters("status".as[String], "numDays".as[Int]).as(StatisticsParams) { params =>
            val timeOfRequest = Instant.now().toEpochMilli
            onComplete(incidentRepo.getSeverityStatistics(obsSysId, timeOfRequest, IncidentStatusForStatistics.withName(params.status), params.numDays)) {
              case Success(severityStats) =>
                logger.debug(s"Successfully retrieved the severity statistics for timestamp ${timeOfRequest}: $severityStats")
                complete(severityStats)
              case Failure(t) =>
                logger.warn(s"An error occurred when trying to get the severity statistics for timestamp $timeOfRequest", t)
                complete(StatusCodes.InternalServerError)
            }
          }
        },
        path("summary") {
          val timeOfRequest = Instant.now().toEpochMilli
          onComplete(incidentRepo.getStatisticsSummary(obsSysId, timeOfRequest)) {
            case Success(summary) =>
              logger.debug(s"Successfully retrieved the statistics summary for timestamp ${timeOfRequest}: $summary")
              complete(summary)
            case Failure(t) =>
              logger.warn(s"An error occurred when trying to get the statistics summary for timestamp $timeOfRequest", t)
              complete(StatusCodes.InternalServerError)
          }
        }
      )
    }

  // These public routes are duplicates of existing non-public routes, and
  // we're keeping both for now in order to make easier the transition away
  // from Lift. The duplication will be removed once we cut the cord with
  // Lift and make all the non-public routes public.
  private def incidentRoutesPublic(obsSysId: String): Route =
    get {
      parameters('limit.as[Int] ? 10, 'offset.as[Int] ? 0, 'assignee.as[Long].?, 'search.?, 'status.?, 'numDays.as[Int].?).as(GetIncidentsReq) { params =>
        onComplete(incidentRepo.getByObservedSystem(obsSysId, params)) {
          case Success(resp) =>
            logger.debug(s"GET list of incidents (${resp.totalCount}")
            complete(resp)
          case Failure(t) =>
            logger.warn("Error while listing incidents: ", t)
            complete(StatusCodes.InternalServerError)
        }
      }
    }

  // TODO: make these routes public (i.e., directly callable from the UI)
  lazy val incidentRoutes =
    pathPrefix("api" / "v1" / "incidents") {
      handleRejections(CommonRejectionHandler.rejectionHandler) {
        authenticateWithUserId { userDetail =>
          concat(
            pathPrefix("observed-system" / Segment) { observedSystemId =>
              concat(
                (path("count" / "by-severity") & get) {
                  onComplete(incidentRepo.getCountBySeverity(observedSystemId)) {
                    case Success(value) => complete(value)
                    case Failure(t) => {
                      logger.error(s"The following exception was thrown when trying to get total count of incidents by their severity for the last 24 hours for observed system [$observedSystemId]: ${t.getMessage}", t)
                      complete(StatusCodes.InternalServerError)
                    }
                  }
                },
                path(Segment) { incidentId =>
                  concat(
                    post {
                      entity(as[UpdateIncidentReq]) { incidentReq =>
                        onComplete(incidentRepo.update(Incident(incidentId, incidentReq, userDetail, observedSystemId))) {
                          case Success(incidentDTO) => {
                            incidentDTO match {
                              case Some(inc) =>
                                logger.debug(s"Updated incident with id: ${inc.id}")
                                complete(inc)
                              case None =>
                                logger.warn(s"No incident updated in database with id: ${incidentId}")
                                complete(HttpResponse(StatusCodes.NotFound, entity = s"Incident not found"))
                            }
                          }
                          case Failure(ex) => {
                            logger.warn("Error while updating incident: ", ex)
                            complete(StatusCodes.InternalServerError)
                          }
                        }
                      }
                    },
                    get {
                      onComplete(incidentRepo.get(incidentId, observedSystemId)) {
                        case Success(incidentDTO) => {
                          incidentDTO match {
                            case Some(inc) =>
                              logger.debug(s"Retrieved incident with id: ${inc.id}")
                              complete(incidentDTO)

                            case None =>
                              logger.warn(s"No incident found in database with id: ${incidentId}")
                              complete(HttpResponse(StatusCodes.NotFound, entity = s"Incident not found"))

                          }
                        }
                        case Failure(ex) => {
                          logger.error(s"Error while fetching incident: ${incidentId}", ex)
                          complete(StatusCodes.InternalServerError)
                        }
                      }
                    }
                  )
                },
                (pathEnd & post) {
                  entity(as[CreateIncidentReq]) { incidentReq =>
                    onComplete(incidentRepo.add(Incident(incidentReq, userDetail, observedSystemId), incidentReq.eventIds)) {
                      case Success(incidentDTO) => {
                        logger.debug(s"Created new incident with id: ${incidentDTO.id}")
                        complete(incidentDTO)
                      }
                      case Failure(ex) => {
                        ex match {
                          case qomplx: QomplxError =>
                            logger.warn(s"Qomplx error while creating new incident: ${qomplx.message}")
                            complete(qomplx.responseCode, qomplx.message)
                          case _ =>
                            logger.warn("Error while creating new incident: ", ex)
                            complete(StatusCodes.InternalServerError)
                        }
                      }
                    }
                  }
                },
                get {
                  parameters('limit.as[Int] ? 10, 'offset.as[Int] ? 0, 'assignee.as[Long].?, 'search.?, 'status.?, 'numDays.as[Int].?).as(GetIncidentsReq) { params =>
                    onComplete(incidentRepo.getByObservedSystem(observedSystemId, params)) {
                      case Success(resp) => {
                        logger.debug(s"GET list of incidents (${resp.totalCount}")
                        complete(resp)
                      }
                      case Failure(ex) => {
                        logger.warn("Error while listing incidents: ", ex)
                        complete(StatusCodes.InternalServerError)
                      }
                    }
                  }
                }
              )
            },
            pathPrefix("files" / "observed-system" / Segment / Segment) { (observedSystemId, incidentId) =>
              concat(
                (post & entity(as[Multipart.FormData])) { formData =>
                  /**
                   * Note:
                   * Every part of multipart request must be consume (or discard) before processing the next one.
                   * That is why we are uploading an incident file to S3 inside the "file" part, see below.
                   * If we just return `dataBytes` of file entity to process later, we may face following error:
                   *   "Substream Source has not been materialized in 5000 milliseconds"
                   */
                  val extractorFut = formData.parts.mapAsync[Either[MultipartUploadResult, Option[AttachFileReq]]](parallelism = 1) {
                    case part: BodyPart if part.name == "file" => {
                      val filename = part.filename.getOrElse {
                        val name = s"$observedSystemId-$incidentId-${UUID.randomUUID()}.file"
                        logger.warn(
                          s"""
                             |THIS SHOULD NEVER BE LOGGED.
                             |A file attachment request for incident $incidentId could not find "filename", hence stored it as '$name'.
                             |""".stripMargin)
                        name
                      }

                      part.entity.dataBytes.runWith(s3BucketHandlerService.uploadSink(filename))
                        .map { uploadResult =>
                          logger.debug(s"incident file successfully uploaded to S3")
                          Left(uploadResult)
                        }
                        .recoverWith { case t =>
                          logger.warn(s"incident file could not be uploaded.", t)
                          Future.failed(new RuntimeException(s"Could not upload file to S3 for the incident $incidentId."))
                        }
                    }
                    case part: BodyPart if part.name == "data" => {
                      Unmarshal(part.entity).to[AttachFileReq].map(attachFileReq => Right(Some(attachFileReq))).recover { case t =>
                        logger.warn(s"Could not extract incident file data for incident $incidentId", t)
                        Right(None)
                      }
                    }
                    case part: BodyPart =>
                      part.entity.discardBytes()
                      logger.warn(s"""Found a body part with name ${part.name}, incident file attachment will require "file" and "data" body part.""")
                      Future.failed(new RuntimeException(s"Could not process incident file attachment request"))
                  }.runFold[(Option[MultipartUploadResult], Option[AttachFileReq])]((None, None)) {
                    case (tuple, Left(uploadResult)) => tuple.copy(_1 = Some(uploadResult))
                    case (tuple, Right(attachFileReqOpt)) => tuple.copy(_2 = attachFileReqOpt)
                  }.recover { case NonFatal(t) =>
                    logger.error(s"""Could not extract body part "file" or "data", in an attempt to linking a new file with the incident $incidentId.""", t)
                    (None, None)
                  }

                  val attachFileOps = for {
                    (Some(_), Some(attachFileReq)) <- extractorFut // proceed only if json parsed, and file uploaded to S3
                    incidentDTO <- incidentRepo.addFile(IncidentFile(attachFileReq, incidentId, userDetail), observedSystemId)
                  } yield incidentDTO

                  onComplete(attachFileOps) {
                    case Success(incidentDTO) => {
                      incidentDTO match {
                        case Some(inc) =>
                          logger.debug(s"Linked new file with incident: ${inc.id}")
                          complete(inc)

                        case None =>
                          logger.warn(s"No incident found in database with id: ${incidentId} & observed system id: ${observedSystemId}")
                          complete(HttpResponse(StatusCodes.NotFound, entity = s"Incident not found"))

                      }
                    }
                    case Failure(ex) => {
                      logger.warn(s"Error while linking new file to incident ${incidentId} ", ex)
                      complete(StatusCodes.InternalServerError)
                    }
                  }
                },
                path(Segment) { fileId =>
                  delete {
                    val future = for {
                      fileOpt <- incidentRepo.getFile(fileId, incidentId, observedSystemId)
                      _ <- if (fileOpt.isDefined) s3BucketHandlerService.delete(fileOpt.get.s3Key) else Future.successful(())
                      incidentDTOOpt <- incidentRepo.deleteFile(fileId, incidentId, observedSystemId)
                    } yield incidentDTOOpt

                    onComplete(future) {
                      case Success(incidentDTO) => {
                        incidentDTO match {
                          case Some(inc) =>
                            logger.debug(s"Deleted file linked to incident: ${inc.id}")
                            complete(inc)
                          case None =>
                            logger.warn(s"No incident found in database with id: ${incidentId} & observed system id: ${observedSystemId}")
                            complete(HttpResponse(StatusCodes.NotFound, entity = s"Incident not found"))
                        }
                      }
                      case Failure(ex) => {
                        logger.warn(s"Error while deleting file linked to incident ${incidentId} ", ex)
                        complete(StatusCodes.InternalServerError)
                      }
                    }
                  } ~
                  get {
                    onComplete(incidentRepo.getFile(fileId, incidentId, observedSystemId)) {
                      case Success(file) => {
                        file match {
                          case Some(f) =>
                            logger.debug(s"Retrieved file linked to incident: ${incidentId}")
                            complete(f)
                          case None =>
                            logger.warn(s"No file found in database with id: ${fileId} in incident ${incidentId} & observed system id: ${observedSystemId}")
                            complete(HttpResponse(StatusCodes.NotFound, entity = s"File not found"))
                        }
                      }
                      case Failure(ex) => {
                        logger.warn(s"Error while fetching file linked to incident ${incidentId} ", ex)
                        complete(StatusCodes.InternalServerError)
                      }
                    }
                  }
                },
                path("download" / Segment) { fileId =>
                  val future = for {
                    fileOpt <- incidentRepo.getFile(fileId, incidentId, observedSystemId)
                    objectOpt <- if (fileOpt.isDefined) s3BucketHandlerService.download(fileOpt.get.s3Key) else Future.successful(None)
                  } yield objectOpt

                  onComplete(future) {
                    case Success(file) => {
                      file match {
                        case Some((byteSource, metadata)) =>
                          logger.debug(s"Downloaded file linked to incident: ${incidentId}")
                          val entity = HttpEntity(ContentTypes.`application/octet-stream`, metadata.contentLength, byteSource)
                          complete(HttpResponse(entity = entity))
                        case None =>
                          logger.warn(s"No file found in database with id: ${fileId} in incident ${incidentId} & observed system id: ${observedSystemId}")
                          complete(HttpResponse(StatusCodes.NotFound, entity = s"File not found"))
                      }
                    }
                    case Failure(ex) => {
                      logger.warn(s"Error while downloading file linked to incident ${incidentId} ", ex)
                      complete(StatusCodes.InternalServerError)
                    }
                  }
                }
              )
            },
            pathPrefix("urls" / "observed-system" / Segment / Segment) { (observedSystemId, incidentId) =>
              concat(
                post {
                  entity(as[AttachURLReq]) { attachUrlReq =>
                    onComplete(incidentRepo.addURL(IncidentURL(attachUrlReq, incidentId, userDetail), observedSystemId)) {
                      case Success(incidentDTO) => {
                        incidentDTO match {
                          case Some(inc) =>
                            logger.debug(s"Attached new url with incident: ${inc.id}")
                            complete(inc)

                          case None =>
                            logger.warn(s"No incident found in database with id: ${incidentId} & observed system id: ${observedSystemId}")
                            complete(HttpResponse(StatusCodes.NotFound, entity = s"Incident not found"))

                        }
                      }
                      case Failure(ex) => {
                        logger.warn(s"Error while linking url to incident ${incidentId} ", ex)
                        complete(StatusCodes.InternalServerError)
                      }
                    }
                  }
                },
                (path(Segment) & delete) { urlId =>
                  onComplete(incidentRepo.deleteURL(urlId, incidentId, observedSystemId)) {
                    case Success(incidentDTO) => {
                      incidentDTO match {
                        case Some(inc) =>
                          logger.debug(s"Deleted url linked to incident: ${inc.id}")
                          complete(inc)
                        case None =>
                          logger.warn(s"No incident found in database with id: ${incidentId} & observed system id: ${observedSystemId}")
                          complete(HttpResponse(StatusCodes.NotFound, entity = s"Incident not found"))
                      }
                    }
                    case Failure(ex) => {
                      logger.warn(s"Error while deleting url linked to incident ${incidentId} ", ex)
                      complete(StatusCodes.InternalServerError)
                    }
                  }
                }
              )
            },
            pathPrefix("notes" / "observed-system" / Segment / Segment) { (observedSystemId, incidentId) =>
              concat(
                post {
                  entity(as[AttachNoteReq]) { attachNoteReq =>
                    onComplete(incidentRepo.addNote(IncidentNote(attachNoteReq, incidentId, userDetail), observedSystemId)) {
                      case Success(incidentDTO) => {
                        incidentDTO match {
                          case Some(inc) =>
                            logger.debug(s"Attached new new to an incident: ${inc.id}")
                            complete(inc)
                          case None =>
                            logger.warn(s"No incident found in database with id: ${incidentId} & observed system id: ${observedSystemId}")
                            complete(HttpResponse(StatusCodes.NotFound, entity = s"Incident not found"))
                        }
                      }
                      case Failure(ex) => {
                        logger.warn(s"Error while linking note to incident ${incidentId} ", ex)
                        complete(StatusCodes.InternalServerError)
                      }
                    }
                  }
                },
                (path(Segment) & delete) { noteId =>
                  onComplete(incidentRepo.deleteNote(noteId, incidentId, observedSystemId)) {
                    case Success(incidentDTO) => {
                      incidentDTO match {
                        case Some(inc) =>
                          logger.debug(s"Deleted note linked to incident: ${inc.id}")
                          complete(inc)
                        case None =>
                          logger.warn(s"No incident found in database with id: ${incidentId} & observed system id: ${observedSystemId}")
                          complete(HttpResponse(StatusCodes.NotFound, entity = s"Incident not found"))
                      }
                    }
                    case Failure(ex) => {
                      logger.warn(s"Error while deleting note linked to incident ${incidentId} ", ex)
                      complete(StatusCodes.InternalServerError)
                    }
                  }
                }
              )
            },
            pathPrefix("attachments" / "observed-system" / Segment / Segment) { (observedSystemId, incidentId) =>
              get {
                onComplete(incidentRepo.getAttachments(incidentId, observedSystemId)) {
                  case Success(attachmentsDTO) => {
                    attachmentsDTO match {
                      case Some(attachments) =>
                        logger.debug(s"Retrieved attachments linked to incident: ${incidentId}")
                        complete(attachments)
                      case None =>
                        logger.warn(s"No incident found in database with id ${incidentId} & observed system id ${observedSystemId}")
                        complete(HttpResponse(StatusCodes.NotFound, entity = s"Incident not found"))
                    }
                  }
                  case Failure(ex) => {
                    logger.warn(s"Error while retrieving attachments linked to incident ${incidentId} ", ex)
                    complete(StatusCodes.InternalServerError)
                  }
                }
              }
            },
            pathPrefix("events" / "observed-system" / Segment / Segment) { (observedSystemId, incidentId) =>
              get {
                onComplete(incidentRepo.getEvents(incidentId, observedSystemId)) {
                  case Success(events) => {
                    logger.debug(s"Retrieved events linked to incident: ${incidentId}")
                    complete(events)
                  }
                  case Failure(ex) => {
                    logger.error(s"Error while retrieving events linked to incident ${incidentId} ", ex)
                    complete(StatusCodes.InternalServerError)
                  }
                }
              }
            },
            (path("categories") & get) {
              onComplete(incidentRepo.getCategories) {
                case Success(categories) => {
                  logger.debug(s"Retrieved all categories successfully")
                  complete(categories)
                }
                case Failure(ex) => {
                  logger.warn("Error while getting categories: ", ex)
                  complete(StatusCodes.InternalServerError)
                }
              }
            },
            (path("tactics") & get) {
              onComplete(incidentRepo.getTactics) {
                case Success(tactics) => {
                  logger.debug(s"Retrieved all tactics successfully")
                  complete(tactics)
                }
                case Failure(ex) => {
                  logger.warn("Error while getting tactics: ", ex)
                  complete(StatusCodes.InternalServerError)
                }
              }
            }
          )
        }
      }
    }
  // format: ON
}

object IncidentListDTO {
  def apply(is: Seq[Incident]): Seq[IncidentDTO] = is.map { inc =>
    IncidentDTO(inc.id, inc.title, inc.description, inc.categories, inc.tactics, inc.resolution, inc.resolutionTimestamp, inc.status, inc.severity, inc.reportTimestamp, inc.reporterId, inc.reporterSource, inc.tagId, inc.assigneeId, inc.assigneeEmail, Nil)
  }
}

object IncidentWithEventIdsListDTO {
  def apply(is: Vector[(Incident, Seq[String])]): Vector[IncidentDTO] =
    is map {
      case (inc, eventIds) =>
        IncidentDTO(inc.id, inc.title, inc.description, inc.categories, inc.tactics, inc.resolution, inc.resolutionTimestamp, inc.status, inc.severity, inc.reportTimestamp, inc.reporterId, inc.reporterSource, inc.tagId, inc.assigneeId, inc.assigneeEmail, eventIds)
    }
}

object IncidentAttachmentsDTO {
  def apply(incidentFiles: Seq[IncidentFile], incidentURLs: Seq[IncidentURL], incidentNotes: Seq[IncidentNote]): AttachmentsDTO =
    AttachmentsDTO(incidentFiles.map(_.toDTO), incidentURLs.map(_.toDTO), incidentNotes.map(_.toDTO))
}

final case class CreateIncidentReq(
  title: String,
  description: Option[String],
  severity: Int,
  categories: Option[List[String]],
  tactics: Option[List[String]],
  reporterSource: Option[IncidentReporterSource],
  eventIds: List[String]) {

  require(severity >= 0 && severity <= 100, s"severity must be at least zero and no greater than 100")
  require(eventIds.length > 0, "At least 1 event id is required to create incident")
}

final case class UpdateIncidentReq(
  title: String,
  description: Option[String],
  categories: Option[List[String]],
  tactics: Option[List[String]],
  resolution: Option[String],
  resolutionTimestamp: Option[Long],
  status: IncidentStatus,
  severity: Int,
  reporterSource: IncidentReporterSource,
  assigneeId: Option[Long],
  assigneeEmail: Option[String]) {

  require(severity >= 0 && severity <= 100, s"severity must be at least zero and no greater than 100")

  assigneeId match {
    case Some(a) =>
      require(assigneeEmail.isDefined, s"Assignee [$a] is missing an email address.")
    case None =>
      require(assigneeEmail.isEmpty, s"An assignee ID is missing.")
  }
}

final case class AttachFileReq(name: String, description: Option[String], s3Key: String)
final case class AttachURLReq(url: String)
final case class AttachNoteReq(note: String)

final case class IncidentDTO(
  id: Option[String],
  title: String,
  description: Option[String],
  categories: Option[List[String]],
  tactics: Option[List[String]],
  resolution: Option[String],
  resolutionTimestamp: Option[Long],
  status: IncidentStatus,
  severity: Int,
  reportTimestamp: Long,
  reporterId: Long,
  reporterSource: IncidentReporterSource,
  observedSystemId: String,
  assigneeId: Option[Long],
  assigneeEmail: Option[String],
  eventIds: Seq[String])

object IncidentDTO {
  val FieldNames =
    Vector(
      "id",
      "title",
      "description",
      "categories",
      "tactics",
      "resolution",
      "resolution-timestamp",
      "status",
      "severity",
      "report-timestamp",
      "reporter-id",
      "reporter-source",
      "observed-system-id",
      "assignee-id",
      "assignee-email",
      "event-ids")

  def convertToFieldVector(dto: IncidentDTO): Vector[String] = {
    Vector(
      dto.id.getOrElse(""),
      dto.title,
      dto.description.getOrElse(""),
      dto.categories.map(_.mkString("|")).getOrElse(""),
      dto.tactics.map(_.mkString("|")).getOrElse(""),
      dto.resolution.getOrElse(""),
      dto.resolutionTimestamp.map(_.toString).getOrElse(""),
      dto.status.toString,
      Template.numberToSeverityStatus(dto.severity),
      dto.reportTimestamp.toString,
      dto.reporterId.toString,
      dto.reporterSource.toString,
      dto.observedSystemId,
      dto.assigneeId.map(_.toString).getOrElse(""),
      dto.assigneeEmail.getOrElse(""),
      dto.eventIds.mkString("|"))
  }
}

/**
 * Note: param values for `limit` and `offset` are restricted to 100 and 500 respectively.
 * if provided values are greater than them, then it will be reverted to their default values.
 * Default values are 10 and 0 respectively.
 */
final case class GetIncidentsReq(private val lmt: Int, private val ofs: Int, assignee: Option[Long], search: Option[String], status: Option[String] = None, numDays: Option[Int] = None) {
  status.foreach { s =>
    require(
      IncidentStatusForStatistics.withNameLowercaseOnlyOption(s).isDefined,
      s"""status must be one of the following: ${IncidentStatusForStatistics.values.map(_.entryName.toLowerCase).mkString(", ")}""")
  }

  numDays.foreach { days =>
    require(Set(1, 3, 7, 30).contains(days), "numDays must be 1, 3, 7, or 30")
  }

  // use `limit` and `offset` rather than `lmt` and `ofs`.
  lazy val limit: Int = if (lmt > 100) 10 else lmt
  lazy val offset: Int = if (ofs > 500) 0 else ofs
}

final case class ExportIncidentsParams(assignee: Option[Long] = None, search: Option[String] = None, status: Option[String] = None, numDays: Option[Int] = None) {
  status.foreach { s =>
    require(
      IncidentStatusForStatistics.withNameLowercaseOnlyOption(s).isDefined,
      s"""status must be one of the following: ${IncidentStatusForStatistics.values.map(_.entryName.toLowerCase).mkString(", ")}""")
  }

  numDays.foreach { days =>
    require(Set(1, 3, 7, 30).contains(days), "numDays must be 1, 3, 7, or 30")
  }
}

final case class GetIncidentsRes(results: Seq[IncidentDTO], totalCount: Int)
final case class IncidentFileDTO(id: String, name: String, description: Option[String], createdBy: Long, createdOn: Long)
final case class IncidentUrlDTO(id: String, url: String, createdBy: Long, createdOn: Long)
final case class IncidentNoteDTO(id: String, note: String, createdBy: Long, createdOn: Long)
final case class AttachmentsDTO(incidentFiles: Seq[IncidentFileDTO], incidentURLs: Seq[IncidentUrlDTO], incidentNotes: Seq[IncidentNoteDTO])
final case class FileData(name: String, byteSource: Source[ByteString, Any])

final case class MinimalStatistics(count: Int, recentDiff: Int)

final case class StatisticsSummary(
  requestTimestampMillis: Long,
  all: MinimalStatistics,
  unassigned: MinimalStatistics,
  assigned: MinimalStatistics,
  closed: MinimalStatistics)

final case class SeverityCounts(low: Int, medium: Int, high: Int)

// used for the following endpoints:
//   /api/v1/incidents/observed-system/{system-id}/statistics
//   /api/v1/incidents/observed-system/{system-id}/statistics/severity
final case class StatisticsParams(status: String, numDays: Int) {
  require(
    IncidentStatusForStatistics.withNameLowercaseOnlyOption(status).isDefined,
    s"""status must be one of the following: ${IncidentStatusForStatistics.values.map(_.entryName.toLowerCase).mkString(", ")}""")

  require(Set(1, 3, 7, 30).contains(numDays), "numDays must be 1, 3, 7, or 30")
}

final case class SeverityStatistics(
  requestTimestampMillis: Long,
  status: String, // IncidentStatusForStatistics
  numDays: Int,
  severity: SeverityCounts)

sealed trait IncidentStatusForStatistics extends EnumEntry with Lowercase

object IncidentStatusForStatistics extends Enum[IncidentStatusForStatistics] {
  val values = findValues
  case object All extends IncidentStatusForStatistics
  case object Assigned extends IncidentStatusForStatistics
  case object Unassigned extends IncidentStatusForStatistics
  case object Closed extends IncidentStatusForStatistics
}

final case class StatisticsDetails(
  requestTimestampMillis: Long,
  status: String,
  numDays: Int,
  granularity: String,
  counts: Vector[Int])
