package qomplx.event.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.unmarshalling.PredefinedFromStringUnmarshallers.CsvSeq
import akka.stream.Materializer
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.JsonSupport
import qomplx.event.directives.SecurityDirectivesForInternalRoutes
import qomplx.event.model.EventReduced
import qomplx.event.repository.{ EventRepository, NoObservedSystemException }
import qomplx.event.util.CommonRejectionHandler

import scala.concurrent.ExecutionContext
import scala.util.{ Failure, Success }

case class GetEventsReq(
  limit: Option[Int],
  offset: Option[Int],
  categories: Option[Seq[String]],
  tactics: Option[Seq[String]],
  fromSeverity: Option[Int],
  toSeverity: Option[Int],
  fromTimestamp: Option[Long],
  toTimestamp: Option[Long],
  queryString: Option[String]) {

  val lim: Int = limit.getOrElse(GetEventsReq.defaultLimit)
  val off: Int = offset.getOrElse(GetEventsReq.defaultOffset)

  require((lim + off) <= GetEventsReq.maxRecords, s"invalid pagination: cannot request more than ${GetEventsReq.maxRecords} records")
}

object GetEventsReq {
  val config = ConfigFactory.load()
  private val maxRecords = config.getInt("cyber-event.max-records")
  private val defaultLimit = config.getInt("cyber-event.default-limit")
  private val defaultOffset = config.getInt("cyber-event.default-offset")

  // helper methods (mostly to use them while writing unit tests)
  def fromLimitAndOffset(limit: Option[Int], offset: Option[Int]): GetEventsReq = GetEventsReq(
    limit, offset, None, None, None, None, None, None, None)

  def fromLimitAndOffsetAndSeverity(limit: Option[Int], offset: Option[Int], fromSeverity: Option[Int], toSeverity: Option[Int]): GetEventsReq = GetEventsReq(
    limit, offset, None, None, fromSeverity, toSeverity, None, None, None)

  def fromLimitAndOffsetAndTimestamp(limit: Option[Int], offset: Option[Int], fromTimestamp: Option[Long], toTimestamp: Option[Long]): GetEventsReq = GetEventsReq(
    limit, offset, None, None, None, None, fromTimestamp, toTimestamp, None)

  def fromLimitAndOffsetAndCategories(limit: Option[Int], offset: Option[Int], categories: Option[Seq[String]]): GetEventsReq = GetEventsReq(
    limit, offset, categories, None, None, None, None, None, None)

  def fromLimitAndOffsetAndTactics(limit: Option[Int], offset: Option[Int], tactics: Option[Seq[String]]): GetEventsReq = GetEventsReq(
    limit, offset, None, tactics, None, None, None, None, None)

  def fromLimitAndOffsetAndQueryString(limit: Option[Int], offset: Option[Int], searchString: Option[String]): GetEventsReq = GetEventsReq(
    limit, offset, None, None, None, None, None, None, searchString)
}

class EventRoutesInternal(val eventRepo: EventRepository, securityDirective: SecurityDirectivesForInternalRoutes)(implicit val ec: ExecutionContext, materializer: Materializer) extends StrictLogging with JsonSupport {
  import securityDirective.authenticateWithUserId

  // format: OFF
  lazy val eventRoutes =
    pathPrefix("api" / "v1" / "event") {
      (path("observed-system" / Segment) & get) { observedSystemId =>
        handleRejections(CommonRejectionHandler.rejectionHandler) {
          authenticateWithUserId { _ =>
            parameters(
              "limit".as[Int].?,
              "offset".as[Int].?,
              "categories".as(CsvSeq[String]).?,
              "tactics".as(CsvSeq[String]).?,
              "from-severity".as[Int].?,
              "to-severity".as[Int].?,
              "from-ts".as[Long].?,
              "to-ts".as[Long].?,
              "q".?).as(GetEventsReq.apply _) { params =>
              onComplete(eventRepo.getByObservedSystem(observedSystemId, params)) {
                case Success(value) => complete(value)
                case Failure(t) => {
                  logger.error(s"The following exception was thrown when trying to get the events for observed system [$observedSystemId]: ${t.getMessage}", t)
                  complete(StatusCodes.InternalServerError)
                }
              }
            }
          }
        }
      } ~
        (path("observed-system" / Segment / "count") & get) { observedSystemId =>
          handleRejections(CommonRejectionHandler.rejectionHandler) {
            authenticateWithUserId { _ =>
              parameter("days".as[Int] ? 90) { days => // default 90 days
                onComplete(eventRepo.getCount(observedSystemId, days)) {
                  case Success(value) => complete(value)
                  case Failure(t) => {
                    logger.error(s"The following exception was thrown when trying to get total count of events for the last [$days] days for observed system [$observedSystemId]: ${t.getMessage}", t)
                    complete(StatusCodes.InternalServerError)
                  }
                }
              }
            }
          }
        } ~
        (path("observed-system" / Segment / "count" / "by-severity") & get) { observedSystemId =>
          handleRejections(CommonRejectionHandler.rejectionHandler) {
            authenticateWithUserId { _ =>
              onComplete(eventRepo.getCountBySeverity(observedSystemId)) {
                case Success(value) => complete(value)
                case Failure(NoObservedSystemException(_)) => complete(StatusCodes.BadRequest)
                case Failure(t) => {
                  logger.error(s"The following exception was thrown when trying to get total count of events by their severity for the last 24 hours for observed system [$observedSystemId]: ${t.getMessage}", t)
                  complete(StatusCodes.InternalServerError)
                }
              }
            }
          }
        } ~
        (path("observed-system" / Segment / Segment) & get) { (observedSystemId, eventId) =>
          handleRejections(CommonRejectionHandler.rejectionHandler) {
            authenticateWithUserId { _ =>
              onComplete(eventRepo.findById(observedSystemId, eventId)) {
                case Success(value) => complete(value)
                case Failure(t) => {
                  logger.error(s"The following exception was thrown when trying to get event by its id for observed system [$observedSystemId]: ${t.getMessage}", t)
                  complete(StatusCodes.InternalServerError)
                }
              }
            }
          }
        }
    }
  // format: ON
}

final case class GetEventsRes(results: Seq[EventReduced], totalCount: Long)