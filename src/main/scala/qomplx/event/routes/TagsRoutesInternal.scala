package qomplx.event.routes

import java.sql.SQLException

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives.{ pathPrefix, _ }
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.directives.SecurityDirectivesForInternalRoutes
import qomplx.event.dto.tags.TagCreateRequest
import qomplx.event.model.Tag
import qomplx.event.repository.TagsRepository
import qomplx.event.util.CommonRejectionHandler
import qomplx.event.{ JsonSupport, UniqueViolationErrorCode }

import scala.concurrent.ExecutionContext
import scala.util.{ Failure, Success }

class TagsRoutesInternal(val tagsRepository: TagsRepository, securityDirective: SecurityDirectivesForInternalRoutes)(implicit val ec: ExecutionContext) extends StrictLogging with JsonSupport {
  import securityDirective.authenticateWithUserId

  // format: OFF
  lazy val tagsRoutes: Route =
    pathPrefix("api" / "v1" / "event" / "tags") {
      handleRejections(CommonRejectionHandler.rejectionHandler) {
        authenticateWithUserId { _ =>
          entity(as[Either[List[TagCreateRequest], TagCreateRequest]]) {
            case Left(list) => addTags(list)
            case Right(one) => addTags(List(one))
          }
        }
      }
    }
  // format: ON

  private def addTags(tags: List[TagCreateRequest]) = {
    onComplete(tagsRepository.add(tags.map(t => Tag(t.source, t.sourceId, t.tagId)))) {
      case Success(_) => complete(StatusCodes.Created)
      case Failure(ex: SQLException) if ex.getSQLState == UniqueViolationErrorCode =>
        logger.error(s"Error at least one of the tag configurations already exist", ex)
        complete((StatusCodes.BadRequest, "tag configuration already exist"))
      case Failure(ex) =>
        logger.error(s"The following exception was thrown when trying to add tags ${tags.mkString("[", ", ", "]")}: ${ex.getMessage}", ex)
        complete(StatusCodes.InternalServerError)
    }
  }
}
