package qomplx.event.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.stream.Materializer
import com.typesafe.scalalogging.StrictLogging
import org.slf4j.MDC
import qomplx.event.JsonSupport
import qomplx.event.directives.SecurityDirectivesForSIEMRoutes
import qomplx.event.directives.SecurityDirectivesForSIEMRoutes.ObsSysIdAndToken
import qomplx.event.repository.{ EventRepository, SIEMEventsResponse }
import qomplx.event.thirdpartyintegrations.repository.ThirdPartySystemConfigRepository
import qomplx.event.util.CommonRejectionHandler

import scala.concurrent.ExecutionContext
import scala.util.Success

class EventSIEMRoutes(val eventRepo: EventRepository, val thirdPartySystemConfigRepo: ThirdPartySystemConfigRepository, val securityDirective: SecurityDirectivesForSIEMRoutes)(implicit val ec: ExecutionContext, materializer: Materializer) extends JsonSupport with StrictLogging {
  import securityDirective.authenticateWithTokenApiKey

  // format: OFF
  lazy val routes =
    pathPrefix("api" / "v1" / "event" / "siem") {
      (pathEndOrSingleSlash & get) {
        authenticateWithTokenApiKey { obsSysIdAndToken =>
          parameters("last".as[Long]) { last =>
              getNextEventBatch(last, obsSysIdAndToken)
          }
        }
      }
    }
  // format: ON

  private def getNextEventBatch(last: Long, obsSysIdAndToken: ObsSysIdAndToken)(implicit materializer: Materializer) = {

    val lastOpt = if (last >= 0) Some(last) else None

    onComplete(thirdPartySystemConfigRepo.getByToken(obsSysIdAndToken.token)) {
      case Success(Some(tpi)) =>
        MDC.put("3rdPartyIntegrationId", tpi.id)
        if (!tpi.enabled) {
          logger.info(s"Disabled Integration, returning empty with same last value - $last")
          complete(SIEMEventsResponse(Nil, last))
        } else if (tpi.recentlyEnabled) {
          logger.info(s"Polling request starts with -1")
          onComplete(thirdPartySystemConfigRepo.setRecentlyEnabled(obsSysIdAndToken.token, false)) {
            case Success(_) =>
              complete(eventRepo.getNextEventBatch(obsSysIdAndToken.obsSystemId, lastOpt))
            case _ =>
              complete(StatusCodes.InternalServerError)
          }
        } else {
          logger.info(s"Polling request starts with $last")
          complete(eventRepo.getNextEventBatch(obsSysIdAndToken.obsSystemId, lastOpt))
        }
      case _ =>
        complete(StatusCodes.NotFound)
    }
  }
}