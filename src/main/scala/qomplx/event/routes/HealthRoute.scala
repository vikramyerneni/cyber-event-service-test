package qomplx.event.routes

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{ ContentTypes, HttpEntity, StatusCodes }
import akka.http.scaladsl.server.Directives._
import qomplx.event.JsonSupport
import qomplx.event.config.db.LiquibaseUtils
import qomplx.event.repository.EventRepository
import qomplx.event.util.SlickPostgresProfile.api._
import qomplx.event.util.{ Health, KafkaMetadataConsumer }
import qomplx.event.vault.VaultManager
import slick.jdbc.PostgresProfile.api.Database
import spray.json._

import scala.concurrent.ExecutionContext
import scala.util.{ Failure, Success }

class HealthRoute(val eventRepo: EventRepository, kafkaMetadataConsumer: KafkaMetadataConsumer,
  val db: Database, // remove it when we are done with migrating long ids with ksuids.
  val vaultManager: VaultManager)(implicit val ec: ExecutionContext, system: ActorSystem) extends JsonSupport {

  lazy val healthRoute =
    get {
      path("health") {
        onComplete(Health(eventRepo, kafkaMetadataConsumer)) {
          case Success(health) =>
            health.status match {
              case "healthy" =>
                complete(StatusCodes.OK, HttpEntity(ContentTypes.`application/json`, health.toJson.compactPrint))
              case _ =>
                complete(StatusCodes.ServiceUnavailable, HttpEntity(ContentTypes.`application/json`, health.toJson.compactPrint))
            }
          case Failure(t) =>
            complete(
              StatusCodes.ServiceUnavailable,
              HttpEntity(ContentTypes.`application/json`, s"""{"status":"unhealthy","messages":["${t.getMessage}"]}"""))
        }

      } ~ path("readiness") {
        complete(if (LiquibaseUtils.getMigrationFlag && vaultManager.health()) StatusCodes.OK else StatusCodes.ServiceUnavailable)
      } ~ path("migrate-user-id") {
        // note: temporary route
        val message = "status of migration process of user long ids to ksuids: "
        onComplete(db.run(sql"""select count(*) from cyber_event_service.amp_incidents where reporter_id_unique is null and created_by_unique is null and last_modified_by_unique is null""".as[Int].head)) {
          case Success(count) => if (count == 0) complete(message + "done") else complete(message + s"still $count records to go.. ")
          case Failure(t) => complete(StatusCodes.ServiceUnavailable)
        }
      }
    }
}