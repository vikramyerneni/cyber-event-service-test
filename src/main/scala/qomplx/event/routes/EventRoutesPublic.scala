package qomplx.event.routes

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.unmarshalling.PredefinedFromStringUnmarshallers.CsvSeq
import akka.stream.Materializer
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.JsonSupport
import qomplx.event.directives.CustomSecurityDirectives
import qomplx.event.repository.EventRepository
import qomplx.event.util.CommonRejectionHandler

import scala.concurrent.ExecutionContext
import scala.util.{ Failure, Success }

class EventRoutesPublic(val eventRepo: EventRepository, securityDirective: CustomSecurityDirectives)(implicit val ec: ExecutionContext, materializer: Materializer) extends StrictLogging with JsonSupport {
  import securityDirective.authenticateWithObsSystemAPIKey

  // format: OFF
  lazy val eventRoutes =
    pathPrefix("api" / "v1" / "event") {
      (path("observed-system" / Segment) & get) { observedSystemId =>
        handleRejections(CommonRejectionHandler.rejectionHandler) {
          authenticateWithObsSystemAPIKey { _ =>
            parameters(
              "limit".as[Int].?,
              "offset".as[Int].?,
              "categories".as(CsvSeq[String]).?,
              "tactics".as(CsvSeq[String]).?,
              "from-severity".as[Int].?,
              "to-severity".as[Int].?,
              "from-ts".as[Long].?,
              "to-ts".as[Long].?,
              "q".?).as(GetEventsReq.apply _) { params =>
              onComplete(eventRepo.getByObservedSystem(observedSystemId, params)) {
                case Success(value) => complete(value)
                case Failure(t) => {
                  logger.error(s"The following exception was thrown when trying to get the events for observed system [$observedSystemId]: ${t.getMessage}", t)
                  complete(StatusCodes.InternalServerError)
                }
              }
            }
          }
        }
      }
    }
  // format: ON
}