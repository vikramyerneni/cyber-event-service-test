package qomplx.event.config.db

import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import slick.jdbc.PostgresProfile.api._

final class PostgresConnection(host: String, port: String, databaseName: String, userName: String, password: String) extends StrictLogging {
  val url = s"jdbc:postgresql://$host:$port/$databaseName?user=$userName&password=$password"

  val database: Database = Database.forURL(url)

  def closeDb(): Unit = database.close()
}

object PostgresConnection {
  // Used when password is supplied via Vault
  def apply(password: String, config: Config): PostgresConnection = {
    init(password, config)
  }

  // Used during local testing when password is taken from config
  def apply(config: Config): PostgresConnection = {
    init(config.getString("cyber-event.db.properties.password"), config)
  }

  private def init(password: String, config: Config): PostgresConnection = {
    val conf = config.getConfig("cyber-event.db.properties")
    val host = conf.getString("serverName")
    val port = conf.getString("portNumber")
    val user = conf.getString("user")
    val database = conf.getString("databaseName")
    new PostgresConnection(host, port, database, user, password)
  }
}