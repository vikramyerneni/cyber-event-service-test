package qomplx.event.config.db

import java.util.concurrent.atomic.AtomicBoolean

import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import liquibase.Liquibase
import liquibase.integration.commandline.CommandLineUtils.createDatabaseObject
import liquibase.resource.ClassLoaderResourceAccessor

object LiquibaseUtils extends StrictLogging {

  // once the migration finishes running, set a global atomic boolean flag to true.
  // This flag is used for completing the readiness check successfully or otherwise. We want the readiness check to
  // succeed only if the migrations have been run. The service isn't "ready" to serve requests until it's  in a
  // consistent state
  private val isMigrated = new AtomicBoolean(false)

  def setMigrationFinished(state: Boolean): Unit = isMigrated.set(state)

  def getMigrationFlag: Boolean = isMigrated.get()

  def runMigration(appConf: Config, postgresConnection: PostgresConnection): Unit = {
    val changelogLocation = appConf.getString("cyber-event.liquibase.migrations.changelog-location")

    val liquibase = createInstance(postgresConnection.url, changelogLocation)

    try {
      logger.info("Running database migrations")
      liquibase.update("")
    } finally {
      liquibase.getDatabase.close()
    }
  }

  private def createInstance(dbUrl: String, changelogLocation: String): Liquibase = {
    val driverClassName = "org.postgresql.Driver"
    def accessor = new ClassLoaderResourceAccessor()

    val database = createDatabaseObject(
      accessor,
      dbUrl,
      null,
      null,
      driverClassName,
      null,
      null,
      false,
      true,
      null,
      null,
      null,
      null,
      null,
      null,
      null)

    new Liquibase(changelogLocation, accessor, database)
  }
}
