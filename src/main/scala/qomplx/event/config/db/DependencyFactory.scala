package qomplx.event.config.db

import java.time.Clock

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.fractal.auth.client.deprecated.{ Client => AuthServiceClient }
import com.typesafe.config.{ Config, ConfigFactory }
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.alerting.email.{ EventsMailStream, EventsMailer }
import qomplx.event.directives.{ CustomSecurityDirectives, SecurityDirectivesForInternalRoutes, SecurityDirectivesForSIEMRoutes, UserAuthenticatorDirectives }
import qomplx.event.kafka.localstorage.WriteToDatabase
import qomplx.event.kafka.warmstorage.{ S3KeyGenerator, WriteToS3 }
import qomplx.event.repository._
import qomplx.event.routes._
import qomplx.event.service.email._
import qomplx.event.service.{ AWSConfig, S3BucketHandlerService }
import qomplx.event.thirdpartyintegrations.repository.{ ThirdPartySystemConfigRepository, TicketingSystemMailRepository }
import qomplx.event.thirdpartyintegrations.routes.ThirdPartySystemConfigRoutes
import qomplx.event.util.{ EncryptionUtils, KafkaMetadataConsumer, SlickPostgresProfile }
import qomplx.event.vault.VaultManager

import scala.concurrent.ExecutionContext

trait DependencyFactory extends StrictLogging {

  implicit val system: ActorSystem = ActorSystem("CyberEventService")
  lazy implicit val materializer: ActorMaterializer = ActorMaterializer()
  lazy implicit val ec: ExecutionContext = system.dispatcher

  implicit val profile = SlickPostgresProfile

  lazy val appConfig: Config = {
    Option(System.getProperties.getProperty("qlocal")) match {
      case Some(_) => ConfigFactory.load("application.local.conf")
      case _ => ConfigFactory.load()
    }
  }

  val authServiceClient = new AuthServiceClient(appConfig.getString("qomplx-auth-service.url"))
  val userAuthenticatorDirectives = new UserAuthenticatorDirectives(authServiceClient)

  lazy val kafkaMetadataConsumer = new KafkaMetadataConsumer(appConfig)

  lazy val vaultConfig: Config = appConfig.getConfig("vault")
  lazy val vaultManager = new VaultManager(vaultConfig)

  val encryptionUtils = new EncryptionUtils(Option(vaultManager))
  encryptionUtils.initialize()

  // Commenting out until we integrate the whole flow. The policy is already associated with the app-role for cyber-event-service
  // lazy val s3BucketHandler = new S3IncidentsBucketHandler(appConfig.getConfig("aws.s3.incidents"), vaultManager)

  // get the Postgres password from Vault
  lazy val postgresConnection = PostgresConnection(vaultManager.postgresPassword, appConfig)
  lazy val postgresDb = postgresConnection.database

  lazy val eventRepository = new EventRepository(postgresDb)
  lazy val incidentRepository = new IncidentRepository(postgresDb)
  lazy val tagsRepository = new TagsRepository(postgresDb)
  lazy val thirdPartySystemConfigRepository = new ThirdPartySystemConfigRepository(postgresDb, encryptionUtils)

  private lazy val securityDirectiveForInternalRoutes = new SecurityDirectivesForInternalRoutes(authServiceClient)

  lazy val mongoDBVaultPassword: String = vaultManager.mongoPassword

  // format: OFF
  private lazy val securityDirectiveForPublicRoutes =
    new CustomSecurityDirectives(new ObservedSystemRepository({
      new MongoDB(
        Seq(
          DependencyFactory.appConfig.getString("mongodb.cyber-db-host")),
        mongoDBVaultPassword,
        DependencyFactory.appConfig.getString("mongodb.cyber-db-name"),
        DependencyFactory.appConfig.getConfig("mongodb.auth"))
    }))
  // format: ON

  lazy val mandrillConfig: MandrillConfig = MandrillConfig(appConfig.getConfig("mandrill"), vaultManager)
  lazy val mandrillService = new MandrillService(system)

  private lazy val securityDirectiveForSIEMRoutes =
    new SecurityDirectivesForSIEMRoutes(thirdPartySystemConfigRepository)

  lazy val eventsMailer: EventsMailer = new EventsMailer(appConfig, mandrillConfig, mandrillService)

  // kafka message handles
  lazy val kafkaConf: Config = appConfig.getConfig("cyber-event.kafka")
  lazy val kafkaLocalStorageWriter = new WriteToDatabase(eventRepository, kafkaConf)
  lazy val s3WarmStorageHandler = new S3BucketHandlerService(AWSConfig(appConfig.getConfig("aws.s3.events-warm-storage"), vaultManager))
  lazy val s3KeyGenerator = new S3KeyGenerator(Clock.systemUTC())
  lazy val kafkaWarmStorageWriter = new WriteToS3(kafkaConf, s3WarmStorageHandler, s3KeyGenerator)
  lazy val eventsEmailStream = new EventsMailStream(appConfig, eventsMailer)

  lazy val eventRoutesInternal = new EventRoutesInternal(eventRepository, securityDirectiveForInternalRoutes)
  lazy val eventRoutesPublic = new EventRoutesPublic(eventRepository, securityDirectiveForPublicRoutes)
  lazy val eventSIEMRoutes = new EventSIEMRoutes(eventRepository, thirdPartySystemConfigRepository, securityDirectiveForSIEMRoutes)
  lazy val s3IncidentFileStorageHandler = new S3BucketHandlerService(AWSConfig(appConfig.getConfig("aws.s3.incident-file-storage"), vaultManager))
  lazy val incidentRoute = new IncidentRoutes(incidentRepository, securityDirectiveForInternalRoutes, s3IncidentFileStorageHandler, userAuthenticatorDirectives)
  lazy val tagsRoutesInternal = new TagsRoutesInternal(tagsRepository, securityDirectiveForInternalRoutes)

  lazy val mailerConfig: Config = appConfig.getConfig("ticketing-system-mailer")
  lazy val ticketingSystemMailRepository = new TicketingSystemMailRepository(postgresDb, mailerConfig)

  lazy val thirdPartySystemConfigRoutes = new ThirdPartySystemConfigRoutes(thirdPartySystemConfigRepository, securityDirectiveForPublicRoutes)
  lazy val healthRoute = new HealthRoute(eventRepository, kafkaMetadataConsumer, postgresDb, vaultManager)

  lazy val migratorConfig: Config = appConfig.getConfig("migrate-user-id")
}

object DependencyFactory extends DependencyFactory
