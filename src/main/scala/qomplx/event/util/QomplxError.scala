package qomplx.event.util

import akka.http.scaladsl.model.{ StatusCode, StatusCodes }

case class QomplxError(val message: String, val responseCode: StatusCode = StatusCodes.BadRequest) extends Exception