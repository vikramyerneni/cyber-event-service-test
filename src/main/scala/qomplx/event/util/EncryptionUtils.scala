package qomplx.event.util

import java.nio.charset.StandardCharsets
import java.nio.file.Paths

import com.google.crypto.tink._
import com.google.crypto.tink.daead.DeterministicAeadKeyTemplates
import com.typesafe.scalalogging.LazyLogging
import qomplx.event.config.db.DependencyFactory
import qomplx.event.vault.VaultManager

final class EncryptionUtils(vaultManager: Option[VaultManager] = None) extends LazyLogging {

  private lazy val keysetHandle = KeysetHandle.generateNew(DeterministicAeadKeyTemplates.AES256_SIV)
  private val associatedData = "cyber-event-service".getBytes(StandardCharsets.UTF_8)

  def initialize(): Unit = {
    com.google.crypto.tink.config.TinkConfig.register()
    //generateKeyset()
  }

  /**
   * Generates keyset files for different environments.
   */
  def generateKeyset(): Unit = {
    CleartextKeysetHandle.write(
      keysetHandle,
      JsonKeysetWriter.withPath(Paths.get("/somePathToFile")))
  }

  def getDeterministicAead(): DeterministicAead = {
    vaultManager match {
      case Some(vault) =>
        val keyFromVault = vault.tinkKeyset
        val key = CleartextKeysetHandle.read(JsonKeysetReader.withString(keyFromVault))
        key.getPrimitive(classOf[DeterministicAead])
      case None =>
        // used only for testing
        val keyPath = this.getClass.getResource("/my_keyset.json").getPath
        val key = CleartextKeysetHandle.read(JsonKeysetReader.withPath(keyPath))
        key.getPrimitive(classOf[DeterministicAead])
    }
  }

  def encryptToken(token: String): Array[Byte] = {
    val daead = getDeterministicAead()
    daead.encryptDeterministically(token.getBytes(StandardCharsets.UTF_8), associatedData)
  }

  def decryptToken(encryptedBytes: Array[Byte]): String = {
    val daead = getDeterministicAead()
    val decryptedBytes = daead.decryptDeterministically(encryptedBytes, associatedData)
    new String(decryptedBytes, StandardCharsets.UTF_8)
  }
}