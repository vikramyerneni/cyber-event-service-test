package qomplx.event.util

import akka.http.scaladsl.model.{ HttpResponse, StatusCodes }
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.directives.CommonRouteDirectives

/**
 * Reconsider the granularity of the error messages sent to the user. Please use CommonRouteDirectives.handleAllExceptions or CommonRouteDirectives.handleCORSAndExceptions
 *
 */
object CommonRejectionHandler extends StrictLogging {

  val rejectionHandler =
    RejectionHandler.newBuilder()
      .handle {
        case MalformedQueryParamRejection(_, msg, _) =>
          logger.warn(s"MalformedQueryParamRejection: $msg")
          complete(HttpResponse(StatusCodes.BadRequest, entity = msg))
        case ValidationRejection(msg, _) =>
          logger.warn(s"ValidationRejection: $msg")
          if (msg.contains("invalid pagination: ")) {
            complete(HttpResponse(StatusCodes.NotAcceptable, entity = msg))
          } else {
            complete(HttpResponse(StatusCodes.BadRequest, entity = msg))
          }
        case MalformedRequestContentRejection(msg, _) =>
          logger.warn(s"MalformedRequestContentRejection: $msg")
          complete(HttpResponse(StatusCodes.BadRequest, entity = msg))
        case MethodRejection(_) =>
          complete(HttpResponse(StatusCodes.NotFound))
        case MissingHeaderRejection(headerName) =>
          logger.warn(s"MissingHeaderRejection: $headerName")
          //TODO: INDICATE HEADERS THAT ARE AUTH AND NOT AUTH, BE HELPFUL ON NON AUTH
          complete(HttpResponse(StatusCodes.BadRequest))
      }
      .handleNotFound { complete(StatusCodes.NotFound) }
      .result()
}
