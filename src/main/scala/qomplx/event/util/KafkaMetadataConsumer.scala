package qomplx.event.util

import akka.actor.{ ActorRef, ActorSystem }
import akka.kafka.{ ConsumerSettings, KafkaConsumerActor, Metadata }
import akka.util.Timeout
import com.typesafe.config.Config
import org.apache.kafka.common.serialization.StringDeserializer
import scala.concurrent.duration._
import scala.concurrent.{ ExecutionContext, Future }
import akka.pattern.ask

final class KafkaMetadataConsumer(appConfig: Config)(implicit system: ActorSystem, ec: ExecutionContext) {
  private val consumerConfig: Config = appConfig.getConfig("akka.kafka.consumer")
  private val kafkaBootstrapServers = appConfig.getString("cyber-event.kafka.bootstrap-server")
  private val kafkaConsumerGroupId = appConfig.getString("cyber-event.kafka.group-id")
  private val timeout = appConfig.getDuration("akka.kafka.consumer.metadata-request-timeout")
  implicit val askTimeout = Timeout(timeout.getSeconds seconds)

  val consumerSettings = ConsumerSettings(consumerConfig, new StringDeserializer, new StringDeserializer)
    .withBootstrapServers(kafkaBootstrapServers)
    .withGroupId(kafkaConsumerGroupId)
    .withMetadataRequestTimeout(timeout)

  val consumer: ActorRef = system.actorOf(KafkaConsumerActor.props(consumerSettings))

  def getTopics: Future[Metadata.Topics] = {
    (consumer ? Metadata.ListTopics).mapTo[Metadata.Topics]
  }

}
