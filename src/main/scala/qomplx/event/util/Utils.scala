package qomplx.event.util

import java.time.Instant
import java.time.temporal.ChronoUnit

import com.github.kolotaev.ride.Id
import org.joda.time.format.{ DateTimeFormat, DateTimeFormatter }

object Utils {
  def generateId(prefix: Option[String] = None): String = {
    val guid = Id()
    prefix match {
      case Some(p) => s"$p-$guid"
      case None => s"$guid"
    }
  }

  /**
   * Subtracts `numHours` hours from `timeMillis`. `numHours` must be a postitive integer.
   */
  def subtractNumHours(timeMillis: Long, numHours: Int): Long = {
    require(numHours > 0, "numHours must be positive")
    val olderTime = Instant.ofEpochMilli(timeMillis).minus(numHours, ChronoUnit.HOURS)
    olderTime.toEpochMilli
  }

  /**
   * Subtracts `numDays` days from `timeMillis`. `numDays` must be a postitive integer.
   */
  def subtractNumDays(timeMillis: Long, numDays: Int): Long = {
    require(numDays > 0, "numDays must be positive")
    val olderTime = Instant.ofEpochMilli(timeMillis).minus(numDays, ChronoUnit.DAYS)
    olderTime.toEpochMilli
  }

  /**
   * Let `p` be a period equal to `requestTimestampMillis - numDays in days`.
   * Returns a number of timestamps that are based on a level of granularity.
   * The granularity depends on the value of the `numDays` parameter:
   *
   * | `numDays` | granularity |
   * | --------- | ----------- |
   * | `1`       | `3hrs`      |
   * | `3`       | `1day`      |
   * | `7`       | `1day`      |
   * | `30`      | `6days`     |
   *
   * For example, 30 `numDays` corresponds to a granularity of six days, meaning
   * that this method will return `30 / 6 = 5` timestamps in six-day intervals for
   * the time period `p`.
   *
   * @param timeMillis start time
   * @param numDays must be 1, 3, 7, 30
   * @return timestamps in ascending order by literal value, but descending order by age
   */
  def segmentTimePeriod(timeMillis: Long, numDays: Int): List[Long] = {
    require(Set(1, 3, 7, 30).contains(numDays), "numDays must be 1, 3, 7, or 30")

    // https://stackoverflow.com/a/45433686
    def unfold[A, B](a: A)(f: A => Option[(A, B)]): Stream[B] =
      f(a).map { case (a, b) => b #:: unfold(a)(f) }
        .getOrElse(Stream.empty)

    if (numDays == 1) {
      // eight three-hour segments...
      // (0-3), (3-6), (6-9), (9-12), (12-15), (15-18), (18-21), (21-24)
      // ...so we need to take nine timestamps:
      // 0, 3, 6, 9, 12, 15, 18, 21, 24
      unfold(timeMillis)(t => Option((subtractNumHours(t, 3), t))).take(9).toList.reverse
    } else if (numDays == 3) {
      // three one-day segments, so take four timestamps
      unfold(timeMillis)(t => Option((subtractNumDays(t, 1), t))).take(4).toList.reverse
    } else if (numDays == 7) {
      // seven one-day segments, so take eight timestamps
      unfold(timeMillis)(t => Option((subtractNumDays(t, 1), t))).take(8).toList.reverse
    } else {
      // five six-day segments, so take six timestamps
      unfold(timeMillis)(t => Option((subtractNumDays(t, 6), t))).take(6).toList.reverse
    }
  }

  def createCountStatements(requestTimestampMillis: Long, numDays: Int, filterClause: Option[String] = None): String = {
    val segmentedTimePeriod = Utils.segmentTimePeriod(requestTimestampMillis, numDays)

    val countStatements =
      segmentedTimePeriod.sliding(2).foldLeft(("", 1)) {
        case ((acc: String, countNum: Int), List(a, b)) =>
          val clause = s"""count(id) filter (where created_on >= $a and created_on < $b ${filterClause.getOrElse("")}) as count$countNum,"""
          (acc + clause, countNum + 1)
      }

    countStatements._1.init // drop the last comma
  }

  /**
   * Sample: 08/27/2020 1:23:17 PM
   */
  val enUSDateTimeFormat: DateTimeFormatter = DateTimeFormat.forPattern("MM/dd/yyyy h:mm:ss a");
}
