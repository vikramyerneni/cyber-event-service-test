package qomplx.event.util

import akka.actor.ActorSystem
import buildinfo.BuildInfo
import qomplx.event.repository.EventRepository
import akka.pattern.after
import com.typesafe.config.Config
import qomplx.event.config.db.DependencyFactory

import scala.collection.immutable.Seq
import scala.concurrent.{ ExecutionContext, Future, TimeoutException }
import scala.concurrent.duration._

sealed trait Severity
case object Fatal extends Severity
case object NonFatal extends Severity

final case class Integration(
  name: String,
  severity: Severity,
  status: String,
  messages: Seq[String]) {

  def isHealthy = if (status.toLowerCase == "healthy") true else false
}

final case class Health(
  service: String,
  version: String,
  commit: String,
  built: String,
  status: String,
  integrations: Seq[Integration])

// TODO: Capture serialization errors and incorporate them into the health check
object Health {
  def apply(repo: EventRepository, kfk: KafkaMetadataConsumer)(implicit ec: ExecutionContext, system: ActorSystem): Future[Health] = {
    val postgresStatus = postgresCheck(repo)
    val kafkaStatus = kafkaCheck(kfk)

    for {
      kafka <- kafkaStatus
      postgres <- postgresStatus
    } yield {
      val status = if (postgres.isHealthy && kafka.isHealthy) "healthy" else "unhealthy"

      Health(
        BuildInfo.name,
        BuildInfo.version,
        BuildInfo.commit,
        BuildInfo.builtAtString,
        status,
        Seq(postgres, kafka))
    }
  }

  private def postgresCheck(repo: EventRepository)(implicit ec: ExecutionContext, system: ActorSystem): Future[Integration] = {
    val timeout: FiniteDuration = DependencyFactory.appConfig.getDuration("cyber-event.db.pingTimeout").toMillis.millis
    val stopwatch = after(timeout, using = system.scheduler)(Future.failed(new TimeoutException))
    val dbPing = repo.pingDatabase()
      .collect {
        case true =>
          Integration("postgres", Fatal, "healthy", Nil)
      }
      .recover {
        case t =>
          Integration("postgres", Fatal, "unhealthy", Seq(t.getMessage))
      }

    Future.firstCompletedOf(stopwatch :: dbPing :: Nil).recoverWith {
      case t: TimeoutException => Future.successful(Integration("postgres", Fatal, "unhealthy", "Postgres timed out or was unreachable" :: Nil))
      case e: Throwable => Future.successful(Integration("postgres", Fatal, "unhealthy", Seq(e.getMessage)))
    }
  }

  private def kafkaCheck(kfk: KafkaMetadataConsumer)(implicit ec: ExecutionContext): Future[Integration] = {
    kfk.getTopics.map { topics =>
      if (topics.response.isSuccess) {
        Integration("kafka", Fatal, "healthy", Seq.empty)
      } else {
        Integration("kafka", Fatal, "unhealthy", List(topics.response.failed.get.getMessage))
      }
    } recoverWith {
      case e: Throwable => Future.successful(Integration("kafka", Fatal, "unhealthy", List(e.getMessage)))
    }
  }
}
