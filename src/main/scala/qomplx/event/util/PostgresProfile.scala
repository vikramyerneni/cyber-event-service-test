package qomplx.event.util

import com.github.tminglei.slickpg.{ PgArraySupport, PgEnumSupport, PgSearchSupport, PgSprayJsonSupport }
import qomplx.event.model.{ IncidentReporterSource, IncidentStatus, PropertyType }
import qomplx.event.thirdpartyintegrations.model.ThirdPartySystem
import slick.jdbc.PostgresProfile

/**
 * TODO:
 * following is a quick solution for adding support for postgresql Enum data type
 * may need to change
 */
trait SlickPostgresProfile extends PostgresProfile with PgEnumSupport with PgArraySupport with PgSprayJsonSupport with PgSearchSupport {
  override val pgjson = "jsonb"

  override val api: API = new API {}

  trait API extends super.API with ArrayImplicits with SprayJsonImplicits with JsonImplicits with SearchImplicits with SearchAssistants {
    implicit val incidentStatusMapper = createEnumJdbcType("amp_incident_status", IncidentStatus)
    implicit val incidentReporterSourceMapper = createEnumJdbcType("amp_incident_reporter_source", IncidentReporterSource)
    implicit val ticketingSystemMapper = createEnumJdbcType("ns_ticketing_system", ThirdPartySystem)
    implicit val propertyType = createEnumJdbcType("amp_property_type", PropertyType)
  }

  val plainApi = new API with SimpleArrayPlainImplicits {}
}

object SlickPostgresProfile extends SlickPostgresProfile