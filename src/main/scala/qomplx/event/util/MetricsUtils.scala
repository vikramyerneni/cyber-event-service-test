package qomplx.event.util

object MetricsUtils {
  val KafkaToPostgresBatchSizeMismatch = "kafka_to_postgres_batch_size_mismatch" // Kamon counter name
  val KafkaToPostgresError = "kafka_to_postgres_error" // Kamon counter name
  val KafkaEmailStreamError = "kafka_email_stream_error" // Kamon counter name

  val KafkaToS3BatchSizeMismatch = "kafka_to_s3_batch_size_mismatch" // Kamon counter name
  val KafkaToS3Error = "kafka_to_s3_error" // Kamon counter name
}
