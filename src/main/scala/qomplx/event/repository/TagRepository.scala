package qomplx.event.repository

import com.typesafe.scalalogging.StrictLogging
import qomplx.event.JsonSupport
import qomplx.event.model.{ Tables, Tag }
import qomplx.event.util.SlickPostgresProfile.api._
import slick.jdbc.PostgresProfile.api.Database

import scala.concurrent.Future

class TagRepository(db: Database) extends JsonSupport with StrictLogging {

  def add(tag: Tag): Future[Int] = db.run(Tables.Tags += tag)
}