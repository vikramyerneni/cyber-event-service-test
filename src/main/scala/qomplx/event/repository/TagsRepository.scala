package qomplx.event.repository

import com.typesafe.scalalogging.StrictLogging
import qomplx.event.{ JsonSupport, _ }
import qomplx.event.model.Tables._
import qomplx.event.util.SlickPostgresProfile.api._
import slick.jdbc.PostgresProfile.api.Database

import scala.concurrent.{ ExecutionContext, Future }

class TagsRepository(db: Database) extends JsonSupport with StrictLogging {

  def add(tags: Seq[model.Tag])(implicit ec: ExecutionContext): Future[Int] = {
    db.run(Tags ++= tags).map(_.sum)
  }

  def count()(implicit ec: ExecutionContext): Future[Int] = {
    db.run(Tags.length.result)
  }
}