package qomplx.event.repository

import akka.stream.Materializer
import akka.stream.scaladsl.{ Sink, Source }
import com.github.tminglei.slickpg.TsQuery
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging
import org.squbs.streams.Deduplicate
import qomplx.event.model.Tables.{ EventTable, _ }
import qomplx.event.model._
import qomplx.event.routes.{ GetEventsReq, GetEventsRes }
import qomplx.event.util.CanonicalValueMap._
import qomplx.event.util.SlickPostgresProfile.api._
import qomplx.event.util.Utils
import qomplx.event.{ ObservedSystemId, _ }
import slick.jdbc.PostgresProfile.api.Database
import slick.jdbc.{ GetResult, ResultSetConcurrency, ResultSetType }
import slick.lifted.Query

import scala.concurrent.{ ExecutionContext, Future }

class EventRepository(db: Database) extends JsonSupport with StrictLogging {
  import EventRepository._

  val MILLIS_IN_90_DAYS: Long = 7776000000L

  def add(events: Seq[EventMessage])(implicit ec: ExecutionContext): Future[Int] = {
    val rows = events.map { event =>
      (event.id.getOrElse(Utils.generateId(Some("EVT"))), event.observedSystemId, event.name, event.description,
        event.`type`, event.severity, event.timestamp, event.source, event.sourceId,
        event.categories.toList, event.tactics.toList, event.extraInfo, event.incIdx,
        event.agentId, event.ruleId, event.customerAccountId)
    }

    db.run(EventsPayloadFieldsTable ++= rows)
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to add events ${events.mkString("[", ", ", "]")}: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }.map(_.sum)
  }

  /**
   * The following method uses a workaround to overcome the limitation of PostgreSQL's full text search operator(s) and function.
   *
   * Approach:
   *   To filter events using event id, name and description, we use data types `tsvector` and `tsquery`. We have a separate column
   *   `tokens_simple` (of type `tsvector`) to store lexemes of the text of above stated three columns. The PostgreSQL provides `@@` operator
   *   to match `tsquery` value against `tsvector` values. We can use a function `to_tsquery` to generate `tsquery` value from
   *   simple string. The `tsquery` value can contain operator such as `&`, `|` and `<->`.
   *   Please refer: https://www.postgresql.org/docs/11/functions-textsearch.html
   *
   * Limitation:
   *   <> Because we want to behave our query as the `contains` method, to filter events by search string, we replace each space with `<->` (FOLLOWED BY)
   *   operator in search string and feed it to `to_tsquery` function.
   *
   *   <> The `to_tsquery` function, when provided hyphened string as input, generates `&` separated sub strings. It seems logical when used independently.
   *   Ex.
   *     postgres=> select to_tsquery('simple', 'In-Depth');
   *     to_tsquery
   *     ----------------------
   *     'in-depth' & 'in' & 'depth'
   *     (1 row)
   *
   *   1) When we use these two things (`<->` and hyphened string) together, semantics doesn't make sense at all.
   *   Ex.
   *     postgres=> select to_tsquery('simple', 'C++ <-> In-Depth');
   *     to_tsquery
   *     ----------------------------------
   *     'c' <-> ( 'in-depth' & 'in' & 'depth' )
   *     (1 row)
   *
   *   `'c' <-> ( 'in-depth' & 'in' & 'depth' )` means that lexemes 'in-depth', 'in' and 'depth' should be at the same distance after the `c`. ¯\_(ツ)_/¯
   *
   *   2) We can use `phraseto_tsquery` function to achieve correct behaviour, but it doesn't support ":*" construct, which helps us to find all
   *   the matches that "starts with" given input.
   *   Ex.
   *     "abc:*" would match "abcd", "abcz" but not "acbd"
   *
   *
   * Workaround:
   *   <> The idea is to use `phraseto_tsquery` function, and append ":*" to the lexemes of the last word of given input.
   *   Let's assume that use have entered a string, "C++ In-Dep", result should have any events which "contains" the given string.
   *
   *   <> Rather than matching "C++ In-Dep"'s tsquery against `tokens_simple` column, we first compute tsquery values of the whole input string
   *   and the last word of the input using `phraseto_tsquery`.
   *   Ex.
   *     postgres=> select phraseto_tsquery('simple', 'C++ In-Dep') as whole, phraseto_tsquery('simple', 'In-Dep') as last;
   *                     whole                |            last
   *     -------------------------------------+-----------------------------
   *      'c' <-> 'in-dep' <-> 'in' <-> 'dep' | 'in-dep' <-> 'in' <-> 'dep'
   *     (1 row)
   *   Then we append ":*" to lexemes of `whole` which are also in `last`. In our case, those are 'in-dep', 'in' and 'dep'.
   *   Please refer the comment of case class `TsQueryHelper`.
   *
   * Note:
   *   <> We have been using "english" dictionary for creating and matching string lexemes. It was causing some issue with certain (stop words) search strings.
   *   It has been changed to "simple" dictionary, which allows to search stop words as well.
   */
  def getByObservedSystem(
    observedSystemId: ObservedSystemId,
    params: GetEventsReq)(implicit ec: ExecutionContext, materializer: Materializer): Future[GetEventsRes] = {

    val lim = params.limit.getOrElse(DefaultLimit)
    val offset = params.offset.getOrElse(DefaultOffset)

    // Enforces that sum of `limit` and `offset` should be less than equal to configured value.
    require((lim + offset) <= MaxRecords, s"invalid pagination: cannot request more than $MaxRecords records")

    // execute `phraseto_tsquery` function for provided search string and the last word of it.
    import TsQueryHelper._
    val tsqueryHelperQuery = params.queryString.fold[DBIO[Option[TsQueryHelper]]](DBIO.successful(None)) { q =>
      val lastWord = q.split(" ").last
      sql"""select phraseto_tsquery('#${dictionary}', $q) as whole, phraseto_tsquery('#${dictionary}', $lastWord) as last""".as[TsQueryHelper].headOption
    }

    def baseQuery(tsqueryOpt: Option[TsQuery]) = {
      findEventsByUnionAll(observedSystemId) { table =>
        table
          .filterOpt(params.categories.map(canonicalCategories))(_.categories @> _)
          .filterOpt(params.tactics.map(canonicalTactics))(_.tactics @> _)
          .filterOpt(params.fromSeverity)(_.severity >= _)
          .filterOpt(params.toSeverity)(_.severity <= _)
          .filterOpt(params.fromTimestamp)(_.timestamp >= _)
          .filterOpt(params.toTimestamp)(_.timestamp <= _)
          .filterOpt(tsqueryOpt) { (e, tsq) => e.tokens_simple @@ tsq }
          .sortBy(_.timestamp.desc)
          .take(lim + offset)
      }
    }

    for {
      tsqueryHelperOpt <- db.run(tsqueryHelperQuery)
      events <- {
        streamEvents(baseQuery(tsqueryHelperOpt.map(_.toTsQuery)).sortBy(_.timestamp.desc), fetchSize = if (lim > 100) 100 else lim)
          .drop(offset)
          .take(lim)
          .runWith(Sink.seq)
      }
      count <- Future.successful(if (lim > events.size) events.size else lim)
    } yield {
      GetEventsRes(EventReducedList(events, observedSystemId), count)
    }
  }

  def getNextEventBatch(obsSysId: String, last: Option[Long] = None)(implicit ec: ExecutionContext, materializer: Materializer): Future[SIEMEventsResponse] = {
    val lim = config.getLong("siem.events.default-batch")

    logger.debug(s"SIEM limit is $lim")

    val query = {
      findEventsByUnionAll(obsSysId) { table =>
        table
          .filterOpt(last)(_.incIdx > _)
          .sortBy(e => if (last.isDefined) e.incIdx.asc else e.timestamp.desc).take(lim)
      }.sortBy(e => if (last.isDefined) e.incIdx.asc else e.timestamp.desc)
    }

    streamEvents(query, fetchSize = lim.toInt).take(lim).runWith(Sink.seq).map { eventQueryResult =>
      val events = if (last.isDefined) eventQueryResult else eventQueryResult.reverse
      SIEMEventsResponse(
        EventReducedList(events, obsSysId),
        events.lastOption.flatMap(_.incIdx).getOrElse(last.getOrElse(-1)))
    }
  }

  def getCount(observedSystemId: ObservedSystemId, forLastNDays: Int)(implicit ec: ExecutionContext): Future[EventsCount] = {
    val updatedLastNDays = if (forLastNDays > 90) 90 else forLastNDays

    db.run(
      sql"""select sum(count) from cyber_event_service.amp_events_count where date > current_date - interval '#${updatedLastNDays}' day
        and (observed_system_id = '#${observedSystemId}' or tag_id = '#${observedSystemId}')
         """.as[Int].head)
      .map(EventsCount)
  }

  // Note: count for last 24 hour from given epoch millis
  def getCountBySeverity(observedSystemId: ObservedSystemId)(implicit ec: ExecutionContext): Future[CountBySeverity] = {
    // Note: The `streamEvents` method won't work here. In order to count events by their severity, we have to go through each of them. And it's
    // possible to have millions of record in last 24 hour interval. We decided to store pre-calculated count using trigger in separate table.
    // see, `22_count_by_severity_precalc.sql` migration file.
    def query = {
      sql"""
           select sum(total), sum(low), sum(medium), sum(high)
           from cyber_event_service.amp_events_count_by_severity
           where tag_id = '#${observedSystemId}' and (datetime > date_trunc('minute', CURRENT_TIMESTAMP)::timestamp - interval '1 day' and datetime <= date_trunc('minute', CURRENT_TIMESTAMP)::timestamp)
         """.as[CountBySeverity].head
    }

    val actions = for {
      isObsAvailable <- Tables.Events.filter(_.observedSystemId === observedSystemId).exists.result
      isTagAvailable <- Tables.Tags.filter(_.tagId === observedSystemId).exists.result
      result <- if (isObsAvailable || isTagAvailable) query else DBIO.failed(NoObservedSystemException())
    } yield result

    db
      .run(actions)
      .recover {
        case t: NoObservedSystemException =>
          logger.info(s"observed system id [$observedSystemId] not found", t)
          throw t
        case t: NoSuchElementException =>
          logger.info(s"no result found to count severity for observed system id [$observedSystemId]", t)
          CountBySeverity(0, 0, 0, 0)
      }
  }

  def findById(tagId: ObservedSystemId, eventId: String)(implicit ec: ExecutionContext, materializer: Materializer): Future[Option[Event]] = {
    streamEvents(findEventsByUnionAll(tagId)(_.filter(_.eventId === eventId))).runWith(Sink.headOption).map(_.map { e =>
      Event(Some(e.eventId), e.name, e.description, e.`type`, e.severity, e.timestamp, e.source,
        e.sourceId, e.categories, e.tactics, e.observedSystemId.getOrElse(tagId), e.extraInfo, e.incIdx,
        e.agentId, e.ruleId, e.customerAccountId)
    })
  }

  /**
   * Deletes events that are older than the given number of days and
   * that are not associated with an incident.
   */
  def removeOlderThanDays(n: Int)(implicit ec: ExecutionContext): Future[Int] = {
    db.run(sql"""delete from cyber_event_service.amp_events where not exists (select 1 from cyber_event_service.amp_event_incident where amp_event_incident.amp_event_id = amp_events.event_id) and to_timestamp(timestamp/1000) < NOW() - INTERVAL '#$n days'""".as[Int].head)
  }

  def pingDatabase()(implicit ec: ExecutionContext): Future[Boolean] = {
    val ping = Query(1) // select 1
    val result = db.run(ping.result)
    result.map(_ == Seq(1))
  }

  // Note: Slick provides mechanism to stream query results along with fetchSize option. The fetchSize determines
  // how many records reside in memory at a time. Here, the `Deduplicate` stage removes already seen record(s).
  // We use this Source and attaches combinators like `drop` and `take` to fulfil our requirement without
  // running out of memory for huge number of events.
  private def streamEvents(query: EventQuery, fetchSize: Int = 0) = {
    Source
      .fromPublisher(db.stream(query.result.withStatementParameters(
        rsType = ResultSetType.ForwardOnly,
        rsConcurrency = ResultSetConcurrency.ReadOnly,
        fetchSize = fetchSize).transactionally))
      .via(Deduplicate())
  }
}

case class EventsCount(count: Int)

case class CountBySeverity(total: Int, low: Int, medium: Int, high: Int)
object CountBySeverity {
  implicit val getCountBySeverityResult: GetResult[CountBySeverity] = GetResult(r => CountBySeverity(r.<<, r.<<, r.<<, r.<<))
}

object EventRepository {

  val config = ConfigFactory.load()
  val DefaultLimit = config.getInt("cyber-event.default-limit")
  val DefaultOffset = config.getInt("cyber-event.default-offset")
  val MaxRecords = config.getInt("cyber-event.max-records")

  // extracts columns needed to store data from kafka message
  val EventsPayloadFieldsTable = Events.map { table =>
    (table.eventId, table.observedSystemId, table.name, table.description,
      table.`type`, table.severity, table.timestamp, table.source, table.sourceId,
      table.categories, table.tactics, table.extraInfo, table.incIdx,
      table.agentId, table.ruleId, table.customerAccountId)
  }

  type EventQuery = Query[Events, EventTable, Seq]

  // Note: initially we had `union` instead of `unionAll`, but because it de-duplicate rows on database side,
  // and given huge number of events, it takes surprisingly long to finish.
  // We decided to use `unionAll` here and remove duplicate rows on application side.
  def findEventsByUnionAll(tagId: String)(buildQuery: EventQuery => EventQuery): EventQuery = {
    val queries = List(
      Tables.Events
        .filter(_.observedSystemId === tagId),
      Tables.Tags
        .filter(_.tagId === tagId)
        .join(Tables.Events).on((tag, event) => tag.propertyId === event.agentId || tag.propertyId === event.ruleId)
        .map(_._2).distinctOn(_.eventId))
    // union of two queries
    queries.map(buildQuery).reduce(_ unionAll _)
  }
}

/**
 * This helper class has a method, it will append `:*` to all the lexemes of the last word, so that final tsquery can behave like `contains` method.
 *
 * scala> TsQueryHelper("'c' <-> 'in-dep' <-> 'in' <-> 'dep'", "'in-dep' <-> 'in' <-> 'dep'").toTsQuery
 * res0: com.github.tminglei.slickpg.TsQuery = TsQuery('c' <-> 'in-dep':* <-> 'in':* <-> 'dep':*)
 *
 * @param whole tsquery string for provided search string
 * @param last tsquery string for the last word of provided search string
 */
case class TsQueryHelper(whole: String, last: String) {
  // final tsquery value that will be match against `tokens_simple`
  def toTsQuery: TsQuery = {
    val lastWordLexemes = last.split("<.>").map(_.trim)
    TsQuery(whole.split(" ").map(_.trim).map { lexeme =>
      if (lastWordLexemes.contains(lexeme)) lexeme ++ ":*"
      else lexeme
    }.mkString(" "))
  }
}

object TsQueryHelper {
  val dictionary = "simple"
  implicit val getTsQueryHelperResult: GetResult[TsQueryHelper] = GetResult(r => TsQueryHelper(r.<<, r.<<))
}

case class SIEMEventsResponse(results: Seq[EventReduced], last: Long)

case class NoObservedSystemException(msg: String = "can't find observed system") extends NoSuchElementException(msg)