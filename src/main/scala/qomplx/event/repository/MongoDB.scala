package qomplx.event.repository

import java.util.concurrent.TimeUnit

import akka.http.scaladsl.util.FastFuture
import com.typesafe.config.Config
import reactivemongo.api._
import reactivemongo.core.commands.{ SilentSuccessfulAuthentication, SuccessfulAuthentication }

import scala.concurrent.{ ExecutionContext, Future }
import scala.concurrent.duration.Duration
import scala.util.Try

final class MongoDB(hosts: Seq[String], password: => String, val database: String, authConfig: Config,
  channelsOpt: Option[Int] = None)(implicit val ec: ExecutionContext) {

  val driver = new MongoDriver

  private val connectionPool = channelsOpt match {
    case None => driver.connection(hosts)
    case Some(channels) => driver.connection(hosts, MongoConnectionOptions.default.copy(nbChannelsPerNode = channels))
  }

  private val useAuthentication = Try(authConfig.getBoolean("enabled")).toOption.getOrElse(false)

  val auth: Future[SuccessfulAuthentication] =
    if (useAuthentication) {
      val authenticationDB = Try(authConfig.getString("db")).toOption.getOrElse("")
      val authenticationUser = Try(authConfig.getString("user")).toOption.getOrElse("")
      connectionPool.authenticate(authenticationDB, authenticationUser, password, FailoverStrategy.default)
    } else {
      FastFuture.successful[SuccessfulAuthentication](SilentSuccessfulAuthentication)
    }

  // DO NOT change to val!
  // see the call-out at: http://reactivemongo.org/releases/0.12/documentation/tutorial/connect-database.html#additional-notes
  def connection: Future[DefaultDB] = connectionPool.database(database)

  def close(): Unit = {
    driver.close(Duration.apply(60, TimeUnit.SECONDS))
  }
}
