package qomplx.event.repository

import java.time.Instant

import com.typesafe.scalalogging.{ Logger, StrictLogging }
import qomplx.event._
import qomplx.event.model._
import qomplx.event.repository.IncidentRepository._
import qomplx.event.routes._
import qomplx.event.thirdpartyintegrations.model.TicketingSystemMail
import qomplx.event.thirdpartyintegrations.repository.{ ThirdPartySystemConfigRepository, TicketingSystemMailRepository }
import qomplx.event.util.SlickPostgresProfile.api._
import qomplx.event.util.{ QomplxError, Utils }
import slick.jdbc.PostgresProfile.api.Database
import slick.jdbc.{ GetResult, PositionedResult }

import scala.concurrent.{ ExecutionContext, Future }

final case class StatsSummaryQueryResult(
  allCount: Int,
  allRecentDiff: Int,
  unassignedCount: Int,
  unassignedRecentDiff: Int,
  assignedCount: Int,
  assignedRecentDiff: Int,
  closedCount: Int,
  closedRecentDiff: Int)

object StatsSummaryQueryResult {
  implicit val getStatsSummary = GetResult(r => StatsSummaryQueryResult(r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<, r.<<))

  def convertToStatisticsSummary(stats: StatsSummaryQueryResult, timestampMillis: Long): StatisticsSummary = {
    StatisticsSummary(
      requestTimestampMillis = timestampMillis,
      all = MinimalStatistics(stats.allCount, stats.allRecentDiff),
      unassigned = MinimalStatistics(stats.unassignedCount, stats.unassignedRecentDiff),
      assigned = MinimalStatistics(stats.assignedCount, stats.assignedRecentDiff),
      closed = MinimalStatistics(stats.closedCount, stats.closedRecentDiff))
  }
}

final case class SeverityStatsQueryResult(low: Int, medium: Int, high: Int)

object SeverityStatsQueryResult {
  implicit val getSeverityStats = GetResult(r => SeverityStatsQueryResult(r.<<, r.<<, r.<<))

  def convertToSeverityStatistics(stats: SeverityStatsQueryResult, timestampMillis: Long, status: IncidentStatusForStatistics, numDays: Int): SeverityStatistics = {
    SeverityStatistics(
      requestTimestampMillis = timestampMillis,
      status = status.entryName,
      numDays = numDays,
      severity = SeverityCounts(stats.low, stats.medium, stats.high))
  }
}

private[repository] object IntResultMap extends GetResult[Vector[Int]] {
  override def apply(pr: PositionedResult): Vector[Int] = {
    val resultSet = pr.rs
    val metaData = resultSet.getMetaData
    // return only the column value, which we expect to be an int
    (1 to pr.numColumns).map(i => resultSet.getInt(i)).toVector
  }
}

class IncidentRepository(db: Database) extends JsonSupport with StrictLogging {

  def get(incidentId: String, observedSystemId: ObservedSystemId)(implicit ec: ExecutionContext): Future[Option[IncidentDTO]] = {
    logger.debug(s"Fetching from database incident with id: ${incidentId} in observed system: ${observedSystemId}")

    val inc = db.run(getIncident(incidentId, observedSystemId).transactionally)
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to fetch an incident ${incidentId}: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }

    logger.debug(s"Fetch Incident from database: ${incidentId} ")

    inc.map(_.headOption.map(_.toIncidentDTO))
  }

  def add(incident: Incident, eventIds: List[String])(implicit ec: ExecutionContext): Future[IncidentDTO] = {
    // generate an id before saving incident
    val incidentWithId = if (incident.id.isDefined) incident else incident.copy(id = Option(Utils.generateId(Some("INC"))))

    val insertStatement = for {
      incident <- insertIncident(incidentWithId)
      validEventIds <- filterOutUnknownEventIds(eventIds)
      _ <- if (validEventIds.nonEmpty) {
        linkEventsWithIncident(validEventIds, incident.id.get)
      } else {
        throw QomplxError("At least one valid event id is required to create an incident")
      }
      configs <- ThirdPartySystemConfigRepository.fetchAllActiveConfigs(incident.tagId)
      _ <- TicketingSystemMailRepository.batchInsert(TicketingSystemMail.fromIncident(configs, incident, validEventIds))
    } yield incident

    logger.debug(s"Inserting incident into database: ${incidentWithId} ")

    val inc = db.run(insertStatement.transactionally)
      .recoverWith {
        case ex: QomplxError =>
          Future.failed(ex)
        case t =>
          logger.error(s"The following exception was thrown when trying to add incident ${incidentWithId.toString}: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }

    logger.debug(s"Incident saved into database: ${incidentWithId} ")

    inc.map(_.toIncidentDTO)
  }

  def update(incident: Incident)(implicit ec: ExecutionContext): Future[Option[IncidentDTO]] = {
    val updateStatement = for {
      _ <- updateIncident(incident)
      updatedIncident <- getIncident(incident.id.get, incident.tagId)
    } yield (updatedIncident)

    logger.debug(s"Updating incident with id: ${incident.id.get} ")

    db.run(updateStatement.transactionally)
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to update an incident ${incident.toString}: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }.map(_.headOption.map(_.toIncidentDTO))
  }

  def addFile(file: IncidentFile, observedSystemId: ObservedSystemId)(implicit ec: ExecutionContext): Future[Option[IncidentDTO]] = {
    val addFileStatement = for {
      incident <- getIncident(file.incidentId, observedSystemId)
      _ <- if (incident.nonEmpty) {
        linkFileWithIncident(file)
      } else {
        DBIO.successful(1)
      }
    } yield (incident)

    logger.debug(s"Linking new file to incident: ${file.incidentId}")

    val result = db.run(addFileStatement.transactionally)
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to link new file to an incident ${file.toString}: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }

    logger.debug(s"File linked to an incident: ${file.incidentId}")

    result.map(_.headOption.map(_.toIncidentDTO))
  }

  def deleteFile(fileId: String, incidentId: String, observedSystemId: ObservedSystemId)(implicit ec: ExecutionContext): Future[Option[IncidentDTO]] = {
    val deleteFileStatement = for {
      incident <- getIncident(incidentId, observedSystemId)
      _ <- if (incident.nonEmpty) {
        deleteFileForIncident(fileId, incidentId)
      } else {
        DBIO.successful(1)
      }
    } yield (incident)

    logger.debug(s"Deleting file linked to incident: ${incidentId}")

    val result = db.run(deleteFileStatement.transactionally)
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to delete file linked to an incident ${fileId}: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }

    logger.debug(s"File ${fileId} deleted linked to an incident: ${incidentId}")

    result.map(_.headOption.map(_.toIncidentDTO))
  }

  def addURL(url: IncidentURL, observedSystemId: ObservedSystemId)(implicit ec: ExecutionContext): Future[Option[IncidentDTO]] = {
    val addUrlStatement = for {
      incident <- getIncident(url.incidentId, observedSystemId)
      _ <- if (incident.nonEmpty) {
        linkUrlWithIncident(url)
      } else {
        DBIO.successful(1)
      }
    } yield (incident)

    logger.debug(s"Linking new url to incident: ${url.incidentId}")

    val result = db.run(addUrlStatement.transactionally)
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to link new url to an incident ${url.toString}: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }

    logger.debug(s"URL linked to an incident: ${url.incidentId}")

    result.map(_.headOption.map(_.toIncidentDTO))
  }

  def deleteURL(urlId: String, incidentId: String, observedSystemId: ObservedSystemId)(implicit ec: ExecutionContext): Future[Option[IncidentDTO]] = {
    val deleteURLStatement = for {
      incident <- getIncident(incidentId, observedSystemId)
      _ <- if (incident.nonEmpty) {
        deleteURLForIncident(urlId, incidentId)
      } else {
        DBIO.successful(1)
      }
    } yield (incident)

    logger.debug(s"Deleting url linked to incident: ${incidentId}")

    val result = db.run(deleteURLStatement.transactionally)
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to delete url linked to an incident ${urlId}: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }

    logger.debug(s"URL ${urlId} deleted linked to an incident: ${incidentId}")

    result.map(_.headOption.map(_.toIncidentDTO))
  }

  def addNote(note: IncidentNote, observedSystemId: ObservedSystemId)(implicit ec: ExecutionContext): Future[Option[IncidentDTO]] = {
    val addNoteStatement = for {
      incident <- getIncident(note.incidentId, observedSystemId)
      _ <- if (incident.nonEmpty) {
        linkNoteWithIncident(note)
      } else {
        DBIO.successful(1)
      }
    } yield (incident)

    logger.debug(s"Linking new note to incident: ${note.incidentId}")

    val result = db.run(addNoteStatement.transactionally)
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to link new note to an incident ${note.toString}: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }

    logger.debug(s"Note linked to an incident: ${note.incidentId}")

    result.map(_.headOption.map(_.toIncidentDTO))
  }

  def deleteNote(noteId: String, incidentId: String, observedSystemId: ObservedSystemId)(implicit ec: ExecutionContext): Future[Option[IncidentDTO]] = {
    val deleteNoteStatement = for {
      incident <- getIncident(incidentId, observedSystemId)
      _ <- if (incident.nonEmpty) {
        deleteNoteForIncident(noteId, incidentId)
      } else {
        DBIO.successful(1)
      }
    } yield (incident)

    logger.debug(s"Deleting note linked to incident: ${incidentId}")

    val result = db.run(deleteNoteStatement.transactionally)
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to delete note linked to an incident ${noteId}: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }

    logger.debug(s"Note ${noteId} deleted linked to an incident: ${incidentId}")

    result.map(_.headOption.map(_.toIncidentDTO))
  }

  def getByObservedSystem(observedSystemId: ObservedSystemId, params: GetIncidentsReq)(implicit ec: ExecutionContext): Future[GetIncidentsRes] = {
    val actions = for {
      incidents <- getIncidents(observedSystemId, params)
      totalCount <- countIncidents(observedSystemId, params)
    } yield GetIncidentsRes(IncidentListDTO(incidents), totalCount)

    db.run(actions.transactionally)
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to get list of incidents: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }
  }

  def getIncidentsAndEventIdsByObservedSystem(observedSystemId: ObservedSystemId, params: ExportIncidentsParams, limit: Int)(implicit ec: ExecutionContext): Future[Vector[IncidentDTO]] = {
    db.run(getIncidentsAndEventIds(observedSystemId, GetIncidentsReq(0, 0, params.assignee, params.search, params.status, params.numDays), limit, Some(logger)))
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to get incidents with event IDs: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }
  }

  def getAttachments(incidentId: String, observedSystemId: ObservedSystemId)(implicit ec: ExecutionContext): Future[Option[AttachmentsDTO]] = {
    val getAttachmentsStatement = for {
      incident <- getIncident(incidentId, observedSystemId)
      incidentAttachmentsDTO <- if (incident.nonEmpty) {
        for {
          incidentFiles <- getIncidentFiles(incidentId)
          incidentURLs <- getIncidentURLs(incidentId)
          incidentNotes <- getIncidentNotes(incidentId)
        } yield Some(IncidentAttachmentsDTO(incidentFiles, incidentURLs, incidentNotes))
      } else {
        DBIO.successful(None)
      }
    } yield incidentAttachmentsDTO

    logger.debug(s"Retrieving attachments linked to an incident: ${incidentId}")

    val result = db.run(getAttachmentsStatement.transactionally)
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to retrieve attachments linked to an incident ${incidentId}: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }

    logger.debug(s"Fetched all attachments linked to an incident: ${incidentId}")

    result
  }

  def getEvents(incidentId: String, tagId: ObservedSystemId)(implicit ec: ExecutionContext): Future[Seq[Event]] = {
    val getEventsStatement = for {
      incident <- getIncident(incidentId, tagId)
      events <- if (incident.nonEmpty) {
        getEventsOfIncident(incident.head.id.get)
      } else {
        logger.warn(s"No incident found with id: ${incidentId} and observed system id: ${tagId}")
        DBIO.successful(List())
      }
    } yield (events)

    logger.debug(s"Retrieving events linked to an incident: ${incidentId}")

    db.run(getEventsStatement.transactionally)
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to retrieve events linked to an incident ${incidentId}: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }.map(EventList(_, tagId))
  }

  def getCategories()(implicit ec: ExecutionContext): Future[Seq[Category]] = {
    logger.debug(s"Fetching list of categories from database")

    db.run(getAllCategories.transactionally)
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to fetch categories: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }
  }

  def getTactics()(implicit ec: ExecutionContext): Future[Seq[Tactic]] = {
    logger.debug(s"Fetching list of tactics from database")

    db.run(getAllTactics.transactionally)
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to fetch tactics: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }
  }

  def getFile(fileId: String, incidentId: String, observedSystemId: ObservedSystemId)(implicit ec: ExecutionContext): Future[Option[IncidentFile]] = {
    logger.debug(s"Fetching file with id: ${fileId} linked to incident: ${incidentId} and observed system with id: ${observedSystemId}")

    val getFileStatement = for {
      incident <- getIncident(incidentId, observedSystemId)
      incidentFile <- if (incident.nonEmpty) {
        getIncidentFile(fileId, incidentId)
      } else {
        logger.warn(s"No incident found with id: ${incidentId} and observed system id: ${observedSystemId}")
        DBIO.successful(Seq())
      }
    } yield (incidentFile)

    db.run(getFileStatement.transactionally)
      .recoverWith {
        case t =>
          logger.error(s"The following exception was thrown when trying to fetch file linked to incident: ${t.getMessage}")
          Future.failed(new RuntimeException(s"An error occurred: ${t.getMessage}"))
      }.map(_.headOption)
  }

  def getCountBySeverity(tagId: ObservedSystemId)(implicit ec: ExecutionContext): Future[CountBySeverity] = {
    val end = System.currentTimeMillis()
    val start = end - (86400 * 1000) // given time minus 24 hours (in millis)
    db.run(
      sql"""
        select sum(1) as total,
        sum(case when severity between 0 and 30 then 1 else 0 end) as low,
        sum(case when severity between 31 and 60 then 1 else 0 end) as medium,
        sum(case when severity between 61 and 100 then 1 else 0 end) as high
        from cyber_event_service.amp_incidents
        where tag_id = '#$tagId' and (created_on >= #$start and created_on <= #$end)""".as[CountBySeverity].head)
  }

  /**
   * Gets the following information for all incidents, unassigned incidents,
   * assigned incidents, and closed incidents for an observed system:
   *
   * 1. Total count
   * 2. The difference between the count from (a) (`requestTimestampMillis` minus one day) to `requestTimestampMillis` and
   *    (b) the count from (`requestTimestampMillis` minus two days) to (`requestTimestampMillis` minus one day)
   *
   * Also includes in the response the time of the request (`requestTimestampMilllis`).
   */
  def getStatisticsSummary(tagId: String, requestTimestampMillis: Long)(implicit ec: ExecutionContext): Future[StatisticsSummary] = {
    import StatsSummaryQueryResult.getStatsSummary

    val twoDaysOlderMillis = Utils.subtractNumDays(requestTimestampMillis, 2)
    val oneDayOlderMillis = Utils.subtractNumDays(requestTimestampMillis, 1)

    val query =
      sql"""
        select
        count(id) filter (where tag_id = '#$tagId') as allCount,
        (count(id) filter (where created_on >= #$oneDayOlderMillis and created_on <= #$requestTimestampMillis and tag_id = '#$tagId')) - (count(id) filter (where created_on >= #$twoDaysOlderMillis and created_on < #$oneDayOlderMillis and tag_id = '#$tagId')) as allRecentDiff,

        count(id) filter (where assignee is null and status in ('NEW', 'OPEN') and tag_id = '#$tagId') as unassignedCount,
        (count(id) filter (where created_on >= #$oneDayOlderMillis and created_on <= #$requestTimestampMillis and assignee is null and status in ('NEW', 'OPEN') and tag_id = '#$tagId')) - (count(id) filter (where created_on >= #$twoDaysOlderMillis and created_on < #$oneDayOlderMillis and assignee is null and status in ('NEW', 'OPEN') and tag_id = '#$tagId')) as unassignedRecentDiff,

        count(id) filter (where assignee is not null and status in ('NEW', 'OPEN') and tag_id = '#$tagId') as assignedCount,
        (count(id) filter (where created_on >= #$oneDayOlderMillis and created_on <= #$requestTimestampMillis and assignee is not null and status in ('NEW', 'OPEN') and tag_id = '#$tagId')) - (count(id) filter (where created_on >= #$twoDaysOlderMillis and created_on < #$oneDayOlderMillis and assignee is not null and status in ('NEW', 'OPEN') and tag_id = '#$tagId')) as assignedRecentDiff,

        count(id) filter (where status = 'CLOSED' and tag_id = '#$tagId') as closedCount,
        (count(id) filter (where created_on >= #$oneDayOlderMillis and created_on <= #$requestTimestampMillis and status = 'CLOSED' and tag_id = '#$tagId')) - (count(id) filter (where created_on >= #$twoDaysOlderMillis and created_on < #$oneDayOlderMillis and status = 'CLOSED' and tag_id = '#$tagId')) as closedRecentDiff

        from cyber_event_service.amp_incidents
      """.as[StatsSummaryQueryResult].head

    db.run(query)
      .map(StatsSummaryQueryResult.convertToStatisticsSummary(_, requestTimestampMillis))
  }

  /**
   * Gets the number of incidents with the given `status` from the time
   * window of `requestTimestampMillis` minus `numDays` days, partitioned
   * by severity level (low, medium, high).
   *
   * Also includes in the response the time of the request (`requestTimestampMilllis`).
   *
   * @param status must be "all", "unassigned", "assigned", or "closed"
   * @param numDays must be 1, 3, 7, 30
   */
  def getSeverityStatistics(observedSystemId: String, requestTimestampMillis: Long, status: IncidentStatusForStatistics, numDays: Int)(implicit ec: ExecutionContext): Future[SeverityStatistics] = {
    require(Set(1, 3, 7, 30).contains(numDays), "numDays must be 1, 3, 7, or 30")
    import IncidentStatusForStatistics._
    import SeverityStatsQueryResult.getSeverityStats

    val daysOlderMillis = Utils.subtractNumDays(requestTimestampMillis, numDays)

    val query =
      status match {
        case All =>
          sql"""
            select
            count(id) filter (where created_on >= #$daysOlderMillis and created_on <= #$requestTimestampMillis and tag_id = '#$observedSystemId' and severity between 0 and 30) as low,
            count(id) filter (where created_on >= #$daysOlderMillis and created_on <= #$requestTimestampMillis and tag_id = '#$observedSystemId' and severity between 31 and 60) as medium,
            count(id) filter (where created_on >= #$daysOlderMillis and created_on <= #$requestTimestampMillis and tag_id = '#$observedSystemId' and severity between 61 and 100) as high
            from cyber_event_service.amp_incidents
          """.as[SeverityStatsQueryResult].head
        case Unassigned =>
          sql"""
            select
            count(id) filter (where created_on >= #$daysOlderMillis and created_on <= #$requestTimestampMillis and assignee is null and status in ('NEW', 'OPEN') and tag_id = '#$observedSystemId' and severity between 0 and 30) as low,
            count(id) filter (where created_on >= #$daysOlderMillis and created_on <= #$requestTimestampMillis and assignee is null and status in ('NEW', 'OPEN') and tag_id = '#$observedSystemId' and severity between 31 and 60) as medium,
            count(id) filter (where created_on >= #$daysOlderMillis and created_on <= #$requestTimestampMillis and assignee is null and status in ('NEW', 'OPEN') and tag_id = '#$observedSystemId' and severity between 61 and 100) as high
            from cyber_event_service.amp_incidents
          """.as[SeverityStatsQueryResult].head
        case Assigned =>
          sql"""
            select
            count(id) filter (where created_on >= #$daysOlderMillis and created_on <= #$requestTimestampMillis and assignee is not null and status in ('NEW', 'OPEN') and tag_id = '#$observedSystemId' and severity between 0 and 30) as low,
            count(id) filter (where created_on >= #$daysOlderMillis and created_on <= #$requestTimestampMillis and assignee is not null and status in ('NEW', 'OPEN') and tag_id = '#$observedSystemId' and severity between 31 and 60) as medium,
            count(id) filter (where created_on >= #$daysOlderMillis and created_on <= #$requestTimestampMillis and assignee is not null and status in ('NEW', 'OPEN') and tag_id = '#$observedSystemId' and severity between 61 and 100) as high
            from cyber_event_service.amp_incidents
          """.as[SeverityStatsQueryResult].head
        case Closed =>
          sql"""
            select
            count(id) filter (where created_on >= #$daysOlderMillis and created_on <= #$requestTimestampMillis and status = 'CLOSED' and tag_id = '#$observedSystemId' and severity between 0 and 30) as low,
            count(id) filter (where created_on >= #$daysOlderMillis and created_on <= #$requestTimestampMillis and status = 'CLOSED' and tag_id = '#$observedSystemId' and severity between 31 and 60) as medium,
            count(id) filter (where created_on >= #$daysOlderMillis and created_on <= #$requestTimestampMillis and status = 'CLOSED' and tag_id = '#$observedSystemId' and severity between 61 and 100) as high
            from cyber_event_service.amp_incidents
          """.as[SeverityStatsQueryResult].head
      }

    db.run(query)
      .map(SeverityStatsQueryResult.convertToSeverityStatistics(_, requestTimestampMillis, status, numDays))
  }

  /**
   * Gets counts for all incidents, unassigned incidents, assigned incidents, and
   * closed incidents for an observed system.
   *
   * Let `p = `requestTimestampMillis - numDays in days`. Returns a number of counts
   * that are based on a level of granularity. The granularity depends on the value of
   * the `numDays` parameter:
   *
   * | `numDays` | `granularity` |
   * | --------- | ------------- |
   * | `1`       | `3hrs`        |
   * | `3`       | `1day`        |
   * | `7`       | `1day`        |
   * | `30`      | `6days`       |
   *
   * For example, 30 `numDays` corresponds to a `granularity` of six days, meaning
   * that there will be `30 / 6 = 5` counts in six-day intervals across the time
   * period `p`.
   *
   * @param status must be "all", "unassigned", "assigned", or "closed"
   * @param numDays must be 1, 3, 7, 30
   */
  def getStatisticsDetails(tagId: String, requestTimestampMillis: Long, status: IncidentStatusForStatistics, numDays: Int)(implicit ec: ExecutionContext): Future[StatisticsDetails] = {
    require(Set(1, 3, 7, 30).contains(numDays), "numDays must be 1, 3, 7, or 30")
    import IncidentRepository.granularityMap
    import IncidentStatusForStatistics._
    implicit val rowConverter = IntResultMap

    val countStatement =
      status match {
        case All =>
          Utils.createCountStatements(requestTimestampMillis, numDays, Option(s"and tag_id = '$tagId'"))
        case Unassigned =>
          Utils.createCountStatements(requestTimestampMillis, numDays, Option(s"and assignee is null and status in ('NEW', 'OPEN') and tag_id = '$tagId'"))
        case Assigned =>
          Utils.createCountStatements(requestTimestampMillis, numDays, Option(s"and assignee is not null and status in ('NEW', 'OPEN') and tag_id = '$tagId'"))
        case Closed =>
          Utils.createCountStatements(requestTimestampMillis, numDays, Option(s"and status = 'CLOSED' and tag_id = '$tagId'"))
      }

    // the number of counts returned (i.e., the number of columns) varies depending on the numDays parameter,
    // so we're using a generic result set (see the rowConverter implicit val above)
    val query = sql"""select #$countStatement from cyber_event_service.amp_incidents""".as[Vector[Int]].head

    val counts: Future[Vector[Int]] = db.run(query)
    counts.map { c =>
      StatisticsDetails(
        requestTimestampMillis,
        status.entryName,
        numDays,
        granularityMap(numDays),
        c)
    }
  }
}

object IncidentRepository {
  private[repository] val granularityMap = Map(1 -> "3hrs", 3 -> "1day", 7 -> "1day", 30 -> "6days")

  def getIncident(incidentId: String) = Tables.Incidents.filter(_.id === incidentId).result
  def insertIncident(incident: Incident) = Tables.Incidents returning Tables.Incidents += incident
  def updateIncident(inc: Incident) = Tables.Incidents.filter(t => t.id === inc.id && t.tagId === inc.tagId)
    .map(inc => (inc.title, inc.description, inc.categories, inc.tactics, inc.resolution, inc.resolutionTimestamp, inc.status, inc.severity, inc.reporterSource, inc.assignee, inc.assigneeEmail, inc.lastModifiedOn, inc.lastModifiedBy))
    .update((inc.title, inc.description, inc.categories, inc.tactics, inc.resolution, inc.resolutionTimestamp, inc.status, inc.severity, inc.reporterSource, inc.assigneeId, inc.assigneeEmail, inc.lastModifiedOn, inc.lastModifiedBy))

  def linkFileWithIncident(file: IncidentFile) = Tables.IncidentFiles += file
  def deleteFileForIncident(fileId: String, incidentID: String) = Tables.IncidentFiles.filter(t => t.id === fileId && t.incidentId === incidentID).delete
  def linkUrlWithIncident(url: IncidentURL) = Tables.IncidentURLs += url
  def deleteURLForIncident(urlId: String, incidentID: String) = Tables.IncidentURLs.filter(t => t.id === urlId && t.incidentId === incidentID).delete
  def linkNoteWithIncident(note: IncidentNote) = Tables.IncidentNotes += note
  def deleteNoteForIncident(noteId: String, incidentID: String) = Tables.IncidentNotes.filter(t => t.id === noteId && t.incidentId === incidentID).delete

  def linkEventsWithIncident(eventIds: Seq[String], incidentId: String) = Tables.EventsInIncident ++= eventIds.map(eventId => (eventId, incidentId))

  private def findIncidents(observedSystemId: ObservedSystemId, params: GetIncidentsReq, logger: Option[Logger] = None) = {
    import IncidentStatusForStatistics._

    logger.map(_.debug(s"Parameters for finding incidents associated with observed system [$observedSystemId]: $params"))
    val timeOfRequest = Instant.now().toEpochMilli

    val baseQuery =
      Tables.Incidents
        .filterOpt(params.assignee)((inc, assigneeId) => inc.assignee === assigneeId)
        .filterOpt(params.search)((inc, search) => inc.title.toLowerCase like s"%${search.trim.toLowerCase}%")
        .filterOpt(params.numDays)((inc, days) => inc.createdOn >= Utils.subtractNumDays(timeOfRequest, days))
        .filter(_.tagId === observedSystemId)

    params.status match {
      case Some(s) =>
        IncidentStatusForStatistics.withName(s) match {
          case All =>
            baseQuery
          case Unassigned =>
            // unassigned incidents have a database status of "NEW" or "OPEN"
            baseQuery.filter(inc => inc.assignee.isEmpty && (inc.status inSet Set(IncidentStatus.NEW, IncidentStatus.OPEN)))
          case Assigned =>
            // assigned incidents have a database status of "NEW" or "OPEN"
            baseQuery.filter(inc => inc.assignee.isDefined && (inc.status inSet Set(IncidentStatus.NEW, IncidentStatus.OPEN)))
          case Closed =>
            baseQuery.filter(_.status === IncidentStatus.CLOSED)
        }
      case None =>
        baseQuery
    }
  }

  def getIncidents(observedSystemId: ObservedSystemId, params: GetIncidentsReq) = {
    findIncidents(observedSystemId, params)
      .sortBy(_.createdOn.desc)
      .drop(params.offset)
      .take(params.limit)
      .result
  }

  /**
   * Used for the CSV export functionality. Similar to the
   * [[qomplx.event.repository.IncidentRepository#getIncidents]] method, but
   * includes the associated event IDs and does not sort the results. Also,
   * in order to return all the results instead of only a page of results,
   * this method ignores the limit in the `params` argument and uses its own
   * explicit `limit` argument.
   */
  def getIncidentsAndEventIds(observedSystemId: ObservedSystemId, params: GetIncidentsReq, limit: Int, logger: Option[Logger] = None)(implicit ec: ExecutionContext) = {
    findIncidents(observedSystemId, params, logger)
      .sortBy(_.createdOn.desc)
      .take(limit) // note that we don't use the limit inside the "params" argument
      .joinLeft(Tables.EventsInIncident).on {
        case (incidents, eventIncidents) =>
          incidents.id === eventIncidents.incidentId
      }
      .result
      .map { results =>
        val groupedByIncidentId = results.groupBy(_._1.id)

        val incidentList =
          groupedByIncidentId collect {
            case (incidentId, join) =>
              val eventIds = join.map(_._2).collect { case Some((evId, _)) => evId }
              (join.head._1, eventIds)
          }

        IncidentWithEventIdsListDTO(incidentList.toVector)
      }
  }

  def getIncident(incidentId: String, tagId: ObservedSystemId) = Tables.Incidents.filter(t => t.id === incidentId && t.tagId === tagId).result

  def countIncidents(observedSystemId: ObservedSystemId, params: GetIncidentsReq) = {
    findIncidents(observedSystemId, params).size.result
  }

  def getIncidentFiles(incidentId: String) = Tables.IncidentFiles.filter(_.incidentId === incidentId).result
  def getIncidentURLs(incidentId: String) = Tables.IncidentURLs.filter(_.incidentId === incidentId).result
  def getIncidentNotes(incidentId: String) = Tables.IncidentNotes.filter(_.incidentId === incidentId).result

  def getEventsOfIncident(incidentId: String) = (Tables.EventsInIncident.filter(_.incidentId === incidentId) join Tables.Events on (_.eventId === _.eventId)).map(_._2).result

  def filterOutUnknownEventIds(eventIds: List[String]) = Tables.Events.filter(_.eventId.inSet(eventIds)).map(_.eventId).result
  def getAllCategories = Tables.Categories.result
  def getAllTactics = Tables.Tactics.result
  def getIncidentFile(fileId: String, incidentId: String) = Tables.IncidentFiles.filter(f => f.id === fileId && f.incidentId === incidentId).result
}
