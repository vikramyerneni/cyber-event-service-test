package qomplx.event.repository

import com.google.common.cache.CacheBuilder
import com.typesafe.scalalogging.StrictLogging
import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.bson.{ BSONDocument, BSONObjectID, Macros }
import scalacache._
import scalacache.guava.GuavaCache
import scalacache.memoization._
import scalacache.modes.scalaFuture._

import scala.concurrent.duration._
import scala.concurrent.{ ExecutionContext, Future }

// copied from the admon-import-service-legacy project
class ObservedSystemRepository(db: MongoDB) extends StrictLogging {

  private implicit val cache: GuavaCache[Option[ObservedSystem]] = {
    val underlyingGuavaCache = CacheBuilder.newBuilder().maximumSize(1000L).build[String, Entry[Option[ObservedSystem]]]
    GuavaCache(underlyingGuavaCache)
  }

  private def collectionFuture(implicit ec: ExecutionContext): Future[BSONCollection] = {
    db.connection.map { conn =>
      conn.collection[BSONCollection]("ds.observed_systems")
    }
  }

  private implicit def observedSystemReader = Macros.reader[ObservedSystem]

  def findByAPIKey(apiKey: String)(implicit @cacheKeyExclude ctx: ExecutionContext): Future[Option[ObservedSystem]] = memoizeF(Some(24.hours)) {
    val query = BSONDocument(ObservedSystem.ApiKey -> apiKey)
    collectionFuture.flatMap { coll =>
      coll.find(query, None).one[ObservedSystem]
    }
  }

  def findById(id: String)(implicit @cacheKeyExclude ctx: ExecutionContext): Future[Option[ObservedSystem]] = memoizeF(Some(24.hours)) {
    BSONObjectID.parse(id).fold(
      { th =>
        logger.info(s"Could not parse '$id' as mongo object id", th)
        Future.successful(None)
      },
      { objectId =>
        val query = BSONDocument(ObservedSystem.Id -> objectId)
        collectionFuture.flatMap { coll =>
          coll.find(query, None).one[ObservedSystem]
        }
      })
  }
}

object ObservedSystem {
  val ApiKey = "user.api_key"
  val Id = "_id"
}

final case class ObservedSystem(_id: BSONObjectID, name: String) {
  def id = _id
}