package qomplx.event.vault

import java.io.{ ByteArrayInputStream, InputStream }
import java.security.{ KeyStore, SecureRandom }
import java.util.Base64

import com.bettercloud.vault.response.HealthResponse
import com.bettercloud.vault.{ Vault, VaultConfig }
import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import enumeratum.values.{ IntEnum, IntEnumEntry }
import javax.net.ssl.{ KeyManagerFactory, SSLContext, TrustManagerFactory }

import scala.collection.JavaConverters._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }
import scala.language.postfixOps
import scala.util.{ Failure, Success, Try }

final case class VaultLoginConfig(
  apiUrl: String,
  roleId: String,
  secretId: String)

//https://www.vaultproject.io/api-docs/system/health#read-health-information
private[vault] sealed abstract class VaultStatusCodes(val value: Int) extends IntEnumEntry
private[vault] object VaultStatusCodes extends IntEnum[VaultStatusCodes] {
  case object Active extends VaultStatusCodes(200)
  case object StandBy extends VaultStatusCodes(429)

  override def values = findValues
}

final class VaultManager(config: Config) extends StrictLogging {
  private val vaultLoginConfig: VaultLoginConfig = {
    val url = config.getString("api-url")
    val roleId = config.getString("role-id")
    val secretId = config.getString("secret-id")
    VaultLoginConfig(url, roleId, secretId)
  }

  private val secretPath = config.getString("secrets-path")
  private val mandrillTokenField = config.getString("secret-field.mandrill-token")
  private val postgresPasswordField = config.getString("secret-field.postgres-password")
  private val mongoPasswordField = config.getString("secret-field.cyber-mongo-password")
  private val s3AccessKeyField = config.getString("secret-field.aws-access-key")
  private val s3SecretKeyField = config.getString("secret-field.aws-secret-key")
  private val tinkKeysetField = config.getString("secret-field.tink-keyset-key")

  lazy val mandrillToken = getKeyFromPath(mandrillTokenField)
  lazy val postgresPassword = getKeyFromPath(postgresPasswordField)
  lazy val mongoPassword = getKeyFromPath(mongoPasswordField)
  lazy val s3AccessKey = getKeyFromPath(s3AccessKeyField)
  lazy val s3SecretKey = getKeyFromPath(s3SecretKeyField)
  lazy val tinkKeyset = getKeyFromPath(tinkKeysetField)

  // This method is public so that tests in other packages can mock it.
  def getKeyFromPath(key: String): String = {
    logger.info(s"[path: $secretPath] [key: $key]")
    Try(getVaultInstance.logical().read(secretPath).getData.asScala) match {
      case Success(data) =>
        logger.info(s"Found data from Vault for [path: $secretPath] [key: $key]")
        data(key)
      case Failure(t) =>
        throw new RuntimeException(s"Failed to get [$key] from [$secretPath] in Vault.", t)
    }
  }

  def getSSLContext(certPath: String): Option[SSLContext] = {
    val vault = getVaultInstance

    getCertAtPath(vault, certPath) match {
      case Some((certificate, password)) =>
        val pswd = password.toCharArray
        val sslCert: InputStream = new ByteArrayInputStream(certificate)

        val keyStore = KeyStore.getInstance("PKCS12")

        keyStore.load(sslCert, pswd)

        val keyManagerFactory: KeyManagerFactory = KeyManagerFactory.getInstance("SunX509")
        keyManagerFactory.init(keyStore, pswd)

        val tmf: TrustManagerFactory = TrustManagerFactory.getInstance("SunX509")
        tmf.init(keyStore)

        val sslContext: SSLContext = SSLContext.getInstance("TLS")
        sslContext.init(keyManagerFactory.getKeyManagers, tmf.getTrustManagers, new SecureRandom)
        Option(sslContext)
      case None => None
    }
  }

  def health(): Boolean = {
    val healthResp: Future[HealthResponse] = Future { getVaultInstance.debug().health() }

    Try(Await.result(healthResp, 30 seconds)).fold({ th =>
      logger.error(s"Couldn't reach out to Vault: ${th.getMessage}")
      false
    }, { resp =>
      import VaultStatusCodes._
      val status = resp.getRestResponse.getStatus
      val msg = resp.getRestResponse.getBody.map(_.toChar).mkString
      if (status == Active.value || status == StandBy.value) {
        logger.debug(s"Vault status [$status]: $msg")
        true
      } else {
        logger.error(s"Couldn't connect to Vault [$status]: $msg")
        false
      }
    })
  }

  private def getCertAtPath(vault: Vault, path: String) = {
    val secrets = vault.logical().read(path).getData
    for {
      cert <- Option(secrets.get("pkcs")).map(Base64.getMimeDecoder.decode)
      pass <- Option(secrets.get("pkcsPassword"))
    } yield (cert, pass)
  }

  def getVaultInstance: Vault = {
    val vaultConf = new VaultConfig().engineVersion(1).address(vaultLoginConfig.apiUrl).token(getToken).build()
    new Vault(vaultConf)
  }

  private def getToken: String = {
    new Vault(new VaultConfig().engineVersion(1).address(vaultLoginConfig.apiUrl).build())
      .auth()
      .loginByAppRole(vaultLoginConfig.roleId, vaultLoginConfig.secretId)
      .getAuthClientToken
  }
}
