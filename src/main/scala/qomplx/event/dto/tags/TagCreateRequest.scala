package qomplx.event.dto.tags

import qomplx.event.model.PropertyType.PropertyType

case class TagCreateRequest(sourceId: String, tagId: String, source: PropertyType)
