package qomplx.event

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import qomplx.event.dto.tags.TagCreateRequest
import qomplx.event.model.PropertyType.PropertyType
import qomplx.event.model._
import qomplx.event.repository.{ CountBySeverity, EventsCount, SIEMEventsResponse }
import qomplx.event.routes._
import qomplx.event.service.email._
import qomplx.event.thirdpartyintegrations.model.ThirdPartySystem
import qomplx.event.thirdpartyintegrations.routes.{ CreateTSConfigPayload, ListTSConfigs, TSConfigDTO, UpdateTSConfigPayload }
import qomplx.event.util._
import spray.json._

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val eventFormat: RootJsonFormat[Event] = jsonFormat16(Event)
  implicit val eventMessageFormat: RootJsonFormat[EventMessage] = jsonFormat16(EventMessage)
  implicit val eventReducedFormat: RootJsonFormat[EventReduced] = jsonFormat15(EventReduced)
  implicit val SIEMResponseFormat: RootJsonFormat[SIEMEventsResponse] = jsonFormat2(SIEMEventsResponse)

  implicit def enumFormat[T <: Enumeration](implicit enu: T): RootJsonFormat[T#Value] =
    new RootJsonFormat[T#Value] {
      def write(obj: T#Value): JsValue = JsString(obj.toString)
      def read(json: JsValue): T#Value = {
        json match {
          case JsString(txt) => enu.withName(txt)
          case somethingElse => throw DeserializationException(s"Expected a value from enum $enu instead of $somethingElse")
        }
      }
    }

  // health-related formats
  implicit object SeverityJsonFormat extends RootJsonFormat[Severity] {
    override def write(s: Severity) = s match {
      case Fatal => JsString("fatal")
      case NonFatal => JsString("non-fatal")
    }

    override def read(value: JsValue) = value match {
      case JsString("fatal") => Fatal
      case JsString("non-fatal") => NonFatal
      case _ => deserializationError("Severity (fatal or non-fatal) expected")
    }
  }

  implicit val mandrillToFormat = jsonFormat3(MandrillTo)
  implicit val mandrillAttachmentFormat: RootJsonFormat[MandrillAttachment] = jsonFormat3(MandrillAttachment.apply)
  implicit val mandrillMessageFormat: RootJsonFormat[MandrillMessage] = jsonFormat7(MandrillMessage)

  implicit val mandrillConfigFormat = jsonFormat2(MandrillConfig.apply(_: String, _: String))
  implicit val mandrillApiCallFormat = jsonFormat5(MandrillApiCall)
  implicit val EmailResponseFormat = jsonFormat2(EmailResponse)

  implicit val incidentStatusFormat = enumFormat(IncidentStatus)
  implicit val IncidentReporterSourceFormat = enumFormat(IncidentReporterSource)
  implicit val createIncidentReqFormat = jsonFormat7(CreateIncidentReq)
  implicit val updateIncidentReqFormat = jsonFormat11(UpdateIncidentReq)

  implicit val ticketingSystemFormat = enumFormat(ThirdPartySystem)
  implicit val createTSConfigPayloadFormat = jsonFormat4(CreateTSConfigPayload)
  implicit val tSConfigDtoFormat = jsonFormat8(TSConfigDTO)
  implicit val updateTSConfigPayloadFormat = jsonFormat5(UpdateTSConfigPayload)
  implicit val listTSConfigsFormat = jsonFormat1(ListTSConfigs)

  implicit val attachFileReqFormat = jsonFormat3(AttachFileReq)
  implicit val attachURLReqFormat = jsonFormat1(AttachURLReq)
  implicit val attachNoteReqFormat = jsonFormat1(AttachNoteReq)

  implicit val incidentDtoFormat = jsonFormat16(IncidentDTO.apply)
  implicit val integrationFormat = jsonFormat4(Integration)
  implicit val healthFormat = jsonFormat6(Health.apply)
  implicit val getIncidentsResFormat = jsonFormat2(GetIncidentsRes)
  implicit val incidentFileDtoFormat = jsonFormat5(IncidentFileDTO)
  implicit val incidentUrlDtoFormat = jsonFormat4(IncidentUrlDTO)
  implicit val incidentNoteDtoFormat = jsonFormat4(IncidentNoteDTO)
  implicit val attachmentsDtoFormat = jsonFormat3(AttachmentsDTO)
  implicit val categoryFormat = jsonFormat1(Category)
  implicit val tacticFormat = jsonFormat1(Tactic)
  implicit val incidentFileFormat = jsonFormat9(IncidentFile.apply)
  implicit val getEventsRes = jsonFormat2(GetEventsRes)
  implicit val eventsCountFormat = jsonFormat1(EventsCount)
  implicit val countBySeverityFormat = jsonFormat4(CountBySeverity.apply)

  implicit val minimalStatsFormat = jsonFormat2(MinimalStatistics)
  implicit val statsSummaryFormat = jsonFormat5(StatisticsSummary)
  implicit val severityCounts = jsonFormat3(SeverityCounts)
  implicit val severityStatistics = jsonFormat4(SeverityStatistics)
  implicit val statistics = jsonFormat5(StatisticsDetails)

  // Tags
  implicit val tagSourceType: RootJsonFormat[PropertyType] = enumFormat(PropertyType)
  implicit val tagCreateRequest: RootJsonFormat[TagCreateRequest] = jsonFormat3(TagCreateRequest)
}
