package qomplx.event.migration

import java.util.concurrent.Executors

import akka.actor.{ Actor, ActorSystem, Props }
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.util.SlickPostgresProfile.api._
import slick.jdbc.PostgresProfile.api.Database

import scala.concurrent.ExecutionContext

// Note: this scheduler would remove severity count older than 1 day.
class CountBySeverityPurger(db: Database) extends Actor with StrictLogging {
  import CountBySeverityPurger.Purge

  private implicit val ec = ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())

  override def receive: Receive = {
    case Purge =>
      logger.info(s"CountBySeverityPurger actor received a Purge message, it will try to remove counts older than 24 hours.")
      db.run(sqlu"delete from cyber_event_service.amp_events_count_by_severity where datetime < date_trunc('minute', CURRENT_TIMESTAMP)::timestamp - interval '1 day`'".transactionally).recover {
        case th =>
          logger.error(s"Something went wrong while deleting older events counts by their severity", th)
      }
  }
}

object CountBySeverityPurger {
  def schedule(db: Database)(implicit system: ActorSystem, ec: ExecutionContext): Unit = {
    val mailer = system.actorOf(Props(new CountBySeverityPurger(db)), "CountBySeverityPurger")
    QuartzSchedulerExtension(system)
      .schedule("count-by-severity-purger", mailer, Purge)
  }

  case object Purge
}
