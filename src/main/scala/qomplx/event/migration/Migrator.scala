package qomplx.event.migration

import java.util.concurrent.Executors

import akka.actor.{ Actor, ActorSystem, Props }
import com.fractal.auth.client.deprecated.Client
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.model.Tables
import qomplx.event.util.SlickPostgresProfile.api._
import slick.jdbc.GetResult
import slick.jdbc.PostgresProfile.api.Database

import scala.concurrent.{ ExecutionContext, Future }

class Migrator(db: Database, migratorConfig: Config, authServiceClient: Client) extends Actor with StrictLogging {
  import Migrator._

  private implicit val ec = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(migratorConfig.getInt("thread-pool-size")))

  private def selectForUpdate(limit: Int = 10) = {
    sql"""
      SELECT id, reporter_id, created_by, last_modified_by FROM cyber_event_service.amp_incidents
      WHERE reporter_id_unique is null and created_by_unique is null and last_modified_by_unique is null
      LIMIT #${limit}
      FOR UPDATE SKIP LOCKED;
       """.as[UserRecord]
  }

  private def updateRow(id: String, reporterIdUnique: String, createdByUnique: String, lastModifiedByUnique: String) = {
    Tables.Incidents.filter(_.id === id).map(r => (r.reporterIdUnique, r.createdByUnique, r.lastModifiedByUnique)).update(reporterIdUnique, createdByUnique, lastModifiedByUnique)
  }

  private def fetchUserIdKsuidMap(ids: List[Long]): Future[Map[Long, String]] = {
    authServiceClient.users.getUsersByIds(ids.distinct).map(_.groupBy(_.id).mapValues(_.map(_.uniqueId).head))
  }

  private def migrate(): Future[Unit] = {
    val actions = for {
      rows <- selectForUpdate(10)
      _ = logger.debug(s"incident rows selected: '${rows.map(_.id).mkString(",")}'")
      map <- DBIO.from(fetchUserIdKsuidMap(rows.flatMap(_.getIds).toList))
      _ <- DBIO.seq(rows.map(r => updateRow(r.id, map(r.reporterId), map(r.createdBy), map(r.lastModifiedBy))): _*)
      _ = logger.debug(s"user long ids updated with their corresponding ksuids.")
    } yield ()
    db.run(actions.transactionally)
  }

  override def receive: Receive = {
    case Run =>
      logger.info(s"Migrator actor received a Run message, it will try to migrate pending user long id to ksuid,")
      migrate().recover {
        case th =>
          logger.error(s"Something went wrong while migrating user long ids to ksuids.", th)
      }
  }
}

object Migrator {
  def schedule(db: Database, migratorConfig: Config, authServiceClient: Client)(implicit system: ActorSystem, ec: ExecutionContext): Unit = {
    val mailer = system.actorOf(props(db, migratorConfig, authServiceClient), "Migrator")
    QuartzSchedulerExtension(system)
      .schedule("migrate-user-id", mailer, Run)
  }

  def props(db: Database, migratorConfig: Config, authServiceClient: Client)(implicit ec: ExecutionContext) =
    Props(new Migrator(db, migratorConfig, authServiceClient))

  case object Run

  case class UserRecord(id: String, reporterId: Long, createdBy: Long, lastModifiedBy: Long) {
    def getIds: List[Long] = List(reporterId, createdBy, lastModifiedBy)
  }
  implicit val getUserRecordResult: GetResult[UserRecord] = GetResult(r =>
    UserRecord(r.<<, r.<<, r.<<, r.<<))
}