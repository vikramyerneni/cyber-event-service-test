package qomplx.event.alerting.email

import java.util.UUID
import java.util.concurrent.Executors

import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.model.EventMessage
import qomplx.event.service.email.{ MandrillApiCall, MandrillAttachment, MandrillConfig, MandrillService }
import qomplx.event.thirdpartyintegrations.templates.Template

import scala.concurrent.{ ExecutionContext, ExecutionContextExecutor, Future }
import scala.jdk.CollectionConverters.asScalaSetConverter
import scala.util.{ Failure, Success, Try }

class EventsMailer(config: Config, mandrillConfig: MandrillConfig, mandrillService: MandrillService) extends StrictLogging {

  private val mailerConfig = config.getConfig("events-mailer")

  private val AttachmentType = "text/csv"
  private val AttachmentFilename = "events.csv"

  private implicit val ec: ExecutionContextExecutor = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(Try(mailerConfig.getInt("thread-pool-size")).toOption.getOrElse(1)))

  private val templatingEngine = new TemplatingEngine("events")

  private val DefaultEmailTemplate = new Template(
    from = mailerConfig.getString("from"),
    to = mailerConfig.getString("recipients").trim.split(",").map(_.trim).filter(_.nonEmpty).toList,
    subject = mailerConfig.getString("subject"),
    body = "")

  private val CompanyInfo = config.getConfig("templates-static-info").entrySet()
    .asScala.map(e => e.getKey -> e.getValue.unwrapped().toString).toMap

  val enabled: Boolean = {
    Try(mailerConfig.getBoolean("enabled")) match {
      case Success(true) if templatingEngine.isLoaded && DefaultEmailTemplate.to.nonEmpty =>
        logger.info("Events mailer - enabled")
        true
      case Success(true) if !templatingEngine.isLoaded =>
        logger.info("Events mailer - disabled. Template loading error.")
        false
      case Success(true) if DefaultEmailTemplate.to.isEmpty =>
        logger.info("Events mailer - disabled. Empty recipients list.")
        false
      case _ =>
        logger.info("Events mailer - disabled")
        false
    }
  }

  def send(events: Seq[EventMessage]): Future[Boolean] = {
    if (enabled) {
      templatingEngine.render(CompanyInfo + ("events_count" -> events.size.toString)) match {
        case Success(content) =>
          logger.debug(s"Sending events email <${DefaultEmailTemplate.subject}> to ${DefaultEmailTemplate.to.mkString(",")}")
          val message = DefaultEmailTemplate.copy(body = content.toString).toMandrillMessage
          val attachment = MandrillAttachment(AttachmentType, AttachmentFilename, CsvExporter.eventsToCsv(events))

          mandrillService.send(MandrillApiCall(
            Some(UUID.randomUUID().toString),
            message.copy(attachments = Seq(attachment)),
            key = mandrillConfig.token,
            endpoint = mandrillConfig.endPoint))
            .map(_ => true)

        case Failure(ex) =>
          logger.debug(s"Template rendering error", ex)
          Future.failed(ex)
      }
    } else {
      Future.successful(true)
    }
  }

}
