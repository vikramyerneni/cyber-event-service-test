package qomplx.event.alerting.email

import java.io.StringWriter

import com.typesafe.scalalogging.StrictLogging
import de.zalando.beard.ast.BeardTemplate
import de.zalando.beard.renderer.{ BeardTemplateRenderer, ClasspathTemplateLoader, CustomizableTemplateCompiler, StringWriterRenderResult, TemplateName }

import scala.util.{ Failure, Success, Try }

class TemplatingEngine(name: String) extends StrictLogging {

  private val templateName = TemplateName(name)

  private val loader = new ClasspathTemplateLoader(
    templatePrefix = "/email_templates/",
    templateSuffix = ".html")

  private val templateCompiler = new CustomizableTemplateCompiler(templateLoader = loader)
  private val renderer = new BeardTemplateRenderer(templateCompiler)

  private val template: Option[BeardTemplate] = templateCompiler.compile(templateName) match {
    case Success(t) =>
      logger.info(s"Template engine :: <$name> template is loaded.")
      Some(t)

    case Failure(exception) =>
      logger.error(s"Template engine :: <$name> template loading error.", exception)
      None
  }

  def isLoaded: Boolean = template.nonEmpty

  def render(ctx: Map[String, Any] = Map.empty): Try[StringWriter] = {
    template match {
      case Some(t) => Try(renderer.render(t, StringWriterRenderResult(), ctx))
      case None => Failure(new Exception(s"Template engine :: <$name> template is not loaded."))
    }

  }
}
