package qomplx.event.alerting.email

import java.io.StringWriter

import com.github.tototoshi.csv.CSVWriter
import org.joda.time.{ DateTime, DateTimeZone }
import qomplx.event.model.EventMessage
import qomplx.event.util.Utils

object CsvExporter {

  private val arrayDelimiter = "|"

  private val EventsHeader = Seq(
    "timestamp",
    "name",
    "description",
    "type",
    "severity",
    "categories",
    "tactics")

  private def severityToString(severity: Int) = {
    if (severity < 31) {
      "Low"
    } else if (severity < 61) {
      "Medium"
    } else "High"
  }

  def eventsToCsv(events: Seq[EventMessage]): String = {
    val content = new StringWriter()
    val csvWriter = CSVWriter.open(content)
    csvWriter.writeRow(EventsHeader)
    events.foreach { e =>
      csvWriter.writeRow(
        Seq(
          new DateTime(e.timestamp, DateTimeZone.UTC).toString(Utils.enUSDateTimeFormat),
          e.name,
          e.description,
          e.`type`,
          severityToString(e.severity),
          e.categories.mkString(arrayDelimiter),
          e.tactics.mkString(arrayDelimiter)))
    }
    csvWriter.flush()
    csvWriter.close()
    content.toString
  }
}
