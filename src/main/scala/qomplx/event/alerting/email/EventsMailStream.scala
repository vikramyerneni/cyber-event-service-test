package qomplx.event.alerting.email

import akka.actor.ActorSystem
import akka.kafka.ConsumerMessage.CommittableOffsetBatch
import akka.kafka.scaladsl.Consumer
import akka.stream.scaladsl.{ Flow, Keep, Sink, Source }
import akka.stream.{ ActorAttributes, ActorMaterializer, Supervision }
import akka.{ Done, NotUsed }
import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import kamon.Kamon
import qomplx.event.kafka.EventConsumer.CommittableEventMessage
import qomplx.event.kafka.{ ConsumerConfig, EventConsumer }
import qomplx.event.util.MetricsUtils

import scala.concurrent.duration._
import scala.concurrent.{ ExecutionContext, Future }
import scala.util.Try

class EventsMailStream(appConfig: Config, eventsMailer: EventsMailer)(implicit system: ActorSystem, ec: ExecutionContext, mat: ActorMaterializer) extends StrictLogging {

  private val kafkaConfig = appConfig.getConfig("cyber-event.kafka")
  private val consumerConfig = ConsumerConfig(kafkaConfig, "cyber-event.kafka.consumer.email-dispatcher", Some("_email_sender"))
  private val eventConsumerSource: Source[CommittableEventMessage, Consumer.Control] = EventConsumer().create(consumerConfig)

  private val timeWindowMinutes = Try(appConfig.getInt("events-mailer.pull-time-window-minutes")).toOption.map(_.minutes)
    .getOrElse(5.minutes)
  private val groupSize = Try(appConfig.getInt("events-mailer.events-group-size")).getOrElse(100)

  private val eventMailsSend = Kamon.counter("event_emails_sent").withoutTags()
  private val eventsSendTotal = Kamon.counter("email_events_sent").withoutTags()
  private val mailSendErrors = Kamon.counter("email_sent_errors").withoutTags()

  private val decider: Supervision.Decider = {
    t =>
      logger.warn("Resuming after an exception was thrown when processing events and sending emails.", t)
      Kamon.counter(MetricsUtils.KafkaEmailStreamError).withoutTags().increment()
      Supervision.Resume
  }

  def mailAlertFlow: Flow[Seq[CommittableEventMessage], Seq[CommittableEventMessage], NotUsed] =
    Flow[Seq[CommittableEventMessage]].mapAsync(1) { msg =>
      eventsMailer.send(msg.map(_.event)).map { _ =>
        eventMailsSend.increment()
        eventsSendTotal.increment(msg.size)
        msg
      }.recover {
        case ex =>
          logger.error(s"Something went wrong while sending events email.", ex)
          mailSendErrors.increment()
          msg
      }
    }

  def run(): (Consumer.Control, Future[Done]) = {
    if (eventsMailer.enabled) {
      logger.info(s"Starting events mail consumer stream. Group size $groupSize, time window: $timeWindowMinutes")

      eventConsumerSource
        .log("deserialize to event")
        .groupedWithin(groupSize, timeWindowMinutes)
        .via(mailAlertFlow)
        .log("send an email")
        .map { msg =>
          msg.map(_.committableOffset)
            .foldLeft(CommittableOffsetBatch.empty) { (batch, elem) => batch.updated(elem) }
        }
        .map(_.commitScaladsl())
        .withAttributes(ActorAttributes.supervisionStrategy(decider))
        .log("Kafka batch commit")
        .toMat(Sink.ignore)(Keep.both)
        .run()
    } else (Consumer.NoopControl, Future.successful(Done))
  }
}
