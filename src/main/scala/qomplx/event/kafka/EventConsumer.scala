package qomplx.event.kafka

import java.util.UUID

import akka.actor.ActorSystem
import akka.kafka.ConsumerMessage.CommittableOffset
import akka.kafka._
import akka.kafka.scaladsl._
import akka.stream.scaladsl.Source
import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import org.apache.kafka.clients.consumer.{ ConsumerConfig => KafkaConsumerConfig }
import org.apache.kafka.common.serialization.StringDeserializer
import qomplx.event.JsonSupport
import qomplx.event.model.{ Event, EventMessage }
import spray.json._

final case class ConsumerConfig(
  bootstrapServers: String,
  topicPattern: String,
  groupId: String,
  dispatcher: String, // Fully qualified config path which holds the dispatcher configuration
  autoOffsetResetConfig: String)

object ConsumerConfig {
  def apply(c: Config, dispatcherConfigPath: String, groupIdPostfix: Option[String] = None): ConsumerConfig = {
    val bootstrapServers = c.getString("bootstrap-server")
    val topicPattern = c.getString("topic-pattern")
    val groupId = c.getString("group-id") ++ groupIdPostfix.getOrElse("")
    val autoOffsetResetConfig = c.getString("auto-offset-reset")
    ConsumerConfig(bootstrapServers, topicPattern, groupId, dispatcherConfigPath, autoOffsetResetConfig)
  }
}

case class EventConsumer(implicit system: ActorSystem) extends JsonSupport with StrictLogging {
  import EventConsumer._
  val uniqueId = UUID.randomUUID()
  def create(config: ConsumerConfig): Source[CommittableEventMessage, Consumer.Control] = {
    val consumerSettings =
      ConsumerSettings(system, new StringDeserializer, new StringDeserializer)
        .withBootstrapServers(config.bootstrapServers)
        .withGroupId(config.groupId)
        .withProperty(KafkaConsumerConfig.AUTO_OFFSET_RESET_CONFIG, config.autoOffsetResetConfig)
        .withDispatcher(config.dispatcher)

    Consumer
      .committableSource(consumerSettings, Subscriptions.topicPattern(config.topicPattern))
      .map { consumerRecord =>
        val value = consumerRecord.record.value
        val event = value.parseJson.convertTo[EventMessage]
        CommittableEventMessage(event, consumerRecord.committableOffset)
      }
  }
}

object EventConsumer {
  case class CommittableEventMessage(event: EventMessage, committableOffset: CommittableOffset)
}
