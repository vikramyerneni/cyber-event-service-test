package qomplx.event.kafka.warmstorage

import akka.actor.{ Actor, ActorSystem, Props }
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.repository.EventRepository

import scala.concurrent.ExecutionContext

class EventPurger(repo: EventRepository, purgerConfig: Config)(implicit ec: ExecutionContext) extends Actor with StrictLogging {
  import EventPurger.Purge

  val olderThanDays = purgerConfig.getInt("older-than-days")

  override def receive: Receive = {
    case Purge =>
      logger.info(s"EventPurger actor received a Purge message.")

      val fut = repo.removeOlderThanDays(olderThanDays)

      fut.foreach { affectedRows =>
        // removes events older than 91 days
        logger.info(s"$affectedRows events (older than $olderThanDays days) have been removed.")
      }

      fut.failed.foreach(t => logger.error("An error was thrown when trying to purge old events.", t))
  }
}

object EventPurger {
  def schedule(eventRepository: EventRepository, purgerConfig: Config)(implicit system: ActorSystem, ec: ExecutionContext): Unit = {
    val purger = system.actorOf(props(eventRepository, purgerConfig), "eventPurger")
    QuartzSchedulerExtension(system)
      .schedule("event-purger", purger, Purge)
  }

  def props(eventRepository: EventRepository, purgerConfig: Config)(implicit ec: ExecutionContext) = Props(new EventPurger(eventRepository, purgerConfig))

  case object Purge
}