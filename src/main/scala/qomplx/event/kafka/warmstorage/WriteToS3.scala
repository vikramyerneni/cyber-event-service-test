package qomplx.event.kafka.warmstorage

import akka.NotUsed
import akka.actor.ActorSystem
import akka.kafka.ConsumerMessage.{ CommittableOffset, CommittableOffsetBatch }
import akka.stream.{ ActorAttributes, ActorMaterializer, Supervision }
import akka.stream.alpakka.s3.MultipartUploadResult
import akka.stream.scaladsl.{ Flow, Keep, Sink }
import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import kamon.Kamon
import qomplx.event.JsonSupport
import qomplx.event.kafka.EventConsumer.CommittableEventMessage
import qomplx.event.kafka.{ ConsumerConfig, EventConsumer }
import qomplx.event.service.S3BucketHandlerService
import qomplx.event.util.MetricsUtils
import spray.json._

import scala.concurrent.ExecutionContext
import scala.concurrent.duration.{ FiniteDuration, _ }

class WriteToS3(kafkaConfig: Config, s3BucketHandlerService: S3BucketHandlerService, s3KeyGenerator: S3KeyGenerator)(implicit system: ActorSystem, ec: ExecutionContext, mat: ActorMaterializer) extends JsonSupport with StrictLogging {
  import WriteToS3._

  private val consumerConfig = ConsumerConfig(kafkaConfig, "cyber-event.kafka.consumer.amazons3-dispatcher", Some("amazon-s3"))
  private val eventConsumer = EventConsumer()
  val eventConsumerSource = eventConsumer.create(consumerConfig)

  private val streamUtilConfig = kafkaConfig.getConfig("akka-stream-util")
  private val n: Int = streamUtilConfig.getInt("max-group")
  private val d: FiniteDuration = streamUtilConfig.getInt("duration").seconds

  private val decider: Supervision.Decider = {
    case t =>
      logger.warn("Resuming after an exception was thrown when processing events and storing them in S3.", t)
      Kamon.counter(MetricsUtils.KafkaToS3Error).withoutTags().increment()
      Supervision.Resume
  }

  def upload: Flow[Seq[CommittableEventMessage], S3UploadInfo, NotUsed] = {
    /**
     * Here, we receive batch of messages and process them as following:
     * We upload a file which contains the batch as JSON array and name it using
     * the timestamp of the first event and the last event, hyphen separated.
     */
    Flow[Seq[CommittableEventMessage]].mapAsync(1) { msg =>
      val events = msg.map(_.event)
      val key = s3KeyGenerator.getUniqueKey(eventConsumer.uniqueId)
      s3BucketHandlerService
        .upload(key, events.toJson.compactPrint)
        .map(S3UploadInfo(_, msg.map(_.committableOffset), events.size, key))
    }
  }

  def run() = {
    eventConsumerSource
      .log("deserialize to event")
      .groupedWithin(n, d)
      .via(upload)
      .log("write to amazon S3 bucket")
      .map {
        case msg @ S3UploadInfo(_, committableMsgs, size, _) if (size == committableMsgs.size) =>
          committableMsgs.foldLeft(CommittableOffsetBatch.empty) { (batch, elem) => batch.updated(elem) }
        case S3UploadInfo(uploaded, committableMsgs, _, _) =>
          logger.error(s"The number of events uploaded to the amazon S3 under file [${uploaded.key}] doesn't match the number of committable messages [${committableMsgs.size}]")
          Kamon.counter(MetricsUtils.KafkaToS3BatchSizeMismatch).withoutTags().increment()
          CommittableOffsetBatch.empty
      }
      .map(_.commitScaladsl())
      .withAttributes(ActorAttributes.supervisionStrategy(decider))
      .log("Kafka batch commit")
      .toMat(Sink.ignore)(Keep.both)
      .run()
  }
}

object WriteToS3 {
  case class S3UploadInfo(uploadResult: MultipartUploadResult, commitableOffsets: Seq[CommittableOffset], eventsCount: Int, s3Key: String)
}