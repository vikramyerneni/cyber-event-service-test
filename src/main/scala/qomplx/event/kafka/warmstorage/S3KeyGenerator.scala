package qomplx.event.kafka.warmstorage

import java.time.Clock
import java.util.UUID
import java.util.concurrent.atomic.AtomicReference

import org.joda.time.format.DateTimeFormat

/**
 * Encapsulates S3 key generator logic
 * key structure: <current-date>/<consumer-unique-id>/<auto-inc-number>.json
 */
class S3KeyGenerator(clock: Clock) { // takes `clock` to make testing easier
  import S3KeyGenerator._
  // `date` format "yyyy-MM-dd"
  private val dateFormat = DateTimeFormat.forPattern("yyyy-MM-dd")
  // helper method to generate current date string
  private def currentDate: String = dateFormat.print(clock.millis())

  // state initialized: current date string and counter value 0
  private val state = new AtomicReference[State](State(currentDate, 0))

  // public api to get s3 key using consumer unique id
  def getUniqueKey(consumerUniqueId: UUID): String = {
    val State(d, c) = state.updateAndGet { state => // update state logic
      val currDate = currentDate
      if (state.date == currDate) { // if stored date is same as current date
        State(state.date, state.counter + 1) // increment the counter
      } else {
        // else reset counter to 1 and set current date
        // we reset it to 1, because we are going to use it right away
        State(currDate, 1)
      }
    }
    "%s/%s/%09d.json".format(d, consumerUniqueId, c) // full key format
  }

  def getState: State = state.get()
}

object S3KeyGenerator {
  // key generator state:
  // `date` stores current date as string
  // `counter` stores auto increment number as long
  case class State(date: String, counter: Long)
}