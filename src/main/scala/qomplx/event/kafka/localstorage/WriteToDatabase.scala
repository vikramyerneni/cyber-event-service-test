package qomplx.event.kafka.localstorage

import akka.{ Done, NotUsed }
import akka.actor.ActorSystem
import akka.kafka.ConsumerMessage.{ CommittableOffset, CommittableOffsetBatch }
import akka.kafka.scaladsl.Consumer
import akka.stream.{ ActorAttributes, ActorMaterializer, Supervision }
import akka.stream.scaladsl.{ Flow, Keep, Sink, Source }
import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import kamon.Kamon
import qomplx.event.kafka.EventConsumer.CommittableEventMessage
import qomplx.event.kafka.{ ConsumerConfig, EventConsumer }
import qomplx.event.repository.EventRepository
import qomplx.event.util.MetricsUtils

import scala.concurrent.{ ExecutionContext, Future }
import scala.concurrent.duration._

class WriteToDatabase(repo: EventRepository, kafkaConfig: Config)(implicit system: ActorSystem, ec: ExecutionContext, mat: ActorMaterializer) extends StrictLogging {

  private val consumerConfig = ConsumerConfig(kafkaConfig, "cyber-event.kafka.consumer.postgresql-dispatcher", Some("postgresql"))
  val eventConsumerSource: Source[CommittableEventMessage, Consumer.Control] = EventConsumer().create(consumerConfig)

  private val decider: Supervision.Decider = {
    t =>
      logger.warn("Resuming after an exception was thrown when processing events and storing them in Postgres.", t)
      Kamon.counter(MetricsUtils.KafkaToPostgresError).withoutTags().increment()
      Supervision.Resume
  }

  def store: Flow[Seq[CommittableEventMessage], (Int, Seq[CommittableOffset]), NotUsed] =
    Flow[Seq[CommittableEventMessage]].mapAsync(1) { msg =>
      repo.add(msg.map(_.event)).map(numAdded => (numAdded, msg.map(_.committableOffset)))
    }

  def run(): (Consumer.Control, Future[Done]) = {
    eventConsumerSource
      .log("deserialize to event")
      .groupedWithin(100, 5.seconds)
      .via(store)
      .log("write to db")
      .map {
        case (numAddedToDb, committableMsgs) if (numAddedToDb == committableMsgs.size) =>
          committableMsgs.foldLeft(CommittableOffsetBatch.empty) { (batch, elem) => batch.updated(elem) }
        case (numAddedToDb, committableMsgs) =>
          logger.warn(s"The number of events added to the database [$numAddedToDb] doesn't match the number of committable messages [${committableMsgs.size}]")
          Kamon.counter(MetricsUtils.KafkaToPostgresBatchSizeMismatch).withoutTags().increment()
          CommittableOffsetBatch.empty
      }
      .map(_.commitScaladsl())
      .withAttributes(ActorAttributes.supervisionStrategy(decider))
      .log("Kafka batch commit")
      .toMat(Sink.ignore)(Keep.both)
      .run()
  }
}
