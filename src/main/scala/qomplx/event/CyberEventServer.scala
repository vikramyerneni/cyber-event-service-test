package qomplx.event

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.RouteResult._
import akka.http.scaladsl.{ ConnectionContext, Http }
import akka.stream.ActorMaterializer
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.config.db.{ DependencyFactory, LiquibaseUtils }
import qomplx.event.kafka.warmstorage.EventPurger
import kamon.Kamon
import qomplx.event.directives.CommonRouteDirectives
import qomplx.event.directives.CommonRouteDirectives.CORSConfig
import qomplx.event.migration.{ CountBySeverityPurger, Migrator }
import qomplx.event.thirdpartyintegrations.cronjob.Mailer

import scala.concurrent.duration.{ Duration, _ }
import scala.concurrent.{ Await, ExecutionContext }
import scala.util.{ Failure, Success }

object CyberEventServer extends JsonSupport with StrictLogging {
  Kamon.init()

  // TODO separate out the dispatchers/actor-systems for different sub-systems based on the need
  private val system = DependencyFactory.system
  private val materializer = DependencyFactory.materializer

  def main(args: Array[String]): Unit = {
    sys.addShutdownHook {
      logger.info(s"Shutting down the DB connection pool")
      val dbPoolShutdownF = DependencyFactory.postgresDb.shutdown
      Await.result(dbPoolShutdownF, 10.seconds)
    }

    val connectionContext = sslConnectionContext

    // internal routes get setup before the migrations are run so that we can fail/succeed the readiness check
    // only after the migrations finish. RTFM on health-checks/readiness-checks.
    bindInternalRoutes()
    // run migrations
    runDbMigrations()

    DependencyFactory.kafkaLocalStorageWriter.run()
    DependencyFactory.kafkaWarmStorageWriter.run()
    DependencyFactory.eventsEmailStream.run()

    EventPurger.schedule(DependencyFactory.eventRepository, DependencyFactory.appConfig.getConfig("postgresql-event-purger"))(system, system.dispatcher)
    Mailer.schedule(DependencyFactory.ticketingSystemMailRepository, DependencyFactory.mailerConfig, DependencyFactory.mandrillConfig, DependencyFactory.mandrillService)(system, system.dispatcher)
    Migrator.schedule(DependencyFactory.postgresDb, DependencyFactory.migratorConfig, DependencyFactory.authServiceClient)(system, system.dispatcher)
    CountBySeverityPurger.schedule(DependencyFactory.postgresDb)(system, system.dispatcher)

    // bind service routes
    bindInternalUIServiceRoutes()
    bindOldSIEPublicMRoutes(connectionContext)
    bindUIPublicRoutes(connectionContext)

    Await.result(system.whenTerminated, Duration.Inf)
  }

  // These are the routes that are not exposed publicly, but used via qweb-cyber. These server the qweb-cyber UI.
  private def bindInternalUIServiceRoutes(): Unit = {
    val routes = DependencyFactory.eventRoutesInternal.eventRoutes ~ DependencyFactory.incidentRoute.incidentRoutes ~
      DependencyFactory.tagsRoutesInternal.tagsRoutes
    bindRoutesToPort(routes, port = 8082, system, materializer, system.dispatcher, connectionContext = None)
  }

  // This is for the health-rotue as well as other things we might want to keep completely internal
  private def bindInternalRoutes(): Unit = {
    lazy val routes: Route = DependencyFactory.healthRoute.healthRoute
    bindRoutesToPort(routes, port = 8083, system, materializer, system.dispatcher, connectionContext = None)
  }

  // these are being used by one customer that may or may not rhyme with Bell
  private def bindOldSIEPublicMRoutes(connectionCtx: Option[ConnectionContext]) = {
    val routes = DependencyFactory.eventRoutesPublic.eventRoutes
    bindRoutesToPort(routes, port = 8084, system, materializer, system.dispatcher, connectionCtx)
  }

  /**
   * These will eventually replace the old SIEM routes completely, once that one customer stops using them.
   * Also includes the routes used by the UI. Currently, the routes used by the UI are separated into two groups. One
   * groups gets called via qweb-cyber backend. The other group, defined below, gets called directly from the UI.
   * There are two distinct sets of routes here:
   *   1. Routes used by third-party systems like qradar/splunk to get events data
   *   2. Routes used by the UI
   */
  private def bindUIPublicRoutes(connectionCtx: Option[ConnectionContext]): Unit = {
    import DependencyFactory.userAuthenticatorDirectives._
    val corsConf = CORSConfig(DependencyFactory.appConfig.getConfig("event-service.cors"))
    val crd = new CommonRouteDirectives(corsConf)
    val routes = {
      crd.handleAllExceptions(DependencyFactory.eventSIEMRoutes.routes) ~ //"api" / "v1" / "event" / "siem"
        crd.handleCORSAndExceptions {
          withAccess { currentUser =>
            DependencyFactory.thirdPartySystemConfigRoutes.routes(currentUser) ~ // "api" / "v1" / "observed-system" / Segment / "configs"
              DependencyFactory.incidentRoute.publicIncidentRoutes(currentUser) // "api" / "v1"  / "incidents" / "observed-system" / Segment
          }
        }
    }
    bindRoutesToPort(routes, port = 8085, system, materializer, system.dispatcher, connectionCtx)
  }

  private def bindRoutesToPort(routes: Route, port: Int, system: ActorSystem, materializer: ActorMaterializer, execCtx: ExecutionContext, connectionContext: Option[ConnectionContext]) = {
    implicit val (as, mat, ec) = (system, materializer, execCtx)
    val http = Http()
    val f = http.bindAndHandle(routes, "0.0.0.0", port, connectionContext = connectionContext.getOrElse(http.defaultServerHttpContext))
    f.onComplete {
      case Success(_) =>
        logger.info(s"Server online at http://0.0.0.0:$port/")
      case Failure(e) =>
        logger.error(s"Failed to bind the service on $port", e)
        system.terminate()
    }
  }

  private def runDbMigrations() = {
    LiquibaseUtils.runMigration(DependencyFactory.appConfig, DependencyFactory.postgresConnection)
    LiquibaseUtils.setMigrationFinished(true) // we use migration flag in readiness endpoint
  }

  private def sslConnectionContext(): Option[ConnectionContext] = {
    // We currently only have SSL certs in prod environments, and that's why this is optional
    sys.env.get("VAULT_SSL_CERT_PATH") match {
      case Some(certPath) if certPath.trim.nonEmpty =>
        logger.info("Found value for 'VAULT_SSL_CERT_PATH'. Enabling SSL.")
        val sslContext = DependencyFactory.vaultManager.getSSLContext(certPath).getOrElse {
          val msg = "Vault didn't return expected data to create an SSL configuration"
          logger.error(msg)
          sys.error(msg)
        }
        logger.info("Found SSL cert info. Binding with HTTPS")
        Some(ConnectionContext.https(sslContext))
      case _ =>
        logger.warn("No value specified for 'VAULT_SSL_CERT_PATH' env var. The server will not be configured to use SSL.")
        None
    }
  }
}
