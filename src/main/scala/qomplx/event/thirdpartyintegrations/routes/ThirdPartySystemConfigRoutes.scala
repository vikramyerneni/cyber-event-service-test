package qomplx.event.thirdpartyintegrations.routes

import java.util.UUID

import akka.http.scaladsl.model.{ HttpEntity, HttpResponse, StatusCodes }
import akka.http.scaladsl.server.Directives.{ pathPrefix, _ }
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.directives.RouteDirectives.complete
import com.fractal.auth.client.deprecated.{ User => AuthServiceUser }
import com.typesafe.scalalogging.StrictLogging
import org.postgresql.util.PSQLException
import qomplx.event.directives.CustomSecurityDirectives
import qomplx.event.thirdpartyintegrations.model.ThirdPartySystem
import qomplx.event.thirdpartyintegrations.repository.ThirdPartySystemConfigRepository
import qomplx.event.{ JsonSupport, UniqueViolationErrorCode }

import scala.util.{ Failure, Success }

// TODO check implicit params
// format: OFF
final class ThirdPartySystemConfigRoutes(repo: ThirdPartySystemConfigRepository, customSecurityDirectives: CustomSecurityDirectives) extends JsonSupport with StrictLogging {
  import customSecurityDirectives._
  def routes(user: AuthServiceUser): Route = {
    pathPrefix("api" / "v1" / "observed-system" / Segment / "configs") { observedSystemId =>
      validObservedSystem(observedSystemId) { _ =>
        (path(Segment / "token") & get) { configId =>
          onSuccess(repo.getToken(configId)) {
            case Some(token) =>
              complete(HttpResponse(entity = HttpEntity(token)))
            case None =>
              complete(HttpResponse(StatusCodes.NotFound, entity = "The requested resource was not found."))
          }
        } ~
        path(Segment) { configId =>
          concat(
            get {
              onComplete(repo.get(observedSystemId, configId)) {
                case Success(Some(config)) =>
                  logger.debug(s"Fetched a third party system integration configuration of observed system: $observedSystemId")
                  complete(config)
                case Success(None) =>
                  logger.debug(s"No config exist to fetch with id $configId under observed system $observedSystemId")
                  complete(StatusCodes.NotFound)
                case Failure(ex) =>
                  logger.error(s"Error while fetching a config with id $configId under observed system $observedSystemId", ex)
                  complete(StatusCodes.InternalServerError)
              }
            },
            put {
              entity(as[UpdateTSConfigPayload]) { payload =>
                onComplete(repo.update(observedSystemId, configId, payload)) {
                  case Success(1) =>
                    logger.debug(s"Updated a third party system integration configuration with id $configId of observed system: $observedSystemId")
                    complete(StatusCodes.OK)
                  case Success(0) =>
                    logger.debug(s"Could not find a third party system integration configuration with id $configId of observed system: $observedSystemId")
                    complete(StatusCodes.NotFound)
                  case Success(_) =>
                    logger.debug(s"Could not update a third party system integration configuration with id $configId of observed system: $observedSystemId")
                    complete(StatusCodes.InternalServerError)
                  case Failure(ex: PSQLException) if ex.getSQLState == UniqueViolationErrorCode =>
                    logger.debug(s"Error configuration name and third party system pair already exist under observed system: $observedSystemId", ex)
                    complete((StatusCodes.BadRequest, "configuration name and email system pair already exist"))
                  case Failure(ex) =>
                    logger.error(s"Error while updating a config with id $configId under observed system $observedSystemId", ex)
                    complete(StatusCodes.InternalServerError)
                }
              }
            },
            delete {
              onComplete(repo.remove(observedSystemId, configId)) {
                case Success(_) =>
                  logger.debug(s"Deleted a third party system integration configuration with id $configId of observed system: $observedSystemId")
                  complete(StatusCodes.OK)
                case Failure(ex) =>
                  logger.error(s"Error while deleting a config with id $configId under observed system $observedSystemId", ex)
                  complete(StatusCodes.InternalServerError)
              }
            })
        } ~
        concat(
          get {
            parameters("enabled".as[Boolean].?, "q".?).as(ListTSConfigsFilters) { filters =>
              onComplete(repo.list(observedSystemId, filters)) {
                case Success(results) =>
                  logger.debug(s"Fetched a list of third party system integration configuration of observed system: $observedSystemId")
                  complete(results)
                case Failure(ex) =>
                  logger.error(s"Error while listing configs under observed system $observedSystemId", ex)
                  complete(StatusCodes.InternalServerError)
              }
            }
          },
          post {
            entity(as[CreateTSConfigPayload]) { payload =>
              val token = UUID.randomUUID().toString

              onComplete(repo.create(observedSystemId, payload, user, token)) {
                case Success(Some(_)) =>
                  logger.debug(s"Added new third party system integration configuration under observed system: $observedSystemId")
                  complete(StatusCodes.Created)
                case Success(None) =>
                  logger.error(s"THIS SHOULD NEVER BE LOGGED! Some config has been updated under observed system: $observedSystemId")
                  complete(StatusCodes.InternalServerError)
                case Failure(ex: PSQLException) if ex.getSQLState == UniqueViolationErrorCode =>
                  logger.debug(s"Error configuration name and third party system pair already exist under observed system: $observedSystemId", ex)
                  complete((StatusCodes.BadRequest, "configuration name and email system pair already exist"))
                case Failure(ex) =>
                  logger.error(s"Error while adding new third party system integration configuration under observed system: $observedSystemId", ex)
                  complete(StatusCodes.InternalServerError)
              }
            }
          })
      }
    }
  }
}
// format: ON

private[thirdpartyintegrations] abstract class NonEmptyEmails(emails: Option[List[String]]) { require(emails.forall(_.nonEmpty)) }
final case class CreateTSConfigPayload(name: String, description: Option[String], thirdPartySystem: ThirdPartySystem.ThirdPartySystem, emails: Option[List[String]]) extends NonEmptyEmails(emails)
final case class UpdateTSConfigPayload(name: String, description: Option[String], thirdPartySystem: ThirdPartySystem.ThirdPartySystem, emails: Option[List[String]], enabled: Boolean) extends NonEmptyEmails(emails)
final case class TSConfigDTO(id: String, observedSystemId: String, thirdPartySystem: ThirdPartySystem.ThirdPartySystem, name: String, description: Option[String], emails: Option[List[String]], enabled: Boolean, recentlyEnabled: Boolean)
final case class ListTSConfigs(results: Seq[TSConfigDTO])

final case class ListTSConfigsFilters(enabled: Option[Boolean], q: Option[String])