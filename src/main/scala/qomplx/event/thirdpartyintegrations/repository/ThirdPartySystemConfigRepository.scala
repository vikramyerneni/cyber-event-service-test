package qomplx.event.thirdpartyintegrations.repository

import java.util.UUID

import com.fractal.auth.client.deprecated.{ User => AuthServiceUser }
import com.typesafe.scalalogging.StrictLogging
import qomplx.event._
import qomplx.event.model.Tables
import qomplx.event.thirdpartyintegrations.model.ThirdPartySystemConfig
import qomplx.event.thirdpartyintegrations.routes._
import qomplx.event.util.EncryptionUtils
import qomplx.event.util.SlickPostgresProfile.api._
import slick.jdbc.PostgresProfile.api.Database

import scala.concurrent.{ ExecutionContext, Future }

class ThirdPartySystemConfigRepository(db: Database, encryptionUtils: EncryptionUtils)(implicit ec: ExecutionContext) extends JsonSupport with StrictLogging {
  import ThirdPartySystemConfigRepository._

  def create(tagId: String, payload: CreateTSConfigPayload, user: AuthServiceUser, token: String): Future[Option[String]] = db.run {
    val encryptedToken = encryptionUtils.encryptToken(token)
    insertOrUpdate(ThirdPartySystemConfig.fromCreatePayload(tagId, payload, user, encryptedToken))
  }

  def get(observedSystemId: String, configId: String): Future[Option[TSConfigDTO]] = {
    db.run(fetch(observedSystemId, configId))
      .map(_.map(_.toTSConfigDto))
  }

  def update(observedSystemId: String, configId: String, payload: UpdateTSConfigPayload): Future[Int] = db.run {
    updateQuery(observedSystemId, configId)
      .update((payload.name, payload.description, payload.thirdPartySystem, payload.emails, payload.enabled, payload.enabled))
    // Sets recentlyEnabled to true if enabled is set to true
  }

  def remove(observedSystemId: String, configId: String): Future[Int] = {
    db.run(delete(observedSystemId, configId))
  }

  def list(observedSystemId: String, filters: ListTSConfigsFilters): Future[ListTSConfigs] = {
    db.run(searchConfigs(observedSystemId, filters.enabled, filters.q))
      .map(seq => ListTSConfigs(seq.map(_.toTSConfigDto)))
  }

  def getToken(configId: String): Future[Option[String]] = {
    val query =
      Tables.TicketingSystemConfigs
        .filter(_.id === configId)
        .map(_.encryptedToken)
        .result
        .headOption
        .map(_.map(encryptionUtils.decryptToken))

    db.run(query)
  }

  def getByToken(token: String): Future[Option[TSConfigDTO]] = {
    val encryptedToken = encryptionUtils.encryptToken(token)

    val query =
      Tables.TicketingSystemConfigs
        .filter(_.encryptedToken === encryptedToken)
        .result
    db.run(query).map(_.headOption.map(_.toTSConfigDto))
  }

  def setRecentlyEnabled(token: String, flag: Boolean): Future[Int] = {
    val encryptedToken = encryptionUtils.encryptToken(token)

    val query =
      Tables.TicketingSystemConfigs
        .filter(_.encryptedToken === encryptedToken)
        .map(_.recentlyEnabled)
        .update(flag)

    db.run(query)
  }

  def getObservedSystemIdByToken(token: String): Future[Option[String]] = {
    val encryptedToken = encryptionUtils.encryptToken(token)

    val query =
      Tables.TicketingSystemConfigs
        .filter(_.encryptedToken === encryptedToken)
        .map(_.tagId)
        .result
        .headOption

    db.run(query)
  }
}

object ThirdPartySystemConfigRepository {
  private def fetchQuery(observedSystemId: String, configId: String) = {
    Tables.TicketingSystemConfigs
      .filter(r => r.tagId === observedSystemId && r.id === configId)
  }

  private def fetch(observedSystemId: String, configId: String): DBIO[Option[ThirdPartySystemConfig]] = {
    fetchQuery(observedSystemId, configId).result.headOption
  }
  private def delete(observedSystemId: String, configId: String): DBIO[Int] = {
    fetchQuery(observedSystemId, configId).delete
  }
  private def insertOrUpdate(ticketingSystemConfig: ThirdPartySystemConfig): DBIO[Option[String]] = {
    (Tables.TicketingSystemConfigs returning Tables.TicketingSystemConfigs.map(_.id)).insertOrUpdate(ticketingSystemConfig)
  }
  private def fetchAllQuery(observedSystemId: String, enabled: Option[Boolean], q: Option[String]) = {
    Tables.TicketingSystemConfigs
      .filter(r => r.tagId === observedSystemId)
      .filterOpt(enabled) { case (r, flag) => r.enabled === flag }
      .filterOpt(q) { case (r, q) => (r.name.toLowerCase like s"%${q.toLowerCase}%") }
  }
  private def searchConfigs(observedSystemId: String, enabled: Option[Boolean], q: Option[String]): DBIO[Seq[ThirdPartySystemConfig]] = {
    fetchAllQuery(observedSystemId, enabled, q).result
  }
  def fetchAllActiveConfigs(observedSystemId: String) = {
    fetchAllQuery(observedSystemId, Some(true), None).result
  }
  private def updateQuery(observedSystemId: String, configId: String) = {
    fetchQuery(observedSystemId, configId)
      .map(r => (r.name, r.description, r.thirdPartySystem, r.emails, r.enabled, r.recentlyEnabled))
  }
}