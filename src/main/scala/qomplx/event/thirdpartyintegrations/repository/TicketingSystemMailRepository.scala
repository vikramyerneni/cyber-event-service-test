package qomplx.event.thirdpartyintegrations.repository

import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import qomplx.event._
import qomplx.event.model.Tables
import qomplx.event.service.email.MandrillResponse
import qomplx.event.thirdpartyintegrations.model.TicketingSystemMail
import qomplx.event.util.SlickPostgresProfile.api._
import slick.jdbc.PostgresProfile.api.Database

import scala.concurrent.duration._
import scala.concurrent.{ Await, ExecutionContext, Future }
import scala.util.Try

class TicketingSystemMailRepository(db: Database, mailerConfig: Config) extends JsonSupport with StrictLogging {
  import TicketingSystemMailRepository._

  private val batchSize = mailerConfig.getInt("send-mail-batch-size")
  private val timeout = mailerConfig.getInt("send-mail-timeout-second")

  /**
   * The heart of the mail sender instance.
   *
   * transaction begin:
   *
   * 1) select limited number of emails to send, and lock them for update.
   * 2) send emails, and keep the record of email id delivered successfully.
   * 3) set rows as delivered for the list of email ids from above step.
   *
   * transaction end:
   *
   * @param sendMail a function to send email
   * @param ex execution context
   * @return number of rows updated as delivered
   */
  def deliverMails(sendMail: TicketingSystemMail => Future[MandrillResponse])(implicit ex: ExecutionContext): Future[Int] = {
    val actions = for {
      rows <- selectForUpdate(batchSize)
      _ = logger.debug(s"Mailer selected ${rows.length} emails from pending emails.")
      succeeded <- DBIO.successful(trySendMail(rows, sendMail))
      _ = logger.debug(s"Mailer has sent ${succeeded.length} emails successfully.")
      affectedRows <- setDelivered(succeeded.flatten)
    } yield affectedRows
    db.run(actions.transactionally)
  }

  private def trySendMail(mails: Vector[TicketingSystemMail], sendMail: TicketingSystemMail => Future[MandrillResponse])(implicit ex: ExecutionContext): Vector[Option[String]] = mails.map { mail =>
    // because this method is going to be called from within a database transaction, we don't want to run it indefinity. We wait on result with given timeout.
    Try(Await.result(sendMail(mail), timeout seconds)).fold(
      { throwable =>
        logger.warn(s"Could not send an email {id: ${mail.id.get}} because ${throwable.getMessage}.", throwable)
        None
      },
      {
        case validResp if validResp.statusCode == 200 => mail.id
        case resp =>
          logger.warn(s"Could not send an email {id: ${mail.id.get}}, returned with status code [${resp.statusCode}].")
          None
      })
  }
}

object TicketingSystemMailRepository {
  def batchInsert(mails: Seq[TicketingSystemMail]) = Tables.TicketingSystemMails ++= mails
  // Please refer "Design Decision Notes" in TICKETING_SYSTEM_INTEGRATION_README.md file.
  private def selectForUpdate(limit: Int = 10) = {
    sql"""
      SELECT * FROM cyber_event_service.ns_ticketing_system_mails
      WHERE delivered = false
      LIMIT #${limit}
      FOR UPDATE SKIP LOCKED;
       """.as[TicketingSystemMail]
  }
  private def setDelivered(ids: Seq[String]) = {
    Tables.TicketingSystemMails
      .filter(_.id inSet ids)
      .map(_.delivered)
      .update(true)
  }
}