package qomplx.event.thirdpartyintegrations.model

import com.github.tminglei.slickpg.utils.PlainSQLUtils._
import qomplx.event
import qomplx.event.model.IncidentStatus.IncidentStatus
import qomplx.event.model.{ Incident, IncidentStatus }
import qomplx.event.thirdpartyintegrations.model.ThirdPartySystem.ThirdPartySystem
import qomplx.event.util.SlickPostgresProfile.plainApi._
import qomplx.event.util.Utils
import slick.jdbc.GetResult

final case class TicketingSystemMail(
  id: Option[String],
  configId: String,
  incId: String,
  incTitle: String,
  incStatus: IncidentStatus,
  incCategories: List[String] = Nil,
  incTactics: List[String] = Nil,
  incEventIds: List[String] = Nil,
  incCreatedOn: Long,
  delivered: Boolean = false,
  createdOn: Long = event.now,
  lastModifiedOn: Long = event.now,
  thirdPartySystem: ThirdPartySystem.ThirdPartySystem,
  emails: List[String],
  incSeverity: Int)

object TicketingSystemMail {
  def fromIncident(configs: Seq[ThirdPartySystemConfig], incident: Incident, eventIds: Seq[String]): Seq[TicketingSystemMail] = configs.map { config =>
    TicketingSystemMail(Some(Utils.generateId()), config.id.get, incident.id.get, incident.title, incident.status,
      incident.categories.getOrElse(Nil), incident.tactics.getOrElse(Nil), eventIds.toList, incident.createdOn, thirdPartySystem = config.thirdPartySystem,
      emails = config.emails.getOrElse(Nil), incSeverity = incident.severity)
  }

  implicit val getIncidentStatusResult: GetResult[IncidentStatus] = GetResult(r => IncidentStatus.withName(r.<<[String]))
  implicit val getThirdPartySystemResult: GetResult[ThirdPartySystem] = GetResult(r => ThirdPartySystem.withName(r.<<[String]))
  implicit val getStringListResult: GetResult[List[String]] = mkGetResult(_.nextArray[String]().toList)
  implicit val getTicketingSystemMailResult: GetResult[TicketingSystemMail] = GetResult(r =>
    TicketingSystemMail(r.<<, r.<<, r.<<, r.<<, r.<<[IncidentStatus], r.<<[List[String]], r.<<[List[String]], r.<<[List[String]], r.<<, r.<<, r.<<, r.<<, r.<<[ThirdPartySystem], r.<<[List[String]], r.<<))
}