package qomplx.event.thirdpartyintegrations.model

import java.util.UUID

import com.fractal.auth.client.deprecated.{ User => AuthServiceUser }
import qomplx.event.now
import qomplx.event.thirdpartyintegrations.routes.{ CreateTSConfigPayload, TSConfigDTO }
import qomplx.event.util.Utils

object ThirdPartySystem extends Enumeration {
  type ThirdPartySystem = Value
  val JIRA, SERVICENOW, REMEDY, ARCHER, SPLUNK, QRADAR = Value
}

final case class ThirdPartySystemConfig(
  id: Option[String],
  tagId: String,
  thirdPartySystem: ThirdPartySystem.ThirdPartySystem,
  name: String,
  description: Option[String],
  emails: Option[List[String]],
  encryptedToken: Array[Byte],
  enabled: Boolean = true,
  createdOn: Long = now,
  createdBy: String,
  lastModifiedOn: Long = now,
  lastModifiedBy: String,
  recentlyEnabled: Boolean = true) {

  def toTSConfigDto = TSConfigDTO(
    id.getOrElse(throw new RuntimeException("TicketingSystemConfig `id` is mandatory, but not found!")), tagId, thirdPartySystem, name, description, emails, enabled, recentlyEnabled)
}

object ThirdPartySystemConfig {
  def fromCreatePayload(tagId: String, payload: CreateTSConfigPayload, user: AuthServiceUser, encryptedToken: Array[Byte]): ThirdPartySystemConfig = {
    ThirdPartySystemConfig(
      Some(Utils.generateId()),
      tagId,
      payload.thirdPartySystem,
      payload.name,
      payload.description,
      payload.emails,
      encryptedToken,
      createdBy = user.uniqueId,
      lastModifiedBy = user.uniqueId,
      recentlyEnabled = true)
  }
}