package qomplx.event.thirdpartyintegrations.templates

import com.typesafe.config.Config
import qomplx.event.service.email.{ MandrillMessage, MandrillTo }
import qomplx.event.thirdpartyintegrations.model.TicketingSystemMail

case class Template(
  from: String,
  to: List[String],
  subject: String,
  body: String) {

  def toMandrillMessage = MandrillMessage(
    subject,
    from,
    to.map(e => MandrillTo(e)),
    html = Some(body))
}

object Template {
  def apply(ticketingSystemMail: TicketingSystemMail, mailerConfig: Config): Template = {
    Template(
      mailerConfig.getString("from"),
      ticketingSystemMail.emails,
      s"${numberToSeverityStatus(ticketingSystemMail.incSeverity)} : ${ticketingSystemMail.incTitle}, ${ticketingSystemMail.incId}",
      createTemplateBody(ticketingSystemMail))
  }

  // implemented as per https://rationemllc.atlassian.net/browse/CYBER-1985?focusedCommentId=82575&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-82575
  def numberToSeverityStatus(severity: Int): String = {
    if (severity < 31) "Low"
    else if (severity < 61) "Medium"
    else if (severity < 101) "High"
    else "Low"
  }

  // Template design as described in https://rationemllc.atlassian.net/browse/CYBER-1538
  // see also, https://rationemllc.atlassian.net/browse/CYBER-1985?focusedCommentId=83544&page=com.atlassian.jira.plugin.system.issuetabpanels%3Acomment-tabpanel#comment-83544
  private def createTemplateBody(ticketingSystemMail: TicketingSystemMail): String = {
    val sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    val date = new java.util.Date(ticketingSystemMail.incCreatedOn)
    s"""
       |<div>
       |<p>Timestamp: ${sdf.format(date)}</p>
       |<p>Title: ${ticketingSystemMail.incTitle}</p>
       |<p>Incident ID: ${ticketingSystemMail.incId}</p>
       |<p>Status: ${ticketingSystemMail.incStatus}</p>
       |<p>Tactics: ${ticketingSystemMail.incTactics.mkString(", ")}</p>
       |<p>Categories: ${ticketingSystemMail.incCategories.mkString(", ")}</p>
       |<p>Related event IDs: ${ticketingSystemMail.incEventIds.mkString(", ")}</p>
       |</div>
       |""".stripMargin
  }
}

