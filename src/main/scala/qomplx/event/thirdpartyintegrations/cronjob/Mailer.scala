package qomplx.event.thirdpartyintegrations.cronjob

import java.util.concurrent.Executors

import akka.actor.{ Actor, ActorSystem, Props }
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.service.email.{ MandrillApiCall, MandrillConfig, MandrillResponse, MandrillService }
import qomplx.event.thirdpartyintegrations.model.TicketingSystemMail
import qomplx.event.thirdpartyintegrations.repository.TicketingSystemMailRepository
import qomplx.event.thirdpartyintegrations.templates.Template

import scala.concurrent.{ ExecutionContext, Future }

class Mailer(repo: TicketingSystemMailRepository, mailerConfig: Config, mandrillConfig: MandrillConfig, mandrillService: MandrillService) extends Actor with StrictLogging {
  import Mailer.Send

  private implicit val ec = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(mailerConfig.getInt("thread-pool-size")))

  private def sendMail(ticketingSystemMail: TicketingSystemMail): Future[MandrillResponse] = {
    val message = Template(ticketingSystemMail, mailerConfig).toMandrillMessage
    mandrillService.send(MandrillApiCall(ticketingSystemMail.id, message, key = mandrillConfig.token, endpoint = mandrillConfig.endPoint))
  }

  override def receive: Receive = {
    case Send =>
      logger.info(s"Mailer actor received a Send message, it will try to send emails from a pending list.")
      repo.deliverMails(sendMail).recover {
        case th =>
          logger.error(s"Something went wrong while sending pending emails.", th)
      }
  }
}

object Mailer {
  def schedule(eventRepository: TicketingSystemMailRepository, mailerConfig: Config, mandrillConfig: MandrillConfig, mandrillService: MandrillService)(implicit system: ActorSystem, ec: ExecutionContext): Unit = {
    val mailer = system.actorOf(props(eventRepository, mailerConfig, mandrillConfig, mandrillService), "Mailer")
    QuartzSchedulerExtension(system)
      .schedule("ticketing-system-mailer", mailer, Send)
  }

  def props(ticketingSystemMailRepository: TicketingSystemMailRepository, mailerConfig: Config, mandrillConfig: MandrillConfig, mandrillService: MandrillService)(implicit ec: ExecutionContext) =
    Props(new Mailer(ticketingSystemMailRepository, mailerConfig, mandrillConfig, mandrillService))

  case object Send
}