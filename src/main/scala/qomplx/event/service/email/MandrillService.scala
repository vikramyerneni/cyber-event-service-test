package qomplx.event.service.email

import java.util.Base64

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.Materializer
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.JsonSupport

import scala.concurrent.{ ExecutionContext, Future }

final case class MandrillTo(
  email: String,
  name: Option[String] = None,
  `type`: String = "to")

object MandrillAttachment {
  def apply(`type`: String, name: String, content: String): MandrillAttachment = new MandrillAttachment(
    `type`,
    name,
    Base64.getEncoder.encodeToString(content.getBytes))
}

final case class MandrillAttachment(
  `type`: String,
  name: String,
  content: String)

final case class MandrillMessage(
  subject: String,
  from_email: String,
  to: List[MandrillTo],
  from_name: Option[String] = None,
  html: Option[String] = None,
  text: Option[String] = None,
  attachments: Seq[MandrillAttachment] = Seq.empty)

final case class MandrillResponse(
  message: MandrillMessage,
  statusCode: Int,
  statusText: String,
  responseBody: String)

final case class EmailResponse(email: String, status: String) {
  override def toString: String =
    s"""{"email": "$email", "status": "$status"}"""
}

final case class MandrillApiCall(
  mailId: Option[String], // TicketingSystemMail Id
  message: MandrillMessage,
  async: Boolean = false,
  key: String,
  endpoint: String) extends SprayJsonSupport {
  def uri() = Uri(endpoint + "/messages/send.json")
}

class MandrillService(system: ActorSystem)(implicit val ec: ExecutionContext, mat: Materializer) extends JsonSupport with StrictLogging {

  val http = Http(system)

  def send(apiCall: MandrillApiCall): Future[MandrillResponse] = {
    makeRequest(apiCall).flatMap((r: HttpResponse) => r match {
      case HttpResponse(status @ StatusCodes.OK, _, _, _) => {
        logger.debug(s"A message was successfully sent by Mandrill with code: ${status.intValue} to " +
          s"${apiCall.message.to.map(_.email).mkString(", ")}")
        Unmarshal(r).to[List[EmailResponse]].map { responses =>
          MandrillResponse(
            message = apiCall.message,
            statusCode = r.status.intValue(),
            statusText = r.status.defaultMessage(),
            responseBody = responses.map(_.toString).mkString("[", ",", "]"))
        }
      }
      case r =>
        val message = s"An error: status code [${r.status.intValue()}] has occurred during a sending."
        logger.error(message)
        r.entity.discardBytes()
        Future.failed(new RuntimeException(message))
    })
  }

  private def makeRequest(apiCall: MandrillApiCall): Future[HttpResponse] = {
    Marshal(apiCall).to[RequestEntity]
      .map(entity => HttpRequest(uri = apiCall.uri(), method = HttpMethods.POST, entity = entity))
      .flatMap(request => http.singleRequest(request))
  }
}

