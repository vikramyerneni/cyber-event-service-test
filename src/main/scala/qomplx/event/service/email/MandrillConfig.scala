package qomplx.event.service.email

import com.typesafe.config.Config
import qomplx.event.vault.VaultManager

final case class MandrillConfig(token: String, endPoint: String)

object MandrillConfig {
  def apply(mandrillConfig: Config, vaultManager: VaultManager): MandrillConfig = {
    val endpointHost = mandrillConfig.getString("host")
    val endpointVersion = mandrillConfig.getString("endpointVersion")
    MandrillConfig(
      vaultManager.mandrillToken,
      endpointHost + "/api/" + endpointVersion)
  }
}