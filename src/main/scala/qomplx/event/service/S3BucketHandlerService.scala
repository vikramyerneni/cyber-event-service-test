package qomplx.event.service

import akka.{ Done, NotUsed }
import akka.actor.ActorSystem
import akka.stream.Materializer
import akka.stream.alpakka.s3.scaladsl.S3
import akka.stream.alpakka.s3.{ MultipartUploadResult, ObjectMetadata, S3Attributes, S3Ext }
import akka.stream.scaladsl.{ Sink, Source }
import akka.util.ByteString
import com.amazonaws.auth.{ AWSStaticCredentialsProvider, BasicAWSCredentials }
import com.amazonaws.regions.Regions
import com.typesafe.config.Config
import qomplx.event.vault.VaultManager

import scala.concurrent.{ ExecutionContext, Future }

class S3BucketHandlerService(awsConfig: AWSConfig)(implicit ec: ExecutionContext, system: ActorSystem) {

  private val defaultEndpoint = awsConfig.endpoint
  private val defaultBucketName = awsConfig.bucketName
  private val regionName = awsConfig.regionName
  private val region = Regions.fromName(regionName)
  private val accessKey = awsConfig.accessKey
  private val secretKey = awsConfig.secretKey

  private val credentials = new BasicAWSCredentials(accessKey, secretKey)
  private val credsProvider = new AWSStaticCredentialsProvider(credentials)

  private val settings = {
    S3Ext(system)
      .settings
      .withCredentialsProvider(credsProvider)
      .withS3RegionProvider(() => region.getName)
      .withPathStyleAccess(true)
      .withEndpointUrl(defaultEndpoint)
  }

  def uploadSink(bucketKey: String): Sink[ByteString, Future[MultipartUploadResult]] = {
    S3.multipartUpload(defaultBucketName, bucketKey).withAttributes(S3Attributes.settings(settings))
  }

  // Note: helper method for unit test
  def createBucket(implicit mat: Materializer): Future[Done] = S3.makeBucket(defaultBucketName)(mat, S3Attributes.settings(settings))

  def upload(key: String, json: String)(implicit mat: Materializer): Future[MultipartUploadResult] = {
    val source = Source.single(ByteString(json))
    source.runWith(uploadSink(key))
  }

  def delete(key: String)(implicit mat: Materializer): Future[Done] = {
    S3.deleteObject(defaultBucketName, key).withAttributes(S3Attributes.settings(settings)).runWith(Sink.ignore)
  }

  def download(key: String)(implicit mat: Materializer): Future[Option[(Source[ByteString, NotUsed], ObjectMetadata)]] = {
    S3.download(defaultBucketName, key).withAttributes(S3Attributes.settings(settings)).runWith(Sink.head)
  }
}

final case class AWSConfig(endpoint: String, bucketName: String, regionName: String, accessKey: String, secretKey: String)

object AWSConfig {
  def apply(s3StorageConfig: Config, vaultManager: VaultManager): AWSConfig = {
    new AWSConfig(
      s3StorageConfig.getString("endpoint"),
      s3StorageConfig.getString("bucket.name"),
      s3StorageConfig.getString("region.name"),
      vaultManager.s3AccessKey,
      vaultManager.s3SecretKey)
  }
}