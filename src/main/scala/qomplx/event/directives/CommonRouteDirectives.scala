package qomplx.event.directives

import akka.http.scaladsl.model.headers.{ HttpOrigin, RawHeader }
import akka.http.scaladsl.model.{ HttpMethods, HttpResponse, StatusCodes }
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.RouteResult.{ Complete, Rejected }
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.directives.BasicDirectives.mapRouteResult
import ch.megard.akka.http.cors.scaladsl.CorsDirectives.{ cors, corsRejectionHandler }
import ch.megard.akka.http.cors.scaladsl.model.HttpOriginMatcher
import ch.megard.akka.http.cors.scaladsl.settings.CorsSettings
import com.typesafe.config.Config
import com.typesafe.config.ConfigException.WrongType
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.directives.CommonRouteDirectives.CORSConfig

import scala.util.Try

case object CommonRouteHandlers extends Rejection

class CommonRouteDirectives(corsConfig: CORSConfig) extends StrictLogging {

  import CommonRouteDirectives._

  def handleCORSAndExceptions: Directive[Unit] = {
    // If the server sends a response with an `Access-Control-Allow-Origin` value that is an explicit origin (rather than the "*" wildcard), then the response should also include a
    // `Vary` response header with the value `Origin` — to indicate to browsers that server responses can differ based on the value of the `Origin` request header.
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Access-Control-Allow-Origin
    val varyHeader = RawHeader("Vary", "Origin")

    // Explanation for each component, right to left:
    // 1-3. `handleAllExceptions` see method docs below
    // 4. `cors` handler works on both request and response. rejecting invalid CORS requests if so configured. On
    //    response side, it add the CORS headers. e.g. `Access Control Allow Origin`. This gets applied to all requests
    // 5. Add "Vary: Origin" header to the response (look up MDN docs on CORS)
    // 6: The cors rejection handler handles the rejections from (4) and converts them into a 400 Bad Request response.
    //    This response doesn't contain the CORS headers since it's a CORS error and we don't need them here.
    (handleRejections(corsRejectionHandler) & respondWithHeader(varyHeader) &
      cors(corsSettings) &
      handleAllExceptions)
  }

  def handleAllExceptions: Directive[Unit] = {
    // 1. `rejectEmptyResponse` converts "empty" responses into rejections (e.g. None, Future[None] or empty-strings
    // 2.  `handleRejections(emptyResponseRejectionHandler)` converts the above rejections into a 404 Not Found response
    val handleEmptyResponse = handleRejections(emptyResponseRejectionHandler) & rejectEmptyResponse
    // The order matters here. Be very careful with changing it.
    // Following up from the above explanation of `handleEmptyResponse` (again, right to left)
    // 3. `handleExceptions` converts failed futures or exceptions into 500 Internal Server Errors
    (handleExceptions(exceptionHandler) &
      handleEmptyResponse)
  }

  /**
   * Rejects the response with a known response value when the response contains an "empty" value. Ignore the "204 No Content" responses
   * because they are supposed to be empty. This will handle Future[None], None, and empty-strings.
   */
  private def rejectEmptyResponse: Directive0 = {
    mapRouteResult {
      case Complete(response) if response.status != StatusCodes.NoContent && response.entity.isKnownEmpty => Rejected(CommonRouteHandlers :: List[Rejection]())
      case x => x
    }
  }

  /**
   * Handles the rejection from `rejectEmptyResponse` method and converts it into a '404 Not Found' response.
   */
  private def emptyResponseRejectionHandler = RejectionHandler.newBuilder.handleAll[CommonRouteHandlers.type] { r =>
    complete(HttpResponse(StatusCodes.NotFound, entity = NotFoundResponseText))
  }.result()

  private def exceptionHandler: ExceptionHandler = {
    ExceptionHandler {
      case e: Exception =>
        extractUri { uri =>
          val resp = HttpResponse(entity = InternalServerErrorResponseText, status = StatusCodes.InternalServerError)
          logger.error(s"Request to $uri resulted in an exception. Completing the request with $resp", e)
          complete(resp)
        }
    }
  }

  private lazy val corsSettings = {
    import HttpMethods._
    CorsSettings.defaultSettings
      .withAllowedOrigins(corsConfig.allowedHostNames)
      .withAllowGenericHttpRequests(corsConfig.allowGenericHTTPRequests)
      .withAllowedMethods(HEAD :: OPTIONS :: GET :: POST :: DELETE :: PUT :: Nil)
  }
}

object CommonRouteDirectives {

  val NotFoundResponseText = "The requested resource was not found, or you don't have permissions to access it"
  val InternalServerErrorResponseText = "Something unexpected just happened. Please try again later."

  final case class CORSConfig(allowedHostNames: HttpOriginMatcher, allowGenericHTTPRequests: Boolean = false)

  object CORSConfig {
    def apply(corsConf: Config): CORSConfig = {
      CORSConfig(parseAllowedOrigins(corsConf), corsConf.getBoolean("allow-generic-http-requests"))
    }

    private[directives] def parseAllowedOrigins(corsConf: Config) = {
      parseConfigStringList(corsConf, "allowed-origins") match {
        case List("*") => HttpOriginMatcher.*
        case origins => HttpOriginMatcher(origins.map(HttpOrigin(_)): _*)
      }
    }

    private def parseConfigStringList(config: Config, path: String): List[String] = {
      import scala.collection.JavaConverters._
      Try(config.getStringList(path).asScala.toList).recover {
        case _: WrongType => config.getString(path).trim.split(",").map(_.trim).toList
      }.get
    }
  }

}
