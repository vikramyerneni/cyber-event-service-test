package qomplx.event.directives

import akka.http.scaladsl.model.headers.HttpChallenge
import akka.http.scaladsl.server.AuthenticationFailedRejection.{ CredentialsMissing, CredentialsRejected }
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.directives.BasicDirectives.{ extract, provide }
import akka.http.scaladsl.server.directives.FutureDirectives.onSuccess
import akka.http.scaladsl.server.directives.RouteDirectives.reject
import com.typesafe.scalalogging.StrictLogging
import org.slf4j.MDC
import qomplx.event.directives.CustomSecurityDirectives._
import qomplx.event.directives.SecurityDirectivesForSIEMRoutes.ObsSysIdAndToken
import qomplx.event.thirdpartyintegrations.repository.ThirdPartySystemConfigRepository

import scala.concurrent.{ ExecutionContext, Future }

/**
 * Security Directives specifically for third-party integration polling endpoints.
 * These authenticate the requests based on the API Token generated for each Third Party Integration Config created by the user.
 *
 * @param thirdPartySystemConfigRepo
 * @param ec
 */
final class SecurityDirectivesForSIEMRoutes(thirdPartySystemConfigRepo: ThirdPartySystemConfigRepository)(implicit ec: ExecutionContext) extends StrictLogging {

  // "Authenticates" by looking for a token as the value of the X-API-KEY header. This will be used in our publicly exposed endpoints for third party integrations.
  def authenticateWithTokenApiKey: Directive1[ObsSysIdAndToken] = {
    logger.info(s"Authenticating by checking the $XApiKeyHeaderName header for the config token")
    withObservedSystemForThirdParty
  }

  private def withObservedSystemForThirdParty: Directive1[ObsSysIdAndToken] = authDirectiveForThirdParty.flatMap {
    case Right(observedSystemId) => provide(observedSystemId)
    case Left(rejection) => reject(rejection)
  }

  private val authDirectiveForThirdParty = extract(extractObservedSystemIdByConfigToken).flatMap(onSuccess(_))

  private def extractObservedSystemIdByConfigToken(context: RequestContext): Future[Either[Rejection, ObsSysIdAndToken]] = {
    context.request.headers.find(_.name.equalsIgnoreCase(XApiKeyHeaderName)).map(_.value) match {
      case None =>
        Future.successful(Left(AuthenticationFailedRejection(CredentialsMissing, HttpChallenge.apply("QOMPLX", Some("Credentials are missing")))))
      case Some(token) =>
        thirdPartySystemConfigRepo.getObservedSystemIdByToken(token) map {
          case Some(observedSystemId) =>
            MDC.put("ObsSysId", s"$observedSystemId")
            Right(ObsSysIdAndToken(observedSystemId, token))
          case None =>
            Left(AuthenticationFailedRejection(CredentialsRejected, HttpChallenge.apply("QOMPLX", Some("Credentials are rejected"))))
        }
    }
  }
}

object SecurityDirectivesForSIEMRoutes {
  val XApiKeyHeaderName = "X-API-KEY"

  final case class ObsSysIdAndToken(obsSystemId: String, token: String)
}
