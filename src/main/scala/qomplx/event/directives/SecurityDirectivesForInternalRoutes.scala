package qomplx.event.directives

import akka.http.scaladsl.model.HttpHeader
import akka.http.scaladsl.model.headers.HttpChallenge
import akka.http.scaladsl.server.AuthenticationFailedRejection.CredentialsMissing
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.BasicDirectives.provide
import akka.http.scaladsl.server.directives.HeaderDirectives.headerValue
import akka.http.scaladsl.server.directives.RouteDirectives.reject
import akka.http.scaladsl.server.{ Directive1, _ }
import com.fractal.auth.client.deprecated.{ Client, User }
import com.typesafe.scalalogging.StrictLogging
import org.slf4j.MDC
import qomplx.event.directives.SecurityDirectivesForInternalRoutes._

import scala.concurrent.ExecutionContext
import scala.util.{ Failure, Success }

/**
 * Directives which expect User-Id and Account-Id in header for each requests. These will be removed when we take care of the tech-debt to remove routes from fweb-cyber.
 * These don't provide any kind of "security", we trust VPN.
 * @param ec
 */
final class SecurityDirectivesForInternalRoutes(authServiceClient: Client)(implicit ec: ExecutionContext) extends StrictLogging {

  // "Authenticates" by looking for the customer-account-id and user-id headers. This will be used by internal services like fw-cyber
  def authenticateWithUserId: Directive1[UserDetail] = {
    logger.debug(s"Authenticating by checking $UserIdHeaderName and $AccountIdHeaderName headers")
    for {
      userId <- getHeaderValue(UserIdHeaderName)
      customerAccountId <- getHeaderValue(AccountIdHeaderName)
      user <- getUser(userId)
    } yield {
      logger.debug(s"Found header-values { $UserIdHeaderName: $userId, $AccountIdHeaderName: $customerAccountId }")
      MDC.put("AccountId", s"${user.defaultAccountId}")
      MDC.put("UserId", s"${user.id}")
      UserDetail(user.id, user.defaultAccountId, user.uniqueId)
    }
  }

  private def getUser(userId: Long): Directive1[User] = {
    onComplete(authServiceClient.users.getUserById(userId)).flatMap {
      case Success(Some(userDetail)) => provide(userDetail)
      case Success(None) =>
        reject(AuthenticationFailedRejection(CredentialsMissing, HttpChallenge.apply("qomplx", Some("Credentials are invalid"))))
      case Failure(t) =>
        logger.error("An error occurred while trying to authenticate user", t)
        throw t
    }
  }

  private def getHeaderValue(headerName: String): Directive1[Long] = {
    headerValue[Long](extractValue(headerName.toLowerCase)) | reject(MissingHeaderRejection(headerName))
  }

  private def extractValue(lowerCaseName: String): HttpHeader => Option[Long] = {
    case HttpHeader(`lowerCaseName`, value) => Some(value.toLong)
    case _ => None
  }
}

final case class UserDetail(userId: Long, customerAccountId: Long, userUniqueId: String)

object SecurityDirectivesForInternalRoutes {
  val UserIdHeaderName = "X-QOMPLX-UserId"
  val AccountIdHeaderName = "X-QOMPLX-CustomerAccountId"
}
