package qomplx.event.directives

import akka.http.scaladsl.model.headers.HttpChallenge
import akka.http.scaladsl.server.AuthenticationFailedRejection.{ CredentialsMissing, CredentialsRejected }
import akka.http.scaladsl.server._
import akka.http.scaladsl.server.directives.BasicDirectives.{ extract, provide }
import akka.http.scaladsl.server.directives.FutureDirectives.onSuccess
import akka.http.scaladsl.server.directives.RouteDirectives.reject
import com.typesafe.scalalogging.StrictLogging
import org.slf4j.MDC
import qomplx.event.directives.CustomSecurityDirectives._
import qomplx.event.repository.{ ObservedSystem, ObservedSystemRepository }

import scala.concurrent.{ ExecutionContext, Future }

/**
 * These directives are for authenticating using the Obs-system API Key.
 * We only have 1 route which uses this and it will be deprecated when we release third-party SPLUNK integration.
 *
 * @param obsSysRepo
 * @param ec
 */
final class CustomSecurityDirectives(obsSysRepo: ObservedSystemRepository)(implicit ec: ExecutionContext) extends StrictLogging {

  // "Authenticates" by looking for the X-API-KEY header. This will be used by external users (for publicly exposed end-points).
  def authenticateWithObsSystemAPIKey: Directive1[ObservedSystem] = {
    logger.info(s"Authenticating by checking the $XApiKeyHeaderName")
    withObservedSystem
  }

  private def withObservedSystem: Directive1[ObservedSystem] = authDirective.flatMap {
    case Right(observedSystem) => provide(observedSystem)
    case Left(rejection) => reject(rejection)
  }

  private val authDirective = extract(extractObsSystem).flatMap(onSuccess(_))

  private def extractObsSystem(context: RequestContext): Future[Either[Rejection, ObservedSystem]] = {
    context.request.headers.find(_.name.equalsIgnoreCase(XApiKeyHeaderName)).map(_.value) match {
      case None =>
        Future.successful(Left(AuthenticationFailedRejection(CredentialsMissing, HttpChallenge.apply("QOMPLX", Some("Credentials are missing")))))
      case Some(key) =>
        obsSysRepo.findByAPIKey(key)(ec) map {
          case Some(obsSys) => {
            MDC.put("ObsSysId", s"${obsSys.id.stringify}")
            Right(obsSys)
          }
          case None => Left(AuthenticationFailedRejection(CredentialsRejected, HttpChallenge.apply("QOMPLX", Some("Credentials are rejected"))))
        }
    }
  }

  def validObservedSystem(obsId: String): Directive1[ObservedSystem] = onSuccess(checkObservedSystemExist(obsId)).flatMap {
    case Right(v) => provide(v)
    case Left(rejection) => reject(rejection)
  }

  private def checkObservedSystemExist(obsId: String): Future[Either[Rejection, ObservedSystem]] = {
    obsSysRepo.findById(obsId)(ec) map {
      case Some(obsSys) => Right(obsSys)
      case None => Left(ValidationRejection(s"Observed system [$obsId] does not exist.", None))
    }
  }
}

object CustomSecurityDirectives {
  val XApiKeyHeaderName = "X-API-KEY"
}
