package qomplx.event.directives

import akka.http.scaladsl.model.headers.HttpChallenge
import akka.http.scaladsl.server.AuthenticationFailedRejection.{ CredentialsMissing, CredentialsRejected }
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.directives.BasicDirectives.provide
import akka.http.scaladsl.server.directives.RouteDirectives.reject
import akka.http.scaladsl.server.{ AuthenticationFailedRejection, AuthorizationFailedRejection, Directive1 }
import com.fractal.auth.client.deprecated.{ Client => AuthServiceClient, User => AuthServiceUser }
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.directives.UserAuthenticatorDirectives._

import scala.concurrent.ExecutionContext
import scala.util.{ Failure, Success }

/**
 * These directives authenticate a Q:OS SSO token with the auth-service for authenticating a request. We also expect obs-system id in params for all the routes using this.
 * For future, we want to just rely on the SSO token auth and remove obs-sys checks.
 *
 * @param authServiceClient
 * @param ec
 */
class UserAuthenticatorDirectives(authServiceClient: AuthServiceClient)(implicit ec: ExecutionContext) extends StrictLogging {

  // TODO make sure this results in an appropriate response codes
  def withAccess: Directive1[AuthServiceUser] = {
    headerValueByName(authHeaderName).flatMap { headerValue =>
      logger.debug("Auth Header contains the Token. Trying to authenticate..")
      onComplete(authServiceClient.authentication.getUserForSSOToken(headerValue)).flatMap {
        case Success(Some(u)) if featureCheck(u) =>
          logger.debug(s"User ${u.email} has been successfully authenticated.")
          provide(u)
        case Success(Some(u)) =>
          logger.debug(s"User ${u.email} didn't have feature access.")
          reject(AuthenticationFailedRejection(CredentialsRejected, HttpChallenge.apply("qomplx", Some("User didn't have feature access"))))
        case Success(None) =>
          reject(AuthenticationFailedRejection(CredentialsMissing, HttpChallenge.apply("qomplx", Some("Credentials are missing or invalid"))))
        case Failure(t) =>
          logger.error("An error occurred while trying to authenticate user", t)
          throw t
      }
    }
  }

  private def featureCheck(user: AuthServiceUser): Boolean =
    user.features.exists { accountFeatures =>
      user.defaultAccountUniqueId.fold(false)(_ == accountFeatures.customerAccountUniqueId) &&
        accountFeatures.features.contains("workflows.ACDP")
    }

  def withAdminAccess(user: AuthServiceUser): Directive1[Unit] = {
    // TODO migrate this to using user.isAdmin when the UI for it is ready
    // TODO make sure this results in an appropriate response codes
    val isAdmin = user.isAdmin || user.roles.flatMap(_.roles).contains("Account Administrator")
    if (isAdmin) {
      logger.debug(s"User ${user.email} is an administrator.")
      provide()
    } else reject(AuthorizationFailedRejection)
  }
}

object UserAuthenticatorDirectives {
  private val authHeaderName: String = "X-QOS-AUTH-TOKEN"
}