-- CYBER-3326
-- re-calculate events count by their severity

BEGIN;

TRUNCATE cyber_event_service.amp_events_count_by_severity;

CREATE TEMP TABLE amp_events_count_by_severity_temp (
  datetime timestamp,
  tag_id text,
  total int,
  low int,
  medium int,
  high int
);


INSERT INTO amp_events_count_by_severity_temp (datetime, tag_id, total, low, medium, high)
SELECT date_trunc('minute', to_timestamp(event.timestamp / 1000))::timestamp,
CASE
    WHEN event.observed_system_id IS NULL AND tag.tag_id IS NULL THEN 'UNKNOWN'
    WHEN event.observed_system_id IS NULL THEN tag.tag_id
    ELSE event.observed_system_id
END,
CASE WHEN event.severity BETWEEN 0 AND 100 THEN 1 ELSE 0 END,
CASE WHEN event.severity BETWEEN 0 AND 30 THEN 1 ELSE 0 END,
CASE WHEN event.severity BETWEEN 31 AND 60 THEN 1 ELSE 0 END,
CASE WHEN event.severity BETWEEN 61 AND 100 THEN 1 ELSE 0 END
FROM cyber_event_service.amp_events event
LEFT JOIN cyber_event_service.amp_tags tag ON (tag.property_id = event.agent_id OR tag.property_id = event.rule_id)
WHERE event.timestamp >= (EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - INTERVAL '1 day')) * 1000)::bigint;

INSERT INTO cyber_event_service.amp_events_count_by_severity (datetime, tag_id, total, low, medium, high)
SELECT datetime, tag_id, sum(total), sum(low), sum(medium), sum(high)
FROM amp_events_count_by_severity_temp WHERE tag_id is not null GROUP BY (datetime, tag_id);

DROP TABLE amp_events_count_by_severity_temp;

COMMIT;

