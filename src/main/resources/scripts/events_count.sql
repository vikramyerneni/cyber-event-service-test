-- CYBER-3326
-- re-calculate events count

BEGIN;

TRUNCATE cyber_event_service.amp_events_count;

CREATE TEMP TABLE amp_events_count_temp (
  date DATE,
  tag_id TEXT
);

INSERT INTO amp_events_count_temp (date, tag_id)
SELECT to_timestamp(timestamp / 1000)::date,
CASE
    WHEN event.observed_system_id IS NULL AND tag.tag_id IS NULL THEN 'UNKNOWN'
    WHEN event.observed_system_id IS NULL THEN tag.tag_id
    ELSE event.observed_system_id
END
FROM cyber_event_service.amp_events event
LEFT JOIN cyber_event_service.amp_tags tag ON (tag.property_id = event.agent_id OR tag.property_id = event.rule_id);

INSERT INTO cyber_event_service.amp_events_count (date, observed_system_id, count, tag_id)
select date, null, count(1), tag_id
from amp_events_count_temp group by (date, tag_id)
ON CONFLICT (date, observed_system_id, tag_id)
DO UPDATE SET count = excluded.count + 1;

DROP TABLE amp_events_count_temp;

COMMIT;

