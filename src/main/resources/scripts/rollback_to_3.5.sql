-- CYBER-3363
-- rollback script to go from 3.6 to 3.5 version

BEGIN;

UPDATE cyber_event_service.amp_events event
SET observed_system_id = tag.tag_id
FROM cyber_event_service.amp_tags tag
WHERE (tag.property_id = event.agent_id OR tag.property_id = event.rule_id) AND event.observed_system_id IS NULL;

UPDATE cyber_event_service.amp_events event
SET observed_system_id = 'UNKNOWN'
WHERE event.observed_system_id IS NULL;

DROP TRIGGER amp_events_count_by_severity ON cyber_event_service.amp_events;
DROP FUNCTION update_events_count_by_severity;
DROP TABLE cyber_event_service.amp_events_count_by_severity;
DELETE FROM public.databasechangelog WHERE id = '22';

CREATE OR REPLACE FUNCTION update_events_count()
RETURNS TRIGGER AS
'
   DECLARE
   BEGIN
   IF TG_OP = ''INSERT'' THEN
      EXECUTE ''INSERT INTO cyber_event_service.amp_events_count (date, observed_system_id, count)
      VALUES (to_timestamp($1.timestamp / 1000)::date, $1.observed_system_id, 1)
      ON CONFLICT (date, observed_system_id)
      DO UPDATE SET count = cyber_event_service.amp_events_count.count + 1'' USING NEW;
      RETURN NEW;
   END IF;
   END;
'
LANGUAGE 'plpgsql';
ALTER TABLE cyber_event_service.amp_events_count DROP CONSTRAINT amp_events_count_unique;
UPDATE cyber_event_service.amp_events_count SET observed_system_id = tag_id WHERE observed_system_id is NULL;
ALTER TABLE cyber_event_service.amp_events_count ALTER COLUMN observed_system_id SET NOT NULL;

CREATE TEMP TABLE amp_events_count_temp AS TABLE cyber_event_service.amp_events_count;
TRUNCATE cyber_event_service.amp_events_count;
INSERT INTO cyber_event_service.amp_events_count (date, observed_system_id, count, tag_id)
SELECT date, observed_system_id, sum(count), null
FROM amp_events_count_temp GROUP BY (date, observed_system_id);

ALTER TABLE cyber_event_service.amp_events_count ADD CONSTRAINT amp_events_count_pkey PRIMARY KEY (date, observed_system_id);
ALTER TABLE cyber_event_service.amp_events_count DROP COLUMN tag_id;
ALTER TABLE cyber_event_service.ns_third_party_integration_configs RENAME COLUMN tag_id TO observed_system_id;
ALTER TABLE cyber_event_service.amp_incidents RENAME COLUMN tag_id TO observed_system_id;
DELETE FROM public.databasechangelog WHERE id = '21';

ALTER TABLE cyber_event_service.amp_events DROP COLUMN customer_account_id;
ALTER TABLE cyber_event_service.amp_events ALTER COLUMN observed_system_id SET NOT NULL;
ALTER TABLE cyber_event_service.amp_events DROP COLUMN rule_id;
ALTER TABLE cyber_event_service.amp_events DROP COLUMN agent_id;
DROP TABLE cyber_event_service.amp_tags;
DROP TYPE amp_property_type;
DELETE FROM public.databasechangelog WHERE id = '20';

DROP TABLE amp_events_count_temp;

DELETE FROM public.databasechangelog where id = '20';
DELETE FROM public.databasechangelog where id = '21';
DELETE FROM public.databasechangelog where id = '22';

COMMIT;
