--liquibase formatted sql

--changeset fractal_internal_user:1 logicalFilePath:14_replace_api_key_with_token.sql

ALTER TABLE cyber_event_service.ns_third_party_integration_configs DROP COLUMN api_key;
ALTER TABLE cyber_event_service.ns_third_party_integration_configs ADD COLUMN encrypted_token bytea NOT NULL;

CREATE UNIQUE INDEX IF NOT EXISTS token_unique
    ON cyber_event_service.ns_third_party_integration_configs USING btree (encrypted_token);

--rollback DROP INDEX IF EXISTS token_unique;

--rollback ALTER TABLE cyber_event_service.ns_third_party_integration_configs DROP COLUMN encrypted_token;

--rollback ALTER TABLE cyber_event_service.ns_third_party_integration_configs ADD COLUMN api_key text;
