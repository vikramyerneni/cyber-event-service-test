--liquibase formatted sql

--changeset fractal_internal_user:1 logicalFilePath:10_amp_events_tokens_simple

ALTER TABLE cyber_event_service.amp_events ADD COLUMN tokens_simple tsvector;

UPDATE cyber_event_service.amp_events SET tokens_simple = (to_tsvector('simple', event_id) || to_tsvector('simple', name) || to_tsvector('simple', description));

CREATE TRIGGER tsvector_amp_events_tokens_simple_update BEFORE INSERT OR UPDATE
ON cyber_event_service.amp_events FOR EACH ROW EXECUTE PROCEDURE
tsvector_update_trigger(
	tokens_simple, 'pg_catalog.simple', event_id, name, description
);

CREATE INDEX amp_events_textsearch_simple_idx ON cyber_event_service.amp_events USING GIN (tokens_simple);

DROP INDEX cyber_event_service.amp_events_textsearch_idx;

DROP TRIGGER tsvector_amp_events_tokens_update ON cyber_event_service.amp_events;

ALTER TABLE cyber_event_service.amp_events DROP COLUMN tokens;

--rollback DROP INDEX cyber_event_service.amp_events_textsearch_simple_idx;

--rollback DROP TRIGGER tsvector_amp_events_tokens_simple_update ON cyber_event_service.amp_events;

--rollback ALTER TABLE cyber_event_service.amp_events DROP COLUMN tokens_simple;