--liquibase formatted sql

--changeset fractal_internal_user:19 logicalFilePath:19_add_user_ksuids_columns

--comment remove migration entry 'delete from public.databasechangelog where id = '19';'

ALTER TABLE cyber_event_service.amp_incidents ADD COLUMN reporter_id_unique text;
ALTER TABLE cyber_event_service.amp_incidents ADD COLUMN created_by_unique text;
ALTER TABLE cyber_event_service.amp_incidents ADD COLUMN last_modified_by_unique text;

--rollback ALTER TABLE cyber_event_service.amp_incidents DROP COLUMN reporter_id_unique;
--rollback ALTER TABLE cyber_event_service.amp_incidents DROP COLUMN created_by_unique;
--rollback ALTER TABLE cyber_event_service.amp_incidents DROP COLUMN last_modified_by_unique;
