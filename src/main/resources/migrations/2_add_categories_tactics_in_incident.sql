--liquibase formatted sql

--changeset fractal_internal_user:1 logicalFilePath:2_add_categories_tactics_in_incident

ALTER TABLE cyber_event_service.amp_incidents ADD COLUMN categories text[] DEFAULT '{}';
ALTER TABLE cyber_event_service.amp_incidents ADD COLUMN tactics text[] DEFAULT '{}';

--rollback ALTER TABLE cyber_event_service.amp_incidents DROP COLUMN categories;

--rollback ALTER TABLE cyber_event_service.amp_incidents DROP COLUMN tactics;
