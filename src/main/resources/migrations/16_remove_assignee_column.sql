--liquibase formatted sql

--changeset fractal_internal_user:16 logicalFilePath:16_remove_assignee_column

ALTER TABLE cyber_event_service.ns_ticketing_system_mails DROP COLUMN inc_assignee;

--rollback ALTER TABLE cyber_event_service.ns_ticketing_system_mails ADD COLUMN inc_assignee text;