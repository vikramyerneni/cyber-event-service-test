--liquibase formatted sql

--changeset fractal_internal_user:1 logicalFilePath:18_add_assignee_email

ALTER TABLE cyber_event_service.amp_incidents ADD COLUMN assignee_email text;

--rollback ALTER TABLE cyber_event_service.amp_incidents DROP COLUMN assignee_email;
