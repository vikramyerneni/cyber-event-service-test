--liquibase formatted sql

--changeset fractal_internal_user:1 logicalFilePath:12_add_incrementing_index.sql

ALTER TABLE cyber_event_service.amp_events ADD COLUMN inc_idx SERIAL;
CREATE INDEX idx_inc_idx ON cyber_event_service.amp_events (inc_idx);

--rollback ALTER TABLE cyber_event_service.amp_events DROP COLUMN inc_idx;
