--liquibase formatted sql

--changeset fractal_internal_user:1 logicalFilePath:3_create_amp_categories_table_with_seed_data

CREATE TABLE IF NOT EXISTS cyber_event_service.amp_categories (
  name text NOT NULL
);

INSERT INTO cyber_event_service.amp_categories values ('Initial Access'), ('Execution'), ('Persistence'), ('Privilege Escalation'), ('Defense Evasion'), ('Credential Access'), ('Discovery'), ('Lateral Movement'), ('Collection'), ('Exfiltration'), ('Command and Control'), ('Initial Access'), ('Execution'), ('Persistence'), ('Privilege Escalation'), ('Defense Evasion'), ('Credential Access'), ('Discovery'), ('Lateral Movement'), ('Collection'), ('Exfiltration'), ('Command and Control'), ('Initial Access'), ('Execution'), ('Persistence'), ('Privilege Escalation'), ('Defense Evasion'), ('Credential Access'), ('Discovery'), ('Lateral Movement'), ('Collection'), ('Exfiltration'), ('Command and Control'), ('Priority Definition Planning'), ('Priority Definition Direction'), ('Target Selection'), ('Technical Information Gathering'), ('People Information Gathering'), ('Organizational Information Gathering'), ('Technical Weakness Identification'), ('People Weakness Identification'), ('Organizational Weakness Identification'), ('Adversary OPSEC'), ('Establish & Maintain Infrastructure'), ('Persona Development'), ('Build Capabilities'), ('Test Capabilities'), ('Stage Capabilities'), ('Initial Access'), ('Execution'), ('Persistence'), ('Privilege Escalation'), ('Defense Evasion'), ('Credential Access'), ('Discovery'), ('Lateral Movement'), ('Collection'), ('Exfiltration'), ('Command and Control'), ('Priority Definition Planning'), ('Priority Definition Direction'), ('Target Selection'), ('Technical Information Gathering'), ('People Information Gathering'), ('Organizational Information Gathering'), ('Technical Weakness Identification'), ('People Weakness Identification'), ('Organizational Weakness Identification'), ('Adversary OPSEC'), ('Establish & Maintain Infrastructure'), ('Persona Development'), ('Build Capabilities'), ('Test Capabilities'), ('Stage Capabilities');

--rollback DROP TABLE cyber_event_service.amp_categories;
