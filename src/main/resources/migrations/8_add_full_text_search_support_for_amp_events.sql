--liquibase formatted sql

--changeset fractal_internal_user:1 logicalFilePath:8_add_full_text_search_support_for_amp_events

ALTER TABLE cyber_event_service.amp_events ADD COLUMN tokens tsvector;

UPDATE cyber_event_service.amp_events SET tokens = (to_tsvector('english', event_id) || to_tsvector('english', name) || to_tsvector('english', description));

CREATE TRIGGER tsvector_amp_events_tokens_update BEFORE INSERT OR UPDATE
ON cyber_event_service.amp_events FOR EACH ROW EXECUTE PROCEDURE
tsvector_update_trigger(
	tokens, 'pg_catalog.english', event_id, name, description
);


CREATE INDEX amp_events_textsearch_idx ON cyber_event_service.amp_events USING GIN (tokens);

--rollback DROP INDEX cyber_event_service.amp_events_textsearch_idx;

--rollback DROP TRIGGER tsvector_amp_events_tokens_update ON cyber_event_service.amp_events;

--rollback ALTER TABLE cyber_event_service.amp_events DROP COLUMN tokens;