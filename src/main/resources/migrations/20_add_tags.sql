--liquibase formatted sql

--changeset fractal_internal_user:20 logicalFilePath:20_add_tags

--comment remove migration entry 'delete from public.databasechangelog where id = '20';'

CREATE TYPE amp_property_type AS ENUM ('AGENT', 'RULE');

CREATE TABLE IF NOT EXISTS cyber_event_service.amp_tags (
  kind amp_property_type NOT NULL,
  property_id text NOT NULL,
  tag_id text NOT NULL,
  UNIQUE(kind, property_id, tag_id)
);

--comment we don't want to populate following columns (millions of entries) with some default value.
ALTER TABLE cyber_event_service.amp_events ADD COLUMN agent_id text;
CREATE INDEX idx_agent_id ON cyber_event_service.amp_events (agent_id);

ALTER TABLE cyber_event_service.amp_events ADD COLUMN rule_id text;
CREATE INDEX idx_rule_id ON cyber_event_service.amp_events (rule_id);

ALTER TABLE cyber_event_service.amp_events ALTER COLUMN observed_system_id DROP NOT NULL;

--comment we will switch to not-null once we remove `observed_system_id` column
ALTER TABLE cyber_event_service.amp_events ADD COLUMN customer_account_id text;

--rollback ALTER TABLE cyber_event_service.amp_events DROP COLUMN customer_account_id text;
--rollback ALTER TABLE cyber_event_service.amp_events ALTER COLUMN observed_system_id SET NOT NULL;
--rollback ALTER TABLE cyber_event_service.amp_events DROP COLUMN rule_id;
--rollback ALTER TABLE cyber_event_service.amp_events DROP COLUMN agent_id;
--rollback DROP TABLE cyber_event_service.amp_tags;
--rollback DROP TYPE property_type;