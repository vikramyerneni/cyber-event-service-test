--liquibase formatted sql

--changeset fractal_internal_user:15 logicalFilePath:15_add_columns_in_mails

ALTER TABLE cyber_event_service.ns_ticketing_system_mails ADD COLUMN third_party_system cyber_event_service.ns_third_party_system NOT NULL;
ALTER TABLE cyber_event_service.ns_ticketing_system_mails ADD COLUMN emails text[] NOT NULL DEFAULT '{}';
ALTER TABLE cyber_event_service.ns_ticketing_system_mails ADD COLUMN inc_severity smallint NOT NULL;

ALTER TABLE cyber_event_service.ns_ticketing_system_mails DROP CONSTRAINT ns_ticketing_system_mails_config_id_fkey;
ALTER TABLE cyber_event_service.ns_ticketing_system_mails ALTER COLUMN config_id SET NOT NULL;

--rollback ALTER TABLE cyber_event_service.ns_ticketing_system_mails ADD CONSTRAINT ns_ticketing_system_mails_config_id_fkey FOREIGN KEY (config_id) REFERENCES cyber_event_service.ns_third_party_integration_configs (id);
--rollback ALTER TABLE cyber_event_service.ns_ticketing_system_mails DROP COLUMN inc_severity;
--rollback ALTER TABLE cyber_event_service.ns_ticketing_system_mails DROP COLUMN emails;
--rollback ALTER TABLE cyber_event_service.ns_ticketing_system_mails DROP COLUMN third_party_system;