--liquibase formatted sql

--changeset fractal_internal_user:1 logicalFilePath:7_add_extra_info

alter table cyber_event_service.amp_events add COLUMN extra_info jsonb;

--rollback alter table cyber_event_service.amp_events drop COLUMN extra_info;
