--liquibase formatted sql

--changeset fractal_internal_user:21 logicalFilePath:21_tag_aware_modules

--comment remove migration entry 'delete from public.databasechangelog where id = '21';'

ALTER TABLE cyber_event_service.amp_incidents RENAME COLUMN observed_system_id TO tag_id;
ALTER TABLE cyber_event_service.ns_third_party_integration_configs RENAME COLUMN observed_system_id TO tag_id;
ALTER TABLE cyber_event_service.amp_events_count ADD COLUMN tag_id text;
ALTER TABLE cyber_event_service.amp_events_count DROP CONSTRAINT amp_events_count_pkey;
ALTER TABLE cyber_event_service.amp_events_count ALTER COLUMN observed_system_id DROP NOT NULL;
ALTER TABLE cyber_event_service.amp_events_count ADD CONSTRAINT amp_events_count_unique UNIQUE (date, observed_system_id, tag_id);

--comment Notes on trigger logic:
--comment 1) Without introducing "record" table, SELECT would return empty records if tag ain't exist, hence no count update
--comment 2) We can't leave `tag_id` null, it won't increment proper row uniquely combined in `amp_events_count_unique` constraint,
--comment    that's why "PLACEHOLDER_TAG" as a placeholder value for all non-existent tags.
--comment    For the same reason, we need "PLACEHOLDER_OBSID" as a placeholder value for observed system id as it could be empty from now on.
--comment 3) `LIMIT 1` is necessary as there is a chance of having multiple tag entry of same tag with different properties.

CREATE OR REPLACE FUNCTION update_events_count()
RETURNS TRIGGER AS
'
   DECLARE
   BEGIN
   IF TG_OP = ''INSERT'' THEN
      EXECUTE ''INSERT INTO cyber_event_service.amp_events_count (date, observed_system_id, count, tag_id)
                SELECT
                    to_timestamp($1.timestamp / 1000)::date,
                    CASE WHEN $1.observed_system_id IS NULL THEN ''''PLACEHOLDER_OBSID'''' ELSE $1.observed_system_id END,
                    1,
                    CASE WHEN tag.tag_id IS NULL THEN ''''PLACEHOLDER_TAG'''' ELSE tag.tag_id END
                FROM (VALUES (1)) AS record
                LEFT JOIN cyber_event_service.amp_tags AS tag ON (tag.property_id = $1.agent_id OR tag.property_id = $1.rule_id) LIMIT 1
                ON CONFLICT (date, observed_system_id, tag_id)
                DO UPDATE SET count = cyber_event_service.amp_events_count.count + 1'' USING NEW;
      RETURN NEW;
   END IF;
   END;
'
LANGUAGE 'plpgsql';

--rollback CREATE OR REPLACE FUNCTION update_events_count()
--rollback RETURNS TRIGGER AS
--rollback '
--rollback    DECLARE
--rollback    BEGIN
--rollback    IF TG_OP = ''INSERT'' THEN
--rollback       EXECUTE ''INSERT INTO cyber_event_service.amp_events_count (date, observed_system_id, count)
--rollback       VALUES (to_timestamp($1.timestamp / 1000)::date, $1.observed_system_id, 1)
--rollback       ON CONFLICT (date, observed_system_id)
--rollback       DO UPDATE SET count = cyber_event_service.amp_events_count.count + 1'' USING NEW;
--rollback       RETURN NEW;
--rollback    END IF;
--rollback    END;
--rollback '
--rollback LANGUAGE 'plpgsql';
--rollback ALTER TABLE cyber_event_service.amp_events_count DROP CONSTRAINT amp_events_count_unique;
--rollback ALTER TABLE cyber_event_service.amp_events_count ALTER COLUMN observed_system_id SET NOT NULL;
--rollback ALTER TABLE cyber_event_service.amp_events_count ADD CONSTRAINT amp_events_count_pkey PRIMARY KEY (date, observed_system_id);
--rollback ALTER TABLE cyber_event_service.amp_events_count DROP COLUMN tag_id;
--rollback ALTER TABLE cyber_event_service.ns_third_party_integration_configs RENAME COLUMN tag_id TO observed_system_id;
--rollback ALTER TABLE cyber_event_service.amp_incidents RENAME COLUMN tag_id TO observed_system_id;