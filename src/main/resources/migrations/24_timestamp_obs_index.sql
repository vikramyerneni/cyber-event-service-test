--liquibase formatted sql

--changeset fractal_internal_user:24 logicalFilePath:24_timestamp_obs_index

--comment remove migration entry 'delete from public.databasechangelog where id = '24';'

--comment A composite index to help with the query performance of SIEM routes -1 checkpoint scenario

CREATE INDEX idx_timestamp_obs_with_order ON cyber_event_service.amp_events (observed_system_id, timestamp desc);

--rollback DROP INDEX idx_timestamp_obs_with_order;

