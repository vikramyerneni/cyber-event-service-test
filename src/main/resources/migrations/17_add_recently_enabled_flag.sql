--liquibase formatted sql

--changeset fractal_internal_user:17 logicalFilePath:17_add_recently_enabled_flag.sql

ALTER TABLE cyber_event_service.ns_third_party_integration_configs ADD COLUMN recently_enabled BOOLEAN DEFAULT false;

--rollback ALTER TABLE cyber_event_service.ns_third_party_integration_configs DROP COLUMN recently_enabled;
