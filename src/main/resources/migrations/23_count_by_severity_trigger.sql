--liquibase formatted sql

--changeset fractal_internal_user:23 logicalFilePath:23_count_by_severity_trigger

--comment remove migration entry 'delete from public.databasechangelog where id = '23';'

--comment a function to update/create severity count record
CREATE OR REPLACE FUNCTION update_events_count_by_severity()
RETURNS TRIGGER AS
'
   DECLARE
   BEGIN
   IF TG_OP = ''INSERT'' THEN
      EXECUTE ''INSERT INTO cyber_event_service.amp_events_count_by_severity (datetime, tag_id, total, low, medium, high)
                SELECT
                    date_trunc(''''minute'''', to_timestamp($1.timestamp / 1000)::timestamp),
                    CASE
                        WHEN $1.observed_system_id IS NULL AND tag.tag_id IS NULL THEN ''''UNKNOWN''''
                        WHEN $1.observed_system_id IS NULL THEN tag.tag_id
                        ELSE $1.observed_system_id
                    END,
                    CASE WHEN $1.severity BETWEEN  0 AND 100 THEN 1 ELSE 0 END,
                    CASE WHEN $1.severity BETWEEN  0 AND  30 THEN 1 ELSE 0 END,
                    CASE WHEN $1.severity BETWEEN 31 AND  60 THEN 1 ELSE 0 END,
                    CASE WHEN $1.severity BETWEEN 61 AND 100 THEN 1 ELSE 0 END
                FROM (VALUES (1)) AS record
                LEFT JOIN cyber_event_service.amp_tags AS tag ON (tag.property_id = $1.agent_id OR tag.property_id = $1.rule_id) LIMIT 1
                ON CONFLICT (datetime, tag_id)
                DO UPDATE SET
                    total  = CASE WHEN $1.severity BETWEEN  0 AND 100 THEN cyber_event_service.amp_events_count_by_severity.total + 1  ELSE cyber_event_service.amp_events_count_by_severity.total END,
                    low    = CASE WHEN $1.severity BETWEEN  0 AND  30 THEN cyber_event_service.amp_events_count_by_severity.low + 1    ELSE cyber_event_service.amp_events_count_by_severity.low END,
                    medium = CASE WHEN $1.severity BETWEEN 31 AND  60 THEN cyber_event_service.amp_events_count_by_severity.medium + 1 ELSE cyber_event_service.amp_events_count_by_severity.medium END,
                    high   = CASE WHEN $1.severity BETWEEN 61 AND 100 THEN cyber_event_service.amp_events_count_by_severity.high + 1   ELSE cyber_event_service.amp_events_count_by_severity.high END'' USING NEW;
      RETURN NEW;
   END IF;
   END;
'
LANGUAGE 'plpgsql';

