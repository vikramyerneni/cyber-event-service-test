--liquibase formatted sql

--changeset fractal_internal_user:1 logicalFilePath:11_ns_third_party_integration_configs

CREATE TYPE cyber_event_service.ns_third_party_system AS ENUM ('JIRA', 'SERVICENOW', 'REMEDY', 'ARCHER', 'SPLUNK', 'QRADAR');

CREATE TABLE IF NOT EXISTS cyber_event_service.ns_third_party_integration_configs (
  id text PRIMARY KEY,
  observed_system_id text NOT NULL,
  third_party_system cyber_event_service.ns_third_party_system NOT NULL,
  name text NOT NULL,
  description text,
  emails text[],
  api_key text,
  enabled boolean NOT NULL DEFAULT true,
  created_on bigint,
  created_by text NOT NULL,
  last_modified_on bigint,
  last_modified_by text NOT NULL,
  CONSTRAINT third_party_system_name_unique UNIQUE (third_party_system, name)
);

CREATE INDEX idx_ns_ts_configurations_observed_system_id ON cyber_event_service.ns_third_party_integration_configs(observed_system_id);

CREATE TABLE IF NOT EXISTS cyber_event_service.ns_ticketing_system_mails (
  id text PRIMARY KEY,
  config_id text REFERENCES cyber_event_service.ns_third_party_integration_configs,
  inc_id text NOT NULL,
  inc_title text NOT NULL,
  inc_status amp_incident_status NOT NULL,
  inc_assignee text,
  inc_categories text[],
  inc_tactics text[],
  inc_event_ids text[],
  inc_created_on bigint NOT NULL,
  delivered boolean NOT NULL DEFAULT false,
  created_on bigint,
  last_modified_on bigint
);

CREATE INDEX idx_ns_mails_integration_id ON cyber_event_service.ns_ticketing_system_mails(config_id);

--rollback DROP TABLE cyber_event_service.ns_ticketing_system_mails;
--rollback DROP TABLE cyber_event_service.ns_third_party_integration_configs;
--rollback DROP TYPE cyber_event_service.ns_third_party_system;