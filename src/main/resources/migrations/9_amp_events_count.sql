--liquibase formatted sql

--changeset fractal_internal_user:1 logicalFilePath:9_amp_events_count

CREATE TABLE cyber_event_service.amp_events_count (
  date DATE NOT NULL,
  observed_system_id text,
  count bigint,
  PRIMARY KEY (date, observed_system_id)
);

CREATE INDEX amp_events_count_idx_date ON cyber_event_service.amp_events_count (date);
CREATE INDEX amp_events_count_idx_observed_system_id ON cyber_event_service.amp_events_count (observed_system_id);

--comment START - DATA MIGRATION OF EXISTING EVENTS
--comment `amp_events_count_temp` is temporary table to store date and observed system id.
--comment (it's a workaround) PostgreSQL doesn't allow an insertion operation along with select and on conflict clause.
--comment we populate its fields using data from existing events
--comment and then update count of actual table `amp_events_count`

CREATE TEMP TABLE amp_events_count_temp (
  date DATE,
  observed_system_id text
);

INSERT INTO amp_events_count_temp (date, observed_system_id)
SELECT to_timestamp(timestamp / 1000)::date, observed_system_id FROM cyber_event_service.amp_events;

INSERT INTO cyber_event_service.amp_events_count (date, observed_system_id, count)
select date, observed_system_id, count(1)
from amp_events_count_temp group by (date, observed_system_id)
ON CONFLICT (date, observed_system_id)
DO UPDATE SET count = excluded.count + 1;

--comment END - DATA MIGRATION OF EXISTING EVENTS

CREATE OR REPLACE FUNCTION update_events_count()
RETURNS TRIGGER AS
'
   DECLARE
   BEGIN
   IF TG_OP = ''INSERT'' THEN
      EXECUTE ''INSERT INTO cyber_event_service.amp_events_count (date, observed_system_id, count) values (to_timestamp($1.timestamp / 1000)::date, $1.observed_system_id, 1) ON CONFLICT (date, observed_system_id) DO UPDATE SET count = cyber_event_service.amp_events_count.count + 1'' USING NEW;
      RETURN NEW;
   END IF;
   END;
'
LANGUAGE 'plpgsql';

CREATE TRIGGER amp_events_count AFTER INSERT ON cyber_event_service.amp_events
  FOR EACH ROW EXECUTE PROCEDURE update_events_count();

--rollback drop trigger amp_events_count on cyber_event_service.amp_events;

--rollback drop function update_events_count;

--rollback drop table cyber_event_service.amp_events_count;