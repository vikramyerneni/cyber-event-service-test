--liquibase formatted sql

--changeset fractal_internal_user:1 logicalFilePath:6_separation_of_categories_and_tactics_in_amp_events

alter table cyber_event_service.amp_events add COLUMN categories text[] DEFAULT '{}';
alter table cyber_event_service.amp_events add COLUMN tactics text[] DEFAULT '{}';

update cyber_event_service.amp_events
set
  categories = (select ARRAY(select json_array_elements_text((misc->>'categories')::json))),
  tactics = (select ARRAY(select json_array_elements_text((misc->>'tactics')::json)));

alter table cyber_event_service.amp_events drop COLUMN misc;

--rollback alter TABLE cyber_event_service.amp_events add COLUMN misc jsonb DEFAULT '{"categories":[], "tactics":[]}';

--rollback update cyber_event_service.amp_events set misc = (select json_build_object('tactics', tactics, 'categories', categories) FROM cyber_event_service.amp_events);

--rollback alter table cyber_event_service.amp_events drop COLUMN categories;
--rollback alter table cyber_event_service.amp_events drop COLUMN tactics;
