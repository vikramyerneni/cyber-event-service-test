--liquibase formatted sql

--changeset fractal_internal_user:1 logicalFilePath:5_add_description_in_incident

ALTER TABLE cyber_event_service.amp_incidents ADD COLUMN description text;

--rollback ALTER TABLE cyber_event_service.amp_incidents DROP COLUMN description;
