--liquibase formatted sql

--changeset fractal_internal_user:1 logicalFilePath:1_initial_schema

CREATE SCHEMA IF NOT EXISTS cyber_event_service;

CREATE TABLE IF NOT EXISTS cyber_event_service.amp_events (
  event_id text PRIMARY KEY,
  observed_system_id text NOT NULL,
  name text NOT NULL,
  description text NOT NULL,
  type text NOT NULL,
  severity int NOT NULL,
  timestamp bigint NOT NULL,
  source text NOT NULL,
  source_id text NOT NULL,
  misc jsonb NOT NULL
);

CREATE INDEX idx_timestamp ON cyber_event_service.amp_events (timestamp);
CREATE INDEX idx_obs_sys_id ON cyber_event_service.amp_events (observed_system_id);

CREATE TYPE amp_incident_status AS ENUM ('NEW', 'OPEN', 'CLOSED');
CREATE TYPE amp_incident_reporter_source AS ENUM ('USER', 'SYSTEM');

CREATE TABLE IF NOT EXISTS cyber_event_service.amp_incidents (
  id text PRIMARY KEY,
  title text NOT NULL,
  resolution text,
  resolution_timestamp bigint,
  status amp_incident_status NOT NULL DEFAULT 'NEW',
  severity smallint NOT NULL DEFAULT 0,
  report_timestamp bigint NOT NULL,
  reporter_id bigint NOT NULL,
  reporter_source amp_incident_reporter_source NOT NULL DEFAULT 'USER',
  observed_system_id text NOT NULL,
  assignee bigint,
  created_on bigint NOT NULL,
  created_by bigint NOT NULL,
  last_modified_on bigint NOT NULL,
  last_modified_by bigint NOT NULL
);

CREATE INDEX idx_amp_incidents_observed_system_id ON cyber_event_service.amp_incidents (observed_system_id);
CREATE INDEX idx_amp_incidents_status ON cyber_event_service.amp_incidents (status);
CREATE INDEX idx_amp_incidents_reporter_id ON cyber_event_service.amp_incidents (reporter_id);
CREATE INDEX idx_amp_incidents_assignee ON cyber_event_service.amp_incidents (assignee);

CREATE TABLE IF NOT EXISTS cyber_event_service.amp_incident_files (
  id text PRIMARY KEY,
  name text NOT NULL,
  description text,
  s3_key text NOT NULL,
  incident_id text NOT NULL ,
  created_on bigint NOT NULL,
  created_by bigint NOT NULL,
  last_modified_on bigint NOT NULL,
  last_modified_by bigint NOT NULL,

  CONSTRAINT fk_amp_incident_files_incident_id FOREIGN KEY(incident_id) REFERENCES cyber_event_service.amp_incidents
);

CREATE INDEX idx_amp_incident_files_incident_id ON cyber_event_service.amp_incident_files (incident_id);

CREATE TABLE IF NOT EXISTS cyber_event_service.amp_incident_urls (
 id text PRIMARY KEY,
 url text NOT NULL,
 incident_id text NOT NULL ,
 created_on bigint NOT NULL,
 created_by bigint NOT NULL,
 last_modified_on bigint NOT NULL,
 last_modified_by bigint NOT NULL,

 CONSTRAINT fk_amp_incident_urls_incident_id FOREIGN KEY(incident_id) REFERENCES cyber_event_service.amp_incidents
);

CREATE INDEX idx_amp_incident_urls_incident_id ON cyber_event_service.amp_incident_urls (incident_id);

CREATE TABLE IF NOT EXISTS cyber_event_service.amp_incident_notes (
  id text PRIMARY KEY,
  note text NOT NULL,
  incident_id text NOT NULL ,
  created_on bigint NOT NULL,
  created_by bigint NOT NULL,
  last_modified_on bigint NOT NULL,
  last_modified_by bigint NOT NULL,

  CONSTRAINT fk_amp_incident_notes_incident_id FOREIGN KEY(incident_id) REFERENCES cyber_event_service.amp_incidents
);

CREATE INDEX idx_amp_incident_notes_incident_id ON cyber_event_service.amp_incident_notes (incident_id);


CREATE TABLE IF NOT EXISTS cyber_event_service.amp_event_incident (
  amp_event_id text NOT NULL,
  amp_incident_id text NOT NULL,

  CONSTRAINT pk_amp_event_incident PRIMARY KEY (amp_event_id, amp_incident_id),
  CONSTRAINT fk_amp_event_incident_amp_event_id FOREIGN KEY(amp_event_id) REFERENCES cyber_event_service.amp_events,
  CONSTRAINT fk_amp_event_incident_amp_incident_id FOREIGN KEY(amp_incident_id) REFERENCES cyber_event_service.amp_incidents
);

--rollback DROP TABLE cyber_event_service.amp_events;

--rollback DROP TABLE cyber_event_service.amp_incidents;

--rollback DROP TABLE cyber_event_service.amp_incident_files;

--rollback DROP TABLE cyber_event_service.amp_incident_urls;

--rollback DROP TABLE cyber_event_service.amp_incident_notes;

--rollback DROP TABLE cyber_event_service.amp_event_incident;

--rollback DROP TYPE IF EXISTS amp_incident_status;

--rollback DROP TYPE IF EXISTS amp_incident_reporter_source;