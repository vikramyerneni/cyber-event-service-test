--liquibase formatted sql

--changeset fractal_internal_user:1 logicalFilePath:13_add_index_on_incident_id.sql

CREATE INDEX idx_amp_event_incident_fk_incidents ON cyber_event_service.amp_event_incident USING btree (amp_incident_id);

--rollback DROP INDEX cyber_event_service.idx_amp_event_incident_fk_incidents;
