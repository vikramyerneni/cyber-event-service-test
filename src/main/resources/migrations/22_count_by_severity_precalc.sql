--liquibase formatted sql

--changeset fractal_internal_user:22 logicalFilePath:22_count_by_severity_precalc

--comment remove migration entry 'delete from public.databasechangelog where id = '22';'

--comment main severity count table
CREATE TABLE cyber_event_service.amp_events_count_by_severity (
  datetime TIMESTAMP NOT NULL,
  tag_id text,
  total int,
  low int,
  medium int,
  high int,
  PRIMARY KEY (datetime, tag_id)
);

--comment creates temporary table to store severity flags
CREATE TEMP TABLE amp_events_count_by_severity_temp (
  datetime timestamp,
  tag_id text,
  total int,
  low int,
  medium int,
  high int
);

--comment inserts flags for existing events
INSERT INTO amp_events_count_by_severity_temp (datetime, tag_id, total, low, medium, high)
SELECT date_trunc('minute', to_timestamp(event.timestamp / 1000))::timestamp, CASE WHEN event.observed_system_id is null THEN tag.tag_id ELSE event.observed_system_id END,
CASE WHEN event.severity BETWEEN 0 AND 100 THEN 1 ELSE 0 END,
CASE WHEN event.severity BETWEEN 0 AND 30 THEN 1 ELSE 0 END,
CASE WHEN event.severity BETWEEN 31 AND 60 THEN 1 ELSE 0 END,
CASE WHEN event.severity BETWEEN 61 AND 100 THEN 1 ELSE 0 END
FROM cyber_event_service.amp_events event
LEFT JOIN cyber_event_service.amp_tags tag ON (tag.property_id = event.agent_id OR tag.property_id = event.rule_id)
WHERE event.timestamp >= (EXTRACT(EPOCH FROM (CURRENT_TIMESTAMP - INTERVAL '1 day')) * 1000)::bigint;

--comment inserts actual severity count in main count table
INSERT INTO cyber_event_service.amp_events_count_by_severity (datetime, tag_id, total, low, medium, high)
SELECT datetime, tag_id, sum(total), sum(low), sum(medium), sum(high)
FROM amp_events_count_by_severity_temp WHERE tag_id is not null GROUP BY (datetime, tag_id);

--comment a function to update/create severity count record
CREATE OR REPLACE FUNCTION update_events_count_by_severity()
RETURNS TRIGGER AS
'
   DECLARE
   BEGIN
   IF TG_OP = ''INSERT'' THEN
      EXECUTE ''INSERT INTO cyber_event_service.amp_events_count_by_severity (datetime, tag_id, total, low, medium, high)
                SELECT
                    date_trunc(''''minute'''', to_timestamp($1.timestamp / 1000)::timestamp),
                    CASE WHEN $1.observed_system_id IS NULL THEN tag.tag_id ELSE $1.observed_system_id END,
                    CASE WHEN $1.severity BETWEEN  0 AND 100 THEN 1 ELSE 0 END,
                    CASE WHEN $1.severity BETWEEN  0 AND  30 THEN 1 ELSE 0 END,
                    CASE WHEN $1.severity BETWEEN 31 AND  60 THEN 1 ELSE 0 END,
                    CASE WHEN $1.severity BETWEEN 61 AND 100 THEN 1 ELSE 0 END
                FROM (VALUES (1)) AS record
                LEFT JOIN cyber_event_service.amp_tags AS tag ON (tag.property_id = $1.agent_id OR tag.property_id = $1.rule_id) LIMIT 1
                ON CONFLICT (datetime, tag_id)
                DO UPDATE SET
                    total  = CASE WHEN $1.severity BETWEEN  0 AND 100 THEN cyber_event_service.amp_events_count_by_severity.total + 1  ELSE cyber_event_service.amp_events_count_by_severity.total END,
                    low    = CASE WHEN $1.severity BETWEEN  0 AND  30 THEN cyber_event_service.amp_events_count_by_severity.low + 1    ELSE cyber_event_service.amp_events_count_by_severity.low END,
                    medium = CASE WHEN $1.severity BETWEEN 31 AND  60 THEN cyber_event_service.amp_events_count_by_severity.medium + 1 ELSE cyber_event_service.amp_events_count_by_severity.medium END,
                    high   = CASE WHEN $1.severity BETWEEN 61 AND 100 THEN cyber_event_service.amp_events_count_by_severity.high + 1   ELSE cyber_event_service.amp_events_count_by_severity.high END'' USING NEW;
      RETURN NEW;
   END IF;
   END;
'
LANGUAGE 'plpgsql';

CREATE TRIGGER amp_events_count_by_severity AFTER INSERT ON cyber_event_service.amp_events
  FOR EACH ROW EXECUTE PROCEDURE update_events_count_by_severity();

--rollback drop trigger amp_events_count_by_severity on cyber_event_service.amp_events;
--rollback drop function update_events_count_by_severity;
--rollback drop table cyber_event_service.amp_events_count_by_severity;

