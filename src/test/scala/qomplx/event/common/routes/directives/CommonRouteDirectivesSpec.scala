package qomplx.event.common.routes.directives

import akka.http.scaladsl.model.headers.{ Origin, _ }
import akka.http.scaladsl.model.{ HttpMethods, HttpRequest, HttpResponse, StatusCodes }
import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.typesafe.config.ConfigFactory
import qomplx.event.BaseSpec
import akka.http.scaladsl.model.headers._
import akka.http.scaladsl.server.Directives._
import qomplx.event.directives.CommonRouteDirectives
import qomplx.event.directives.CommonRouteDirectives.CORSConfig

import scala.concurrent.Future

class CommonRouteDirectivesSpec extends BaseSpec with ScalatestRouteTest {

  private val basicRouteActualResponse = "CommonRouteDirectivesSpec is working"
  private val basicRoute = get {
    complete(basicRouteActualResponse)
  }

  private val defaultAllowedOrigin1 = "http://www.example1.com"
  private val defaultAllowedOrigin2 = "http://www.example2.com"
  private def basicCommonDirective(allowedOriginCSV: String = s"$defaultAllowedOrigin1,$defaultAllowedOrigin2") = {
    import scala.collection.JavaConverters._
    val props = Map("allow-generic-http-requests" -> false, "allowed-origins" -> allowedOriginCSV)
    val conf = ConfigFactory.parseMap(props.asJava)
    new CommonRouteDirectives(CORSConfig(conf))
  }

  "Common Routes Directive" should {
    "provide CORS Directive that" can {
      "Allow requests with origin headers, if the origin matches the configured origin" in {
        val basicWrappedRoute = basicCommonDirective().handleCORSAndExceptions { basicRoute }
        val request = HttpRequest(uri = "/", headers = Origin(defaultAllowedOrigin2) :: Nil)
        request ~> basicWrappedRoute ~> check {
          status should be(StatusCodes.OK)
          containsVaryOriginHeader(response) shouldBe true
          accessControlAllowOriginShouldMatchOrigin(response, defaultAllowedOrigin2)
        }
      }

      "Prevent requests with origin headers, if the Origin header doesn't match the configured origin" in {
        val invalidOrigin = "http://www.another-example.com"
        val basicWrappedRoute = basicCommonDirective().handleCORSAndExceptions { basicRoute }
        Get() ~> Origin(invalidOrigin) ~> basicWrappedRoute ~> check {
          status should be(StatusCodes.BadRequest)
          responseAs[String] should include(s"invalid origin '$invalidOrigin'")
        }
      }

      "Prevent requests without the origin header when configured with strict settings" in {
        val basicWrappedRoute = basicCommonDirective().handleCORSAndExceptions { basicRoute }
        Get() ~> basicWrappedRoute ~> check {
          status should be(StatusCodes.BadRequest)
          responseAs[String] shouldBe "CORS: malformed request"
        }
      }

      "Allow requests from arbitrary origins when configured with wild-card origin '*'" in {
        val basicWrappedRoute = basicCommonDirective("*").handleCORSAndExceptions { basicRoute }
        val randOrigins = "https://www.random.org" :: "https://www.random-another.org" :: Nil

        randOrigins.foreach { randOrigin =>
          Get() ~> Origin(randOrigin) ~> basicWrappedRoute ~> check {
            status should be(StatusCodes.OK)
            responseAs[String] shouldBe basicRouteActualResponse
            containsVaryOriginHeader(response) shouldBe true
            accessControlAllowOriginShouldMatchOrigin(response, randOrigin)
          }
        }
      }

      "Send correct CORS headers back even if the request returns 404" in {
        val basicWrappedRoute = basicCommonDirective().handleCORSAndExceptions { complete(HttpResponse(StatusCodes.NotFound)) }
        Get() ~> Origin(defaultAllowedOrigin2) ~> basicWrappedRoute ~> check {
          status should be(StatusCodes.NotFound)
          containsCORSHeaders(response) shouldBe true
        }
      }

      "Responds to OPTIONS requests correctly" in {
        val basicWrappedRoute = basicCommonDirective("*").handleCORSAndExceptions { basicRoute }
        Options() ~> `Access-Control-Request-Method`(HttpMethods.GET) ~> Origin(defaultAllowedOrigin2) ~> basicWrappedRoute ~> check {
          status should be(StatusCodes.OK)
          containsCORSHeaders(response) shouldBe true
        }
      }
    }

    "provide an empty-response handler that" can {
      "convert a None result into a 404" in {
        val route = basicCommonDirective().handleCORSAndExceptions {
          get { complete(None: Option[String]) }
        }

        Get() ~> Origin(defaultAllowedOrigin2) ~> route ~> check {
          assertNotFoundResponse
          containsCORSHeaders(response) shouldBe true
        }
      }

      "convert a Future[None] result into a 404" in {
        val route = basicCommonDirective().handleCORSAndExceptions {
          get { complete(Future.successful(None: Option[String])) }
        }

        Get() ~> Origin(defaultAllowedOrigin2) ~> route ~> check {
          assertNotFoundResponse
          containsCORSHeaders(response) shouldBe true
        }
      }

      "ignore a StatusCodes.NoContent response" in {
        val route = basicCommonDirective().handleCORSAndExceptions {
          get { complete(HttpResponse(StatusCodes.NoContent)) }
        }

        Get() ~> Origin(defaultAllowedOrigin2) ~> route ~> check {
          status shouldBe StatusCodes.NoContent
          containsCORSHeaders(response) shouldBe true
        }
      }
    }

    "provide an exception handler that" can {
      "handle exceptions contained in failed futures" in {
        val route = basicCommonDirective().handleCORSAndExceptions {
          get { complete(Future.failed(new RuntimeException("Test Failure")): Future[String]) }
        }

        Get() ~> Origin(defaultAllowedOrigin2) ~> route ~> check {
          status shouldBe StatusCodes.InternalServerError
          responseAs[String] shouldBe CommonRouteDirectives.InternalServerErrorResponseText
          containsCORSHeaders(response) shouldBe true
        }
      }

      "handle other exceptions thrown by routes" in {
        val route = basicCommonDirective().handleCORSAndExceptions {
          get { complete(throw new RuntimeException("Test Failure")) }
        }

        Get() ~> Origin(defaultAllowedOrigin2) ~> route ~> check {
          status shouldBe StatusCodes.InternalServerError
          responseAs[String] shouldBe CommonRouteDirectives.InternalServerErrorResponseText
          containsCORSHeaders(response) shouldBe true
        }
      }
    }
  }

  private def containsVaryOriginHeader(response: HttpResponse) = {
    response.headers.exists { h =>
      h.value() == Origin.name && h.name() == "Vary"
    }
  }

  private def containsCORSHeaders(response: HttpResponse) = {
    (`Access-Control-Allow-Origin`.name :: "Vary" :: Nil).forall {
      response.headers.map(_.name()).contains
    }
  }

  private def accessControlAllowOriginShouldMatchOrigin(response: HttpResponse, origin: String) = {
    val headerOpt = response.headers.find(_.name() == `Access-Control-Allow-Origin`.name)

    headerOpt shouldBe defined
    headerOpt.value.value() shouldBe origin
  }

  private def assertNotFoundResponse: Unit = {
    status shouldBe StatusCodes.NotFound
    responseAs[String] shouldBe CommonRouteDirectives.NotFoundResponseText
  }

  private def assertResponseHasCORSHeaders(response: HttpResponse) = {

  }
}
