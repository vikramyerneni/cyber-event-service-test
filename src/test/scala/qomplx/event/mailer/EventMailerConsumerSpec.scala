package qomplx.event.mailer

import com.typesafe.config.ConfigFactory
import net.manub.embeddedkafka.EmbeddedKafka
import org.mockito.{ ArgumentMatchers, Mockito }
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.Eventually
import org.scalatest.mockito.MockitoSugar
import org.scalatest.time.{ Millis, Seconds, Span }
import qomplx.event.{ BaseSpecWithActorSystem, JsonSupport }
import qomplx.event.alerting.email.{ EventsMailStream, EventsMailer }
import qomplx.event.model.EventMessage
import qomplx.event.service.email.{ MandrillConfig, MandrillResponse, MandrillService }
import spray.json._

import scala.concurrent.Future

class EventMailerConsumerSpec extends BaseSpecWithActorSystem with BeforeAndAfterAll with EmbeddedKafka with JsonSupport with Eventually {

  implicit val defaultPatience: PatienceConfig = PatienceConfig(timeout = Span(120, Seconds), interval = Span(5000, Millis))

  override def beforeAll(): Unit = {
    super.beforeAll()
  }

  override def afterAll(): Unit = {
    super.afterAll()
  }

  val testObsSysId = "obs123"
  val testObsSysIdOpt: Option[String] = Some(testObsSysId)
  val extraInfoJson: JsObject = JsObject(Map("team" -> JsString("cyber")))

  val mandrillConfig = new MandrillConfig("test", "test")
  val mandrillServiceMock: MandrillService = MockitoSugar.mock[MandrillService]
  Mockito.when(mandrillServiceMock.send(ArgumentMatchers.any())).thenReturn(Future.successful(MandrillResponse(null, 200, "ok", "ok")))

  val testEvents =
    Seq(
      EventMessage(None, "GT-Event-1", "description", "Kerberos attack", 90, 897352956563853L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, testObsSysIdOpt, Some(extraInfoJson)),
      EventMessage(None, "GT-Event-2", "description", "Kerberos attack", 90, 897352956563860L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, testObsSysIdOpt),
      EventMessage(None, "GT-Event-3", "description", "Kerberos attack", 90, 897352956563870L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, testObsSysIdOpt))

  "KafkaEventConsumerMails" should {
    "Not send emails if mailer is disabled" in {
      withRunningKafka {
        val eventsMailer: EventsMailer = new EventsMailer(appConfig, mandrillConfig, mandrillServiceMock)
        val mailer = new EventsMailStream(appConfig, eventsMailer)

        eventsMailer.enabled shouldBe false
        testEvents.foreach(e => publishStringMessageToKafka(s"cyber-amp-events-$testObsSysId-1", e.toJson.compactPrint))
        val (_, f) = mailer.run()
        f.futureValue
      }
    }

    "Call mandrill API if email sending is enabled" in {
      withRunningKafka {
        val eventsMailer: EventsMailer = new EventsMailer(ConfigFactory.parseString("""events-mailer {
                                                                                      |  enabled = true
                                                                                      |  from = "noreply@qomplx.com"
                                                                                      |  recipients = "hello@mail.com"
                                                                                      |  subject = "events notification"
                                                                                      |  events-group-size = 2
                                                                                      |  pull-time-window-minutes = 1
                                                                                      |}""".stripMargin).withFallback(appConfig), mandrillConfig, mandrillServiceMock)
        val mailer = new EventsMailStream(ConfigFactory.parseString("""events-mailer {
                                            |  events-group-size = 2
                                            |}""".stripMargin).withFallback(appConfig), eventsMailer)

        eventsMailer.enabled shouldBe true
        testEvents.foreach(e => publishStringMessageToKafka(s"cyber-amp-events-$testObsSysId-1", e.toJson.compactPrint))
        val (control, _) = mailer.run()
        eventually {
          Mockito.verify(mandrillServiceMock).send(ArgumentMatchers.any())
        }
        control.shutdown()
      }
    }
  }
}
