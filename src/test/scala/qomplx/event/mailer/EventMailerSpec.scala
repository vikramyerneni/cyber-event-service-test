package qomplx.event.mailer

import java.util.Base64

import com.typesafe.config.ConfigFactory
import org.mockito.{ ArgumentMatchers, Mockito }
import org.scalatest.BeforeAndAfterAll
import org.scalatest.mockito.MockitoSugar
import qomplx.event.BaseSpecWithActorSystem
import qomplx.event.alerting.email.{ CsvExporter, EventsMailer, TemplatingEngine }
import qomplx.event.model.EventMessage
import qomplx.event.service.email._
import spray.json.{ JsObject, JsString }

import scala.concurrent.Future
import scala.jdk.CollectionConverters.asScalaSetConverter

class EventMailerSpec extends BaseSpecWithActorSystem with BeforeAndAfterAll {

  override def beforeAll(): Unit = {
    super.beforeAll()
  }

  override def afterAll(): Unit = {
    super.afterAll()
  }

  private val testObsSysId = "obs123"
  private val testObsSysIdOpt: Option[String] = Some(testObsSysId)
  private val extraInfoJson: JsObject = JsObject(Map("team" -> JsString("cyber")))

  private val testEvents =
    Seq(
      EventMessage(None, "GT-Event-1", """my super "description" with quotes""", "Kerberos attack", 90, 1598523797675L, "Kerberos pipeline", "19475653", Seq("lateral movement1"), Seq("hello", "world"), testObsSysIdOpt, Some(extraInfoJson)),
      EventMessage(None, "GT-Event-2", "description", "Kerberos attack", 59, 1568523797675L, "Kerberos pipeline", "19475654", Seq("lateral movement2"), Nil, testObsSysIdOpt),
      EventMessage(None, "GT-Event-3", "description", "Kerberos attack", 10, 1578523797675L, "Kerberos pipeline", "19475655", Seq("lateral movement3"), Nil, testObsSysIdOpt))

  "Template engine" should {

    "Render events template" in {
      val engine = new TemplatingEngine("events")
      engine.isLoaded shouldBe true

      val CompanyInfo = appConfig.getConfig("templates-static-info").entrySet()
        .asScala.map(e => e.getKey -> e.getValue.unwrapped().toString).toMap

      val result = engine.render(CompanyInfo + ("events_count" -> testEvents.size.toString))
      result.isSuccess shouldBe true
      val content = result.get.toString

      content should include(CompanyInfo.getOrElse("company_name", ""))
    }
  }

  "Mailer" should {

    val mandrillConfig = new MandrillConfig("test", "test")
    val mandrillServiceMock = MockitoSugar.mock[MandrillService]

    Mockito.when(mandrillServiceMock.send(ArgumentMatchers.any[MandrillApiCall]))
      .thenReturn(Future.successful(MandrillResponse(null, 200, "Ok", "Ok")))

    "Create Events Mailer" in {
      val mailer = new EventsMailer(ConfigFactory.parseString("""events-mailer {
                                                                |  enabled = true
                                                                |  from = "noreply@qomplx.com"
                                                                |  recipients = "hello@mail.com"
                                                                |  subject = "events notification"
                                                                |}""".stripMargin).withFallback(appConfig), mandrillConfig, mandrillServiceMock)
      mailer.enabled shouldBe true
    }

    "Disable Events Mailer (enabled flag)" in {
      val mailer = new EventsMailer(ConfigFactory.parseString("""events-mailer {
                                                                |  enabled = false
                                                                |  from = "noreply@qomplx.com"
                                                                |  recipients = "hello@mail.com"
                                                                |  subject = "events notification"
                                                                |}""".stripMargin).withFallback(appConfig), mandrillConfig, mandrillServiceMock)
      mailer.enabled shouldBe false
    }

    "Disable Events Mailer (empty recipients)" in {
      val mailer = new EventsMailer(ConfigFactory.parseString("""events-mailer {
                                                                |  enabled = true
                                                                |  from = "noreply@qomplx.com"
                                                                |  recipients = ""
                                                                |  subject = "events notification"
                                                                |}""".stripMargin).withFallback(appConfig), mandrillConfig, mandrillServiceMock)
      mailer.enabled shouldBe false
    }

    "Disable Events Mailer (trash in recipients)" in {
      val mailer = new EventsMailer(ConfigFactory.parseString("""events-mailer {
                                                                |  enabled = true
                                                                |  from = "noreply@qomplx.com"
                                                                |  recipients = "  ,  ,,,,"
                                                                |  subject = "events notification"
                                                                |}""".stripMargin).withFallback(appConfig), mandrillConfig, mandrillServiceMock)
      mailer.enabled shouldBe false
    }

    "Encode attachment" in {
      val attachment = MandrillAttachment("text/csv", "csv.file", "Hello,World")
      new String(Base64.getDecoder.decode(attachment.content)) shouldBe "Hello,World"
    }

    "Export events to CSV" in {
      val content = CsvExporter.eventsToCsv(testEvents)
      content should include(""""my super ""description"" with quotes"""")
      content should include("09/15/2019 5:03:17 AM")
      content should include("lateral movement1,hello|world")
      content should include("Medium")
    }
  }

}
