package qomplx.event.util

import java.util.UUID

import com.typesafe.config.{ Config, ConfigFactory }
import org.scalatest.{ BeforeAndAfterAll, Matchers, WordSpec }
import qomplx.event.vault.VaultManager

import scala.concurrent.ExecutionContext

final class EncryptionUtilsSpec extends WordSpec with Matchers with BeforeAndAfterAll {
  lazy val conf: Config = ConfigFactory.load()
  lazy val vaultManager = new VaultManager(conf.getConfig("vault"))
  lazy val encryptionUtils = new EncryptionUtils(Option(vaultManager))

  override def beforeAll(): Unit = {
    super.beforeAll()
    encryptionUtils.initialize()
  }

  "EncryptionUtils" should {
    "encrypt a token without Vault" in {
      val token = UUID.randomUUID().toString
      println(s"Token: $token")

      val nonVaultEncryptionUtils = new EncryptionUtils()
      val encryptedBytes = nonVaultEncryptionUtils.encryptToken(token)
      val decrypted = nonVaultEncryptionUtils.decryptToken(encryptedBytes)
      println(s"Decrypted: $decrypted")

      token should be(decrypted)
    }

    // Ignored by default, this test queries the dev instance of Vault for the keyset.
    "encrypt a token with Vault" ignore {
      val token = UUID.randomUUID().toString
      println(s"Token: $token")

      val encryptedBytes = encryptionUtils.encryptToken(token)
      val decrypted = encryptionUtils.decryptToken(encryptedBytes)
      println(s"Decrypted: $decrypted")

      token should be(decrypted)
    }

    "encrypt and decrypt deterministically" in {
      val token = UUID.randomUUID().toString
      println(s"Token: $token")

      val nonVaultEncryptionUtils = new EncryptionUtils()
      val encryptedBytes = nonVaultEncryptionUtils.encryptToken(token)
      val decrypted = nonVaultEncryptionUtils.decryptToken(encryptedBytes)
      println(s"Decrypted: $decrypted")

      token should be(decrypted)

      val encryptAgain = nonVaultEncryptionUtils.encryptToken(decrypted)
      encryptAgain.sameElements(encryptedBytes) should be(true) // <--
      val decryptedAgain = nonVaultEncryptionUtils.decryptToken(encryptAgain)
      token should be(decryptedAgain) // <--

      val encryptYetAgain = nonVaultEncryptionUtils.encryptToken(decryptedAgain)
      encryptYetAgain.sameElements(encryptedBytes) should be(true) // <--
      val decryptedYetAgain = nonVaultEncryptionUtils.decryptToken(encryptYetAgain)
      token should be(decryptedYetAgain) // <--
    }

    // This is used for printing a compact version of a keyset file.
    "print a compact keyset JSON file" ignore {
      import java.nio.file.Paths
      import akka.stream.ActorMaterializer
      import akka.actor.ActorSystem
      import akka.stream.scaladsl._
      import spray.json._

      implicit val system: ActorSystem = ActorSystem("EncryptionUtils")
      implicit val materializer: ActorMaterializer = ActorMaterializer()
      implicit val ec: ExecutionContext = system.dispatcher

      val pathName = "" // replace this with the path to which the generated keyset file is located
      val path = Paths.get(pathName)

      FileIO.fromPath(path)
        .via(JsonFraming.objectScanner(Int.MaxValue))
        .map(_.utf8String.parseJson.compactPrint)
        .runForeach(println)
    }
  }
}
