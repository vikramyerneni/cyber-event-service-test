package qomplx.event.util

import java.time.Duration
import java.time.Instant

import com.typesafe.scalalogging.StrictLogging
import org.scalatest.{ Matchers, WordSpec }

class UtilsSpec extends WordSpec with Matchers with StrictLogging {
  "Utils" should {
    "generate a unique id" in {
      val id = Utils.generateId(Some("ABC"))
      logger.debug(s"Generated event ID: $id")
      id should startWith("ABC")
    }

    "subtract hours from a millisecond epoch time" in {
      val now = Instant.now()
      val twoHoursAgo = Utils.subtractNumHours(now.toEpochMilli, 2)
      Duration.between(now, Instant.ofEpochMilli(twoHoursAgo)).toHours should be(-2)
    }

    "throw an exception if numHours is not positive when subtracting hours" in {
      an[IllegalArgumentException] should be thrownBy {
        Utils.subtractNumHours(Instant.now().toEpochMilli, 0)
      }

      an[IllegalArgumentException] should be thrownBy {
        Utils.subtractNumHours(Instant.now().toEpochMilli, -1)
      }
    }

    "subtract days from a millisecond epoch time" in {
      val now = Instant.now()
      val twoDaysAgo = Utils.subtractNumDays(now.toEpochMilli, 2)
      Duration.between(now, Instant.ofEpochMilli(twoDaysAgo)).toDays should be(-2)
    }

    "throw an exception if numDays is not positive when subtracting days" in {
      an[IllegalArgumentException] should be thrownBy {
        Utils.subtractNumDays(Instant.now().toEpochMilli, 0)
      }

      an[IllegalArgumentException] should be thrownBy {
        Utils.subtractNumDays(Instant.now().toEpochMilli, -1)
      }
    }

    "divide a one-day period into eight three-hour segments with nine timestamps" in {
      val now = Instant.now()
      val segments = Utils.segmentTimePeriod(now.toEpochMilli, 1)
      logger.info(s"Segments for a one-day period: $segments")
      segments should have size 9

      val instants = segments.map(Instant.ofEpochMilli(_))
      instants.sliding(2).foreach {
        case List(a, b) => // from an epoch timestamp perspective, a is older than b (i.e., from a literal perspective, a < b)
          Duration.between(a, b).toHours should be(3)
      }
    }

    "divide a three-day period into three one-day segments with four timestamps" in {
      val now = Instant.now()
      val segments = Utils.segmentTimePeriod(now.toEpochMilli, 3)
      logger.info(s"Segments for a three-day period: $segments")
      segments should have size 4

      val instants = segments.map(Instant.ofEpochMilli(_))
      instants.sliding(2).foreach {
        case List(a, b) => // from an epoch timestamp perspective, a is older than b (i.e., from a literal perspective, a < b)
          Duration.between(a, b).toDays should be(1)
      }
    }

    "divide a seven-day period into seven one-day segments with eight timestamps" in {
      val now = Instant.now()
      val segments = Utils.segmentTimePeriod(now.toEpochMilli, 7)
      logger.info(s"Segments for a seven-day period: $segments")
      segments should have size 8

      val instants = segments.map(Instant.ofEpochMilli(_))
      instants.sliding(2).foreach {
        case List(a, b) => // from an epoch timestamp perspective, a is older than b (i.e., from a literal perspective, a < b)
          Duration.between(a, b).toDays should be(1)
      }
    }

    "divide a thirty-day period into five six-day segments with six timestamps" in {
      val now = Instant.now()
      val segments = Utils.segmentTimePeriod(now.toEpochMilli, 30)
      logger.info(s"Segments for a thirty-day period: $segments")
      segments should have size 6

      val instants = segments.map(Instant.ofEpochMilli(_))
      instants.sliding(2).foreach {
        case List(a, b) => // from an epoch timestamp perspective, a is older than b (i.e., from a literal perspective, a < b)
          Duration.between(a, b).toDays should be(6)
      }
    }

    "throw an exception if numDays is not 1, 3, 7, or 30 when segmenting a time period" in {
      an[IllegalArgumentException] should be thrownBy {
        Utils.segmentTimePeriod(Instant.now.toEpochMilli, 0)
      }
    }

    "create count statements with no additional clause" in {
      val now = Instant.now()
      val countStatement = Utils.createCountStatements(now.toEpochMilli, 3)

      // this is not really a test but it logs the count statement for inspection
      logger.info(s"Count statement with no additional clause: $countStatement")
    }

    "create count statements with an additional clause" in {
      val now = Instant.now()
      val countStatement = Utils.createCountStatements(now.toEpochMilli, 3, Option("and assignee is null and status in ('NEW', 'OPEN')"))

      // this is not really a test but it logs the count statement for inspection
      logger.info(s"Count statement with an additional clause: $countStatement")
    }
  }
}
