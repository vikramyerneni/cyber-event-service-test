package qomplx.event

import com.dimafeng.testcontainers.PostgreSQLContainer
import com.typesafe.config.ConfigFactory
import qomplx.event.config.db.{ LiquibaseUtils, PostgresConnection }
import org.scalatest.{ BeforeAndAfterAll, Suite }

trait IntegrationTestContainers extends BeforeAndAfterAll { self: Suite =>
  val config = ConfigFactory.load()

  private val (dbName, userName, pw) = ("test-database", "myuser", "mypassword")

  private lazy val postgresContainer = PostgreSQLContainer(
    "postgres:11.2",
    databaseName = dbName,
    username = userName,
    password = pw,
    mountPostgresDataToTmpfs = true)

  protected lazy val postgresConnection: PostgresConnection = new PostgresConnection(
    host = postgresContainer.containerIpAddress,
    port = postgresContainer.mappedPort(5432).toString,
    databaseName = dbName,
    userName = userName,
    password = pw)

  override def beforeAll(): Unit = {
    super.beforeAll()
    postgresContainer.start()
    LiquibaseUtils.runMigration(config, postgresConnection)
  }

  override protected def afterAll(): Unit = {
    postgresContainer.stop()
    super.afterAll()
  }
}
