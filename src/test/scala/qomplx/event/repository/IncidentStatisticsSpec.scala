package qomplx.event.repository

import java.time.Instant
import java.util.UUID

import org.scalatest.BeforeAndAfter
import qomplx.event.model._
import qomplx.event.routes._
import qomplx.event.util.Utils
import qomplx.event.util.SlickPostgresProfile.api._
import qomplx.event.BaseSpecWithMigrations

import scala.concurrent.Future
import qomplx.event.util.Severity

/**
 * Tests for the statistics-related methods in [[qomplx.event.repository.IncidentRepository]].
 * [[qomplx.event.repository.IncidentRepositorySpec]] is getting unwieldy, hence the use of this
 * separate class.
 */
final class IncidentStatisticsSpec extends BaseSpecWithMigrations with BeforeAndAfter {
  private lazy val db = postgresConnection.database
  private lazy val eventRepo = new EventRepository(db)
  private lazy val incidentRepo = new IncidentRepository(db)
  private lazy val repoUtils = new RepositorySpecUtils(db, logger)

  private val tagId = UUID.randomUUID().toString
  private val observedSystemIdOpt = Some(tagId)

  // all incidents must be associated with an event; we'll use the same event for the incidents in this test
  private val eventId = "EVT-test1"
  private val eventMessage = EventMessage(Option(eventId), "Event1", "test event", "Kerberos attack", 90, Instant.now().toEpochMilli, "Kerberos pipeline", "pipelineId123", Seq("lateral movement"), Nil, observedSystemIdOpt)
  private val event = Event(Option(eventId), "Event1", "test event", "Kerberos attack", 90, Instant.now().toEpochMilli, "Kerberos pipeline", "pipelineId123", Seq("lateral movement"), Nil, tagId)

  // templates with which to create incidents
  val unassignedIncident = Incident(id = Some("INC-unassigned-template"), title = "UnassignedIncidentTemplate", severity = 99, reporterId = 1, tagId = tagId, createdBy = 1, lastModifiedBy = 1)
  val assignedIncident = Incident(id = Some("INC-assigned-template"), title = "AssignedIncidentTemplate", severity = 99, reporterId = 1, tagId = tagId, assigneeId = Option(5), createdBy = 1, lastModifiedBy = 1)
  val closedIncident = Incident(id = Some("INC-closed-template"), title = "ClosedIncidentTemplate", status = IncidentStatus.CLOSED, severity = 99, reporterId = 1, tagId = tagId, assigneeId = Option(7), createdBy = 1, lastModifiedBy = 1)

  override def beforeAll(): Unit = {
    super.beforeAll()

    val numEventsAdded = eventRepo.add(List(eventMessage)).futureValue
    numEventsAdded should be(1)
  }

  // delete the event-incident mappings and the incidents after each test
  after {
    val numMappingsDeleted = repoUtils.deleteEventIncidentMappings(List(event), db).futureValue
    logger.info(s"Deleted $numMappingsDeleted mappings")

    val numIncidentsDeleted = repoUtils.deleteIncidentsForObservedSystem(tagId, db).futureValue
    logger.info(s"Deleted $numIncidentsDeleted incidents")
  }

  override def afterAll(): Unit = {
    val numEventsDeleted = repoUtils.deleteEvents(List(event), db).futureValue
    logger.info(s"Deleted $numEventsDeleted events")

    super.afterAll()
  }

  "IncidentRepository's statistics API" should {
    "get a statistics summary of all types of incidents" in {
      val now = Instant.now().toEpochMilli
      val fiveDaysAgo = Utils.subtractNumDays(now, 5)
      val twoDaysAgo = Utils.subtractNumDays(now, 2)
      val oneDayAgo = Utils.subtractNumDays(now, 1)

      // unassigned incidents with a database status of "NEW" or "OPEN"
      val unassignedIncidents =
        Vector(
          unassignedIncident.copy(id = Some("INC-unassigned1"), title = "UnassignedIncident1", status = IncidentStatus.NEW, createdOn = (oneDayAgo - 1000L)),
          unassignedIncident.copy(id = Some("INC-unassigned2"), title = "UnassignedIncident2", status = IncidentStatus.OPEN, createdOn = (oneDayAgo - 1000L)),
          unassignedIncident.copy(id = Some("INC-unassigned3"), title = "UnassignedIncident3", status = IncidentStatus.NEW, createdOn = (now - 1000L)),
          unassignedIncident.copy(id = Some("INC-unassigned4"), title = "UnassignedIncident4", status = IncidentStatus.OPEN, createdOn = fiveDaysAgo))

      // assigned incidents with a database status of "NEW" or "OPEN"
      val assignedIncidents =
        Vector(
          assignedIncident.copy(id = Some("INC-assigned1"), title = "AssignedIncident1", status = IncidentStatus.NEW, createdOn = (oneDayAgo - 1000L)),
          assignedIncident.copy(id = Some("INC-assigned2"), title = "AssignedIncident2", status = IncidentStatus.OPEN, createdOn = (now - 1000L)),
          assignedIncident.copy(id = Some("INC-assigned3"), title = "AssignedIncident3", status = IncidentStatus.NEW, createdOn = (now - 1000L)),
          assignedIncident.copy(id = Some("INC-assigned4"), title = "AssignedIncident4", status = IncidentStatus.OPEN, createdOn = fiveDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned5"), title = "AssignedIncident5", status = IncidentStatus.NEW, createdOn = fiveDaysAgo))

      val closedIncidents =
        Vector(
          closedIncident.copy(id = Some("INC-closed1"), title = "ClosedIncident1", createdOn = (oneDayAgo - 1000L)),
          closedIncident.copy(id = Some("INC-closed2"), title = "ClosedIncident2", createdOn = (oneDayAgo - 1000L)),
          closedIncident.copy(id = Some("INC-closed3"), title = "ClosedIncident3", createdOn = (oneDayAgo - 1000L)),
          closedIncident.copy(id = Some("INC-closed4"), title = "ClosedIncident4", createdOn = (now - 1000L)),
          closedIncident.copy(id = Some("INC-closed5"), title = "ClosedIncident5", createdOn = (now - 1000L)),
          closedIncident.copy(id = Some("INC-closed6"), title = "ClosedIncident6", createdOn = fiveDaysAgo))

      val numUnassignedIncidentsAdded = Future.sequence(unassignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numUnassignedIncidentsAdded should be(4)

      val numAssignedIncidentsAdded = Future.sequence(assignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numAssignedIncidentsAdded should be(5)

      val numClosedIncidentsAdded = Future.sequence(closedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numClosedIncidentsAdded should be(6)

      val summary = incidentRepo.getStatisticsSummary(tagId, now).futureValue
      summary.requestTimestampMillis should be(now)

      summary.unassigned.count should be(numUnassignedIncidentsAdded)
      val unassignedDiff = summary.unassigned.recentDiff
      unassignedDiff should be(-1)

      summary.assigned.count should be(numAssignedIncidentsAdded)
      val assignedDiff = summary.assigned.recentDiff
      assignedDiff should be(1)

      summary.closed.count should be(numClosedIncidentsAdded)
      val closedDiff = summary.closed.recentDiff
      closedDiff should be(-1)

      summary.all.count should be(numUnassignedIncidentsAdded + numAssignedIncidentsAdded + numClosedIncidentsAdded)
      summary.all.recentDiff should be(unassignedDiff + assignedDiff + closedDiff)
    }

    "throw an exception if numDays is not 1, 3, 7, or 30 when retrieving severity statistics" in {
      an[IllegalArgumentException] should be thrownBy {
        incidentRepo.getSeverityStatistics(tagId, Instant.now.toEpochMilli, IncidentStatusForStatistics.All, 0)
      }
    }

    "get severity statistics for unassigned incidents" in new SeverityFixture {
      val unassignedIncidents =
        Vector(
          unassignedIncident.copy(id = Some("INC-unassigned1"), title = "UnassignedIncident1", status = IncidentStatus.NEW, severity = lowSeverity, createdOn = (now - 1000L)),
          unassignedIncident.copy(id = Some("INC-unassigned2"), title = "UnassignedIncident2", status = IncidentStatus.OPEN, severity = highSeverity, createdOn = twoDaysAgo),
          unassignedIncident.copy(id = Some("INC-unassigned3"), title = "UnassignedIncident3", status = IncidentStatus.NEW, severity = mediumSeverity, createdOn = fiveDaysAgo),
          unassignedIncident.copy(id = Some("INC-unassigned4"), title = "UnassignedIncident4", status = IncidentStatus.OPEN, severity = mediumSeverity, createdOn = fiveDaysAgo),
          unassignedIncident.copy(id = Some("INC-unassigned5"), title = "UnassignedIncident5", status = IncidentStatus.NEW, severity = highSeverity, createdOn = twentyDaysAgo),
          unassignedIncident.copy(id = Some("INC-unassigned6"), title = "UnassignedIncident6", status = IncidentStatus.OPEN, severity = lowSeverity, createdOn = twentyDaysAgo),
          unassignedIncident.copy(id = Some("INC-unassigned7"), title = "UnassignedIncident7", status = IncidentStatus.NEW, severity = highSeverity, createdOn = twentyDaysAgo),
          unassignedIncident.copy(id = Some("INC-unassigned8"), title = "UnassignedIncident8", status = IncidentStatus.OPEN, severity = lowSeverity, createdOn = fortyDaysAgo))

      val numUnassignedIncidentsAdded = Future.sequence(unassignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numUnassignedIncidentsAdded should be(8)

      verifySeverityStats(IncidentStatusForStatistics.Unassigned, now)
    }

    "get severity statistics for assigned incidents" in new SeverityFixture {
      val assignedIncidents =
        Vector(
          assignedIncident.copy(id = Some("INC-assigned1"), title = "AssignedIncident1", status = IncidentStatus.NEW, severity = lowSeverity, createdOn = (now - 1000L)),
          assignedIncident.copy(id = Some("INC-assigned2"), title = "AssignedIncident2", status = IncidentStatus.OPEN, severity = highSeverity, createdOn = twoDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned3"), title = "AssignedIncident3", status = IncidentStatus.NEW, severity = mediumSeverity, createdOn = fiveDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned4"), title = "AssignedIncident4", status = IncidentStatus.OPEN, severity = mediumSeverity, createdOn = fiveDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned5"), title = "AssignedIncident5", status = IncidentStatus.NEW, severity = highSeverity, createdOn = twentyDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned6"), title = "AssignedIncident6", status = IncidentStatus.OPEN, severity = lowSeverity, createdOn = twentyDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned7"), title = "AssignedIncident7", status = IncidentStatus.NEW, severity = highSeverity, createdOn = twentyDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned8"), title = "AssignedIncident8", status = IncidentStatus.OPEN, severity = lowSeverity, createdOn = fortyDaysAgo))

      val numAssignedIncidentsAdded = Future.sequence(assignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numAssignedIncidentsAdded should be(8)

      verifySeverityStats(IncidentStatusForStatistics.Assigned, now)
    }

    "get severity statistics for closed incidents" in new SeverityFixture {
      val closedIncidents =
        Vector(
          closedIncident.copy(id = Some("INC-closed1"), title = "ClosedIncident1", severity = lowSeverity, createdOn = (now - 1000L)),
          closedIncident.copy(id = Some("INC-closed2"), title = "ClosedIncident2", severity = highSeverity, createdOn = twoDaysAgo),
          closedIncident.copy(id = Some("INC-closed3"), title = "ClosedIncident3", severity = mediumSeverity, createdOn = fiveDaysAgo),
          closedIncident.copy(id = Some("INC-closed4"), title = "ClosedIncident4", severity = mediumSeverity, createdOn = fiveDaysAgo),
          closedIncident.copy(id = Some("INC-closed5"), title = "ClosedIncident5", severity = highSeverity, createdOn = twentyDaysAgo),
          closedIncident.copy(id = Some("INC-closed6"), title = "ClosedIncident6", severity = lowSeverity, createdOn = twentyDaysAgo),
          closedIncident.copy(id = Some("INC-closed7"), title = "ClosedIncident7", severity = highSeverity, createdOn = twentyDaysAgo),
          closedIncident.copy(id = Some("INC-closed8"), title = "ClosedIncident8", severity = lowSeverity, createdOn = fortyDaysAgo))

      val numClosedIncidentsAdded = Future.sequence(closedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numClosedIncidentsAdded should be(8)

      verifySeverityStats(IncidentStatusForStatistics.Closed, now)
    }

    "get severity statistics for all incidents" in new SeverityFixture {
      // this test uses a mix of unassigned, assigned, and closed incidents
      val allIncidents =
        Vector(
          closedIncident.copy(id = Some("INC-closed1"), title = "ClosedIncident1", severity = lowSeverity, createdOn = (now - 1000L)),
          assignedIncident.copy(id = Some("INC-assigned2"), title = "AssignedIncident2", status = IncidentStatus.OPEN, severity = highSeverity, createdOn = twoDaysAgo),
          unassignedIncident.copy(id = Some("INC-unassigned3"), title = "UnassignedIncident3", status = IncidentStatus.NEW, severity = mediumSeverity, createdOn = fiveDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned4"), title = "AssignedIncident4", status = IncidentStatus.OPEN, severity = mediumSeverity, createdOn = fiveDaysAgo),
          closedIncident.copy(id = Some("INC-closed5"), title = "ClosedIncident5", severity = highSeverity, createdOn = twentyDaysAgo),
          unassignedIncident.copy(id = Some("INC-unassigned6"), title = "UnassignedIncident6", status = IncidentStatus.OPEN, severity = lowSeverity, createdOn = twentyDaysAgo),
          closedIncident.copy(id = Some("INC-closed7"), title = "ClosedIncident7", severity = highSeverity, createdOn = twentyDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned8"), title = "AssignedIncident8", status = IncidentStatus.OPEN, severity = lowSeverity, createdOn = fortyDaysAgo))

      val numAllIncidentsAdded = Future.sequence(allIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numAllIncidentsAdded should be(8)

      verifySeverityStats(IncidentStatusForStatistics.All, now)
    }

    "get one-day statistics details for incidents" in {
      val now = Instant.now().toEpochMilli
      val twoHoursAgo = Utils.subtractNumHours(now, 2)
      val fiveHoursAgo = Utils.subtractNumHours(now, 5)
      val eightHoursAgo = Utils.subtractNumHours(now, 8)
      val elevenHoursAgo = Utils.subtractNumHours(now, 11)
      val fourteenHoursAgo = Utils.subtractNumHours(now, 14)
      val seventeenHoursAgo = Utils.subtractNumHours(now, 17)
      val twentyHoursAgo = Utils.subtractNumHours(now, 20)
      val twentyThreeHoursAgo = Utils.subtractNumHours(now, 23)

      val unassignedIncidents =
        Vector(
          unassignedIncident.copy(id = Some("INC-unassigned1"), title = "UnassignedIncident1", status = IncidentStatus.NEW, createdOn = twoHoursAgo),
          unassignedIncident.copy(id = Some("INC-unassigned2"), title = "UnassignedIncident2", status = IncidentStatus.OPEN, createdOn = fiveHoursAgo),
          unassignedIncident.copy(id = Some("INC-unassigned3"), title = "UnassignedIncident3", status = IncidentStatus.NEW, createdOn = eightHoursAgo),
          unassignedIncident.copy(id = Some("INC-unassigned4"), title = "UnassignedIncident4", status = IncidentStatus.OPEN, createdOn = elevenHoursAgo),
          unassignedIncident.copy(id = Some("INC-unassigned5"), title = "UnassignedIncident5", status = IncidentStatus.NEW, createdOn = fourteenHoursAgo),
          unassignedIncident.copy(id = Some("INC-unassigned6"), title = "UnassignedIncident6", status = IncidentStatus.OPEN, createdOn = seventeenHoursAgo),
          unassignedIncident.copy(id = Some("INC-unassigned7"), title = "UnassignedIncident7", status = IncidentStatus.NEW, createdOn = twentyHoursAgo),
          unassignedIncident.copy(id = Some("INC-unassigned8"), title = "UnassignedIncident8", status = IncidentStatus.OPEN, createdOn = twentyThreeHoursAgo),
          unassignedIncident.copy(id = Some("INC-unassigned9"), title = "UnassignedIncident9", status = IncidentStatus.OPEN, createdOn = twentyThreeHoursAgo - 1000L))

      val numUnassignedIncidentsAdded = Future.sequence(unassignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numUnassignedIncidentsAdded should be(9)

      val assignedIncidents =
        Vector(
          assignedIncident.copy(id = Some("INC-assigned1"), title = "AssignedIncident1", status = IncidentStatus.NEW, createdOn = twoHoursAgo),
          assignedIncident.copy(id = Some("INC-assigned2"), title = "AssignedIncident2", status = IncidentStatus.OPEN, createdOn = fiveHoursAgo),
          assignedIncident.copy(id = Some("INC-assigned3"), title = "AssignedIncident3", status = IncidentStatus.NEW, createdOn = eightHoursAgo),
          assignedIncident.copy(id = Some("INC-assigned4"), title = "AssignedIncident4", status = IncidentStatus.OPEN, createdOn = elevenHoursAgo),
          assignedIncident.copy(id = Some("INC-assigned5"), title = "AssignedIncident5", status = IncidentStatus.NEW, createdOn = fourteenHoursAgo),
          assignedIncident.copy(id = Some("INC-assigned6"), title = "AssignedIncident6", status = IncidentStatus.OPEN, createdOn = seventeenHoursAgo),
          assignedIncident.copy(id = Some("INC-assigned7"), title = "AssignedIncident7", status = IncidentStatus.NEW, createdOn = twentyHoursAgo),
          assignedIncident.copy(id = Some("INC-assigned8"), title = "AssignedIncident8", status = IncidentStatus.OPEN, createdOn = twentyThreeHoursAgo),
          assignedIncident.copy(id = Some("INC-assigned9"), title = "AssignedIncident9", status = IncidentStatus.OPEN, createdOn = twentyThreeHoursAgo - 1000L))

      val numAssignedIncidentsAdded = Future.sequence(assignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numAssignedIncidentsAdded should be(9)

      val closedIncidents =
        Vector(
          closedIncident.copy(id = Some("INC-closed1"), title = "ClosedIncident1", createdOn = twoHoursAgo),
          closedIncident.copy(id = Some("INC-closed2"), title = "ClosedIncident2", createdOn = fiveHoursAgo),
          closedIncident.copy(id = Some("INC-closed3"), title = "ClosedIncident3", createdOn = eightHoursAgo),
          closedIncident.copy(id = Some("INC-closed4"), title = "ClosedIncident4", createdOn = elevenHoursAgo),
          closedIncident.copy(id = Some("INC-closed5"), title = "ClosedIncident5", createdOn = fourteenHoursAgo),
          closedIncident.copy(id = Some("INC-closed6"), title = "ClosedIncident6", createdOn = seventeenHoursAgo),
          closedIncident.copy(id = Some("INC-closed7"), title = "ClosedIncident7", createdOn = twentyHoursAgo),
          closedIncident.copy(id = Some("INC-closed8"), title = "ClosedIncident8", createdOn = twentyThreeHoursAgo),
          closedIncident.copy(id = Some("INC-closed9"), title = "ClosedIncident9", createdOn = twentyThreeHoursAgo - 1000L))

      val numClosedIncidentsAdded = Future.sequence(closedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numClosedIncidentsAdded should be(9)

      verifyOneDayDetails(IncidentStatusForStatistics.Unassigned, now)
      verifyOneDayDetails(IncidentStatusForStatistics.Assigned, now)
      verifyOneDayDetails(IncidentStatusForStatistics.Closed, now)

      val allDetails = incidentRepo.getStatisticsDetails(tagId, now, IncidentStatusForStatistics.All, 1).futureValue
      allDetails.requestTimestampMillis should be(now)
      allDetails.status should be(IncidentStatusForStatistics.All.entryName)
      allDetails.numDays should be(1)
      allDetails.granularity should be(IncidentRepository.granularityMap(1))
      allDetails.counts should be(Vector(6, 3, 3, 3, 3, 3, 3, 3))
    }

    "get three-day statistics details for incidents" in {
      val now = Instant.now().toEpochMilli
      val oneDayAgo = Utils.subtractNumDays(now, 1)
      val twoDaysAgo = Utils.subtractNumDays(now, 2)
      val threeDaysAgo = Utils.subtractNumDays(now, 3)

      val unassignedIncidents =
        Vector(
          unassignedIncident.copy(id = Some("INC-unassigned1"), title = "UnassignedIncident1", status = IncidentStatus.NEW, createdOn = now - 1000L),
          unassignedIncident.copy(id = Some("INC-unassigned2"), title = "UnassignedIncident2", status = IncidentStatus.OPEN, createdOn = oneDayAgo - 1000L),
          unassignedIncident.copy(id = Some("INC-unassigned3"), title = "UnassignedIncident3", status = IncidentStatus.NEW, createdOn = twoDaysAgo - 1000L),
          unassignedIncident.copy(id = Some("INC-unassigned4"), title = "UnassignedIncident4", status = IncidentStatus.OPEN, createdOn = threeDaysAgo))

      val numUnassignedIncidentsAdded = Future.sequence(unassignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numUnassignedIncidentsAdded should be(4)

      val assignedIncidents =
        Vector(
          assignedIncident.copy(id = Some("INC-assigned1"), title = "AssignedIncident1", status = IncidentStatus.NEW, createdOn = now - 1000L),
          assignedIncident.copy(id = Some("INC-assigned2"), title = "AssignedIncident2", status = IncidentStatus.OPEN, createdOn = oneDayAgo - 1000L),
          assignedIncident.copy(id = Some("INC-assigned3"), title = "AssignedIncident3", status = IncidentStatus.NEW, createdOn = twoDaysAgo - 1000L),
          assignedIncident.copy(id = Some("INC-assigned4"), title = "AssignedIncident4", status = IncidentStatus.OPEN, createdOn = threeDaysAgo))

      val numAssignedIncidentsAdded = Future.sequence(assignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numAssignedIncidentsAdded should be(4)

      val closedIncidents =
        Vector(
          closedIncident.copy(id = Some("INC-closed1"), title = "ClosedIncident1", createdOn = now - 1000L),
          closedIncident.copy(id = Some("INC-closed2"), title = "ClosedIncident2", createdOn = oneDayAgo - 1000L),
          closedIncident.copy(id = Some("INC-closed3"), title = "ClosedIncident3", createdOn = twoDaysAgo - 1000L),
          closedIncident.copy(id = Some("INC-closed4"), title = "ClosedIncident4", createdOn = threeDaysAgo))

      val numClosedIncidentsAdded = Future.sequence(closedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numClosedIncidentsAdded should be(4)

      verifyThreeDayDetails(IncidentStatusForStatistics.Unassigned, now)
      verifyThreeDayDetails(IncidentStatusForStatistics.Assigned, now)
      verifyThreeDayDetails(IncidentStatusForStatistics.Closed, now)

      val allDetails = incidentRepo.getStatisticsDetails(tagId, now, IncidentStatusForStatistics.All, 3).futureValue
      allDetails.requestTimestampMillis should be(now)
      allDetails.status should be(IncidentStatusForStatistics.All.entryName)
      allDetails.numDays should be(3)
      allDetails.granularity should be(IncidentRepository.granularityMap(3))
      allDetails.counts should be(Vector(6, 3, 3))
    }

    "get seven-day statistics details for incidents" in {
      val now = Instant.now().toEpochMilli
      val oneDayAgo = Utils.subtractNumDays(now, 1)
      val twoDaysAgo = Utils.subtractNumDays(now, 2)
      val threeDaysAgo = Utils.subtractNumDays(now, 3)
      val fourDaysAgo = Utils.subtractNumDays(now, 4)
      val fiveDaysAgo = Utils.subtractNumDays(now, 5)
      val sixDaysAgo = Utils.subtractNumDays(now, 6)
      val sevenDaysAgo = Utils.subtractNumDays(now, 7)

      val unassignedIncidents =
        Vector(
          unassignedIncident.copy(id = Some("INC-unassigned1"), title = "UnassignedIncident1", status = IncidentStatus.NEW, createdOn = now - 1000L),
          unassignedIncident.copy(id = Some("INC-unassigned2"), title = "UnassignedIncident2", status = IncidentStatus.OPEN, createdOn = oneDayAgo - 1000L),
          unassignedIncident.copy(id = Some("INC-unassigned3"), title = "UnassignedIncident3", status = IncidentStatus.NEW, createdOn = twoDaysAgo - 1000L),
          unassignedIncident.copy(id = Some("INC-unassigned4"), title = "UnassignedIncident4", status = IncidentStatus.OPEN, createdOn = threeDaysAgo - 1000L),
          unassignedIncident.copy(id = Some("INC-unassigned5"), title = "UnassignedIncident5", status = IncidentStatus.NEW, createdOn = fourDaysAgo - 1000L),
          unassignedIncident.copy(id = Some("INC-unassigned6"), title = "UnassignedIncident6", status = IncidentStatus.OPEN, createdOn = fiveDaysAgo - 1000L),
          unassignedIncident.copy(id = Some("INC-unassigned7"), title = "UnassignedIncident7", status = IncidentStatus.NEW, createdOn = sixDaysAgo - 1000L),
          unassignedIncident.copy(id = Some("INC-unassigned8"), title = "UnassignedIncident8", status = IncidentStatus.OPEN, createdOn = sevenDaysAgo))

      val numUnassignedIncidentsAdded = Future.sequence(unassignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numUnassignedIncidentsAdded should be(8)

      val assignedIncidents =
        Vector(
          assignedIncident.copy(id = Some("INC-assigned1"), title = "AssignedIncident1", status = IncidentStatus.NEW, createdOn = now - 1000L),
          assignedIncident.copy(id = Some("INC-assigned2"), title = "AssignedIncident2", status = IncidentStatus.OPEN, createdOn = oneDayAgo - 1000L),
          assignedIncident.copy(id = Some("INC-assigned3"), title = "AssignedIncident3", status = IncidentStatus.NEW, createdOn = twoDaysAgo - 1000L),
          assignedIncident.copy(id = Some("INC-assigned4"), title = "AssignedIncident4", status = IncidentStatus.OPEN, createdOn = threeDaysAgo - 1000L),
          assignedIncident.copy(id = Some("INC-assigned5"), title = "AssignedIncident5", status = IncidentStatus.NEW, createdOn = fourDaysAgo - 1000L),
          assignedIncident.copy(id = Some("INC-assigned6"), title = "AssignedIncident6", status = IncidentStatus.OPEN, createdOn = fiveDaysAgo - 1000L),
          assignedIncident.copy(id = Some("INC-assigned7"), title = "AssignedIncident7", status = IncidentStatus.NEW, createdOn = sixDaysAgo - 1000L),
          assignedIncident.copy(id = Some("INC-assigned8"), title = "AssignedIncident8", status = IncidentStatus.OPEN, createdOn = sevenDaysAgo))

      val numAssignedIncidentsAdded = Future.sequence(assignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numAssignedIncidentsAdded should be(8)

      val closedIncidents =
        Vector(
          closedIncident.copy(id = Some("INC-closed1"), title = "ClosedIncident1", createdOn = now - 1000L),
          closedIncident.copy(id = Some("INC-closed2"), title = "ClosedIncident2", createdOn = oneDayAgo - 1000L),
          closedIncident.copy(id = Some("INC-closed3"), title = "ClosedIncident3", createdOn = twoDaysAgo - 1000L),
          closedIncident.copy(id = Some("INC-closed4"), title = "ClosedIncident4", createdOn = threeDaysAgo - 1000L),
          closedIncident.copy(id = Some("INC-closed5"), title = "ClosedIncident5", createdOn = fourDaysAgo - 1000L),
          closedIncident.copy(id = Some("INC-closed6"), title = "ClosedIncident6", createdOn = fiveDaysAgo - 1000L),
          closedIncident.copy(id = Some("INC-closed7"), title = "ClosedIncident7", createdOn = sixDaysAgo - 1000L),
          closedIncident.copy(id = Some("INC-closed8"), title = "ClosedIncident8", createdOn = sevenDaysAgo))

      val numClosedIncidentsAdded = Future.sequence(closedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numClosedIncidentsAdded should be(8)

      verifySevenDayDetails(IncidentStatusForStatistics.Unassigned, now)
      verifySevenDayDetails(IncidentStatusForStatistics.Assigned, now)
      verifySevenDayDetails(IncidentStatusForStatistics.Closed, now)

      val allDetails = incidentRepo.getStatisticsDetails(tagId, now, IncidentStatusForStatistics.All, 7).futureValue
      allDetails.requestTimestampMillis should be(now)
      allDetails.status should be(IncidentStatusForStatistics.All.entryName)
      allDetails.numDays should be(7)
      allDetails.granularity should be(IncidentRepository.granularityMap(7))
      allDetails.counts should be(Vector(6, 3, 3, 3, 3, 3, 3))
    }

    "get 30-day statistics details for incidents" in {
      val now = Instant.now().toEpochMilli
      val oneDayAgo = Utils.subtractNumDays(now, 1)
      val sevenDaysAgo = Utils.subtractNumDays(now, 7)
      val thirteenDaysAgo = Utils.subtractNumDays(now, 13)
      val nineteenDaysAgo = Utils.subtractNumDays(now, 19)
      val twentySevenDaysAgo = Utils.subtractNumDays(now, 27)
      val twentyNineDaysAgo = Utils.subtractNumDays(now, 29)

      val unassignedIncidents =
        Vector(
          unassignedIncident.copy(id = Some("INC-unassigned1"), title = "UnassignedIncident1", status = IncidentStatus.NEW, createdOn = oneDayAgo),
          unassignedIncident.copy(id = Some("INC-unassigned2"), title = "UnassignedIncident2", status = IncidentStatus.OPEN, createdOn = sevenDaysAgo),
          unassignedIncident.copy(id = Some("INC-unassigned3"), title = "UnassignedIncident3", status = IncidentStatus.NEW, createdOn = thirteenDaysAgo),
          unassignedIncident.copy(id = Some("INC-unassigned4"), title = "UnassignedIncident4", status = IncidentStatus.OPEN, createdOn = nineteenDaysAgo),
          unassignedIncident.copy(id = Some("INC-unassigned5"), title = "UnassignedIncident5", status = IncidentStatus.NEW, createdOn = twentySevenDaysAgo),
          unassignedIncident.copy(id = Some("INC-unassigned6"), title = "UnassignedIncident6", status = IncidentStatus.OPEN, createdOn = twentyNineDaysAgo))

      val numUnassignedIncidentsAdded = Future.sequence(unassignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numUnassignedIncidentsAdded should be(6)

      val assignedIncidents =
        Vector(
          assignedIncident.copy(id = Some("INC-assigned1"), title = "AssignedIncident1", status = IncidentStatus.NEW, createdOn = oneDayAgo),
          assignedIncident.copy(id = Some("INC-assigned2"), title = "AssignedIncident2", status = IncidentStatus.OPEN, createdOn = sevenDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned3"), title = "AssignedIncident3", status = IncidentStatus.NEW, createdOn = thirteenDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned4"), title = "AssignedIncident4", status = IncidentStatus.OPEN, createdOn = nineteenDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned5"), title = "AssignedIncident5", status = IncidentStatus.NEW, createdOn = twentySevenDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned6"), title = "AssignedIncident6", status = IncidentStatus.OPEN, createdOn = twentyNineDaysAgo))

      val numAssignedIncidentsAdded = Future.sequence(assignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numAssignedIncidentsAdded should be(6)

      val closedIncidents =
        Vector(
          closedIncident.copy(id = Some("INC-closed1"), title = "ClosedIncident1", createdOn = oneDayAgo),
          closedIncident.copy(id = Some("INC-closed2"), title = "ClosedIncident2", createdOn = sevenDaysAgo),
          closedIncident.copy(id = Some("INC-closed3"), title = "ClosedIncident3", createdOn = thirteenDaysAgo),
          closedIncident.copy(id = Some("INC-closed4"), title = "ClosedIncident4", createdOn = nineteenDaysAgo),
          closedIncident.copy(id = Some("INC-closed5"), title = "ClosedIncident5", createdOn = twentySevenDaysAgo),
          closedIncident.copy(id = Some("INC-closed6"), title = "ClosedIncident6", createdOn = twentyNineDaysAgo))

      val numClosedIncidentsAdded = Future.sequence(closedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numClosedIncidentsAdded should be(6)

      verifyThirtyDayDetails(IncidentStatusForStatistics.Unassigned, now)
      verifyThirtyDayDetails(IncidentStatusForStatistics.Assigned, now)
      verifyThirtyDayDetails(IncidentStatusForStatistics.Closed, now)

      val allDetails = incidentRepo.getStatisticsDetails(tagId, now, IncidentStatusForStatistics.All, 30).futureValue
      allDetails.requestTimestampMillis should be(now)
      allDetails.status should be(IncidentStatusForStatistics.All.entryName)
      allDetails.numDays should be(30)
      allDetails.granularity should be(IncidentRepository.granularityMap(30))
      allDetails.counts should be(Vector(6, 3, 3, 3, 3))
    }

    "throw an exception if numDays is not 1, 3, 7, or 30 when retrieving statistics details" in {
      an[IllegalArgumentException] should be thrownBy {
        incidentRepo.getStatisticsDetails(tagId, Instant.now.toEpochMilli, IncidentStatusForStatistics.All, 0)
      }
    }
  }

  "IncidentRepository's incident API" should {
    val defaultParams = GetIncidentsReq(lmt = 10, ofs = 0, assignee = None, search = None)

    "get incidents that were created in the past day" in {
      val now = Instant.now().toEpochMilli
      val twoDaysAgo = Utils.subtractNumDays(now, 2)

      val unassignedIncidents =
        Vector(
          unassignedIncident.copy(id = Some("INC-unassigned1"), title = "UnassignedIncident1", status = IncidentStatus.NEW, createdOn = now - 1000L),
          unassignedIncident.copy(id = Some("INC-unassigned2"), title = "UnassignedIncident2", status = IncidentStatus.OPEN, createdOn = now - 2000L),
          unassignedIncident.copy(id = Some("INC-unassigned3"), title = "UnassignedIncident3", status = IncidentStatus.NEW, createdOn = now - 3000L),
          unassignedIncident.copy(id = Some("INC-unassigned4"), title = "UnassignedIncident4", status = IncidentStatus.OPEN, createdOn = twoDaysAgo))

      val numUnassignedIncidentsAdded = Future.sequence(unassignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numUnassignedIncidentsAdded should be(4)

      val paramsForUnassigned = defaultParams.copy(status = Option("unassigned"), numDays = Option(1))
      val unassignedResult = incidentRepo.getByObservedSystem(tagId, paramsForUnassigned).futureValue
      val unassignedCount = unassignedResult.totalCount
      unassignedCount should be(3)
      unassignedResult.results.forall(inc => inc.assigneeId.isEmpty && (inc.status == IncidentStatus.NEW || inc.status == IncidentStatus.OPEN)) should be(true)

      val assignedIncidents =
        Vector(
          assignedIncident.copy(id = Some("INC-assigned1"), title = "AssignedIncident1", status = IncidentStatus.NEW, createdOn = now - 1000L),
          assignedIncident.copy(id = Some("INC-assigned2"), title = "AssignedIncident2", status = IncidentStatus.OPEN, createdOn = now - 2000L),
          assignedIncident.copy(id = Some("INC-assigned3"), title = "AssignedIncident3", status = IncidentStatus.NEW, createdOn = now - 3000L),
          assignedIncident.copy(id = Some("INC-assigned4"), title = "AssignedIncident4", status = IncidentStatus.OPEN, createdOn = twoDaysAgo))

      val numAssignedIncidentsAdded = Future.sequence(assignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numAssignedIncidentsAdded should be(4)

      val paramsForAssigned = defaultParams.copy(status = Option("assigned"), numDays = Option(1))
      val assignedResult = incidentRepo.getByObservedSystem(tagId, paramsForAssigned).futureValue
      val assignedCount = assignedResult.totalCount
      assignedCount should be(3)
      assignedResult.results.forall(inc => inc.assigneeId.isDefined && (inc.status == IncidentStatus.NEW || inc.status == IncidentStatus.OPEN)) should be(true)

      val closedIncidents =
        Vector(
          closedIncident.copy(id = Some("INC-closed1"), title = "ClosedIncident1", createdOn = now - 1000L),
          closedIncident.copy(id = Some("INC-closed2"), title = "ClosedIncident2", createdOn = now - 2000L),
          closedIncident.copy(id = Some("INC-closed3"), title = "ClosedIncident3", createdOn = now - 3000L),
          closedIncident.copy(id = Some("INC-closed4"), title = "ClosedIncident4", createdOn = twoDaysAgo))

      val numClosedIncidentsAdded = Future.sequence(closedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numClosedIncidentsAdded should be(4)

      val paramsForClosed = defaultParams.copy(status = Option("closed"), numDays = Option(1))
      val closedResult = incidentRepo.getByObservedSystem(tagId, paramsForClosed).futureValue
      val closedCount = closedResult.totalCount
      closedCount should be(3)
      closedResult.results.forall(_.status == IncidentStatus.CLOSED) should be(true)

      val paramsForAll = defaultParams.copy(status = Option("all"), numDays = Option(1))
      val allCount = incidentRepo.getByObservedSystem(tagId, paramsForAll).futureValue.totalCount
      allCount should be(unassignedCount + assignedCount + closedCount)
    }

    "get incidents that were created in the past three days" in {
      val now = Instant.now().toEpochMilli
      val twoDaysAgo = Utils.subtractNumDays(now, 2)
      val fourDaysAgo = Utils.subtractNumDays(now, 4)

      val unassignedIncidents =
        Vector(
          unassignedIncident.copy(id = Some("INC-unassigned1"), title = "UnassignedIncident1", status = IncidentStatus.NEW, createdOn = now - 1000L),
          unassignedIncident.copy(id = Some("INC-unassigned2"), title = "UnassignedIncident2", status = IncidentStatus.OPEN, createdOn = now - 2000L),
          unassignedIncident.copy(id = Some("INC-unassigned3"), title = "UnassignedIncident3", status = IncidentStatus.NEW, createdOn = twoDaysAgo),
          unassignedIncident.copy(id = Some("INC-unassigned4"), title = "UnassignedIncident4", status = IncidentStatus.OPEN, createdOn = fourDaysAgo))

      val numUnassignedIncidentsAdded = Future.sequence(unassignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numUnassignedIncidentsAdded should be(4)

      val paramsForUnassigned = defaultParams.copy(status = Option("unassigned"), numDays = Option(3))
      val unassignedResult = incidentRepo.getByObservedSystem(tagId, paramsForUnassigned).futureValue
      val unassignedCount = unassignedResult.totalCount
      unassignedCount should be(3)
      unassignedResult.results.forall(inc => inc.assigneeId.isEmpty && (inc.status == IncidentStatus.NEW || inc.status == IncidentStatus.OPEN)) should be(true)

      val assignedIncidents =
        Vector(
          assignedIncident.copy(id = Some("INC-assigned1"), title = "AssignedIncident1", status = IncidentStatus.NEW, createdOn = now - 1000L),
          assignedIncident.copy(id = Some("INC-assigned2"), title = "AssignedIncident2", status = IncidentStatus.OPEN, createdOn = now - 2000L),
          assignedIncident.copy(id = Some("INC-assigned3"), title = "AssignedIncident3", status = IncidentStatus.NEW, createdOn = twoDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned4"), title = "AssignedIncident4", status = IncidentStatus.OPEN, createdOn = fourDaysAgo))

      val numAssignedIncidentsAdded = Future.sequence(assignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numAssignedIncidentsAdded should be(4)

      val paramsForAssigned = defaultParams.copy(status = Option("assigned"), numDays = Option(3))
      val assignedResult = incidentRepo.getByObservedSystem(tagId, paramsForAssigned).futureValue
      val assignedCount = assignedResult.totalCount
      assignedCount should be(3)
      assignedResult.results.forall(inc => inc.assigneeId.isDefined && (inc.status == IncidentStatus.NEW || inc.status == IncidentStatus.OPEN)) should be(true)

      val closedIncidents =
        Vector(
          closedIncident.copy(id = Some("INC-closed1"), title = "ClosedIncident1", createdOn = now - 1000L),
          closedIncident.copy(id = Some("INC-closed2"), title = "ClosedIncident2", createdOn = now - 2000L),
          closedIncident.copy(id = Some("INC-closed3"), title = "ClosedIncident3", createdOn = twoDaysAgo),
          closedIncident.copy(id = Some("INC-closed4"), title = "ClosedIncident4", createdOn = fourDaysAgo))

      val numClosedIncidentsAdded = Future.sequence(closedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numClosedIncidentsAdded should be(4)

      val paramsForClosed = defaultParams.copy(status = Option("closed"), numDays = Option(3))
      val closedResult = incidentRepo.getByObservedSystem(tagId, paramsForClosed).futureValue
      val closedCount = closedResult.totalCount
      closedCount should be(3)
      closedResult.results.forall(_.status == IncidentStatus.CLOSED) should be(true)

      val paramsForAll = defaultParams.copy(status = Option("all"), numDays = Option(3))
      val allCount = incidentRepo.getByObservedSystem(tagId, paramsForAll).futureValue.totalCount
      allCount should be(unassignedCount + assignedCount + closedCount)
    }

    "get incidents that were created in the past seven days" in {
      val now = Instant.now().toEpochMilli
      val twoDaysAgo = Utils.subtractNumDays(now, 2)
      val eightDaysAgo = Utils.subtractNumDays(now, 8)

      val unassignedIncidents =
        Vector(
          unassignedIncident.copy(id = Some("INC-unassigned1"), title = "UnassignedIncident1", status = IncidentStatus.NEW, createdOn = now - 1000L),
          unassignedIncident.copy(id = Some("INC-unassigned2"), title = "UnassignedIncident2", status = IncidentStatus.OPEN, createdOn = now - 2000L),
          unassignedIncident.copy(id = Some("INC-unassigned3"), title = "UnassignedIncident3", status = IncidentStatus.NEW, createdOn = twoDaysAgo),
          unassignedIncident.copy(id = Some("INC-unassigned4"), title = "UnassignedIncident4", status = IncidentStatus.OPEN, createdOn = eightDaysAgo))

      val numUnassignedIncidentsAdded = Future.sequence(unassignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numUnassignedIncidentsAdded should be(4)

      val paramsForUnassigned = defaultParams.copy(status = Option("unassigned"), numDays = Option(7))
      val unassignedResult = incidentRepo.getByObservedSystem(tagId, paramsForUnassigned).futureValue
      val unassignedCount = unassignedResult.totalCount
      unassignedCount should be(3)
      unassignedResult.results.forall(inc => inc.assigneeId.isEmpty && (inc.status == IncidentStatus.NEW || inc.status == IncidentStatus.OPEN)) should be(true)

      val assignedIncidents =
        Vector(
          assignedIncident.copy(id = Some("INC-assigned1"), title = "AssignedIncident1", status = IncidentStatus.NEW, createdOn = now - 1000L),
          assignedIncident.copy(id = Some("INC-assigned2"), title = "AssignedIncident2", status = IncidentStatus.OPEN, createdOn = now - 2000L),
          assignedIncident.copy(id = Some("INC-assigned3"), title = "AssignedIncident3", status = IncidentStatus.NEW, createdOn = twoDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned4"), title = "AssignedIncident4", status = IncidentStatus.OPEN, createdOn = eightDaysAgo))

      val numAssignedIncidentsAdded = Future.sequence(assignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numAssignedIncidentsAdded should be(4)

      val paramsForAssigned = defaultParams.copy(status = Option("assigned"), numDays = Option(7))
      val assignedResult = incidentRepo.getByObservedSystem(tagId, paramsForAssigned).futureValue
      val assignedCount = assignedResult.totalCount
      assignedCount should be(3)
      assignedResult.results.forall(inc => inc.assigneeId.isDefined && (inc.status == IncidentStatus.NEW || inc.status == IncidentStatus.OPEN)) should be(true)

      val closedIncidents =
        Vector(
          closedIncident.copy(id = Some("INC-closed1"), title = "ClosedIncident1", createdOn = now - 1000L),
          closedIncident.copy(id = Some("INC-closed2"), title = "ClosedIncident2", createdOn = now - 2000L),
          closedIncident.copy(id = Some("INC-closed3"), title = "ClosedIncident3", createdOn = twoDaysAgo),
          closedIncident.copy(id = Some("INC-closed4"), title = "ClosedIncident4", createdOn = eightDaysAgo))

      val numClosedIncidentsAdded = Future.sequence(closedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numClosedIncidentsAdded should be(4)

      val paramsForClosed = defaultParams.copy(status = Option("closed"), numDays = Option(7))
      val closedResult = incidentRepo.getByObservedSystem(tagId, paramsForClosed).futureValue
      val closedCount = closedResult.totalCount
      closedCount should be(3)
      closedResult.results.forall(_.status == IncidentStatus.CLOSED) should be(true)

      val paramsForAll = defaultParams.copy(status = Option("all"), numDays = Option(7))
      val allCount = incidentRepo.getByObservedSystem(tagId, paramsForAll).futureValue.totalCount
      allCount should be(unassignedCount + assignedCount + closedCount)
    }

    "get incidents that were created in the past 30 days" in {
      val now = Instant.now().toEpochMilli
      val eightDaysAgo = Utils.subtractNumDays(now, 8)
      val fortyDaysAgo = Utils.subtractNumDays(now, 40)

      val unassignedIncidents =
        Vector(
          unassignedIncident.copy(id = Some("INC-unassigned1"), title = "UnassignedIncident1", status = IncidentStatus.NEW, createdOn = now - 1000L),
          unassignedIncident.copy(id = Some("INC-unassigned2"), title = "UnassignedIncident2", status = IncidentStatus.OPEN, createdOn = now - 2000L),
          unassignedIncident.copy(id = Some("INC-unassigned3"), title = "UnassignedIncident3", status = IncidentStatus.NEW, createdOn = eightDaysAgo),
          unassignedIncident.copy(id = Some("INC-unassigned4"), title = "UnassignedIncident4", status = IncidentStatus.OPEN, createdOn = fortyDaysAgo))

      val numUnassignedIncidentsAdded = Future.sequence(unassignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numUnassignedIncidentsAdded should be(4)

      val paramsForUnassigned = defaultParams.copy(status = Option("unassigned"), numDays = Option(30))
      val unassignedResult = incidentRepo.getByObservedSystem(tagId, paramsForUnassigned).futureValue
      val unassignedCount = unassignedResult.totalCount
      unassignedCount should be(3)
      unassignedResult.results.forall(inc => inc.assigneeId.isEmpty && (inc.status == IncidentStatus.NEW || inc.status == IncidentStatus.OPEN)) should be(true)

      val assignedIncidents =
        Vector(
          assignedIncident.copy(id = Some("INC-assigned1"), title = "AssignedIncident1", status = IncidentStatus.NEW, createdOn = now - 1000L),
          assignedIncident.copy(id = Some("INC-assigned2"), title = "AssignedIncident2", status = IncidentStatus.OPEN, createdOn = now - 2000L),
          assignedIncident.copy(id = Some("INC-assigned3"), title = "AssignedIncident3", status = IncidentStatus.NEW, createdOn = eightDaysAgo),
          assignedIncident.copy(id = Some("INC-assigned4"), title = "AssignedIncident4", status = IncidentStatus.OPEN, createdOn = fortyDaysAgo))

      val numAssignedIncidentsAdded = Future.sequence(assignedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numAssignedIncidentsAdded should be(4)

      val paramsForAssigned = defaultParams.copy(status = Option("assigned"), numDays = Option(30))
      val assignedResult = incidentRepo.getByObservedSystem(tagId, paramsForAssigned).futureValue
      val assignedCount = assignedResult.totalCount
      assignedCount should be(3)
      assignedResult.results.forall(inc => inc.assigneeId.isDefined && (inc.status == IncidentStatus.NEW || inc.status == IncidentStatus.OPEN)) should be(true)

      val closedIncidents =
        Vector(
          closedIncident.copy(id = Some("INC-closed1"), title = "ClosedIncident1", createdOn = now - 1000L),
          closedIncident.copy(id = Some("INC-closed2"), title = "ClosedIncident2", createdOn = now - 2000L),
          closedIncident.copy(id = Some("INC-closed3"), title = "ClosedIncident3", createdOn = eightDaysAgo),
          closedIncident.copy(id = Some("INC-closed4"), title = "ClosedIncident4", createdOn = fortyDaysAgo))

      val numClosedIncidentsAdded = Future.sequence(closedIncidents.map(inc => incidentRepo.add(inc, List(eventId)))).futureValue.size
      numClosedIncidentsAdded should be(4)

      val paramsForClosed = defaultParams.copy(status = Option("closed"), numDays = Option(30))
      val closedResult = incidentRepo.getByObservedSystem(tagId, paramsForClosed).futureValue
      val closedCount = closedResult.totalCount
      closedCount should be(3)
      closedResult.results.forall(_.status == IncidentStatus.CLOSED) should be(true)

      val paramsForAll = defaultParams.copy(status = Option("all"), numDays = Option(30))
      val allCount = incidentRepo.getByObservedSystem(tagId, paramsForAll).futureValue.totalCount
      allCount should be(unassignedCount + assignedCount + closedCount)
    }
  }

  trait SeverityFixture {
    val now = Instant.now().toEpochMilli
    val twoDaysAgo = Utils.subtractNumDays(now, 2)
    val fiveDaysAgo = Utils.subtractNumDays(now, 5)
    val twentyDaysAgo = Utils.subtractNumDays(now, 20)
    val fortyDaysAgo = Utils.subtractNumDays(now, 40)

    val lowSeverity = 14
    val mediumSeverity = 50
    val highSeverity = 90
  }

  private def verifySeverityStats(status: IncidentStatusForStatistics, requestTimestamp: Long): Unit = {
    val severityStatsOneDay = incidentRepo.getSeverityStatistics(tagId, requestTimestamp, status, 1).futureValue
    severityStatsOneDay.numDays should be(1)
    severityStatsOneDay.requestTimestampMillis should be(requestTimestamp)
    severityStatsOneDay.status should be(status.entryName)
    severityStatsOneDay.severity.low should be(1)
    severityStatsOneDay.severity.medium should be(0)
    severityStatsOneDay.severity.high should be(0)

    val severityStatsThreeDays = incidentRepo.getSeverityStatistics(tagId, requestTimestamp, status, 3).futureValue
    severityStatsThreeDays.numDays should be(3)
    severityStatsThreeDays.requestTimestampMillis should be(requestTimestamp)
    severityStatsThreeDays.status should be(status.entryName)
    severityStatsThreeDays.severity.low should be(1)
    severityStatsThreeDays.severity.medium should be(0)
    severityStatsThreeDays.severity.high should be(1)

    val severityStatsSevenDays = incidentRepo.getSeverityStatistics(tagId, requestTimestamp, status, 7).futureValue
    severityStatsSevenDays.numDays should be(7)
    severityStatsSevenDays.requestTimestampMillis should be(requestTimestamp)
    severityStatsSevenDays.status should be(status.entryName)
    severityStatsSevenDays.severity.low should be(1)
    severityStatsSevenDays.severity.medium should be(2)
    severityStatsSevenDays.severity.high should be(1)

    val severityStatsThirtyDays = incidentRepo.getSeverityStatistics(tagId, requestTimestamp, status, 30).futureValue
    severityStatsThirtyDays.numDays should be(30)
    severityStatsThirtyDays.requestTimestampMillis should be(requestTimestamp)
    severityStatsThirtyDays.status should be(status.entryName)
    severityStatsThirtyDays.severity.low should be(2)
    severityStatsThirtyDays.severity.medium should be(2)
    severityStatsThirtyDays.severity.high should be(3)
  }

  private def verifyOneDayDetails(status: IncidentStatusForStatistics, requestTimestamp: Long): Unit = {
    val details = incidentRepo.getStatisticsDetails(tagId, requestTimestamp, status, 1).futureValue
    details.requestTimestampMillis should be(requestTimestamp)
    details.status should be(status.entryName)
    details.numDays should be(1)
    details.granularity should be(IncidentRepository.granularityMap(1))
    details.counts should be(Vector(2, 1, 1, 1, 1, 1, 1, 1))
  }

  private def verifyThreeDayDetails(status: IncidentStatusForStatistics, requestTimestamp: Long): Unit = {
    val details = incidentRepo.getStatisticsDetails(tagId, requestTimestamp, status, 3).futureValue
    details.requestTimestampMillis should be(requestTimestamp)
    details.status should be(status.entryName)
    details.numDays should be(3)
    details.granularity should be(IncidentRepository.granularityMap(3))
    details.counts should be(Vector(2, 1, 1))
  }

  private def verifySevenDayDetails(status: IncidentStatusForStatistics, requestTimestamp: Long): Unit = {
    val details = incidentRepo.getStatisticsDetails(tagId, requestTimestamp, status, 7).futureValue
    details.requestTimestampMillis should be(requestTimestamp)
    details.status should be(status.entryName)
    details.numDays should be(7)
    details.granularity should be(IncidentRepository.granularityMap(7))
    details.counts should be(Vector(2, 1, 1, 1, 1, 1, 1))
  }

  private def verifyThirtyDayDetails(status: IncidentStatusForStatistics, requestTimestamp: Long): Unit = {
    val details = incidentRepo.getStatisticsDetails(tagId, requestTimestamp, status, 30).futureValue
    details.requestTimestampMillis should be(requestTimestamp)
    details.status should be(status.entryName)
    details.numDays should be(30)
    details.granularity should be(IncidentRepository.granularityMap(30))
    details.counts should be(Vector(2, 1, 1, 1, 1))
  }
}
