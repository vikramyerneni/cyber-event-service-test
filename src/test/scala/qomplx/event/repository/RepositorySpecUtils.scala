package qomplx.event.repository

import com.typesafe.scalalogging.Logger
import qomplx.event.model.Event
import qomplx.event.util.SlickPostgresProfile.api._
import qomplx.event.ObservedSystemId

import scala.concurrent.{ ExecutionContext, Future }

/**
 * Contains utility methods that are useful for the tests and that are absent in the repository APIs.
 */
class RepositorySpecUtils(db: Database, logger: Logger) {
  def deleteIncidentsForObservedSystem(tagId: ObservedSystemId, db: Database)(implicit ec: ExecutionContext): Future[Int] = {
    val whereClause = s"(tag_id = '${tagId}')"

    val delete = s"delete from cyber_event_service.amp_incidents where $whereClause"
    val deleteStatement = sqlu"#$delete"

    logger.info(s"Running the following delete statement: $delete")

    db.run(deleteStatement.transactionally)
      .recover {
        case t =>
          logger.error(s"The following exception was thrown when trying to delete incidents for $tagId: ${t.getMessage}")
          0
      }
  }

  def deleteEventIncidentMappings(events: Seq[Event], db: Database)(implicit ec: ExecutionContext): Future[Int] = {
    val whereClause = events.map(e => s"(amp_event_id = '${e.id.get}')").mkString(" or ")

    val delete = s"delete from cyber_event_service.amp_event_incident where $whereClause"
    val deleteStatement = sqlu"#$delete"

    logger.info(s"Running the following delete statement: $delete")

    db.run(deleteStatement.transactionally)
      .recover {
        case t =>
          logger.error(s"The following exception was thrown when trying to delete the event-incident mappings for the following events: ${events.map(_.id)}", t)
          0
      }
  }

  def deleteEvents(events: Seq[Event], db: Database)(implicit ec: ExecutionContext): Future[Int] = {
    val whereClause = events.map(e => s"(event_id = '${e.id.get}')").mkString(" or ")

    val delete = s"delete from cyber_event_service.amp_events where $whereClause"
    val deleteStatement = sqlu"#$delete"

    logger.info(s"Running the following delete statement: $delete")

    db.run(deleteStatement.transactionally)
      .recover {
        case t =>
          logger.error(s"The following exception was thrown when trying to delete the following events: ${events.map(_.id)}", t)
          0
      }
  }
}
