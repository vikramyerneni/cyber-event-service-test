package qomplx.event.repository

import java.util.NoSuchElementException

import org.scalatest.BeforeAndAfter
import qomplx.event.BaseSpecWithMigrations
import qomplx.event.model._
import qomplx.event.routes.GetEventsReq
import qomplx.event.util.SlickPostgresProfile.api._
import spray.json._

import scala.concurrent.{ ExecutionContext, Future }

final class EventRepositorySpec extends BaseSpecWithMigrations with BeforeAndAfter {
  private lazy val db = postgresConnection.database
  private lazy val eventRepository = new EventRepository(db)
  private lazy val incidentRepository = new IncidentRepository(db)
  private lazy val tagsRepository = new TagRepository(db)

  before {
    val numDeleted = delete().futureValue
    logger.debug(s"Deleted $numDeleted records before running the test")
  }

  override def beforeAll(): Unit = {
    super.beforeAll()
  }

  // test data
  val tagId = s"${java.util.UUID.randomUUID()}-test-obs123"
  val obsSysIdOpt = Some(tagId)
  val tag = Tag(PropertyType.AGENT, "AGENT_01", tagId)

  // Utility method for testing. The real API does not delete events, hence its absence there.
  def delete()(implicit ec: ExecutionContext): Future[Int] = {

    def mkAction(query: String) = sqlu"#$query"

    val deleteEventsByObsid = s"delete from cyber_event_service.amp_events where observed_system_id='$tagId'"
    val deleteEventsByTag = {
      s"delete from cyber_event_service.amp_events where event_id in " +
        s"(select e.event_id from cyber_event_service.amp_tags t left join cyber_event_service.amp_events e on (t.property_id = e.agent_id or t.property_id = e.rule_id) where t.tag_id = '$tagId')"
    }
    val deleteTags = s"delete from cyber_event_service.amp_tags where tag_id='$tagId'"
    val deleteSeverityCount = s"delete from cyber_event_service.amp_events_count_by_severity where tag_id='$tagId'"

    val actions = for {
      i <- mkAction(deleteEventsByObsid)
      j <- mkAction(deleteEventsByTag)
      k <- mkAction(deleteTags)
      l <- mkAction(deleteSeverityCount)
    } yield i + j + k + l

    db.run(actions.transactionally)
      .recover {
        case t =>
          logger.error(s"The following exception was thrown when trying to delete the events/tags for $tagId: ${t.getMessage}")
          0
      }
  }

  def removeGeneratedFieldForComparison(e: EventReduced) = e.copy(incIdx = None)

  def eventIncidentMapCount(eventIds: Seq[String])(implicit ec: ExecutionContext): Future[Int] = {
    db.run(sql"""select count(amp_event_id) from cyber_event_service.amp_event_incident where amp_event_id in ('#${eventIds.mkString("','")}')""".as[Int].head)
  }

  def truncateEventsCount()(implicit ec: ExecutionContext): Future[Int] = {
    val truncate = s"truncate cyber_event_service.amp_events_count"
    val truncateStatement = sqlu"#$truncate"

    logger.debug(s"Running the following truncate statement: $truncate")

    db.run(truncateStatement.transactionally)
      .recover {
        case t =>
          logger.error(s"The following exception was thrown when trying to truncate the events count: ${t.getMessage}")
          0
      }
  }

  "EventRepository" should {
    "throw an exception if the sum of limit and offset is greater than provided value" in {
      an[IllegalArgumentException] should be thrownBy {
        eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffset(Option(10), Option(1000)))
      }
    }

    "throw exception if given tagId/obsId not exist" in {
      eventRepository.getCountBySeverity(tagId).failed.futureValue shouldBe a[NoObservedSystemException]
    }

    "return zero count if no events exist for given tagId/obsId" in {
      tagsRepository.add(tag)
      eventRepository.getCountBySeverity(tagId).futureValue shouldEqual CountBySeverity(0, 0, 0, 0)
    }

    "get the events for an observed system" in {
      val extraInfoJson = JsObject(Map("team" -> JsString("cyber")))

      val eventsToInsert =
        Seq(
          EventMessage(Option("event1"), "GT-Event-1", "description", "Kerberos attack", 90, 897352956563853L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, Some(extraInfoJson), agentId = Some(tag.propertyId)),
          EventMessage(Option("event2"), "GT-Event-2", "description", "Kerberos attack", 90, 897352956563860L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event3"), "GT-Event-3", "description", "Kerberos attack", 90, 897352956563870L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)))

      tagsRepository.add(tag)
      val eventsToInsertReduced = eventsToInsert.map(_.toEventReduced(tagId))
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val eventsFromDb = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffset(None, None)).futureValue
      eventsFromDb.results.toSet.map(removeGeneratedFieldForComparison) should be(eventsToInsertReduced.toSet)

      val eventsFromDbWithLimitAndOffset = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffset(Option(1), Option(2))).futureValue
      eventsFromDbWithLimitAndOffset.results.toSet.map(removeGeneratedFieldForComparison) should be(Seq(eventsToInsertReduced.head).toSet)

      val eventsFromDbWithLimit = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffset(Option(2), None)).futureValue
      eventsFromDbWithLimit.results.toSet.toSet.map(removeGeneratedFieldForComparison) should be(eventsToInsertReduced.tail.toSet)

      val eventsFromDbWithOffset = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffset(None, Option(2))).futureValue
      eventsFromDbWithOffset.results.toSet.map(removeGeneratedFieldForComparison) should be(Seq(eventsToInsertReduced.head).toSet)
    }

    "insert events with agent and rule ID's" in {
      val eventsToInsert =
        Seq(
          EventMessage(None, "GT-Event-1", "description", "Kerberos attack", 90, 897352956563853L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some("agent_id_1")),
          EventMessage(None, "GT-Event-2", "description", "Kerberos attack", 90, 897352956563860L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, ruleId = Some("rule_id_1")),
          EventMessage(None, "GT-Event-3", "description", "Kerberos attack", 90, 897352956563870L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some("agent_id_2"), ruleId = Some("rule_id_2")))

      tagsRepository.add(tag.copy(propertyId = "agent_id_1"))
      tagsRepository.add(tag.copy(propertyId = "rule_id_1", kind = PropertyType.RULE))
      tagsRepository.add(tag.copy(propertyId = "agent_id_2"))
      tagsRepository.add(tag.copy(propertyId = "rule_id_2", kind = PropertyType.RULE))
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val eventsFromDb = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffset(None, None)).futureValue
      eventsFromDb.totalCount shouldEqual eventsToInsert.size
      eventsFromDb.results.find(_.agentId.nonEmpty) shouldBe 'defined
      eventsFromDb.results.find(_.ruleId.nonEmpty) shouldBe 'defined
      eventsFromDb.results.find(a => a.agentId.nonEmpty && a.ruleId.nonEmpty) shouldBe 'defined
    }

    "find single event by its id" in {
      val eventId = "event-1"
      val eventsToInsert =
        Seq(
          EventMessage(Some(eventId), "GT-Event-1", "description", "Kerberos attack", 90, 897352956563853L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Some("event-2"), "GT-Event-2", "description", "Kerberos attack", 90, 897352956563860L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Some("event-3"), "GT-Event-3", "description", "Kerberos attack", 90, 897352956563870L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val eventsFromDb = eventRepository.findById(tagId, "event-1").futureValue
      eventsFromDb.nonEmpty shouldBe true
      eventsFromDb.get.id shouldEqual Some(eventId)
    }

    "even if agent/rule tag does not exist, find events using `observed_system_id` of `amp_events` table" in {
      val extraInfoJson = JsObject(Map("team" -> JsString("cyber")))

      val eventsToInsert =
        Seq(
          EventMessage(Option("event1"), "GT-Event-1", "description", "Kerberos attack", 90, 897352956563853L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, Some(extraInfoJson), agentId = Some(tag.propertyId)),
          EventMessage(Option("event2"), "GT-Event-2", "description", "Kerberos attack", 90, 897352956563860L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event3"), "GT-Event-3", "description", "Kerberos attack", 90, 897352956563870L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)))

      tagsRepository.add(tag.copy(propertyId = "AGENT_02"))
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val eventsFromDb = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffset(None, None)).futureValue
      eventsFromDb.totalCount shouldEqual numAdded
    }

    "insert events that do not have IDs" in {
      val eventsToInsert =
        Seq(
          EventMessage(None, "GT-Event-1", "description", "Kerberos attack", 90, 897352956563853L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-2", "description", "Kerberos attack", 90, 897352956563860L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-3", "description", "Kerberos attack", 90, 897352956563870L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)))

      val eventsToInsertReduced = eventsToInsert.map(_.toEventReduced(tagId))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val eventsFromDb = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffset(None, None)).futureValue
      eventsFromDb.results should have size (eventsToInsertReduced.size)
      eventsFromDb.results.forall(_.id.isDefined)
    }

    "insert events with weird JSON" in {
      val eventsToInsert =
        Seq(
          EventMessage(None, "GT-Event-1", "description", "Kerberos attack", 90, 897352956563853L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, Some(JsObject("'Hello'" -> JsString("World"))), agentId = Some(tag.propertyId)))

      val eventsToInsertReduced = eventsToInsert.map(_.toEventReduced(tagId))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val eventsFromDb = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffset(None, None)).futureValue
      eventsFromDb.results should have size (eventsToInsertReduced.size)
      eventsFromDb.results.forall(_.id.isDefined)
    }

    "filter events by severity range" in {
      val eventsToInsert =
        Seq(
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl0"), "GT-Event-1", "description", "Kerberos attack", 10, 897352956563853L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl1"), "GT-Event-2", "description", "Kerberos attack", 20, 897352956563860L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl2"), "GT-Event-3", "description", "Kerberos attack", 30, 897352956563870L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl3"), "GT-Event-4", "description", "Kerberos attack", 40, 897352956563880L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl4"), "GT-Event-5", "description", "Kerberos attack", 50, 897352956563890L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl5"), "GT-Event-6", "description", "Kerberos attack", 60, 897352956563000L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl6"), "GT-Event-7", "description", "Kerberos attack", 70, 897352956563910L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl7"), "GT-Event-8", "description", "Kerberos attack", 80, 897352956563920L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl8"), "GT-Event-9", "description", "Kerberos attack", 90, 897352956563930L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)))

      val eventsToInsertReduced = eventsToInsert.map(_.toEventReduced(tagId))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      // filter >= 10 and <= 50
      val filter1 = eventsToInsertReduced.filter(e => e.severity >= 10 && e.severity <= 50).sortWith(_.timestamp >= _.timestamp)
      val eventsFromDb1 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndSeverity(Option(10), Option(0), Option(10), Option(50))).futureValue
      eventsFromDb1.results should have size filter1.size
      eventsFromDb1.results.map(removeGeneratedFieldForComparison) shouldEqual filter1

      // filter >= 10
      val filter2 = eventsToInsertReduced.filter(e => e.severity >= 10).sortWith(_.timestamp >= _.timestamp)
      val eventsFromDb2 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndSeverity(Option(10), Option(0), Option(10), None)).futureValue
      eventsFromDb2.results should have size filter2.size
      eventsFromDb2.results.map(removeGeneratedFieldForComparison) shouldEqual filter2

      // filter <= 50
      val filter3 = eventsToInsertReduced.filter(e => e.severity <= 50).sortWith(_.timestamp >= _.timestamp)
      val eventsFromDb3 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndSeverity(Option(10), Option(0), None, Option(50))).futureValue
      eventsFromDb3.results should have size filter3.size
      eventsFromDb3.results.map(removeGeneratedFieldForComparison) shouldEqual filter3
    }

    "filter events by timestamp range" in {
      val eventsToInsert =
        Seq(
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl0"), "GT-Event-1", "description", "Kerberos attack", 10, 897352956563853L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl1"), "GT-Event-2", "description", "Kerberos attack", 20, 897352956563860L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl2"), "GT-Event-3", "description", "Kerberos attack", 30, 897352956563870L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl3"), "GT-Event-4", "description", "Kerberos attack", 40, 897352956563880L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl4"), "GT-Event-5", "description", "Kerberos attack", 50, 897352956563890L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl5"), "GT-Event-6", "description", "Kerberos attack", 60, 897352956563000L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl6"), "GT-Event-7", "description", "Kerberos attack", 70, 897352956563910L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl7"), "GT-Event-8", "description", "Kerberos attack", 80, 897352956563920L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl8"), "GT-Event-9", "description", "Kerberos attack", 90, 897352956563930L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)))

      val eventsToInsertReduced = eventsToInsert.map(_.toEventReduced(tagId))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      // filter >= 897352956563870L and <= 897352956563920L
      val filter1 = eventsToInsertReduced.filter(e => e.timestamp >= 897352956563870L && e.timestamp <= 897352956563920L).sortWith(_.timestamp >= _.timestamp)
      val eventsFromDb1 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndTimestamp(Option(10), Option(0), Option(897352956563870L), Option(897352956563920L))).futureValue
      eventsFromDb1.results should have size filter1.size
      eventsFromDb1.results.map(removeGeneratedFieldForComparison) shouldEqual filter1

      // filter >= 897352956563870L
      val filter2 = eventsToInsertReduced.filter(e => e.timestamp >= 897352956563870L).sortWith(_.timestamp >= _.timestamp)
      val eventsFromDb2 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndTimestamp(Option(10), Option(0), Option(897352956563870L), None)).futureValue
      eventsFromDb2.results should have size filter2.size
      eventsFromDb2.results.map(removeGeneratedFieldForComparison) shouldEqual filter2

      // filter <= 897352956563920L
      val filter3 = eventsToInsertReduced.filter(e => e.timestamp <= 897352956563920L).sortWith(_.timestamp >= _.timestamp)
      val eventsFromDb3 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndTimestamp(Option(10), Option(0), None, Option(897352956563920L))).futureValue
      eventsFromDb3.results should have size filter3.size
      eventsFromDb3.results.map(removeGeneratedFieldForComparison) shouldEqual filter3
    }

    "filter events by categories" in {
      val eventsToInsert =
        Seq(
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl0"), "GT-Event-1", "description", "Kerberos attack", 10, 897352956563853L, "Kerberos pipeline", "19475653", Seq("Test Capabilities", "Technical Information Gathering"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl1"), "GT-Event-2", "description", "Kerberos attack", 20, 897352956563860L, "Kerberos pipeline", "19475654", Seq("a", "b", "c"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl2"), "GT-Event-3", "description", "Kerberos attack", 30, 897352956563870L, "Kerberos pipeline", "19475655", Seq("a"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl3"), "GT-Event-4", "description", "Kerberos attack", 40, 897352956563880L, "Kerberos pipeline", "19475653", Seq("a", "b", "c", "d"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl4"), "GT-Event-5", "description", "Kerberos attack", 50, 897352956563890L, "Kerberos pipeline", "19475654", Seq("Test Capabilities", "Technical Information Gathering"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl5"), "GT-Event-6", "description", "Kerberos attack", 60, 897352956563000L, "Kerberos pipeline", "19475655", Seq.empty, Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl6"), "GT-Event-7", "description", "Kerberos attack", 70, 897352956563910L, "Kerberos pipeline", "19475653", Seq("Test Capabilities", "Technical Information Gathering", "c"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl7"), "GT-Event-8", "description", "Kerberos attack", 80, 897352956563920L, "Kerberos pipeline", "19475654", Seq.empty, Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl8"), "GT-Event-9", "description", "Kerberos attack", 90, 897352956563930L, "Kerberos pipeline", "19475655", Seq("a"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)))

      val eventsToInsertReduced = eventsToInsert.map(_.toEventReduced(tagId))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val filter1 = eventsToInsertReduced.filter(e => e.categories containsSlice Seq("Test Capabilities", "Technical Information Gathering")).sortWith(_.timestamp >= _.timestamp)
      val eventsFromDb1 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndCategories(Option(10), Option(0), Option(Seq("test capabilities", "technical information gathering")))).futureValue
      eventsFromDb1.results should have size filter1.size
      eventsFromDb1.results.map(removeGeneratedFieldForComparison) shouldEqual filter1

      val filter2 = eventsToInsertReduced.filter(e => e.categories containsSlice Seq()).sortWith(_.timestamp >= _.timestamp)
      val eventsFromDb2 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndCategories(Option(10), Option(0), Option(Seq()))).futureValue
      eventsFromDb2.results should have size filter2.size
      eventsFromDb2.results.map(removeGeneratedFieldForComparison) shouldEqual filter2

      val filter3 = eventsToInsertReduced.sortWith(_.timestamp >= _.timestamp)
      val eventsFromDb3 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndCategories(Option(10), Option(0), None)).futureValue
      eventsFromDb3.results should have size filter3.size
      eventsFromDb3.results.map(removeGeneratedFieldForComparison) shouldEqual filter3
    }

    "filter events by tactics" in {
      val eventsToInsert =
        Seq(
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl0"), "GT-Event-1", "description", "Kerberos attack", 10, 897352956563853L, "Kerberos pipeline", "19475653", Nil, Seq("Winlogon Helper DLL", "Exploitation for Defense Evasion"), obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl1"), "GT-Event-2", "description", "Kerberos attack", 20, 897352956563860L, "Kerberos pipeline", "19475654", Nil, Seq("a", "b", "c"), obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl2"), "GT-Event-3", "description", "Kerberos attack", 30, 897352956563870L, "Kerberos pipeline", "19475655", Nil, Seq("a"), obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl3"), "GT-Event-4", "description", "Kerberos attack", 40, 897352956563880L, "Kerberos pipeline", "19475653", Nil, Seq("Winlogon Helper DLL", "Exploitation for Defense Evasion", "c", "d"), obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl4"), "GT-Event-5", "description", "Kerberos attack", 50, 897352956563890L, "Kerberos pipeline", "19475654", Nil, Seq("Winlogon Helper DLL", "Exploitation for Defense Evasion"), obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl5"), "GT-Event-6", "description", "Kerberos attack", 60, 897352956563000L, "Kerberos pipeline", "19475655", Nil, Seq.empty, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl6"), "GT-Event-7", "description", "Kerberos attack", 70, 897352956563910L, "Kerberos pipeline", "19475653", Nil, Seq("a", "b", "c"), obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl7"), "GT-Event-8", "description", "Kerberos attack", 80, 897352956563920L, "Kerberos pipeline", "19475654", Nil, Seq.empty, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl8"), "GT-Event-9", "description", "Kerberos attack", 90, 897352956563930L, "Kerberos pipeline", "19475655", Nil, Seq("a"), obsSysIdOpt, agentId = Some(tag.propertyId)))

      val eventsToInsertReduced = eventsToInsert.map(_.toEventReduced(tagId))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val filter1 = eventsToInsertReduced.filter(e => e.tactics containsSlice Seq("Winlogon Helper DLL", "Exploitation for Defense Evasion")).sortWith(_.timestamp >= _.timestamp)
      val eventsFromDb1 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndTactics(Option(10), Option(0), Option(Seq("winlogon helper dll", "exploitation for defense evasion")))).futureValue
      eventsFromDb1.results should have size filter1.size
      eventsFromDb1.results.map(removeGeneratedFieldForComparison) shouldEqual filter1

      val filter2 = eventsToInsertReduced.filter(e => e.tactics containsSlice Seq()).sortWith(_.timestamp >= _.timestamp)
      val eventsFromDb2 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndTactics(Option(10), Option(0), Option(Seq()))).futureValue
      eventsFromDb2.results should have size filter2.size
      eventsFromDb2.results.map(removeGeneratedFieldForComparison) shouldEqual filter2

      val filter3 = eventsToInsertReduced.sortWith(_.timestamp >= _.timestamp)
      val eventsFromDb3 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndTactics(Option(10), Option(0), None)).futureValue
      eventsFromDb3.results should have size filter3.size
      eventsFromDb3.results.map(removeGeneratedFieldForComparison) shouldEqual filter3
    }

    "search events using event_id, name and description" in {
      val eventsToInsert =
        Seq(
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl0"), "C++ In-Depth Series", "is designed to lift professionals (and aspiring professionals) to the next level of programming skill", "Kerberos attack", 10, 897352956563853L, "Kerberos pipeline", "19475653", Nil, Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl1"), "C++ In-Depth-1 Series", "Lorem ipsum dolor amet sit, consectetur adipiscing elit.", "Kerberos attack", 20, 897352956563860L, "Kerberos pipeline", "19475654", Nil, Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl2"), "Nunc justo sem", "Dolor sit amet lorem ipsum, consectetur elit adipiscing.", "Kerberos attack", 30, 897352956563870L, "Kerberos pipeline", "19475655", Nil, Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl3"), "In-Depth C++ Series", "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Kerberos attack", 40, 897352956563880L, "Kerberos pipeline", "19475653", Nil, Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl4"), "C++ In-Depth Series", "is designed to lift professionals (and aspiring professionals) to the next level of programming skill", "Kerberos attack", 50, 897352956563890L, "Kerberos pipeline", "19475654", Nil, Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl5"), "Justo nunc sem", "Lorem sit amet ipsum dolor, consectetur adipiscing elit.", "Kerberos attack", 60, 897352956563000L, "Kerberos pipeline", "19475655", Nil, Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl6"), "Nunc justo sem", "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Kerberos attack", 70, 897352956563910L, "Kerberos pipeline", "19475653", Nil, Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl7"), "C++ In-Depth Series", "is designed to lift professionals (and aspiring professionals) to the next level of programming skill", "Kerberos attack", 80, 897352956563920L, "Kerberos pipeline", "19475654", Nil, Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl8"), "Nunc justo sem", "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Kerberos attack", 90, 897352956563930L, "Kerberos pipeline", "19475655", Nil, Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl9"), "DCSync attack", "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "Kerberos attack", 90, 897352956563940L, "Kerberos pipeline", "19475655", Nil, Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl10"), "Nunc justo sem", "DCSync attack", "Kerberos attack", 90, 897352956563950L, "Kerberos pipeline", "19475655", Nil, Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl11"), "DCSync attack", "pass-the-hash", "Kerberos attack", 90, 897352956563960L, "Kerberos pipeline", "19475655", Nil, Nil, obsSysIdOpt, agentId = Some(tag.propertyId)))

      val eventsToInsertReduced = eventsToInsert.map(_.toEventReduced(tagId))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val filter1 = eventsToInsertReduced.filter { e =>
        e.id.exists(_.toLowerCase.contains("c++ in-dep")) || e.name.toLowerCase.contains("c++ in-dep") || e.description.toLowerCase.contains("c++ in-dep")
      }.sortWith(_.timestamp >= _.timestamp).take(10)
      val eventsFromDb1 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndQueryString(Option(10), Option(0), Option("C++ In-Dep"))).futureValue
      eventsFromDb1.results should have size filter1.size
      eventsFromDb1.results.map(removeGeneratedFieldForComparison) shouldEqual filter1

      val filter2 = eventsToInsertReduced.filter { e =>
        e.id.exists(_.toLowerCase.contains("c++ in-depth-1")) || e.name.toLowerCase.contains("c++ in-depth-1") || e.description.toLowerCase.contains("c++ in-depth-1")
      }.sortWith(_.timestamp >= _.timestamp).take(10)
      val eventsFromDb2 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndQueryString(Option(10), Option(0), Option("C++ In-Depth-1"))).futureValue
      eventsFromDb2.results should have size filter2.size
      eventsFromDb2.results.map(removeGeneratedFieldForComparison) shouldEqual filter2

      val filter3 = eventsToInsertReduced.filter { e =>
        e.id.exists(_.toLowerCase.contains("in-depth c++")) || e.name.toLowerCase.contains("in-depth c++") || e.description.toLowerCase.contains("in-depth c++")
      }.sortWith(_.timestamp >= _.timestamp).take(10)
      val eventsFromDb3 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndQueryString(Option(10), Option(0), Option("IN-depth C++"))).futureValue
      eventsFromDb3.results should have size filter3.size
      eventsFromDb3.results.map(removeGeneratedFieldForComparison) shouldEqual filter3

      val filter4 = eventsToInsertReduced.filter { e =>
        e.id.exists(_.toLowerCase.contains("evt-bl9qrit05af5ecnvvvl")) || e.name.toLowerCase.contains("evt-bl9qrit05af5ecnvvvl") || e.description.toLowerCase.contains("evt-bl9qrit05af5ecnvvvl")
      }.sortWith(_.timestamp >= _.timestamp).take(10)
      val eventsFromDb4 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndQueryString(Option(10), Option(0), Option("EVT-bl9qrit05af5ecnvvvl"))).futureValue
      eventsFromDb4.results should have size filter4.size
      eventsFromDb4.results.map(removeGeneratedFieldForComparison) shouldEqual filter4

      val filter5 = eventsToInsertReduced.filter { e =>
        e.id.exists(_.toLowerCase.contains("consectetur adipi")) || e.name.toLowerCase.contains("consectetur adipi") || e.description.toLowerCase.contains("consectetur adipi")
      }.sortWith(_.timestamp >= _.timestamp).take(10)
      val eventsFromDb5 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndQueryString(Option(10), Option(0), Option("CONSECTETUR ADIPI"))).futureValue
      eventsFromDb5.results should have size filter5.size
      eventsFromDb5.results.map(removeGeneratedFieldForComparison) shouldEqual filter5

      val filter6 = eventsToInsertReduced.filter { e =>
        e.id.exists(_.toLowerCase.contains("nunc")) || e.name.toLowerCase.contains("nunc") || e.description.toLowerCase.contains("nunc")
      }.sortWith(_.timestamp >= _.timestamp).take(10)
      val eventsFromDb6 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndQueryString(Option(10), Option(0), Option("Nunc"))).futureValue
      eventsFromDb6.results should have size filter6.size
      eventsFromDb6.results.map(removeGeneratedFieldForComparison) shouldEqual filter6

      val filter7 = eventsToInsertReduced.filter { e =>
        e.id.exists(_.toLowerCase.contains("dcsy")) || e.name.toLowerCase.contains("dcsy") || e.description.toLowerCase.contains("dcsy")
      }.sortWith(_.timestamp >= _.timestamp).take(10)
      val eventsFromDb7 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndQueryString(Option(10), Option(0), Option("DCSy"))).futureValue
      eventsFromDb7.results should have size filter7.size
      eventsFromDb7.results.map(removeGeneratedFieldForComparison) shouldEqual filter7

      val filter8 = eventsToInsertReduced.filter { e =>
        e.id.exists(_.toLowerCase.contains("the")) || e.name.toLowerCase.contains("the") || e.description.toLowerCase.contains("the")
      }.sortWith(_.timestamp >= _.timestamp).take(10)
      val eventsFromDb8 = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffsetAndQueryString(Option(10), Option(0), Option("The"))).futureValue
      eventsFromDb8.results should have size filter8.size
      eventsFromDb8.results.map(removeGeneratedFieldForComparison) shouldEqual filter8
    }

    "apply all filters and search string at once" in {
      val eventsToInsert =
        Seq(
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl0"), "GT-Event-1", "I saw xyz abc yesterday", "Kerberos attack", 10, 897352956563853L, "Kerberos pipeline", "19475653", Seq("Initial Access"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl1"), "GT-Event-2", "I saw xyz yesterday abc", "Kerberos attack", 20, 897352956563860L, "Kerberos pipeline", "19475654", Nil, Seq("a"), obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl2"), "GT-Event-3", "I saw xyz abc yesterday", "Kerberos attack", 30, 897352956563870L, "Kerberos pipeline", "19475655", Seq("Initial Access"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl3"), "GT-Event-4", "I saw xyz yesterday abc", "Kerberos attack", 40, 897352956563880L, "Kerberos pipeline", "19475653", Nil, Seq("Drive-by Compromise", "Account Manipulation"), obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl4"), "Event-GT-5", "I saw xyz abc yesterday", "Kerberos attack", 50, 897352956563890L, "Kerberos pipeline", "19475654", Seq("Initial Access"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl5"), "Event-GT-6", "I saw xyz yesterday abc", "Kerberos attack", 60, 897352956563000L, "Kerberos pipeline", "19475655", Nil, Seq("Drive-by Compromise", "Account Manipulation"), obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl6"), "Event-GT-7", "I saw xyz abc yesterday", "Kerberos attack", 70, 897352956563910L, "Kerberos pipeline", "19475653", Seq("Initial Access"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl7"), "Event-GT-8", "I saw xyz yesterday abc", "Kerberos attack", 80, 897352956563920L, "Kerberos pipeline", "19475654", Seq("Initial Access"), Seq("Drive-by Compromise", "Account Manipulation"), obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("EVT-bl9qrit05af5ecnvvvl8"), "Event-GT-9", "I saw xyz abc yesterday", "Kerberos attack", 90, 897352956563930L, "Kerberos pipeline", "19475655", Seq("Initial Access"), Seq("Drive-by Compromise", "Account Manipulation"), obsSysIdOpt, agentId = Some(tag.propertyId)))

      val eventsToInsertReduced = eventsToInsert.map(_.toEventReduced(tagId))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val filter1 = {
        eventsToInsertReduced
          .filter { e =>
            e.name.toLowerCase.contains("xyz abc") ||
              e.id.exists(_.contains("xyz abc")) ||
              e.description.toLowerCase.contains("xyz abc")
          }
          .filter(e => e.timestamp >= 897352956563880L && e.timestamp <= 897352956563930L)
          .filter(e => e.severity >= 30 && e.severity <= 90)
          .filter(e => e.categories containsSlice Seq("Initial Access"))
          .filter(e => e.tactics containsSlice Seq("Account Manipulation"))
          .sortWith(_.timestamp >= _.timestamp)
      }

      val eventsFromDb1 = eventRepository.getByObservedSystem(
        tagId,
        GetEventsReq(
          limit = Option(10), offset = Option(0),
          fromSeverity = Option(30), toSeverity = Option(90),
          fromTimestamp = Option(897352956563880L), toTimestamp = Option(897352956563930L),
          categories = Option(Seq("Initial Access")), tactics = Option(Seq("Account Manipulation")),
          queryString = Option("XYZ ABC"))).futureValue
      eventsFromDb1.results should have size filter1.size
      eventsFromDb1.results.map(removeGeneratedFieldForComparison) shouldEqual filter1
    }

    "ping the database for health checks" in {
      eventRepository.pingDatabase().futureValue should be(true)
    }

    "events count for last N days" in {
      val numTruncated = truncateEventsCount().futureValue
      logger.debug(s"$numTruncated events truncated.")

      val extraInfoJson = JsObject(Map("team" -> JsString("cyber")))

      val eventsToInsert =
        Seq(
          EventMessage(Option("event1"), "GT-Event-1", "description", "Kerberos attack", 90, System.currentTimeMillis(), "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, Some(extraInfoJson)),
          EventMessage(Option("event2"), "GT-Event-2", "description", "Kerberos attack", 90, System.currentTimeMillis(), "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, None, agentId = Some(tag.propertyId)),
          EventMessage(Option("event3"), "GT-Event-3", "description", "Kerberos attack", 90, System.currentTimeMillis(), "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, None, ruleId = Some(tag.propertyId)))

      tagsRepository.add(tag)
      tagsRepository.add(tag.copy(kind = PropertyType.RULE))
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val eventsFromDb = eventRepository.getCount(tagId, 90).futureValue
      eventsFromDb.count should be(eventsToInsert.length)
    }

    "events count by their severity for last 24 hours" in {
      val numTruncated = truncateEventsCount().futureValue
      logger.debug(s"$numTruncated events truncated.")

      val extraInfoJson = JsObject(Map("team" -> JsString("cyber")))

      val today = System.currentTimeMillis()
      val yesterday = today - (86400 * 1000) - 1
      val ereyesterday = yesterday - (86400 * 1000) - 1

      val eventsToInsert =
        Seq(
          EventMessage(Option("event1"), "GT-Event-1", "description", "Kerberos attack", 90, today, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, Some(extraInfoJson), agentId = Some(tag.propertyId)),
          EventMessage(Option("event2"), "GT-Event-2", "description", "Kerberos attack", 60, today, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event3"), "GT-Event-3", "description", "Kerberos attack", 30, today, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event4"), "GT-Event-2", "description", "Kerberos attack", 90, yesterday, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event5"), "GT-Event-3", "description", "Kerberos attack", 60, yesterday, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event6"), "GT-Event-2", "description", "Kerberos attack", 30, yesterday, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event7"), "GT-Event-3", "description", "Kerberos attack", 90, ereyesterday, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event8"), "GT-Event-2", "description", "Kerberos attack", 60, ereyesterday, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event9"), "GT-Event-3", "description", "Kerberos attack", 30, ereyesterday, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event10"), "GT-Event-3", "description", "Kerberos attack", 90, today, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val eventsFromDb = eventRepository.getCountBySeverity(tagId).futureValue
      eventsFromDb should be(CountBySeverity(4, 1, 1, 2))
    }

    "events count by their severity trigger must gracefully exit" in {
      val numTruncated = truncateEventsCount().futureValue
      logger.debug(s"$numTruncated events truncated.")

      val extraInfoJson = JsObject(Map("team" -> JsString("cyber")))

      val today = System.currentTimeMillis()
      val yesterday = today - (86400 * 1000) - 1
      val ereyesterday = yesterday - (86400 * 1000) - 1

      val eventsToInsert =
        Seq(
          EventMessage(None, "GT-Event-1", "description", "Kerberos attack", 90, today, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, None, Some(extraInfoJson)),
          EventMessage(None, "GT-Event-2", "description", "Kerberos attack", 60, today, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, None),
          EventMessage(None, "GT-Event-3", "description", "Kerberos attack", 30, today, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, None),
          EventMessage(None, "GT-Event-2", "description", "Kerberos attack", 90, yesterday, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, Some(tagId), agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-3", "description", "Kerberos attack", 60, yesterday, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, Some(tagId), agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-2", "description", "Kerberos attack", 30, yesterday, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, Some(tagId), agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-3", "description", "Kerberos attack", 90, ereyesterday, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, Some(tagId), agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-2", "description", "Kerberos attack", 60, ereyesterday, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, Some(tagId), agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-3", "description", "Kerberos attack", 30, ereyesterday, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, Some(tagId), agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-3", "description", "Kerberos attack", 90, today, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, None))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      def query = {
        sql"""
           select sum(total), sum(low), sum(medium), sum(high)
           from cyber_event_service.amp_events_count_by_severity
           where tag_id = 'UNKNOWN' and (datetime > date_trunc('minute', CURRENT_TIMESTAMP)::timestamp - interval '1 day' and datetime <= date_trunc('minute', CURRENT_TIMESTAMP)::timestamp)
         """.as[CountBySeverity].head
      }

      val result = db.run(query).futureValue
      result should be(CountBySeverity(4, 1, 1, 2))
    }

    "retrieve events after a certain incremental index" in {
      val last = 5

      val ts = System.currentTimeMillis()
      val eventsToInsert =
        Seq(
          EventMessage(Option("event1"), "GT-Event-1", "description", "Kerberos attack", 90, ts, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event2"), "GT-Event-2", "description", "Kerberos attack", 60, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event3"), "GT-Event-3", "description", "Kerberos attack", 30, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event4"), "GT-Event-2", "description", "Kerberos attack", 90, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event5"), "GT-Event-3", "description", "Kerberos attack", 60, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event6"), "GT-Event-2", "description", "Kerberos attack", 30, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event7"), "GT-Event-3", "description", "Kerberos attack", 90, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event8"), "GT-Event-2", "description", "Kerberos attack", 60, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event9"), "GT-Event-3", "description", "Kerberos attack", 30, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val eventsFromDb = eventRepository.getNextEventBatch(tagId, Some(last)).futureValue
      eventsFromDb.results.forall(_.incIdx.exists(_ > last)) should be(true)
      eventsFromDb.results.size shouldNot be(0)
    }

    "return an empty list for too-high index" in {
      val last = 1000

      val ts = System.currentTimeMillis()
      val eventsToInsert =
        Seq(
          EventMessage(Option("event1"), "GT-Event-1", "description", "Kerberos attack", 90, ts, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event2"), "GT-Event-2", "description", "Kerberos attack", 60, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event3"), "GT-Event-3", "description", "Kerberos attack", 30, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event4"), "GT-Event-2", "description", "Kerberos attack", 90, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event5"), "GT-Event-3", "description", "Kerberos attack", 60, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event6"), "GT-Event-2", "description", "Kerberos attack", 30, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event7"), "GT-Event-3", "description", "Kerberos attack", 90, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event8"), "GT-Event-2", "description", "Kerberos attack", 60, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event9"), "GT-Event-3", "description", "Kerberos attack", 30, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val eventsFromDb = eventRepository.getNextEventBatch(tagId, Some(last)).futureValue
      eventsFromDb.results.size should be(0)
    }

    "return the last index from the batch" in {
      val last = 5

      val ts = System.currentTimeMillis()
      val eventsToInsert =
        Seq(
          EventMessage(Option("event1"), "GT-Event-1", "description", "Kerberos attack", 90, ts, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event2"), "GT-Event-2", "description", "Kerberos attack", 60, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event3"), "GT-Event-3", "description", "Kerberos attack", 30, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event4"), "GT-Event-2", "description", "Kerberos attack", 90, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event5"), "GT-Event-3", "description", "Kerberos attack", 60, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event6"), "GT-Event-2", "description", "Kerberos attack", 30, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event7"), "GT-Event-3", "description", "Kerberos attack", 90, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event8"), "GT-Event-2", "description", "Kerberos attack", 60, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event9"), "GT-Event-3", "description", "Kerberos attack", 30, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val eventsFromDb = eventRepository.getNextEventBatch(tagId, Some(last)).futureValue
      val lastFromList = eventsFromDb.results.map(_.incIdx.getOrElse(0L)).max
      eventsFromDb.last should be(lastFromList)
      eventsFromDb.results.size shouldNot be(0)
    }

    "return the same last value when the response is empty" in {
      val last = 1000

      val ts = System.currentTimeMillis()
      val eventsToInsert =
        Seq(
          EventMessage(Option("event1"), "GT-Event-1", "description", "Kerberos attack", 90, ts, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event2"), "GT-Event-2", "description", "Kerberos attack", 60, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event3"), "GT-Event-3", "description", "Kerberos attack", 30, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event4"), "GT-Event-2", "description", "Kerberos attack", 90, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event5"), "GT-Event-3", "description", "Kerberos attack", 60, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event6"), "GT-Event-2", "description", "Kerberos attack", 30, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event7"), "GT-Event-3", "description", "Kerberos attack", 90, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event8"), "GT-Event-2", "description", "Kerberos attack", 60, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event9"), "GT-Event-3", "description", "Kerberos attack", 30, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val eventsFromDb = eventRepository.getNextEventBatch(tagId, Some(last)).futureValue
      eventsFromDb.last should be(last)
      eventsFromDb.results.size should be(0)
    }

    "return the most recent events for last < 0" in {
      val last = -1

      val ts = System.currentTimeMillis()
      val eventsToInsert =
        Seq(
          EventMessage(Option("event1"), "GT-Event-1", "description", "Kerberos attack", 90, ts, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event2"), "GT-Event-2", "description", "Kerberos attack", 60, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event3"), "GT-Event-3", "description", "Kerberos attack", 30, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event4"), "GT-Event-2", "description", "Kerberos attack", 90, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event5"), "GT-Event-3", "description", "Kerberos attack", 60, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event6"), "GT-Event-2", "description", "Kerberos attack", 30, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event7"), "GT-Event-3", "description", "Kerberos attack", 90, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event8"), "GT-Event-2", "description", "Kerberos attack", 60, ts, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event9"), "GT-Event-3", "description", "Kerberos attack", 30, ts, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val eventsFromDb = eventRepository.getNextEventBatch(tagId, Some(last)).futureValue
      eventsFromDb.results.size should be(4)
    }

    "remove old events" in {
      val numTruncated = truncateEventsCount().futureValue
      logger.debug(s"$numTruncated events truncated.")

      val now = System.currentTimeMillis()
      val fourDaysAgo = now - (4 * 86400 * 1000)

      val eventsToInsert =
        Seq(
          EventMessage(Option("event1"), "GT-Event-1", "description", "Kerberos attack", 90, fourDaysAgo, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event2"), "GT-Event-2", "description", "Kerberos attack", 60, fourDaysAgo, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event3"), "GT-Event-3", "description", "Kerberos attack", 30, fourDaysAgo, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event4"), "GT-Event-2", "description", "Kerberos attack", 90, fourDaysAgo, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event5"), "GT-Event-3", "description", "Kerberos attack", 60, now, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event6"), "GT-Event-2", "description", "Kerberos attack", 30, fourDaysAgo, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event7"), "GT-Event-3", "description", "Kerberos attack", 90, fourDaysAgo, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event8"), "GT-Event-2", "description", "Kerberos attack", 60, fourDaysAgo, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)),
          EventMessage(Option("event9"), "GT-Event-3", "description", "Kerberos attack", 30, fourDaysAgo, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt, agentId = Some(tag.propertyId)))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val eventIds = List("event1", "event2")
      // insert incident to populate event and incident map table
      val incidentAdded = incidentRepository.add(Incident(
        id = Some("Incident-1"),
        title = "Incident Title",
        severity = 30,
        reporterId = 1L,
        tagId = tagId,
        createdBy = 1L,
        lastModifiedBy = 1L), eventIds).futureValue
      incidentAdded.id should be(Some("Incident-1"))

      val mapCount1 = eventIncidentMapCount(eventIds).futureValue
      mapCount1 should be(eventIds.length)

      // events that are linked to incidents should not be deleted
      val numPurged = eventRepository.removeOlderThanDays(3).futureValue
      numPurged should be(6)

      val mapCount2 = eventIncidentMapCount(eventIds).futureValue
      mapCount2 should be(2)
    }

    "return events with non-empty observed system id and same as tagId" in {
      // kafka messages without obs id, though property and tag is available in db
      val eventsToInsert =
        Seq(
          EventMessage(None, "GT-Event-1", "description", "Kerberos attack", 10, 897352956563853L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, None, agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-2", "description", "Kerberos attack", 20, 897352956563860L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, None, agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-3", "description", "Kerberos attack", 30, 897352956563870L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, None, agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-4", "description", "Kerberos attack", 40, 897352956563880L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, None, agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-5", "description", "Kerberos attack", 50, 897352956563890L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, None, agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-6", "description", "Kerberos attack", 60, 897352956563000L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, None, agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-7", "description", "Kerberos attack", 70, 897352956563910L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, None, agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-8", "description", "Kerberos attack", 80, 897352956563920L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, None, agentId = Some(tag.propertyId)),
          EventMessage(None, "GT-Event-9", "description", "Kerberos attack", 90, 897352956563930L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, None, agentId = Some(tag.propertyId)))

      tagsRepository.add(tag)
      val numAdded = eventRepository.add(eventsToInsert).futureValue
      numAdded should be(eventsToInsert.size)

      val resp = eventRepository.getByObservedSystem(tagId, GetEventsReq.fromLimitAndOffset(None, None)).futureValue
      resp.results.forall(_.observedSystemId == tagId) shouldEqual true
    }
  }
}