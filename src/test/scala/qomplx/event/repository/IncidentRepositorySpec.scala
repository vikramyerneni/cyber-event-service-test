package qomplx.event.repository

import com.fractal.auth.client.deprecated.{ User => AuthServiceUser }
import com.typesafe.config.Config
import org.mockito.Mockito.when
import org.mockito.ArgumentMatchers.any
import org.scalatest.mockito.MockitoSugar
import qomplx.event.config.db.LiquibaseUtils
import qomplx.event.model._
import qomplx.event.routes._
import qomplx.event.thirdpartyintegrations.model.{ ThirdPartySystem, TicketingSystemMail }
import qomplx.event.thirdpartyintegrations.repository.{ ThirdPartySystemConfigRepository, TicketingSystemMailRepository }
import qomplx.event.thirdpartyintegrations.routes.CreateTSConfigPayload
import qomplx.event.util.{ EncryptionUtils, QomplxError }
import qomplx.event.util.SlickPostgresProfile.api._
import qomplx.event.{ BaseSpecWithMigrations, JsonSupport, ObservedSystemId }

import scala.concurrent.duration._
import scala.concurrent.{ Await, ExecutionContext, Future }

class IncidentRepositorySpec extends BaseSpecWithMigrations with JsonSupport {

  val encryptionUtils = MockitoSugar.mock[EncryptionUtils]
  when(encryptionUtils.encryptToken(any[String])).thenReturn(Array.empty[Byte])

  private lazy val db = postgresConnection.database
  private lazy val eventRepository = new EventRepository(db)
  private lazy val incidentRepository = new IncidentRepository(db)
  private lazy val thirdPartySystemConfigRepo = new ThirdPartySystemConfigRepository(db, encryptionUtils)

  private lazy val repoUtils = new RepositorySpecUtils(db, logger)

  // test data
  val tagId = s"${java.util.UUID.randomUUID()}-test-obs123"
  val obsSysIdOpt = Some(tagId)
  val obsSysId1 = s"${java.util.UUID.randomUUID()}-test-obs123"
  val eventMessages =
    Seq(
      EventMessage(Option("event-1"), "GT-Event-1", "description", "Kerberos attack", 90, 897352956563853L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, obsSysIdOpt),
      EventMessage(Option("event-2"), "GT-Event-2", "description", "Kerberos attack", 90, 897352956563860L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, obsSysIdOpt),
      EventMessage(Option("event-3"), "GT-Event-3", "description", "Kerberos attack", 90, 897352956563870L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, obsSysIdOpt))
  val events =
    Seq(
      Event(Option("event-1"), "GT-Event-1", "description", "Kerberos attack", 90, 897352956563853L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, tagId),
      Event(Option("event-2"), "GT-Event-2", "description", "Kerberos attack", 90, 897352956563860L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, tagId),
      Event(Option("event-3"), "GT-Event-3", "description", "Kerberos attack", 90, 897352956563870L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, tagId))
  val random = scala.util.Random
  val assigneeId = 1L
  val searchString = "IncidentTest"
  val incidents = for (i <- 1 to 14) yield {
    val title = if (i % 2 == 0) s"TestIncident-$i" else s"$i-$searchString-$i" // even entries title
    val (aId, aEmail) =
      if (i % 2 == 0) (Option(assigneeId), Option("bob@somecompany.com")) // even entries assignee id 1
      else (None, None)

    Incident(id = Some(s"INC-test-$i"), title = title, severity = random.nextInt(100),
      reporterId = 1, tagId = tagId, createdBy = 1, lastModifiedBy = 1, assigneeId = aId, assigneeEmail = aEmail,
      createdOn = qomplx.event.now + (i * 1000))
  }

  val incidentToAdd = Incident(id = Some("INC-test"), title = "TestIncident", severity = 99, reporterId = 1, tagId = tagId, createdBy = 1, lastModifiedBy = 1)
  val incidentFileToAdd = IncidentFile(name = "Incident filename", description = None, s3Key = "Incident s3 key", incidentId = incidentToAdd.id.get, createdBy = 1, lastModifiedBy = 1)
  val incidentUrlToAdd = IncidentURL(url = "https://qomplx.com", incidentId = incidentToAdd.id.get, createdBy = 1, lastModifiedBy = 1)
  val incidentNoteToAdd = IncidentNote(note = "Fractal is now complex", incidentId = incidentToAdd.id.get, createdBy = 1, lastModifiedBy = 1)

  // third party configuration
  val payload = CreateTSConfigPayload("config 1", Some("config 1 desc"), ThirdPartySystem.JIRA, Option(List("a@b.com", "y@z.com")))
  val user = MockitoSugar.mock[AuthServiceUser]
  when(user.uniqueId).thenReturn("1")

  private def listMails(configId: String): Future[Seq[TicketingSystemMail]] = db.run(Tables.TicketingSystemMails.filter(_.configId === configId).result)

  override def beforeAll(): Unit = {
    super.beforeAll()

    val numDeletedMappingEntry = repoUtils.deleteEventIncidentMappings(events, db).futureValue
    logger.info(s"Deleted $numDeletedMappingEntry records before running the tests")

    // delete incidents linked to observed system id
    val numDeleted = repoUtils.deleteIncidentsForObservedSystem(tagId, db).futureValue
    logger.info(s"Deleted $numDeleted records before running the tests")

    // insert events
    val numAddedEvents = eventRepository.add(eventMessages).futureValue
    logger.info(s"Added $numAddedEvents event records before running the tests")

    // insert incidents
    val numAddedIncidents =
      Future.sequence(incidents.map(inc => {
        incidentRepository.add(inc, List("event-1"))
      })).futureValue
    logger.info(s"Added $numAddedIncidents incident records before running the tests")
  }

  override def afterAll(): Unit = {
    val numDeletedMappingEntry = repoUtils.deleteEventIncidentMappings(events, db).futureValue
    logger.info(s"Deleted $numDeletedMappingEntry records before finishing the tests")

    val numDeleted = repoUtils.deleteIncidentsForObservedSystem(tagId, db).futureValue
    logger.info(s"Deleted $numDeleted records before finishing the tests")

    val numDeletedEvents = repoUtils.deleteEvents(events, db).futureValue
    logger.info(s"Deleted $numDeletedEvents event records before finishing the tests")

    super.afterAll()
  }

  def getFile(fileId: String, incidentId: String, db: Database): Future[Option[IncidentFile]] = {
    val getFileStatement = Tables.IncidentFiles.filter(file => file.id === fileId && file.incidentId === incidentId).result
    val files = db.run(getFileStatement.transactionally)
      .recover {
        case t =>
          logger.error(s"The following exception was thrown when trying to fetch file ${fileId}: ${t.getMessage}")
          Seq()
      }
    files.map(_.headOption)
  }

  def getURL(urlId: String, incidentId: String, db: Database): Future[Option[IncidentURL]] = {
    val getUrlStatement = Tables.IncidentURLs.filter(url => url.id === urlId && url.incidentId === incidentId).result
    val urls = db.run(getUrlStatement.transactionally)
      .recover {
        case t =>
          logger.error(s"The following exception was thrown when trying to fetch url ${urlId}: ${t.getMessage}")
          Seq()
      }
    urls.map(_.headOption)
  }

  def getNote(noteId: String, incidentId: String, db: Database): Future[Option[IncidentNote]] = {
    val getNoteStatement = Tables.IncidentNotes.filter(note => note.id === noteId && note.incidentId === incidentId).result
    val notes = db.run(getNoteStatement.transactionally)
      .recover {
        case t =>
          logger.error(s"The following exception was thrown when trying to fetch note ${noteId}: ${t.getMessage}")
          Seq()
      }
    notes.map(_.headOption)
  }

  "list incident method" should {
    s"list incidents of observed system ${tagId}" in {
      val limit = 10
      val offset = 0
      val filteredIncidents = incidents
        .filter(_.tagId == tagId)
      val params = GetIncidentsReq(limit, offset, None, None)
      val actualResult = incidentRepository.getByObservedSystem(tagId, params).futureValue

      validateGetIncidentsRes(actualResult, params, GetIncidentsRes(
        IncidentListDTO(
          filteredIncidents
            .sortWith(_.createdOn > _.createdOn)
            .slice(offset, offset + params.limit)), filteredIncidents.length))
    }

    s"list incidents of observed system ${obsSysId1}, should be empty" in {
      val limit = 10
      val offset = 0
      val filteredIncidents = incidents
        .filter(_.tagId == obsSysId1)
      val params = GetIncidentsReq(limit, offset, None, None)
      val actualResult = incidentRepository.getByObservedSystem(obsSysId1, params).futureValue

      validateGetIncidentsRes(actualResult, params, GetIncidentsRes(
        IncidentListDTO(
          filteredIncidents
            .sortWith(_.createdOn > _.createdOn)
            .slice(offset, offset + params.limit)), filteredIncidents.length))
    }

    s"limit and offset values should be reset to default values" in {
      val limit = 120
      val offset = 510
      val filteredIncidents = incidents
        .filter(_.tagId == tagId)
      val params = GetIncidentsReq(limit, offset, None, None)
      val actualResult = incidentRepository.getByObservedSystem(tagId, params).futureValue

      validateGetIncidentsRes(actualResult, params, GetIncidentsRes(
        IncidentListDTO(
          filteredIncidents
            .sortWith(_.createdOn > _.createdOn)
            .slice(params.offset, params.offset + params.limit)), filteredIncidents.length))

      params.limit shouldEqual 10
      params.offset shouldEqual 0
    }

    s"list incidents of observed system ${tagId} where assignee is ${assigneeId}" in {
      val limit = 10
      val offset = 0
      val filteredIncidents = incidents
        .filter(_.tagId == tagId)
        .filter(_.assigneeId == Option(assigneeId))
      val params = GetIncidentsReq(limit, offset, Option(assigneeId), None)
      val actualResult = incidentRepository.getByObservedSystem(tagId, params).futureValue

      validateGetIncidentsRes(actualResult, params, GetIncidentsRes(
        IncidentListDTO(
          filteredIncidents
            .sortWith(_.createdOn > _.createdOn)
            .slice(offset, offset + limit)), filteredIncidents.length))
    }

    s"list incidents of observed system ${tagId} where search string is ${searchString}" in {
      val limit = 10
      val offset = 0
      val filteredIncidents = incidents
        .filter(_.tagId == tagId)
        .filter(_.title.contains(searchString))
      val params = GetIncidentsReq(limit, offset, None, Option(searchString))
      val actualResult = incidentRepository.getByObservedSystem(tagId, params).futureValue

      validateGetIncidentsRes(actualResult, params, GetIncidentsRes(
        IncidentListDTO(
          filteredIncidents
            .sortWith(_.createdOn > _.createdOn)
            .slice(offset, offset + limit)), filteredIncidents.length))
    }

    s"list incidents of observed system ${tagId} where search assignee is ${assigneeId} and string is ${searchString}" in {
      val limit = 10
      val offset = 0
      val filteredIncidents = incidents
        .filter(_.tagId == tagId)
        .filter(_.assigneeId == Option(assigneeId))
        .filter(_.title.startsWith(searchString))
      val params = GetIncidentsReq(limit, offset, Option(assigneeId), Option(searchString))
      val actualResult = incidentRepository.getByObservedSystem(tagId, params).futureValue

      validateGetIncidentsRes(actualResult, params, GetIncidentsRes(
        IncidentListDTO(
          filteredIncidents
            .sortWith(_.createdOn > _.createdOn)
            .slice(offset, offset + limit)), filteredIncidents.length))
    }
  }

  "list incidents and event IDs method" should {
    s"list incidents and their event IDs based on observed system ${tagId}" in {
      val filteredIncidents = incidents.filter(_.tagId == tagId)
      val params = ExportIncidentsParams()
      val result = incidentRepository.getIncidentsAndEventIdsByObservedSystem(tagId, params, 100).futureValue
      result.forall { inc =>
        inc.eventIds.size should be(1)
        inc.eventIds.head == "event-1"
      }
    }
  }

  "add incident method" should {
    "add incident and return DTO object" in {
      val events = List("event-1")

      incidentRepository.add(incidentToAdd, events).futureValue
      val actualIncidentDTO = incidentRepository.get(incidentFileToAdd.incidentId, tagId).futureValue
      actualIncidentDTO.isDefined shouldEqual true
      validateIncidentDTO(actualIncidentDTO.get, incidentToAdd.toIncidentDTO)
    }

    "add incident and related email entries" in {
      val configId = thirdPartySystemConfigRepo.create(tagId, payload, user, "some-long-token").futureValue.value
      val events = List("event-1")
      incidentRepository.add(incidentToAdd.copy(id = Some("INC-test-101")), events).futureValue
      val mails = listMails(configId).futureValue
      mails.length shouldEqual 1
      mails.head.configId shouldEqual configId
    }

    "throw an custom error if events in incident is not valid" in {
      val events = List("event-not-valid")

      an[QomplxError] should be thrownBy {
        Await.result(incidentRepository.add(incidentToAdd.copy(id = Some("new-id")), events), 5 seconds)
      }
    }

    "throw an error if incident exists with same primary key" in {
      val events = List("event-1")

      an[RuntimeException] should be thrownBy {
        incidentRepository.add(incidentToAdd, events).futureValue
      }
    }
  }

  "get incident method" should {
    "get incident and return DTO object" in {
      val actualIncidentDTO = incidentRepository.get(incidentToAdd.id.get, incidentToAdd.tagId).futureValue
      actualIncidentDTO.isDefined shouldEqual true
      validateIncidentDTO(actualIncidentDTO.get, incidentToAdd.toIncidentDTO)
    }

    "return None if incident doesn't exist in observed system" in {
      val actualIncidentDTO = incidentRepository.get("id-not-exist", tagId).futureValue
      actualIncidentDTO shouldEqual None
    }
  }

  "add file to incident method" should {
    "add file to incident and return DTO object" in {
      incidentRepository.addFile(incidentFileToAdd, tagId).futureValue
      val actualFile = getFile(incidentFileToAdd.id, incidentFileToAdd.incidentId, db).futureValue
      actualFile.isDefined shouldEqual true
      validateIncidentFile(actualFile.get, incidentFileToAdd)
    }

    "throw an error if incident file exist with same primary key" in {
      an[RuntimeException] should be thrownBy {
        incidentRepository.addFile(incidentFileToAdd, tagId).futureValue
      }
    }

    "return None if incident doesn't exist in observed system" in {
      incidentRepository.addFile(incidentFileToAdd, "observed-system-not-exist").futureValue
      val actualFile = getFile(incidentFileToAdd.id, "observed-system-not-exist", db).futureValue
      actualFile shouldEqual None
    }
  }

  "delete file linked to incident method" should {
    "delete file to incident and return DTO object" in {
      incidentRepository.deleteFile(incidentFileToAdd.id, incidentFileToAdd.incidentId, tagId).futureValue
      val actualFile = getFile(incidentFileToAdd.id, incidentFileToAdd.incidentId, db).futureValue
      actualFile shouldEqual None
    }

    "return None if incident doesn't exist in observed system" in {
      val actualIncidentDTO = incidentRepository.deleteFile(incidentFileToAdd.id, incidentFileToAdd.incidentId, "observed-system-not-exist").futureValue
      actualIncidentDTO shouldEqual None
    }
  }

  "add url to incident method" should {
    "add url to incident and return DTO object" in {
      incidentRepository.addURL(incidentUrlToAdd, tagId).futureValue
      val actualUrl = getURL(incidentUrlToAdd.id, incidentUrlToAdd.incidentId, db).futureValue
      actualUrl.isDefined shouldEqual true
      validateIncidentUrl(actualUrl.get, incidentUrlToAdd)
    }

    "throw an error if incident url exist with same primary key" in {
      an[RuntimeException] should be thrownBy {
        incidentRepository.addURL(incidentUrlToAdd, tagId).futureValue
      }
    }

    "return None if incident doesn't exist in observed system" in {
      val actualIncidentDTO = incidentRepository.addURL(incidentUrlToAdd, "observed-system-not-exist").futureValue
      actualIncidentDTO shouldEqual None
    }
  }

  "delete url linked to incident method" should {
    "delete url to incident and return DTO object" in {
      incidentRepository.deleteURL(incidentUrlToAdd.id, incidentUrlToAdd.incidentId, tagId).futureValue
      val actualUrl = getURL(incidentUrlToAdd.id, incidentUrlToAdd.incidentId, db).futureValue
      actualUrl shouldEqual None
    }

    "return None if incident doesn't exist in observed system" in {
      val actualIncidentDTO = incidentRepository.deleteURL(incidentUrlToAdd.id, incidentFileToAdd.incidentId, "observed-system-not-exist").futureValue
      actualIncidentDTO shouldEqual None
    }
  }

  "add note to incident method" should {
    "add note to incident and return DTO object" in {
      incidentRepository.addNote(incidentNoteToAdd, tagId).futureValue
      val actualNote = getNote(incidentNoteToAdd.id, incidentNoteToAdd.incidentId, db).futureValue
      actualNote.isDefined shouldEqual true
      validateIncidentNote(actualNote.get, incidentNoteToAdd)
    }

    "throw an error if incident note exist with same primary key" in {
      an[RuntimeException] should be thrownBy {
        incidentRepository.addNote(incidentNoteToAdd, tagId).futureValue
      }
    }

    "return None if incident doesn't exist in observed system" in {
      incidentRepository.addNote(incidentNoteToAdd, "observed-system-not-exist").futureValue
      val actualNote = getNote(incidentNoteToAdd.id, "observed-system-not-exist", db).futureValue
      actualNote shouldEqual None
    }
  }

  "delete note linked to incident method" should {
    "delete note to incident and return DTO object" in {
      incidentRepository.deleteNote(incidentNoteToAdd.id, incidentFileToAdd.incidentId, tagId).futureValue
      val actualNote = getNote(incidentNoteToAdd.id, incidentNoteToAdd.incidentId, db).futureValue
      actualNote shouldEqual None
    }

    "return None if incident doesn't exist in observed system" in {
      val actualIncidentDTO = incidentRepository.deleteNote(incidentNoteToAdd.id, incidentFileToAdd.incidentId, "observed-system-not-exist").futureValue
      actualIncidentDTO shouldEqual None
    }
  }

  "update incident method" should {
    "update incident and return DTO object" in {
      val incidentToUpdate = incidentToAdd.copy(severity = 1, status = IncidentStatus.OPEN, assigneeId = Option(101L), assigneeEmail = Option("bob@somecompany.com"))

      val actualIncidentDTO = incidentRepository.update(incidentToUpdate).futureValue
      actualIncidentDTO.isDefined shouldEqual true
      validateIncidentDTO(actualIncidentDTO.get, incidentToUpdate.toIncidentDTO)
    }

    "return None if incident doesn't exist in observed system" in {
      val incidentToUpdate = incidentToAdd.copy(id = Some("id-not-exist"), severity = 1, status = IncidentStatus.OPEN)

      val actualIncidentDTO = incidentRepository.update(incidentToUpdate).futureValue
      actualIncidentDTO shouldEqual None
    }
  }

  "get incident attachments method" should {
    "get attachments and return DTO object" in {
      incidentRepository.addFile(incidentFileToAdd, tagId).futureValue
      incidentRepository.addNote(incidentNoteToAdd, tagId).futureValue
      incidentRepository.addURL(incidentUrlToAdd, tagId).futureValue
      val expectedAttachmentsDTO = AttachmentsDTO(Vector(incidentFileToAdd.toDTO), Vector(incidentUrlToAdd.toDTO), Vector(incidentNoteToAdd.toDTO))

      val attachmentsDTO = incidentRepository.getAttachments(incidentToAdd.id.get, incidentToAdd.tagId).futureValue
      attachmentsDTO.get shouldEqual expectedAttachmentsDTO

      incidentRepository.deleteNote(incidentNoteToAdd.id, incidentFileToAdd.incidentId, tagId).futureValue
      incidentRepository.deleteURL(incidentUrlToAdd.id, incidentFileToAdd.incidentId, tagId).futureValue
    }

    "return None if incident doesn't exist in observed system" in {
      val attachmentsDTO = incidentRepository.getAttachments(incidentToAdd.id.get, "observed-system-not-exist").futureValue
      attachmentsDTO shouldEqual None
    }
  }

  "get events method" should {
    "return event list linked to an incident" in {
      val eventList = incidentRepository.getEvents(incidentToAdd.id.get, incidentToAdd.tagId).futureValue
      eventList.toList.map(_.copy(incIdx = None)) shouldEqual Vector(events.head).toList
    }

    "return empty list if incident doesn't exist in observed system" in {
      val eventList = incidentRepository.getEvents(incidentToAdd.id.get, "observed-system-not-exist").futureValue
      eventList should be('empty)
    }
  }

  "get categories method" should {
    "return categories list from database" in {
      val categoriesList = incidentRepository.getCategories().futureValue
      categoriesList should be('nonEmpty)
    }
  }

  "get tactics method" should {
    "return tactics list from database" in {
      val tacticsList = incidentRepository.getTactics().futureValue
      tacticsList should be('nonEmpty)
    }
  }

  "get file method" should {
    "return single file details linked to an incident" in {
      val incidentFile = incidentRepository.getFile(incidentFileToAdd.id, incidentFileToAdd.incidentId, tagId).futureValue
      incidentFile.get shouldEqual incidentFileToAdd

      incidentRepository.deleteFile(incidentFileToAdd.id, incidentFileToAdd.incidentId, tagId).futureValue
    }

    "return None if file or incident doesn't exist" in {
      val incidentFile = incidentRepository.getFile(incidentFileToAdd.id, incidentFileToAdd.incidentId, "observed-system-not-exist").futureValue
      incidentFile shouldEqual None
    }
  }

  "count incidents by their severity" should {
    "total must match sum of all level" in {
      val count = incidentRepository.getCountBySeverity(tagId).futureValue
      count.total should be(count.low + count.medium + count.high)
    }
  }

  private def validateIncidentDTO(actualIncidentDTO: IncidentDTO, expectedIncidentDTO: IncidentDTO) = {
    actualIncidentDTO.id should be('nonEmpty)
    actualIncidentDTO.title shouldEqual expectedIncidentDTO.title
    actualIncidentDTO.status shouldEqual expectedIncidentDTO.status
    actualIncidentDTO.reporterSource shouldEqual expectedIncidentDTO.reporterSource
    actualIncidentDTO.reporterId shouldEqual expectedIncidentDTO.reporterId
    actualIncidentDTO.observedSystemId shouldEqual expectedIncidentDTO.observedSystemId
    actualIncidentDTO.resolution shouldEqual expectedIncidentDTO.resolution
    actualIncidentDTO.assigneeId shouldEqual expectedIncidentDTO.assigneeId
    actualIncidentDTO.assigneeEmail shouldEqual expectedIncidentDTO.assigneeEmail
  }

  private def validateIncidentFile(actualIncidentFile: IncidentFile, expectedIncidentFile: IncidentFile) = {
    actualIncidentFile.id shouldEqual expectedIncidentFile.id
    actualIncidentFile.name shouldEqual expectedIncidentFile.name
    actualIncidentFile.description shouldEqual expectedIncidentFile.description
    actualIncidentFile.s3Key shouldEqual expectedIncidentFile.s3Key
    actualIncidentFile.incidentId shouldEqual expectedIncidentFile.incidentId
  }

  private def validateIncidentUrl(actualIncidentUrl: IncidentURL, expectedIncidentUrl: IncidentURL) = {
    actualIncidentUrl.id shouldEqual expectedIncidentUrl.id
    actualIncidentUrl.url shouldEqual expectedIncidentUrl.url
    actualIncidentUrl.incidentId shouldEqual expectedIncidentUrl.incidentId
  }

  private def validateIncidentNote(actualIncidentNote: IncidentNote, expectedIncidentNote: IncidentNote) = {
    actualIncidentNote.id shouldEqual expectedIncidentNote.id
    actualIncidentNote.note shouldEqual expectedIncidentNote.note
    actualIncidentNote.incidentId shouldEqual expectedIncidentNote.incidentId
  }

  private def validateGetIncidentsRes(actualResult: GetIncidentsRes, params: GetIncidentsReq, expectedResult: GetIncidentsRes) = {
    actualResult.results shouldEqual expectedResult.results
    actualResult.totalCount shouldEqual expectedResult.totalCount
    actualResult.results.length should be <= params.limit
  }
}

