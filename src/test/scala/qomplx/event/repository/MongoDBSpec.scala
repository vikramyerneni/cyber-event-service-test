package qomplx.event.repository

import com.typesafe.config.ConfigFactory
import qomplx.event.BaseSpec

import reactivemongo.core.commands.SuccessfulAuthentication

import scala.concurrent.{ Await, ExecutionContext }
import scala.concurrent.duration._
import ExecutionContext.Implicits.global

class MongoDBSpec extends BaseSpec {
  "Mongo" should {
    "authenticate" ignore {
      /**
       * Mongo instance added to /test/docker/docker-compose.yml
       *
       * I can't figure out how to do the replica set initiation with code... would love to automate.
       *
       * docker exec -it mongodb /bin/bash
       *
       * mongo
       *
       * Run this to instantiate the replica set:
       * rs.initiate(
       * {
       * _id : 'rs0',
       * members: [
       * { _id : 0, host : "mongodb:27017" }]})
       *
       * Run this to set up the user / password for authentication
       * use admin
       *
       * db.createUser({user: "db_user", pwd: "secret_password", roles: [ "readWrite", "dbAdmin" ]});
       */

      // tests connection to local database
      val host = "127.0.0.1:27017/mongodb"
      val config = ConfigFactory.load("application.conf")
      val mongo = new MongoDB(Seq(host), "secret_password", "admin", config.getConfig("mongodb.auth"), Option[Int](3))
      val testAuth = mongo.auth

      mongo.isInstanceOf[MongoDB] should be(true)
      val authResult = Await.result(testAuth, 5.seconds)
      authResult.isInstanceOf[SuccessfulAuthentication]

      // tests config values exist as expected in test/application.conf
      val dbName = config.getString("mongodb.cyber-db-name")
      val authEnabled = config.getBoolean("mongodb.auth.enabled")
      val authenticationDB = config.getString("mongodb.auth.db")
      val authenticationUser = config.getString("mongodb.auth.user")
      val authenticationPassword = config.getString("mongodb.auth.password")

      dbName should be("admin")
      authEnabled should be(true)
      authenticationDB should be("admin")
      authenticationUser should be("db_user")
      authenticationPassword should be("secret_password")
    }
  }
}
