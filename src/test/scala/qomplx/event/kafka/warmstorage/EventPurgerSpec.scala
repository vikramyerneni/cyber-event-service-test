package qomplx.event.kafka.warmstorage

import akka.actor.{ Actor, Props }
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.BaseSpecWithActorSystem

import EventPurger.Purge

class TestActor extends Actor with StrictLogging {
  def receive = {
    case Purge =>
      logger.info("Received a purge message")
  }
}

object TestActor {
  def props(): Props = Props(new TestActor())
}

class EventPurgerSpec extends BaseSpecWithActorSystem {

  "EventPurger" should {
    "send a message to the purger actor every ten seconds" in {
      val testActor = system.actorOf(TestActor.props())
      QuartzSchedulerExtension(system).schedule("EveryTenSeconds", testActor, Purge)
      Thread.sleep(30000L)
      system.terminate()
    }
  }
}