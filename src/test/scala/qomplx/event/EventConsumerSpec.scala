package qomplx.event

import java.time.Clock
import java.util.UUID

import akka.Done
import akka.kafka.ConsumerMessage.CommittableOffsetBatch
import akka.stream.scaladsl.{ Keep, Sink }
import io.findify.s3mock.S3Mock
import net.manub.embeddedkafka.EmbeddedKafka
import org.mockito.Mockito.when
import org.scalatest.BeforeAndAfterAll
import org.scalatest.mockito.MockitoSugar
import qomplx.event.alerting.email.EventsMailer
import qomplx.event.kafka.localstorage.WriteToDatabase
import qomplx.event.kafka.warmstorage.WriteToS3.S3UploadInfo
import qomplx.event.kafka.warmstorage.{ S3KeyGenerator, WriteToS3 }
import qomplx.event.model.EventMessage
import qomplx.event.repository.EventRepository
import qomplx.event.service.{ AWSConfig, S3BucketHandlerService }
import qomplx.event.vault.VaultManager
import spray.json._

import scala.concurrent.duration._
import scala.concurrent.{ Await, Future }

class EventConsumerSpec extends BaseSpecWithActorSystem with BeforeAndAfterAll with EmbeddedKafka with JsonSupport {

  val kafkaConf = appConfig.getConfig("cyber-event.kafka")
  val api: S3Mock = S3Mock(port = 8001, dir = "/tmp/s3")

  override def beforeAll(): Unit = {
    super.beforeAll()
    api.start
  }

  override def afterAll(): Unit = {
    api.stop
    super.afterAll()
  }

  val testObsSysId = "obs123"
  val testObsSysIdOpt = Some(testObsSysId)
  val extraInfoJson = JsObject(Map("team" -> JsString("cyber")))

  val testEvents =
    Seq(
      EventMessage(None, "GT-Event-1", "description", "Kerberos attack", 90, 897352956563853L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, testObsSysIdOpt, Some(extraInfoJson)),
      EventMessage(None, "GT-Event-2", "description", "Kerberos attack", 90, 897352956563860L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, testObsSysIdOpt),
      EventMessage(None, "GT-Event-3", "description", "Kerberos attack", 90, 897352956563870L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, testObsSysIdOpt))

  val mockEventRepository = MockitoSugar.mock[EventRepository]
  when(mockEventRepository.add(testEvents)).thenReturn(Future.successful(testEvents.length))

  val mockBadEventRepository = MockitoSugar.mock[EventRepository]
  when(mockBadEventRepository.add(testEvents)).thenReturn(Future.failed(new RuntimeException("database error")))

  val mockVaultManager = MockitoSugar.mock[VaultManager]
  when(mockVaultManager.s3AccessKey).thenReturn("")
  when(mockVaultManager.s3SecretKey).thenReturn("")

  val s3BucketHandlerService = new S3BucketHandlerService(AWSConfig(appConfig.getConfig("aws.s3.events-warm-storage"), mockVaultManager))

  val kafkaLocalStorageWriter = new WriteToDatabase(mockEventRepository, kafkaConf)
  val kafkaLocalStorageWriterBad = new WriteToDatabase(mockBadEventRepository, kafkaConf)
  val s3KeyGenerator = new S3KeyGenerator(Clock.systemUTC())
  val kafkaWarmStorageWriter = new WriteToS3(kafkaConf, s3BucketHandlerService, s3KeyGenerator)

  "KafkaEventConsumer" should {
    "pass along the offset batch" in {
      val stream =
        kafkaLocalStorageWriter.eventConsumerSource
          .groupedWithin(100, 5 seconds)
          .via(kafkaLocalStorageWriter.store)
          .map {
            case msg @ (numAddedToDb, committableMsgs) if (numAddedToDb == committableMsgs.size) =>
              logger.debug(s"Msg: $msg")
              numAddedToDb should be(testEvents.size)
              committableMsgs should have size (testEvents.size)

              committableMsgs.foldLeft(CommittableOffsetBatch.empty) { (batch, elem) => batch.updated(elem) }
            case (numAddedToDb, committableMsgs) =>
              logger.error(s"The number of events added to the database [$numAddedToDb] doesn't match the number of committable messages [${committableMsgs.size}]")
              CommittableOffsetBatch.empty
          }
          .toMat(Sink.seq)(Keep.both)

      withRunningKafka {
        testEvents.foreach(e => publishStringMessageToKafka(s"cyber-amp-events-$testObsSysId-1", e.toJson.compactPrint))

        val (control, offsetBatches) = stream.run()

        Thread.sleep(7000L)

        control.shutdown()
        offsetBatches.futureValue.foreach(batch => logger.debug(s"Offset batch: ${batch.offsets}"))
        offsetBatches.futureValue.head.offsets.head._2 should be(testEvents.size - 1) // offsets start at 0
      }
    }

    "log a serialization error and continue processing" in {
      val stream =
        kafkaLocalStorageWriter.eventConsumerSource
          .groupedWithin(100, 5 seconds)
          .via(kafkaLocalStorageWriter.store)
          .map {
            case msg @ (numAddedToDb, committableMsgs) if (numAddedToDb == committableMsgs.size) =>
              logger.debug(s"Msg: $msg")
              numAddedToDb should be(testEvents.size)
              committableMsgs should have size (testEvents.size)

              committableMsgs.foldLeft(CommittableOffsetBatch.empty) { (batch, elem) => batch.updated(elem) }
            case (numAddedToDb, committableMsgs) =>
              logger.error(s"The number of events added to the database [$numAddedToDb] doesn't match the number of committable messages [${committableMsgs.size}]")
              CommittableOffsetBatch.empty
          }
          .toMat(Sink.seq)(Keep.both)

      withRunningKafka {
        testEvents.foreach(e => publishStringMessageToKafka(s"cyber-amp-events-$testObsSysId-2", e.toJson.compactPrint))

        // send a non-event message, which will throw a serialization exception
        publishStringMessageToKafka(s"cyber-amp-events-$testObsSysId-2", """{"id":"bad-message"}""")

        val (control, offsetBatches) = stream.run()

        Thread.sleep(10000L)

        control.shutdown()
        offsetBatches.futureValue.foreach(batch => logger.debug(s"Offset batch: ${batch.offsets}"))
        offsetBatches.futureValue.head.offsets.head._2 should be(testEvents.size - 1) // offsets start at 0
      }
    }

    "pass downstream an empty offset batch if there is a database error" ignore {
      val stream =
        kafkaLocalStorageWriterBad.eventConsumerSource
          .groupedWithin(100, 5 seconds)
          .via(kafkaLocalStorageWriter.store)
          .map {
            case msg @ (numAddedToDb, committableMsgs) if (numAddedToDb == committableMsgs.size) =>
              logger.debug(s"Msg: $msg")
              numAddedToDb should be(testEvents.size)
              committableMsgs should have size (testEvents.size)

              committableMsgs.foldLeft(CommittableOffsetBatch.empty) { (batch, elem) => batch.updated(elem) }
            case (numAddedToDb, committableMsgs) =>
              logger.error(s"The number of events added to the database [$numAddedToDb] doesn't match the number of committable messages [${committableMsgs.size}]")
              CommittableOffsetBatch.empty
          }
          .toMat(Sink.seq)(Keep.both)

      withRunningKafka {
        testEvents.foreach(e => publishStringMessageToKafka(s"cyber-amp-events-$testObsSysId-3", e.toJson.compactPrint))

        val (control, offsetBatches) = stream.run()

        Thread.sleep(7000L)

        control.shutdown()
        offsetBatches.futureValue should be(empty)
      }
    }

    "consume and commit the messages" in {
      withRunningKafka {
        testEvents.foreach(e => publishStringMessageToKafka(s"cyber-amp-events-$testObsSysId-4", e.toJson.compactPrint))

        val (consumerControl, consumerDone) =
          kafkaLocalStorageWriter.eventConsumerSource
            .log("deserialize to event")
            .groupedWithin(100, 5 seconds)
            .via(kafkaLocalStorageWriter.store)
            .log("write to db")
            .map {
              case (numAddedToDb, committableMsgs) if (numAddedToDb == committableMsgs.size) =>
                committableMsgs.foldLeft(CommittableOffsetBatch.empty) { (batch, elem) => batch.updated(elem) }
              case (numAddedToDb, committableMsgs) =>
                logger.error(s"The number of events added to the database [$numAddedToDb] doesn't match the number of committable messages [${committableMsgs.size}]")
                CommittableOffsetBatch.empty
            }
            .map(_.commitScaladsl())
            .log("Kafka batch commit")
            .toMat(Sink.ignore)(Keep.both)
            .run()

        Thread.sleep(7000L)

        consumerControl.shutdown()
        consumerDone.futureValue should be(Done)
      }
    }

    "S3 bucket key unique generator" ignore {
      val clock = MockitoSugar.mock[Clock]
      // first millis will init state, and rest will be consider in each `getUniqueKey` call
      when(clock.millis())
        .thenReturn(1577059200000L, 1577059300000L, 1577202213000L, 1577202214000L)
      val mockS3KeyGenerator = new S3KeyGenerator(clock)
      val consumerUniqueId = UUID.randomUUID()

      mockS3KeyGenerator.getUniqueKey(consumerUniqueId)
      val stateOpt1 = mockS3KeyGenerator.getState
      stateOpt1.date should be("2019-12-23")
      stateOpt1.counter should be(1L)

      mockS3KeyGenerator.getUniqueKey(consumerUniqueId)
      val stateOpt2 = mockS3KeyGenerator.getState
      stateOpt2.date should be("2019-12-24")
      stateOpt2.counter should be(1L)

      mockS3KeyGenerator.getUniqueKey(consumerUniqueId)
      val stateOpt3 = mockS3KeyGenerator.getState
      stateOpt3.date should be("2019-12-24")
      stateOpt3.counter should be(2L)
    }

    "consume and upload events to amazon S3" in {
      val group = 10
      val stream =
        kafkaWarmStorageWriter.eventConsumerSource
          .groupedWithin(group, 5 seconds)
          .via(kafkaWarmStorageWriter.upload)
          .map {
            case msg @ S3UploadInfo(uploaded, committableMsgs, size, keyName) if (size == committableMsgs.size) =>
              logger.debug(s"Msg: $msg")
              uploaded.key should be(keyName)
              committableMsgs.foldLeft(CommittableOffsetBatch.empty) { (batch, elem) => batch.updated(elem) }
            case S3UploadInfo(uploaded, committableMsgs, _, _) =>
              logger.error(s"The number of events uploaded to the amazon S3 under file [${uploaded.key}] doesn't match the number of committable messages [${committableMsgs.size}]")
              CommittableOffsetBatch.empty
          }
          .toMat(Sink.seq)(Keep.both)

      withRunningKafka {
        val lotsOfEvents = for (i <- 1 to 100) yield {
          EventMessage(None, s"GT-Event-$i", "description", "Kerberos attack", 90, 897352956563860L + i * 2, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, testObsSysIdOpt)
        }
        lotsOfEvents.foreach(e => publishStringMessageToKafka(s"cyber-amp-events-$testObsSysId-5", e.toJson.compactPrint))
        Await.result(s3BucketHandlerService.createBucket, 5 seconds)
        val (control, offsetBatches) = stream.run()
        Thread.sleep(30000L)
        control.shutdown()
        offsetBatches.futureValue.foreach(batch => logger.debug(s"Offset batch: ${batch.offsets}"))
        offsetBatches.futureValue.head.offsets.head._2 should be((lotsOfEvents.size / group) - 1) // offsets start at 0
      }
    }
  }
}
