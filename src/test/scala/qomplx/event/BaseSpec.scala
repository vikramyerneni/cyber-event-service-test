package qomplx.event

import com.typesafe.scalalogging.StrictLogging
import org.scalatest.{ BeforeAndAfterAll, Matchers, OptionValues, WordSpec }
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar

trait BaseSpec extends WordSpec
  with Matchers
  with ScalaFutures
  with OptionValues
  with MockitoSugar
  with StrictLogging
  with BeforeAndAfterAll
