package qomplx.event

import akka.actor.ActorSystem
import akka.stream.{ ActorMaterializer, ActorMaterializerSettings, Supervision }
import com.typesafe.config.ConfigFactory

import scala.concurrent.ExecutionContext

trait BaseSpecWithActorSystem extends BaseSpec {

  private val resumingDecider: Supervision.Decider = {
    case e: Exception =>
      logger.warn(s"Resuming after the following exception was thrown: ${e.getMessage}")
      Supervision.Resume
  }

  implicit val system: ActorSystem = ActorSystem("CyberEventService")
  implicit val materializer: ActorMaterializer = ActorMaterializer(ActorMaterializerSettings(system).withSupervisionStrategy(resumingDecider))
  implicit val ec: ExecutionContext = system.dispatcher

  val appConfig = ConfigFactory.load()
}

