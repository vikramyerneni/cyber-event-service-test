package qomplx.event.thirdpartyintegrations.repository

import java.util.UUID

import com.fractal.auth.client.deprecated.{ User => AuthServiceUser }
import org.mockito.Mockito.when
import org.postgresql.util.PSQLException
import org.scalatest.BeforeAndAfter
import org.scalatest.mockito.MockitoSugar
import qomplx.event.config.db.LiquibaseUtils
import qomplx.event.thirdpartyintegrations.model.ThirdPartySystem
import qomplx.event.thirdpartyintegrations.routes.{ CreateTSConfigPayload, ListTSConfigsFilters, TSConfigDTO, UpdateTSConfigPayload }
import qomplx.event.util.EncryptionUtils
import qomplx.event.util.SlickPostgresProfile.api._
import qomplx.event.{ BaseSpecWithMigrations, JsonSupport, UniqueViolationErrorCode }

import scala.collection.convert.WrapAsJava.seqAsJavaList
import scala.concurrent.{ ExecutionContext, Future }

class ThirdPartySystemConfigRepositorySpec extends BaseSpecWithMigrations with BeforeAndAfter with JsonSupport {
  private lazy val db = postgresConnection.database

  private val encryptionUtils = new EncryptionUtils()
  encryptionUtils.initialize()

  private lazy val repo = new ThirdPartySystemConfigRepository(db, encryptionUtils)

  before {
    val numDeleted = delete().futureValue
    logger.debug(s"Deleted $numDeleted records before running the test")
    LiquibaseUtils.runMigration(config, postgresConnection)
  }

  // test data
  val tagId = s"${java.util.UUID.randomUUID()}-test-obs123"
  val payload = CreateTSConfigPayload("config 1", Some("config 1 desc"), ThirdPartySystem.JIRA, Option(List("a@b.com", "y@z.com")))

  val user = MockitoSugar.mock[AuthServiceUser]
  when(user.uniqueId).thenReturn("1")

  // Utility method for testing. The real API does not delete configurations filtered by observedSystemId, hence its absence there.
  def delete()(implicit ec: ExecutionContext): Future[Int] = {
    val delete = s"delete from cyber_event_service.ns_third_party_integration_configs where tag_id='$tagId'"
    val deleteStatement = sqlu"#$delete"

    logger.debug(s"Running the following delete statement: $delete")

    db.run(deleteStatement.transactionally)
      .recover {
        case t =>
          logger.error(s"The following exception was thrown when trying to delete the configurations for $tagId: ${t.getMessage}")
          0
      }
  }

  "ThirdPartySystemConfigRepository" should {
    "create method" should {
      val payloadNameExist = CreateTSConfigPayload("config 1", Some("config 2 desc"), ThirdPartySystem.JIRA, Option(List("c@d.com")))
      "not create config if name/system pair is already exist" in {
        createConfigAndAssert()
        val token = UUID.randomUUID().toString
        val ex = repo.create(tagId, payloadNameExist, user, token).failed.futureValue
        ex shouldBe a[PSQLException]
        ex.asInstanceOf[PSQLException].getSQLState shouldEqual UniqueViolationErrorCode
      }
    }

    "get method" should {
      "return none if provided id does not exist" in {
        val resultOpt = repo.get(tagId, "wrong id").futureValue
        resultOpt shouldBe a[None.type]
      }
    }

    "create and get method" should {
      "get config of provided id" in {
        val idOpt = createConfigAndAssert()
        val result = repo.get(tagId, idOpt.get).futureValue.get
        result.id shouldEqual idOpt.get
        validateCreateResult(result, payload)
      }
    }

    "update method" should {
      "update config of provided id" in {
        val idOpt = createConfigAndAssert()
        val updatePayload = UpdateTSConfigPayload("config 2", Some("config 2 desc"), ThirdPartySystem.ARCHER, Option(List("c@d.com")), false)
        val affectedRows = repo.update(tagId, idOpt.get, updatePayload).futureValue
        val result = repo.get(tagId, idOpt.get).futureValue.get
        affectedRows shouldEqual 1
        result.id shouldEqual idOpt.get
        validateUpdateResult(result, updatePayload)
      }

      "return none if provided id to update config does not exist" in {
        createConfigAndAssert()
        val updatePayload = UpdateTSConfigPayload("config 2", Some("config 2 desc"), ThirdPartySystem.ARCHER, Option(List("c@d.com")), false)
        val affectedRows = repo.update(tagId, "wrong id", updatePayload).futureValue
        affectedRows shouldEqual 0
      }

      "throw exception if name/system already exist" in {
        val token1 = UUID.randomUUID().toString
        val token2 = UUID.randomUUID().toString

        val _ = repo.create(tagId, payload, user, token1).futureValue
        val idOpt2 = repo.create(tagId, payload.copy(name = "config 2"), user, token2).futureValue
        val updatePayload = UpdateTSConfigPayload("config 1", Some("config 2 desc"), ThirdPartySystem.JIRA, Option(List("c@d.com")), false)
        val ex = repo.update(tagId, idOpt2.get, updatePayload).failed.futureValue
        ex shouldBe a[PSQLException]
        ex.asInstanceOf[PSQLException].getSQLState shouldEqual UniqueViolationErrorCode
      }
    }

    "remove method" should {
      "delete config of provided id" in {
        val idOpt = createConfigAndAssert()
        val affectedRows = repo.remove(tagId, idOpt.get).futureValue
        affectedRows shouldEqual 1
        val result = repo.get(tagId, idOpt.get).futureValue
        result shouldBe a[None.type]
      }

      "return zero if provided id does not exist" in {
        createConfigAndAssert()
        val affectedRows = repo.remove(tagId, "wrong id").futureValue
        affectedRows shouldEqual 0
      }
    }

    "tokens" should {
      "be associated to a config" in {
        val token = UUID.randomUUID().toString
        val configId = repo.create(tagId, payload.copy(name = UUID.randomUUID().toString), user, token).futureValue
        configId.isDefined should be(true)

        val result = repo.getToken(configId.get).futureValue.get
        result shouldBe token
      }

      "be unique" in {
        val token = UUID.randomUUID().toString
        val configId = repo.create(tagId, payload.copy(name = UUID.randomUUID().toString), user, token).futureValue
        configId.isDefined should be(true)

        an[RuntimeException] should be thrownBy {
          repo.create(tagId, payload.copy(name = UUID.randomUUID().toString), user, token).futureValue
        }
      }

      "be associated (via a config) to an observed system" in {
        val observedSystemId = UUID.randomUUID().toString
        val token = UUID.randomUUID().toString
        val configId = repo.create(observedSystemId, payload.copy(name = UUID.randomUUID().toString), user, token).futureValue

        val result = repo.getObservedSystemIdByToken(token).futureValue
        result should be(Some(observedSystemId))
      }
    }

    "list method" should {
      val payload1 = payload.copy(name = "config 2", thirdPartySystem = ThirdPartySystem.ARCHER)
      val payload2 = payload.copy(name = "something 3", thirdPartySystem = ThirdPartySystem.ARCHER)
      val payload3 = payload.copy(name = "name 4", thirdPartySystem = ThirdPartySystem.SERVICENOW)
      val data = List(payload, payload1, payload2, payload3)

      val noFilters = ListTSConfigsFilters(None, None)
      val onlyDisabledFilters = ListTSConfigsFilters(Some(false), None)
      val onlyQFilters = ListTSConfigsFilters(None, Some("something"))
      val allFilters = ListTSConfigsFilters(Some(true), Some("config"))

      "list all if no filters provided" in {
        val created = Future.sequence(data.map(p => repo.create(tagId, p, user, UUID.randomUUID().toString))).futureValue
        created should be('nonEmpty)
        val result = repo.list(tagId, noFilters).futureValue
        result.results.length shouldEqual data.length
      }

      "return empty results if provided observed system has none" in {
        val created = Future.sequence(data.map(p => repo.create(tagId, p, user, UUID.randomUUID().toString))).futureValue
        created should be('nonEmpty)
        val result = repo.list("wrong id", noFilters).futureValue
        result.results.length shouldEqual 0
      }

      "return only disabled configs" in {
        val idOpts = Future.sequence(data.map(p => repo.create(tagId, p, user, UUID.randomUUID().toString))).futureValue
        val id = idOpts.get(2).get
        repo.update(tagId, id, UpdateTSConfigPayload("something 3", None, ThirdPartySystem.ARCHER, Option(List("a@b.com")), false)).futureValue
        val result = repo.list(tagId, onlyDisabledFilters).futureValue
        result.results.length shouldEqual 1
      }

      "return configs as per search string" in {
        val created = Future.sequence(data.map(p => repo.create(tagId, p, user, UUID.randomUUID().toString))).futureValue
        created should be('nonEmpty)
        val result = repo.list(tagId, onlyQFilters).futureValue
        result.results.length shouldEqual data.count(d => d.name.contains("something"))
      }

      "return configs as filters" in {
        val idOpts = Future.sequence(data.map(p => repo.create(tagId, p, user, UUID.randomUUID().toString))).futureValue
        val id2 = idOpts.get(1).get
        val id3 = idOpts.get(2).get
        repo.update(tagId, id2, UpdateTSConfigPayload("config 2", None, ThirdPartySystem.ARCHER, Option(List("a@b.com")), true)).futureValue
        repo.update(tagId, id3, UpdateTSConfigPayload("something 3", None, ThirdPartySystem.ARCHER, Option(List("a@b.com")), false)).futureValue
        val result = repo.list(tagId, allFilters).futureValue
        result.results.length shouldEqual data.count(d => d.name.contains("config"))
      }
    }
  }

  private def validateCreateResult(result: TSConfigDTO, payload: CreateTSConfigPayload) = {
    result.name shouldEqual payload.name
    result.description shouldEqual payload.description
    result.thirdPartySystem shouldEqual payload.thirdPartySystem
    result.emails shouldEqual payload.emails
  }

  private def validateUpdateResult(result: TSConfigDTO, payload: UpdateTSConfigPayload) = {
    result.name shouldEqual payload.name
    result.description shouldEqual payload.description
    result.thirdPartySystem shouldEqual payload.thirdPartySystem
    result.emails shouldEqual payload.emails
    result.enabled shouldEqual payload.enabled
  }

  private def createConfigAndAssert(): Option[String] = {
    val token = UUID.randomUUID().toString
    val idOpt = repo.create(tagId, payload, user, token).futureValue
    idOpt shouldBe a[Some[_]]
    idOpt
  }
}
