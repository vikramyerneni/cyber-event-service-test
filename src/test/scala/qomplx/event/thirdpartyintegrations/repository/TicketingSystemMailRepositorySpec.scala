package qomplx.event.thirdpartyintegrations.repository

import java.util.concurrent.TimeoutException

import com.typesafe.config.ConfigFactory
import org.mockito.Mockito.when
import org.scalatest.BeforeAndAfter
import org.scalatest.compatible.Assertion
import org.scalatest.mockito.MockitoSugar
import qomplx.event.config.db.LiquibaseUtils
import qomplx.event.model.{ IncidentStatus, Tables }
import qomplx.event.service.email.MandrillResponse
import qomplx.event.thirdpartyintegrations.model.{ ThirdPartySystem, TicketingSystemMail }
import qomplx.event.util.SlickPostgresProfile.api._
import qomplx.event.{ BaseSpecWithMigrations, JsonSupport }

import scala.concurrent.Future
import scala.util.Random

class TicketingSystemMailRepositorySpec extends BaseSpecWithMigrations with BeforeAndAfter with JsonSupport {
  private lazy val db = postgresConnection.database
  private lazy val mailerConfig = ConfigFactory.load().getConfig("ticketing-system-mailer")
  private lazy val repo = new TicketingSystemMailRepository(db, mailerConfig)

  private val sendMailBatchSize = mailerConfig.getInt("send-mail-batch-size")

  // test data
  val configId = s"${java.util.UUID.randomUUID()}-test-ci123"
  val validMandrillResponse = MockitoSugar.mock[MandrillResponse]
  when(validMandrillResponse.statusCode).thenReturn(200)
  val invalidMandrillResponse = MockitoSugar.mock[MandrillResponse]
  when(invalidMandrillResponse.statusCode).thenReturn(500)

  // testing helper
  private def findAll(configId: String) = Tables.TicketingSystemMails.filter(_.configId === configId)
  private def listMailsByConfigId(configId: String): Future[Seq[TicketingSystemMail]] = db.run(findAll(configId).result)
  private def deleteMailsByConfigId(configId: String): Future[Int] = db.run(findAll(configId).delete)

  before {
    val numDeleted = deleteMailsByConfigId(configId).futureValue
    logger.debug(s"Deleted $numDeleted records before running the test")
    LiquibaseUtils.runMigration(config, postgresConnection)
  }

  "TicketingSystemMailRepository" should {
    "deliverMails method" should {
      "send successfully email and update rows" in {
        insertMailsAndValidate(sendMailBatchSize)
        val affectedRows = repo.deliverMails(_ => Future.successful(validMandrillResponse)).futureValue
        affectedRows shouldEqual sendMailBatchSize
        val rows = listMailsByConfigId(configId).futureValue
        rows.forall(_.delivered) shouldEqual true
      }

      "simulate some failed emails and updated rows should match the same number of successfully delivered mails" in {
        insertMailsAndValidate(sendMailBatchSize)
        var succeed = 0
        val magicSender: TicketingSystemMail => Future[MandrillResponse] = { _ =>
          Random.nextInt() % 3 match {
            case 0 => Future.failed(new TimeoutException())
            case 1 => Future.successful(invalidMandrillResponse)
            case 2 => {
              succeed += 1
              Future.successful(validMandrillResponse)
            }
          }
        }
        val affectedRows = repo.deliverMails(magicSender).futureValue
        affectedRows shouldEqual succeed
        val rows = listMailsByConfigId(configId).futureValue
        rows.count(_.delivered) shouldEqual succeed
      }

      "count should be zero if no mails are pending to send" in {
        val affectedRows = repo.deliverMails(_ => Future.successful(validMandrillResponse)).futureValue
        affectedRows shouldEqual 0
        val rows = listMailsByConfigId(configId).futureValue
        rows.length shouldEqual 0
      }

      "count should be zero if mail sender face some issue and rows should not be updated" in {
        insertMailsAndValidate(sendMailBatchSize)
        val affectedRows = repo.deliverMails(_ => Future.failed(new TimeoutException())).futureValue
        affectedRows shouldEqual 0
        val rows = listMailsByConfigId(configId).futureValue
        rows.forall(_.delivered) shouldEqual false
      }
    }
  }

  private def insertMailsAndValidate(n: Int): Assertion = {
    val time = qomplx.event.now
    val mails = for (i <- 1 to n) yield {
      TicketingSystemMail(
        Some(s"id-$i"), configId, s"incId-$i", s"incTitle-$i", IncidentStatus.NEW,
        List.empty, List.empty, List.empty, time + (i * 1000),
        thirdPartySystem = ThirdPartySystem.JIRA, emails = List.empty, incSeverity = 60)
    }
    val result = db.run(TicketingSystemMailRepository.batchInsert(mails)).futureValue
    result.value shouldEqual n
  }
}
