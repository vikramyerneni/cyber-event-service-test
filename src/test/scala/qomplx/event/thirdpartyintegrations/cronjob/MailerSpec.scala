package qomplx.event.thirdpartyintegrations.cronjob

import akka.actor.{ Actor, Props }
import com.typesafe.akka.extension.quartz.QuartzSchedulerExtension
import com.typesafe.scalalogging.StrictLogging
import qomplx.event.BaseSpecWithActorSystem
import qomplx.event.thirdpartyintegrations.cronjob.Mailer.Send

class TestActor extends Actor with StrictLogging {
  def receive = {
    case Send =>
      logger.info("Received a send message")
  }
}

object TestActor {
  def props(): Props = Props(new TestActor())
}

class MailerSpec extends BaseSpecWithActorSystem {
  "MailerSpec" should {
    "send a message to the mailer actor every ten seconds" in {
      val testActor = system.actorOf(TestActor.props())
      QuartzSchedulerExtension(system).schedule("EveryTenSeconds", testActor, Send)
      Thread.sleep(30000L)
      system.terminate()
    }
  }
}