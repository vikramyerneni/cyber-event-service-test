package qomplx.event.thirdpartyintegrations.routes

import java.time.Instant
import java.util.UUID

import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.server.Directives.handleRejections
import com.fractal.auth.client.deprecated.{ AuthenticationClient, CustomerAccountFeatures, Client => AuthServiceClient, User => AuthServiceUser }
import org.mockito.ArgumentMatchers.{ any, eq => meq }
import org.mockito.Mockito.when
import org.postgresql.util.PSQLException
import org.scalatest.mockito.MockitoSugar
import qomplx.event.JsonSupport
import qomplx.event.directives.{ CustomSecurityDirectives, UserAuthenticatorDirectives }
import qomplx.event.repository.{ ObservedSystem, ObservedSystemRepository }
import qomplx.event.routes.BaseRouteSpec
import qomplx.event.thirdpartyintegrations.model.ThirdPartySystem
import qomplx.event.thirdpartyintegrations.repository.ThirdPartySystemConfigRepository
import qomplx.event.util.CommonRejectionHandler
import reactivemongo.bson.BSONObjectID

import scala.collection.immutable.Seq
import scala.concurrent.{ ExecutionContext, Future }

class ThirdPartySystemConfigRouteSpec extends BaseRouteSpec with JsonSupport {
  val obsSysId = "5e275ee3c9e77c0008c77590"
  private val configId = "TS-bpbptep9gntgf1nvvvsg"
  private val now = Instant.now().toEpochMilli
  private val exception = new Exception("something went wrong!")
  private val psqlException = MockitoSugar.mock[PSQLException]
  when(psqlException.getSQLState).thenReturn(qomplx.event.UniqueViolationErrorCode)

  private val xAuthToken: HttpHeader = RawHeader("X-QOS-AUTH-TOKEN", "1")

  val user = MockitoSugar.mock[AuthServiceUser]
  val userDefaultAccountUniqueId = "1"
  val userFeatures = List(CustomerAccountFeatures(1, userDefaultAccountUniqueId, List("workflows.ACDP")))
  when(user.uniqueId).thenReturn("1")
  when(user.defaultAccountUniqueId).thenReturn(Option(userDefaultAccountUniqueId))
  when(user.features).thenReturn(userFeatures)
  val authenticationClient = MockitoSugar.mock[AuthenticationClient]
  when(authenticationClient.getUserForSSOToken(any[String])(any[ExecutionContext])).thenReturn(Future.successful(Some(user)))
  val authServiceClientMock = MockitoSugar.mock[AuthServiceClient]
  when(authServiceClientMock.authentication).thenReturn(authenticationClient)

  private val dto = TSConfigDTO(configId, obsSysId, ThirdPartySystem.JIRA, "config 1", Some("config 1 desc"), Option(List("a@b.com")), false, false)

  private val ticketingSystemConfigRepositoryMock: ThirdPartySystemConfigRepository = MockitoSugar.mock[ThirdPartySystemConfigRepository]
  private val authenticator = new UserAuthenticatorDirectives(authServiceClientMock)

  val observedSystemRepositoryMock = MockitoSugar.mock[ObservedSystemRepository]
  when(observedSystemRepositoryMock.findById(any[String])(any[ExecutionContext])).thenReturn(Future.successful(None))
  when(observedSystemRepositoryMock.findById(meq(obsSysId))(any[ExecutionContext])).thenReturn(Future.successful(Some(ObservedSystem(BSONObjectID.generate(), "DUMMY"))))

  private val routes = handleRejections(CommonRejectionHandler.rejectionHandler) {
    authenticator.withAccess { user =>
      new ThirdPartySystemConfigRoutes(
        ticketingSystemConfigRepositoryMock,
        new CustomSecurityDirectives(observedSystemRepositoryMock)).routes(user)
    }
  }

  "ThirdPartySystemConfigRoute" should {
    "validate create configurations api" should {
      val request = HttpRequest(method = HttpMethods.POST, uri = s"/api/v1/observed-system/$obsSysId/configs")
      val invalidObsRequest = HttpRequest(method = HttpMethods.POST, uri = s"/api/v1/observed-system/${obsSysId + 1}/configs")
      val validCreateConfigEntity = HttpEntity(ContentTypes.`application/json`, """{"name":"config 1","description":"config 1 desc","thirdPartySystem":"JIRA","emails":["a@b.com", "y@z.com"]}""")
      val invalidCreateConfigEntity = HttpEntity(ContentTypes.`application/json`, """{"name":"config 2","thirdPartySystem":"GARBAGE","emails":["a@b.com"]}""")
      val emptyCreateConfigEntity = HttpEntity(ContentTypes.`application/json`, """{"name":"config 2","thirdPartySystem":"GARBAGE","emails":[]}""")
      val missingRequiredFieldCreateConfigEntity = HttpEntity(ContentTypes.`application/json`, """{"description":"config 3 desc","thirdPartySystem":"JIRA","emails":["a@b.com"]}""")
      assertMissingHeaders(request)

      "return bad request if observed system not exist" in {
        invalidObsRequest.copy(headers = Seq(xAuthToken)) ~> routes ~> check {
          status should be(StatusCodes.BadRequest)
        }
      }

      "should create configuration and return 201" in {
        when(ticketingSystemConfigRepositoryMock.create(any[String], any[CreateTSConfigPayload], any[AuthServiceUser], any[String])).thenReturn(Future.successful(Some("1")))
        val requestWithBody = request.copy(entity = validCreateConfigEntity, headers = Seq(xAuthToken))
        requestWithBody ~> routes ~> check {
          status should be(StatusCodes.Created)
        }
      }

      "should return bad request if one of the fields is malformed" in {
        val requestWithBody = request.copy(entity = invalidCreateConfigEntity, headers = Seq(xAuthToken))
        requestWithBody ~> routes ~> check {
          status should be(StatusCodes.BadRequest)
        }
      }

      "should return bad request if emails are empty" in {
        val requestWithBody = request.copy(entity = emptyCreateConfigEntity, headers = Seq(xAuthToken))
        requestWithBody ~> routes ~> check {
          status should be(StatusCodes.BadRequest)
        }
      }

      "should return internal server error if repo process fails" in {
        when(ticketingSystemConfigRepositoryMock.create(any[String], any[CreateTSConfigPayload], any[AuthServiceUser], any[String])).thenReturn(Future.failed(exception))
        val requestWithBody = request.copy(entity = validCreateConfigEntity, headers = Seq(xAuthToken))
        requestWithBody ~> routes ~> check {
          status should be(StatusCodes.InternalServerError)
        }
      }

      "should return bad request if required field is missing in request body" in {
        val requestWithBody = request.copy(entity = missingRequiredFieldCreateConfigEntity, headers = Seq(xAuthToken))
        requestWithBody ~> routes ~> check {
          status should be(StatusCodes.BadRequest)
        }
      }

      "return bad request if name/system pair exist" in {
        when(ticketingSystemConfigRepositoryMock.create(any[String], any[CreateTSConfigPayload], any[AuthServiceUser], any[String])).thenReturn(Future.failed(psqlException))
        val requestWithBody = request.copy(entity = validCreateConfigEntity, headers = Seq(xAuthToken))
        requestWithBody ~> routes ~> check {
          status should be(StatusCodes.BadRequest)
        }
      }
    }
    "validate update configurations api" should {
      val request = HttpRequest(method = HttpMethods.PUT, uri = s"/api/v1/observed-system/$obsSysId/configs/$configId")
      val invalidObsRequest = HttpRequest(method = HttpMethods.PUT, uri = s"/api/v1/observed-system/${obsSysId + 1}/configs/$configId")
      val validUpdateConfigEntity = HttpEntity(ContentTypes.`application/json`, """{"name":"config 1","description":"config 1 desc","thirdPartySystem":"JIRA","emails":["a@b.com"],"enabled":false}""")
      val invalidUpdateConfigEntity = HttpEntity(ContentTypes.`application/json`, """{"name":"config 1","description":"config 1 desc","thirdPartySystem":"JIRA","emails":[],"enabled":false}""")
      assertMissingHeaders(request)

      "return bad request if observed system not exist" in {
        invalidObsRequest.copy(headers = Seq(xAuthToken)) ~> routes ~> check {
          status should be(StatusCodes.BadRequest)
        }
      }

      "should return not found if config does not exist" in {
        when(ticketingSystemConfigRepositoryMock.update(any[String], any[String], any[UpdateTSConfigPayload])).thenReturn(Future.successful(0))
        val requestWithBody = request.copy(entity = validUpdateConfigEntity, headers = Seq(xAuthToken))
        requestWithBody ~> routes ~> check {
          status should be(StatusCodes.NotFound)
        }
      }

      "should return bad request if emails are empty" in {
        when(ticketingSystemConfigRepositoryMock.update(any[String], any[String], any[UpdateTSConfigPayload])).thenReturn(Future.successful(1))
        val requestWithBody = request.copy(entity = invalidUpdateConfigEntity, headers = Seq(xAuthToken))
        requestWithBody ~> routes ~> check {
          status should be(StatusCodes.BadRequest)
        }
      }

      "should update" in {
        when(ticketingSystemConfigRepositoryMock.update(any[String], any[String], any[UpdateTSConfigPayload])).thenReturn(Future.successful(1))
        val requestWithBody = request.copy(entity = validUpdateConfigEntity, headers = Seq(xAuthToken))
        requestWithBody ~> routes ~> check {
          status should be(StatusCodes.OK)
        }
      }

      "should return internal error if repo process fails" in {
        when(ticketingSystemConfigRepositoryMock.update(any[String], any[String], any[UpdateTSConfigPayload])).thenReturn(Future.failed(exception))
        val requestWithBody = request.copy(entity = validUpdateConfigEntity, headers = Seq(xAuthToken))
        requestWithBody ~> routes ~> check {
          status should be(StatusCodes.InternalServerError)
        }
      }
    }
    "validate get configurations api" should {
      val request = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/observed-system/$obsSysId/configs/$configId")
      val invalidObsRequest = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/observed-system/${obsSysId + 1}/configs/$configId")
      assertMissingHeaders(request)

      "return bad request if observed system not exist" in {
        invalidObsRequest.copy(headers = Seq(xAuthToken)) ~> routes ~> check {
          status should be(StatusCodes.BadRequest)
        }
      }

      "should return not found if config does not exist" in {
        when(ticketingSystemConfigRepositoryMock.get(any[String], any[String])).thenReturn(Future.successful(None))
        request.copy(headers = Seq(xAuthToken)) ~> routes ~> check {
          status should be(StatusCodes.NotFound)
        }
      }
      "should return config object" in {
        when(ticketingSystemConfigRepositoryMock.get(any[String], any[String])).thenReturn(Future.successful(Some(dto)))
        request.copy(headers = Seq(xAuthToken)) ~> routes ~> check {
          status should be(StatusCodes.OK)
          contentType should be(ContentTypes.`application/json`)
          val resultTSConfigDTO = entityAs[TSConfigDTO]
          validateTSConfigDTO(resultTSConfigDTO, dto)
        }
      }
      "should return internal error if repo process fails" in {
        when(ticketingSystemConfigRepositoryMock.get(any[String], any[String])).thenReturn(Future.failed(exception))
        request.copy(headers = Seq(xAuthToken)) ~> routes ~> check {
          status should be(StatusCodes.InternalServerError)
        }
      }
    }

    "get token endpoint" should {
      "return a token" in {
        val token = UUID.randomUUID().toString
        when(ticketingSystemConfigRepositoryMock.getToken(configId)).thenReturn(Future.successful(Option(token)))
        val request = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/observed-system/$obsSysId/configs/$configId/token")

        request.copy(headers = Seq(xAuthToken)) ~> routes ~> check {
          entityAs[String] should be(token)
        }
      }
    }

    "validate list configurations api" should {
      val request = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/observed-system/$obsSysId/configs")
      val requestFilter1 = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/observed-system/$obsSysId/configs?enabled=false")
      val requestFilter2 = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/observed-system/$obsSysId/configs?enabled=true&q=config")
      val invalidObsRequest = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/observed-system/${obsSysId + 1}/configs")
      val dto1 = dto.copy(name = "config 1")
      val dto2 = dto.copy(name = "config 2", enabled = false)
      val dto3 = dto.copy(name = "something 3")
      val list = ListTSConfigs(Seq(dto1, dto2, dto3))
      val listFilter1 = ListTSConfigs(Seq(dto2))
      val listFilter2 = ListTSConfigs(Seq(dto1, dto2))

      "return bad request if observed system not exist" in {
        invalidObsRequest.copy(headers = Seq(xAuthToken)) ~> routes ~> check {
          status should be(StatusCodes.BadRequest)
        }
      }
      "should return all config objects" in {
        when(ticketingSystemConfigRepositoryMock.list(any[String], any[ListTSConfigsFilters])).thenReturn(Future.successful(list))
        request.copy(headers = Seq(xAuthToken)) ~> routes ~> check {
          status should be(StatusCodes.OK)
          contentType should be(ContentTypes.`application/json`)
          val results = entityAs[ListTSConfigs]
          results.results.length shouldEqual list.results.length
        }
      }
      "should return only enabled config objects" in {
        when(ticketingSystemConfigRepositoryMock.list(any[String], any[ListTSConfigsFilters])).thenReturn(Future.successful(listFilter1))
        requestFilter1.copy(headers = Seq(xAuthToken)) ~> routes ~> check {
          status should be(StatusCodes.OK)
          contentType should be(ContentTypes.`application/json`)
          val results = entityAs[ListTSConfigs]
          results.results.length shouldEqual listFilter1.results.length
        }
      }
      "should return only string matched config objects" in {
        when(ticketingSystemConfigRepositoryMock.list(any[String], any[ListTSConfigsFilters])).thenReturn(Future.successful(listFilter2))
        requestFilter2.copy(headers = Seq(xAuthToken)) ~> routes ~> check {
          status should be(StatusCodes.OK)
          contentType should be(ContentTypes.`application/json`)
          val results = entityAs[ListTSConfigs]
          results.results.length shouldEqual listFilter2.results.length
        }
      }
      "should return internal error if repo process fails" in {
        when(ticketingSystemConfigRepositoryMock.list(any[String], any[ListTSConfigsFilters])).thenReturn(Future.failed(exception))
        request.copy(headers = Seq(xAuthToken)) ~> routes ~> check {
          status should be(StatusCodes.InternalServerError)
        }
      }
    }
    "validate delete configurations api" should {
      val request = HttpRequest(method = HttpMethods.DELETE, uri = s"/api/v1/observed-system/$obsSysId/configs/$configId")
      val invalidObsRequest = HttpRequest(method = HttpMethods.DELETE, uri = s"/api/v1/observed-system/${obsSysId + 1}/configs/$configId")
      assertMissingHeaders(request)

      "return bad request if observed system not exist" in {
        invalidObsRequest.copy(headers = Seq(xAuthToken)) ~> routes ~> check {
          status should be(StatusCodes.BadRequest)
        }
      }

      "should delete config" in {
        when(ticketingSystemConfigRepositoryMock.remove(any[String], any[String])).thenReturn(Future.successful(1))
        request.copy(headers = Seq(xAuthToken)) ~> routes ~> check {
          status should be(StatusCodes.OK)
        }
      }
      "should return internal error if repo process fails" in {
        when(ticketingSystemConfigRepositoryMock.remove(any[String], any[String])).thenReturn(Future.failed(exception))
        request.copy(headers = Seq(xAuthToken)) ~> routes ~> check {
          status should be(StatusCodes.InternalServerError)
        }
      }
    }
  }

  private def assertMissingHeaders(request: HttpRequest) = {
    "should reject request if the required headers are not present" in {
      request ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }
  }

  private def validateTSConfigDTO(result: TSConfigDTO, original: TSConfigDTO) = {
    result.id shouldEqual original.id
    result.name shouldEqual original.name
    result.description shouldEqual original.description
    result.thirdPartySystem shouldEqual original.thirdPartySystem
    result.emails shouldEqual original.emails
  }
}
