package qomplx.event.routes

import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.testkit.RouteTestTimeout
import akka.testkit.TestDuration
import org.mockito.Mockito.when
import org.postgresql.util.PSQLException
import org.scalatest.BeforeAndAfter
import org.scalatest.mockito.MockitoSugar
import qomplx.event.directives.SecurityDirectivesForInternalRoutes._
import qomplx.event.dto.tags.TagCreateRequest
import qomplx.event.model.PropertyType
import qomplx.event.repository.TagsRepository
import qomplx.event.{ IntegrationTestContainers, JsonSupport }

import scala.collection.immutable.Seq
import scala.concurrent.duration._

class TagsRouteSpec extends BaseRouteSpec with JsonSupport with IntegrationTestContainers with BeforeAndAfter {

  implicit val timeout = RouteTestTimeout(10.seconds dilated)

  private lazy val db = postgresConnection.database
  private lazy val tagsRepository = new TagsRepository(db)

  private val userIdHeader: HttpHeader = RawHeader(UserIdHeaderName, "1")
  private val accountIdHeader: HttpHeader = RawHeader(AccountIdHeaderName, "1")
  private val idHeaders = Seq(userIdHeader, accountIdHeader)
  private val Base = "/api/v1/event/tags"
  private lazy val service = new TagsRoutesInternal(tagsRepository, securityDirectiveForInternalRoutes)

  override def beforeAll(): Unit = {
    super.beforeAll()
  }

  "Event tag routes" should {
    "Insert ONE tag" in {
      Post(Base, TagCreateRequest("source_id", "tag_id", PropertyType.AGENT)) ~> addHeaders(userIdHeader, accountIdHeader) ~> service.tagsRoutes ~> check {
        status should be(StatusCodes.Created)
        tagsRepository.count().futureValue should be > 0
      }
    }

    "Insert List of tags" in {
      val list = List(
        TagCreateRequest("source_id_1", "tag_id_1", PropertyType.AGENT),
        TagCreateRequest("source_id_2", "tag_id_2", PropertyType.RULE))

      Post(Base, list) ~> addHeaders(userIdHeader, accountIdHeader) ~> service.tagsRoutes ~> check {
        status should be(StatusCodes.Created)
        tagsRepository.count().futureValue should be > 1
      }
    }

    "Fails with duplicated tag" in {
      Post(Base, TagCreateRequest("source_id", "tag_id", PropertyType.AGENT)) ~> addHeaders(userIdHeader, accountIdHeader) ~> service.tagsRoutes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }
  }
}
