package qomplx.event.routes

import akka.http.scaladsl.testkit.ScalatestRouteTest
import com.fractal.auth.client.deprecated.{ Client, User, UsersClient }
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{ BeforeAndAfterAll, Matchers, OptionValues, WordSpec }
import qomplx.event.directives.SecurityDirectivesForInternalRoutes
import org.mockito.ArgumentMatchers.{ any, anyString }
import org.mockito.Mockito.when

import scala.concurrent.{ ExecutionContext, Future }

/**
 * Doesn't extend [[qomplx.event.BaseSpecWithActorSystem]] because doing so breaks the
 * implicit magic with [[akka.http.scaladsl.testkit.ScalatestRouteTest]].
 */
trait BaseRouteSpec extends WordSpec
  with Matchers
  with BeforeAndAfterAll
  with ScalaFutures
  with ScalatestRouteTest
  with OptionValues
  with StrictLogging {

  val appConfig = ConfigFactory.load()

  val useApiKeyHeader = appConfig.getBoolean("cyber-event.use-api-key-header")

  val authServiceClient = MockitoSugar.mock[Client]
  val usersClient = MockitoSugar.mock[UsersClient]
  val userMock = MockitoSugar.mock[User]
  when(userMock.id).thenReturn(1)
  when(userMock.defaultAccountId).thenReturn(1)
  when(userMock.uniqueId).thenReturn("1")
  when(usersClient.getUserById(any[Long])(any[ExecutionContext])).thenReturn(Future.successful(Some(userMock)))
  when(authServiceClient.users).thenReturn(usersClient)

  val securityDirectiveForInternalRoutes = new SecurityDirectivesForInternalRoutes(authServiceClient)
}