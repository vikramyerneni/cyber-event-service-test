package qomplx.event.routes

import java.time.Instant

import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.testkit.RouteTestTimeout
import akka.stream.Materializer
import akka.testkit.TestDuration
import org.mockito.ArgumentMatchers.{ any, anyString }
import org.mockito.Mockito.when
import org.scalatest.mockito.MockitoSugar
import qomplx.event.JsonSupport
import qomplx.event.directives.SecurityDirectivesForInternalRoutes._
import qomplx.event.model.{ Event, EventMessage }
import qomplx.event.repository.EventRepository
import spray.json.{ JsObject, JsString }

import scala.collection.immutable.Seq
import scala.concurrent.duration._
import scala.concurrent.{ ExecutionContext, Future }

class EventRouteSpec extends BaseRouteSpec with JsonSupport {

  implicit val timeout = RouteTestTimeout(10.seconds dilated)

  val testObsSysId = "test-obs123"
  val testObsSysIdOpt = Some(testObsSysId)
  val now = Instant.now().toEpochMilli

  private val userIdHeader: HttpHeader = RawHeader(UserIdHeaderName, "1")
  private val accountIdHeader: HttpHeader = RawHeader(AccountIdHeaderName, "1")
  private val idHeaders = Seq(userIdHeader, accountIdHeader)

  val testEvents =
    Seq(
      Event(None, "GT-Event-1", "has a long description", "Kerberos attack", 90, now, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, testObsSysId),
      Event(None, "GT-Event-2", "has a short description", "Kerberos attack", 90, now + 5000L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, testObsSysId),
      Event(None, "GT-Event-3", "has a description", "Kerberos attack", 90, now + 10000L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, testObsSysId))

  val testEventsReduced = testEvents.map(_.toEventReduced)

  val eventRepositoryMock: EventRepository = MockitoSugar.mock[EventRepository]
  val routes = (new EventRoutesInternal(eventRepositoryMock, securityDirectiveForInternalRoutes)).eventRoutes

  // TODO: add tests for missing headers, similar to what is in IncidentRouteSpec
  "EventRoutes" should {
    "return failure when calling the list end point with user-id validation/authentication header, but without the customer-account-id header" in {
      val request = HttpRequest(uri = "/api/v1/event/observed-system/system-nope", headers = Seq(userIdHeader))
      when(eventRepositoryMock.getByObservedSystem(anyString(), any[GetEventsReq])(any[ExecutionContext], any[Materializer])).thenReturn(Future.successful(GetEventsRes(List(), 0)))
      request ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    "return an empty array if no events are associated with an observed system" in {
      val request = HttpRequest(uri = "/api/v1/event/observed-system/system-nope", headers = idHeaders)
      when(eventRepositoryMock.getByObservedSystem(anyString(), any[GetEventsReq])(any[ExecutionContext], any[Materializer])).thenReturn(Future.successful(GetEventsRes(List(), 0)))

      request ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        entityAs[GetEventsRes].results should be(List())
      }
    }

    "return an array of events" in {
      val request = HttpRequest(uri = s"/api/v1/event/observed-system/$testObsSysId", headers = idHeaders)
      when(eventRepositoryMock.getByObservedSystem(anyString(), any[GetEventsReq])(any[ExecutionContext], any[Materializer]))
        .thenReturn(Future.successful(GetEventsRes(testEvents.map(_.toEventReduced), testEvents.length)))

      request ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        entityAs[GetEventsRes].results should be(testEvents.map(_.toEventReduced))
      }
    }

    "return an array of events with weird json" in {
      val testEvent = Seq(Event(None, "GT-Event-4", "has a description", "Kerberos attack", 90, now + 10000L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, testObsSysId, Some(JsObject("'balh'" -> JsString("data")))))
      val request = HttpRequest(uri = s"/api/v1/event/observed-system/$testObsSysId", headers = idHeaders)
      when(eventRepositoryMock.getByObservedSystem(anyString(), any[GetEventsReq])(any[ExecutionContext], any[Materializer]))
        .thenReturn(Future.successful(GetEventsRes(testEvent.map(_.toEventReduced), testEvent.length)))

      request ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        entityAs[GetEventsRes].results should be(testEvent.map(_.toEventReduced))
      }
    }

    "return an array of events filtering by search" in {
      val events = testEventsReduced.filter(_.name == "GT-Event-1")
      val request = HttpRequest(uri = s"/api/v1/event/observed-system/$testObsSysId?q=Long%20Description", headers = idHeaders)
      when(eventRepositoryMock.getByObservedSystem(anyString(), any[GetEventsReq])(any[ExecutionContext], any[Materializer]))
        .thenReturn(Future.successful(GetEventsRes(events, events.length)))

      request ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        entityAs[GetEventsRes].results should be(events)
      }
    }

    """return a bad request if the "limit" parameter is not a number""" in {
      val request = HttpRequest(uri = s"/api/v1/event/observed-system/$testObsSysId?limit=limitless", headers = idHeaders)

      request ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    """return a bad request if the "offset" parameter is not a number""" in {
      val request = HttpRequest(uri = s"/api/v1/event/observed-system/$testObsSysId?limit=10&offset=zero", headers = idHeaders)

      request ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    """return a bad request if the "offset" parameter is more than 990""" in {
      val request = HttpRequest(uri = s"/api/v1/event/observed-system/$testObsSysId?limit=10&offset=991", headers = idHeaders)

      request ~> routes ~> check {
        status should be(StatusCodes.NotAcceptable)
      }
    }

    """return a bad request if the "from-severity" parameter is not a number""" in {
      val request = HttpRequest(uri = s"/api/v1/event/observed-system/$testObsSysId?limit=10&offset=0&from-severity=zero", headers = idHeaders)

      request ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    """return a bad request if the "to-severity" parameter is not a number""" in {
      val request = HttpRequest(uri = s"/api/v1/event/observed-system/$testObsSysId?limit=10&offset=0&to-severity=zero", headers = idHeaders)

      request ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    """return a bad request if the "from-ts" parameter is not a long number""" in {
      val request = HttpRequest(uri = s"/api/v1/event/observed-system/$testObsSysId?limit=10&offset=0&from-ts=zero", headers = idHeaders)

      request ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    """return a bad request if the "to-ts" parameter is not a number""" in {
      val request = HttpRequest(uri = s"/api/v1/event/observed-system/$testObsSysId?limit=10&offset=0&to-ts=zero", headers = idHeaders)

      request ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }
  }
}
