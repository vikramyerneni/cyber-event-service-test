package qomplx.event.routes

import java.io.ByteArrayInputStream
import java.time.Instant

import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.RawHeader
import akka.http.scaladsl.testkit.RouteTestTimeout
import akka.stream.alpakka.csv.scaladsl.{ CsvParsing, CsvToMap }
import akka.stream.scaladsl.{ Sink, Source, StreamConverters }
import akka.testkit.TestDuration
import akka.util.ByteString
import com.fractal.auth.client.deprecated.{ AuthenticationClient, Client => AuthServiceClient, User => AuthServiceUser }
import io.findify.s3mock.S3Mock
import org.mockito.ArgumentMatchers
import org.mockito.ArgumentMatchers.{ any, anyInt, anyLong, anyString, eq => argeq }
import org.mockito.Mockito.when
import org.scalatest.mockito.MockitoSugar
import qomplx.event.JsonSupport
import qomplx.event.directives.CustomSecurityDirectives._
import qomplx.event.directives.UserAuthenticatorDirectives
import qomplx.event.model._
import qomplx.event.repository.IncidentRepository
import qomplx.event.service.{ AWSConfig, S3BucketHandlerService }
import qomplx.event.directives.SecurityDirectivesForInternalRoutes._
import qomplx.event.vault.VaultManager

import scala.collection.immutable.Seq
import scala.concurrent.duration._
import scala.concurrent.{ ExecutionContext, Future }

class IncidentRouteSpec extends BaseRouteSpec with JsonSupport {

  implicit val timeout = RouteTestTimeout(10.seconds dilated)

  val api: S3Mock = S3Mock(port = 8001, dir = "/tmp/s3")

  override def beforeAll(): Unit = {
    super.beforeAll()
    api.start
  }

  override def afterAll(): Unit = {
    api.stop
    super.afterAll()
  }

  val testObsSysId = "test-obs123"
  val testObsSysIdOpt = Some(testObsSysId)
  val testObsSysId1 = "test-obs1234"
  val testObsSysId1Opt = Some(testObsSysId1)
  val fileId = "file-test-123"
  val urlId = "url-test-123"
  val noteId = "url-test-123"
  val now = Instant.now().toEpochMilli

  private val userIdH: HttpHeader = RawHeader(UserIdHeaderName, "1")
  private val accountIdH: HttpHeader = RawHeader(AccountIdHeaderName, "1")
  private val xApiKeyH: HttpHeader = RawHeader(XApiKeyHeaderName, "d0502c6e-3b51-4d15-b1b6-6e1a28e93800")
  private val authTokenH: HttpHeader = RawHeader("X-QOS-AUTH-TOKEN", "1")

  val incidentDTO = IncidentDTO(Some("INC-bkrc7grufnf333vu1c40"), "Incident 1", Some("Incident 1 description"), None, None, None, None, IncidentStatus.NEW, 11,
    now, 1, IncidentReporterSource.USER, testObsSysId, None, None, Nil)

  val incidentFile = IncidentFile.map(fileId, "Test file", None, "files/2020/example", incidentDTO.id.get, now, 1l, now, 1l)
  val attachmentsDTO = AttachmentsDTO(List(incidentFile.toDTO), List(IncidentUrlDTO("url-test-123", "https://qomplx.com", 1l, 1l)), List())
  val eventList = Seq(
    Event(Option("event1"), "GT-Event-1", "description", "Kerberos attack", 90, 897352956563853L, "Kerberos pipeline", "19475653", Seq("lateral movement"), Nil, testObsSysId),
    Event(Option("event2"), "GT-Event-2", "description", "Kerberos attack", 90, 897352956563860L, "Kerberos pipeline", "19475654", Seq("lateral movement"), Nil, testObsSysId),
    Event(Option("event3"), "GT-Event-3", "description", "Kerberos attack", 90, 897352956563870L, "Kerberos pipeline", "19475655", Seq("lateral movement"), Nil, testObsSysId1))
  val categories = Seq(Category("Category 1"), Category("Category 2"), Category("Category 3"))
  val tactics = Seq(Tactic("Tactic 1"), Tactic("Tactic 2"), Tactic("Tactic 3"))
  val updatedIncidentDTO = incidentDTO.copy(resolution = Some("Resolved"), resolutionTimestamp = Some(now), status = IncidentStatus.OPEN, severity = 1)
  val assigneeId = 1L
  val searchString = "Updated"
  val incidents = Seq(
    incidentDTO,
    incidentDTO.copy(title = "Updated Incident 1 Title"),
    incidentDTO.copy(assigneeId = Option(assigneeId)))

  val incidentRepositoryMock: IncidentRepository = MockitoSugar.mock[IncidentRepository]

  val mockVaultManager = MockitoSugar.mock[VaultManager]
  when(mockVaultManager.s3AccessKey).thenReturn("")
  when(mockVaultManager.s3SecretKey).thenReturn("")

  val s3BucketHandlerService = new S3BucketHandlerService(AWSConfig(appConfig.getConfig("aws.s3.incident-file-storage"), mockVaultManager))
  val fileContent = "HELLO WORLD"

  val user = MockitoSugar.mock[AuthServiceUser]
  when(user.uniqueId).thenReturn("1")
  val authenticationClient = MockitoSugar.mock[AuthenticationClient]
  when(authenticationClient.getUserForSSOToken(any[String])(any[ExecutionContext])).thenReturn(Future.successful(Some(user)))
  val authServiceClientMock = MockitoSugar.mock[AuthServiceClient]
  when(authServiceClientMock.authentication).thenReturn(authenticationClient)
  val authenticator = new UserAuthenticatorDirectives(authServiceClientMock)

  val routes = (new IncidentRoutes(incidentRepositoryMock, securityDirectiveForInternalRoutes, s3BucketHandlerService, authenticator)).incidentRoutes
  val publicRoutes = (new IncidentRoutes(incidentRepositoryMock, securityDirectiveForInternalRoutes, s3BucketHandlerService, authenticator)).publicIncidentRoutes(user)

  "list incidents api" should {
    val requestWithNoHeaders = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/incidents/observed-system/${testObsSysId}")
    val requestWithEmptyOsid = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/incidents/observed-system/${testObsSysId1}", headers = Seq(userIdH, accountIdH, xApiKeyH))
    val requestWithLimitAndOffset = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/incidents/observed-system/${testObsSysId}?offset=1&limit=2", headers = Seq(userIdH, accountIdH, xApiKeyH))
    val requestWithAboveLimitValues = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/incidents/observed-system/${testObsSysId}?limit=110&offset=510", headers = Seq(userIdH, accountIdH, xApiKeyH))
    val requestWithAssignee = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/incidents/observed-system/${testObsSysId}?assignee=$assigneeId", headers = Seq(userIdH, accountIdH, xApiKeyH))
    val requestWithSearch = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/incidents/observed-system/${testObsSysId}?search=$searchString", headers = Seq(userIdH, accountIdH, xApiKeyH))
    val requestWithAll = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/incidents/observed-system/${testObsSysId}?offset=0&limit=10&assignee=$assigneeId&search=$searchString", headers = Seq(userIdH, accountIdH, xApiKeyH))

    assertMissingHeaders(requestWithNoHeaders)

    s"return no incidents as there isn't any incidents for provided observed system id: $testObsSysId1" in {
      when(incidentRepositoryMock.getByObservedSystem(ArgumentMatchers.eq(testObsSysId1), any[GetIncidentsReq])(any[ExecutionContext])).thenReturn(Future.successful(GetIncidentsRes(List(), 0)))
      requestWithEmptyOsid ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        validateListResult(entityAs[GetIncidentsRes], resultCount = 0, 0,
          incidents.filter(_.observedSystemId == testObsSysId1))
      }
    }

    "return two incidents for total three incidents when offset is 1 and limit is 2" in {
      when(incidentRepositoryMock.getByObservedSystem(ArgumentMatchers.eq(testObsSysId), any[GetIncidentsReq])(any[ExecutionContext])).thenReturn(Future.successful(GetIncidentsRes(incidents.tail, 3)))
      requestWithLimitAndOffset ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        validateListResult(entityAs[GetIncidentsRes], resultCount = 2, incidents.length, incidents.tail)
      }
    }

    "return up to ten incidents from the top when limit and offset params are greater than 500 and 100 respectively" in {
      when(incidentRepositoryMock.getByObservedSystem(ArgumentMatchers.eq(testObsSysId), any[GetIncidentsReq])(any[ExecutionContext])).thenReturn(Future.successful(GetIncidentsRes(incidents, 3)))
      requestWithAboveLimitValues ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        validateListResult(entityAs[GetIncidentsRes], resultCount = incidents.length, incidents.length, incidents)
      }
    }

    s"match only one incident for provided assignee: '$assigneeId'" in {
      when(incidentRepositoryMock.getByObservedSystem(ArgumentMatchers.eq(testObsSysId), any[GetIncidentsReq])(any[ExecutionContext])).thenReturn(Future.successful(GetIncidentsRes(List(incidents.last), 1)))
      requestWithAssignee ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        validateListResult(entityAs[GetIncidentsRes], resultCount = 1, 1,
          incidents.filter(_.assigneeId == Option(assigneeId)))
      }
    }

    s"match only one incident for provided search string: '$searchString'" in {
      when(incidentRepositoryMock.getByObservedSystem(ArgumentMatchers.eq(testObsSysId), any[GetIncidentsReq])(any[ExecutionContext])).thenReturn(Future.successful(GetIncidentsRes(List(incidents.tail.head), 1)))
      requestWithSearch ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        validateListResult(entityAs[GetIncidentsRes], resultCount = 1, 1,
          incidents.filter(_.title.startsWith(searchString)))
      }
    }

    s"return no incidents for provided assignee: '$assigneeId' and search string: '$searchString'" in {
      when(incidentRepositoryMock.getByObservedSystem(ArgumentMatchers.eq(testObsSysId), any[GetIncidentsReq])(any[ExecutionContext])).thenReturn(Future.successful(GetIncidentsRes(List(), 0)))
      requestWithAll ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        validateListResult(entityAs[GetIncidentsRes], resultCount = 0, 0,
          incidents.filter(_.assigneeId == Option(assigneeId)).filter(_.title.startsWith(searchString)))
      }
    }

    val timeFilterUri = s"/api/v1/incidents/observed-system/${testObsSysId}?status=unassigned&numDays=1"

    "filter incidents based on status and time" in {
      val requestForUnassigned = HttpRequest(method = HttpMethods.GET, uri = timeFilterUri, headers = Seq(authTokenH))
      val requestForAssigned = HttpRequest(method = HttpMethods.GET, uri = timeFilterUri.replaceAllLiterally("unassigned", "assigned"), headers = Seq(authTokenH))
      val requestForClosed = HttpRequest(method = HttpMethods.GET, uri = timeFilterUri.replaceAllLiterally("unassigned", "closed"), headers = Seq(authTokenH))
      val requestForAll = HttpRequest(method = HttpMethods.GET, uri = timeFilterUri.replaceAllLiterally("unassigned", "all"), headers = Seq(authTokenH))

      val response = GetIncidentsRes(List(incidentDTO), 1)

      when(incidentRepositoryMock.getByObservedSystem(anyString, any[GetIncidentsReq])(any[ExecutionContext]))
        .thenReturn(Future.successful(response))

      requestForUnassigned ~> publicRoutes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        responseAs[GetIncidentsRes] should be(response)
      }

      requestForAssigned ~> publicRoutes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        responseAs[GetIncidentsRes] should be(response)
      }

      requestForClosed ~> publicRoutes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        responseAs[GetIncidentsRes] should be(response)
      }

      requestForAll ~> publicRoutes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        responseAs[GetIncidentsRes] should be(response)
      }
    }

    "return a 400 if the status parameter doesn't have an acceptable value" in {
      HttpRequest(method = HttpMethods.GET, uri = timeFilterUri.replaceAllLiterally("unassigned", "nope"), headers = Seq(authTokenH)) ~> publicRoutes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    "return a 400 if the numDays parameter doesn't have an acceptable value" in {
      HttpRequest(method = HttpMethods.GET, uri = timeFilterUri.replaceAllLiterally("numDays=1", "numDays=99"), headers = Seq(authTokenH)) ~> publicRoutes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    val exportToCsvUri = s"/api/v1/incidents/observed-system/${testObsSysId}/export?format=csv"

    "export to CSV format" in {
      val currentTime = System.currentTimeMillis()

      val incident =
        IncidentDTO(
          Some("INC-bkrc7grufnf333vu1c40"),
          "Incident 1",
          Some("Incident 1 description"),
          Some(List("category1", "category2")),
          Some(List("tactic1", "tactic2")),
          None,
          None,
          IncidentStatus.NEW,
          11,
          currentTime,
          1,
          IncidentReporterSource.USER,
          testObsSysId,
          Some(101L),
          Some("bob@somecompany.com"),
          Seq("event1", "event2"))

      val incidentsToExport =
        (1 to 10).map { i =>
          incident.copy(description = Some(s"Updated $i"))
        }.toVector

      when(incidentRepositoryMock.getIncidentsAndEventIdsByObservedSystem(ArgumentMatchers.eq(testObsSysId), any[ExportIncidentsParams], any[Int])(any[ExecutionContext])).thenReturn(Future.successful(incidentsToExport))

      val request = HttpRequest(uri = exportToCsvUri, headers = Seq(authTokenH))

      request ~> publicRoutes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`text/csv(UTF-8)`)
        val csvAsString = responseAs[String]
        logger.info(s"CSV:\n$csvAsString")

        val csvMaps =
          Source
            .single(ByteString(csvAsString))
            .via(CsvParsing.lineScanner())
            .via(CsvToMap.toMapAsStrings())
            .runWith(Sink.seq)
            .futureValue

        csvMaps.foreach { csvMap =>
          csvMap("id") should be("INC-bkrc7grufnf333vu1c40")
          csvMap("title") should be("Incident 1")
          csvMap("description").contains("Updated") should be(true)
          csvMap("categories") should be("category1|category2")
          csvMap("tactics") should be("tactic1|tactic2")
          csvMap("resolution") should be("")
          csvMap("resolution-timestamp") should be("")
          csvMap("status") should be("NEW")
          csvMap("severity") should be("Low")
          csvMap("report-timestamp") should be(currentTime.toString)
          csvMap("reporter-id") should be("1")
          csvMap("reporter-source") should be("USER")
          csvMap("observed-system-id") should be(testObsSysId)
          csvMap("assignee-id") should be("101")
          csvMap("assignee-email") should be("bob@somecompany.com")
          csvMap("event-ids") should be("event1|event2")
        }
      }
    }

    "return a 400 if the status parameter doesn't have an acceptable value (CSV export)" in {
      HttpRequest(method = HttpMethods.GET, uri = exportToCsvUri + "&status=nope", headers = Seq(authTokenH)) ~> publicRoutes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    "return a 400 if the numDays parameter doesn't have an acceptable value (CSV export)" in {
      HttpRequest(method = HttpMethods.GET, uri = exportToCsvUri + "&numDays=99", headers = Seq(authTokenH)) ~> publicRoutes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }
  }

  "add incident api" should {
    val request = HttpRequest(method = HttpMethods.POST, uri = s"/api/v1/incidents/observed-system/${testObsSysId}")
    val validCreateIncidentEntity = HttpEntity(ContentTypes.`application/json`, """{"eventIds":["EVT-1","EVT-2"],"reporterSource":"USER","severity":11,"title":"Incident 1","categories":[],"tactics":[]}""")
    val invalidCreateIncidentEntity = HttpEntity(ContentTypes.`application/json`, """{"eventIds":["EVT-1","EVT-2"],"reporterSource":"USER","severity":111,"title":"Incident 1","categories":[],"tactics":[]}""")
    val missingRequiredFieldCreateIncidentEntity = HttpEntity(ContentTypes.`application/json`, """{"eventIds":["EVT-1","EVT-2"],"reporterSource":"USER","severity":1}""")
    assertMissingHeaders(request)

    "return bad request if severity is not in valid range" in {
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH), entity = invalidCreateIncidentEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    "return bad request if required param is missing in request body" in {
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH), entity = missingRequiredFieldCreateIncidentEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    "create and return incident with id" in {
      when(incidentRepositoryMock.add(any[Incident], any[List[String]])(any[ExecutionContext])).thenReturn(Future.successful(incidentDTO))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH), entity = validCreateIncidentEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val actualIncidentDTO = entityAs[IncidentDTO]
        validateIncidentDTO(actualIncidentDTO, incidentDTO)
      }
    }
  }

  "update incident api" should {
    val request = HttpRequest(method = HttpMethods.POST, uri = s"/api/v1/incidents/observed-system/${testObsSysId}/${incidentDTO.id.get}")
    val validUpdateIncidentEntity = HttpEntity(ContentTypes.`application/json`, s"""{"id":"INC-bkreburufnf66invvua0","resolution":"Resolved","resolutionTimestamp":${now},"observedSystemId":"test-obs123","reportTimestamp":1563878907844,"reporterId":1,"reporterSource":"USER","severity":1,"status":"OPEN","title":"Incident 1","categories":[],"tactics":[]}""")
    val invalidUpdateIncidentEntity = HttpEntity(ContentTypes.`application/json`, s"""{"id":"INC-bkreburufnf66invvua0","resolution":"Resolved","resolutionTimestamp":${now},"observedSystemId":"test-obs123","reportTimestamp":1563878907844,"reporterId":1,"reporterSource":"USER","severity":111,"status":"OPEN","title":"Incident 1","categories":[],"tactics":[]}""")
    val missingRequiredFieldUpdateIncidentEntity = HttpEntity(ContentTypes.`application/json`, s"""{"id":"INC-bkreburufnf66invvua0","resolution":"Resolved","resolutionTimestamp":${now},"observedSystemId":"test-obs123","reportTimestamp":1563878907844,"reporterId":1,"reporterSource":"USER","severity":111,"status":"OPEN","title":"Incident 1"}""")
    val missingAssigneeEmailEntity = HttpEntity(ContentTypes.`application/json`, s"""{"id":"INC-bkreburufnf66invvua0","resolution":"Resolved","resolutionTimestamp":${now},"observedSystemId":"test-obs123","reportTimestamp":1563878907844,"reporterId":1,"reporterSource":"USER","severity":111,"status":"OPEN","title":"Incident 1","assignee":101}""")
    val missingAssigneeIdEntity = HttpEntity(ContentTypes.`application/json`, s"""{"id":"INC-bkreburufnf66invvua0","resolution":"Resolved","resolutionTimestamp":${now},"observedSystemId":"test-obs123","reportTimestamp":1563878907844,"reporterId":1,"reporterSource":"USER","severity":111,"status":"OPEN","title":"Incident 1","assignee-email":"bob@somecompany.com"}""")

    assertMissingHeaders(request)

    "return bad request if severity is not in valid range" in {
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH), entity = invalidUpdateIncidentEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    "return bad request if required param is missing in request body" in {
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH), entity = missingRequiredFieldUpdateIncidentEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    "return a bad request if the assignee ID is present and the assignee email is missing" in {
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH), entity = missingAssigneeEmailEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    "return a bad request if the assignee ID is missing and the assignee email is present" in {
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH), entity = missingAssigneeIdEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    "create and return incident with id" in {
      when(incidentRepositoryMock.update(any[Incident])(any[ExecutionContext])).thenReturn(Future.successful(Some(updatedIncidentDTO)))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH), entity = validUpdateIncidentEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        validateIncidentDTO(entityAs[IncidentDTO], updatedIncidentDTO)
      }
    }
  }

  "get incident api" should {
    val request = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/incidents/observed-system/${testObsSysId}/${incidentDTO.id.get}")

    assertMissingHeaders(request)

    "return incident details if incident found in observed system" in {
      when(incidentRepositoryMock.get(argeq(incidentDTO.id.get), argeq(testObsSysId))(any[ExecutionContext])).thenReturn(Future.successful(Some(incidentDTO)))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val actualIncidentDTO = entityAs[IncidentDTO]
        validateIncidentDTO(actualIncidentDTO, incidentDTO)
      }
    }

    "return 404 response if incident not found in observed system" in {
      when(incidentRepositoryMock.get(anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(None))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.NotFound)
      }
    }

  }

  "attach file api" should {
    val postRequest = HttpRequest(method = HttpMethods.POST, uri = s"/api/v1/incidents/files/observed-system/${testObsSysId}/${incidentDTO.id.get}")
    val validAttachFileReqEntity = HttpEntity(ContentTypes.`application/json`, """{"name":"Test file","description":"Description of file","s3Key":"s3 key"}""")
    val invalidAttachFileReqEntity = HttpEntity(ContentTypes.`application/json`, """{"name":"Test file","description":"Description of file"}""")

    def createMultipartData(dataBodypart: BodyPartEntity) = {
      val bytes = fileContent.getBytes()
      val file = HttpEntity(ContentTypes.`application/octet-stream`, bytes.length, StreamConverters.fromInputStream(() => new ByteArrayInputStream(bytes)))
      val filePart = Multipart.FormData.BodyPart("file", file, Map("filename" -> incidentFile.s3Key))
      val dataPart = Multipart.FormData.BodyPart("data", dataBodypart)
      Multipart.FormData(filePart, dataPart)
    }

    assertMissingHeaders(postRequest)

    "attach file to incident and return incident details" in {
      s3BucketHandlerService.createBucket
      when(incidentRepositoryMock.addFile(any[IncidentFile], anyString())(any[ExecutionContext])).thenReturn(Future.successful(Some(incidentDTO)))
      val multipartForm = createMultipartData(validAttachFileReqEntity)
      val requestWithFile = postRequest.copy(headers = Seq(userIdH, accountIdH), entity = multipartForm.toEntity)
      requestWithFile ~> routes ~> check {
        status shouldEqual StatusCodes.OK
      }
    }

    "return 404 response if incident not found in observed system" in {
      when(incidentRepositoryMock.addFile(any[IncidentFile], anyString())(any[ExecutionContext])).thenReturn(Future.successful(None))
      val multipartForm = createMultipartData(validAttachFileReqEntity)
      val requestWithBody = postRequest.copy(headers = Seq(userIdH, accountIdH), entity = multipartForm.toEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.NotFound)
      }
    }

    "return 500 if param is missing in request body" in {
      val multipartForm = createMultipartData(invalidAttachFileReqEntity)
      val requestWithBody = postRequest.copy(headers = Seq(userIdH, accountIdH), entity = multipartForm.toEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.InternalServerError)
      }
    }
  }

  "get file api" should {
    val request = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/incidents/files/observed-system/${testObsSysId}/${incidentDTO.id.get}/${fileId}")
    val downloadRequest = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/incidents/files/observed-system/${testObsSysId}/${incidentDTO.id.get}/download/${fileId}")

    assertMissingHeaders(request)

    "get file linked to an incident and return file details" in {
      when(incidentRepositoryMock.getFile(anyString(), anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(Some(incidentFile)))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val actualIncidentFile = entityAs[IncidentFile]
        validateIncidentFile(actualIncidentFile, incidentFile)
      }
    }

    "download file" in {
      when(incidentRepositoryMock.getFile(anyString(), anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(Some(incidentFile)))
      val requestWithBody = downloadRequest.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/octet-stream`)
        entityAs[String] should be(fileContent)
      }
    }

    "return 404 response if file not found in incident" in {
      when(incidentRepositoryMock.getFile(anyString(), anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(None))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.NotFound)
      }
    }

    "return 500 if error is thrown while deleting file" in {
      when(incidentRepositoryMock.getFile(anyString(), anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.failed(new RuntimeException("Some database error")))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.InternalServerError)
      }
    }
  }

  "delete file api" should {
    val request = HttpRequest(method = HttpMethods.DELETE, uri = s"/api/v1/incidents/files/observed-system/${testObsSysId}/${incidentDTO.id.get}/${fileId}")

    assertMissingHeaders(request)

    "delete file linked to an incident and return incident details" in {
      when(incidentRepositoryMock.getFile(anyString(), anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(Some(incidentFile)))
      when(incidentRepositoryMock.deleteFile(anyString(), anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(Some(incidentDTO)))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val actualIncidentDTO = entityAs[IncidentDTO]
        validateIncidentDTO(actualIncidentDTO, incidentDTO)
      }
    }

    "return 404 response if incident not found in observed system" in {
      when(incidentRepositoryMock.getFile(anyString(), anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(None))
      when(incidentRepositoryMock.deleteFile(anyString(), anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(None))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.NotFound)
      }
    }

    "return 500 if error is thrown while deleting file" in {
      when(incidentRepositoryMock.deleteFile(anyString(), anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.failed(new RuntimeException("Some database error")))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.InternalServerError)
      }
    }
  }

  "link urls api" should {
    val request = HttpRequest(method = HttpMethods.POST, uri = s"/api/v1/incidents/urls/observed-system/${testObsSysId}/${incidentDTO.id.get}")
    val validAttachUrlReqEntity = HttpEntity(ContentTypes.`application/json`, """{"url":"https://qomplx.com"}""")
    val invalidAttachUrlReqEntity = HttpEntity(ContentTypes.`application/json`, """{"notValid":"https://qomplx.com"}""")

    assertMissingHeaders(request)

    "link url to incident and return incident details" in {
      when(incidentRepositoryMock.addURL(any[IncidentURL], anyString())(any[ExecutionContext])).thenReturn(Future.successful(Some(incidentDTO)))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH), entity = validAttachUrlReqEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val actualIncidentDTO = entityAs[IncidentDTO]
        validateIncidentDTO(actualIncidentDTO, incidentDTO)
      }
    }

    "return 404 response if incident not found in observed system" in {
      when(incidentRepositoryMock.addURL(any[IncidentURL], anyString())(any[ExecutionContext])).thenReturn(Future.successful(None))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH), entity = validAttachUrlReqEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.NotFound)
      }
    }

    "return 400 if param is missing in request body" in {
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH), entity = invalidAttachUrlReqEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }
  }

  "delete url api" should {
    val request = HttpRequest(method = HttpMethods.DELETE, uri = s"/api/v1/incidents/urls/observed-system/${testObsSysId}/${incidentDTO.id.get}/${urlId}")

    assertMissingHeaders(request)

    "delete url linked to an incident and return incident details" in {
      when(incidentRepositoryMock.deleteURL(anyString(), anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(Some(incidentDTO)))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val actualIncidentDTO = entityAs[IncidentDTO]
        validateIncidentDTO(actualIncidentDTO, incidentDTO)
      }
    }

    "return 404 response if incident not found in observed system" in {
      when(incidentRepositoryMock.deleteURL(anyString(), anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(None))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.NotFound)
      }
    }

    "return 500 if error is thrown while deleting file" in {
      when(incidentRepositoryMock.deleteURL(anyString(), anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.failed(new RuntimeException("Some database error")))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.InternalServerError)
      }
    }
  }

  "link notes api" should {
    val request = HttpRequest(method = HttpMethods.POST, uri = s"/api/v1/incidents/notes/observed-system/${testObsSysId}/${incidentDTO.id.get}")
    val validAttachNoteReqEntity = HttpEntity(ContentTypes.`application/json`, """{"note":"Fractal is now Qomplx"}""")
    val invalidAttachNoteReqEntity = HttpEntity(ContentTypes.`application/json`, """{"notValid":"Not valid request body"}""")

    assertMissingHeaders(request)

    "link note to incident and return incident details" in {
      when(incidentRepositoryMock.addNote(any[IncidentNote], anyString())(any[ExecutionContext])).thenReturn(Future.successful(Some(incidentDTO)))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH), entity = validAttachNoteReqEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val actualIncidentDTO = entityAs[IncidentDTO]
        validateIncidentDTO(actualIncidentDTO, incidentDTO)
      }
    }

    "return 404 response if incident not found in observed system" in {
      when(incidentRepositoryMock.addNote(any[IncidentNote], anyString())(any[ExecutionContext])).thenReturn(Future.successful(None))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH), entity = validAttachNoteReqEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.NotFound)
      }
    }

    "return 400 if param is missing in request body" in {
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH), entity = invalidAttachNoteReqEntity)
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }
  }

  "delete note api" should {
    val request = HttpRequest(method = HttpMethods.DELETE, uri = s"/api/v1/incidents/notes/observed-system/${testObsSysId}/${incidentDTO.id.get}/${noteId}")

    assertMissingHeaders(request)

    "delete note linked to an incident and return incident details" in {
      when(incidentRepositoryMock.deleteNote(anyString(), anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(Some(incidentDTO)))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val actualIncidentDTO = entityAs[IncidentDTO]
        validateIncidentDTO(actualIncidentDTO, incidentDTO)
      }
    }

    "return 404 response if incident not found in observed system" in {
      when(incidentRepositoryMock.deleteNote(anyString(), anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(None))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.NotFound)
      }
    }

    "return 500 if error is thrown while deleting file" in {
      when(incidentRepositoryMock.deleteNote(anyString(), anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.failed(new RuntimeException("Some database error")))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.InternalServerError)
      }
    }
  }

  "get attachments api" should {
    val request = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/incidents/attachments/observed-system/${testObsSysId}/${incidentDTO.id.get}")

    assertMissingHeaders(request)

    "get attachments linked to an incident" in {
      when(incidentRepositoryMock.getAttachments(anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(Some(attachmentsDTO)))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val actualAttachmentsDTO = entityAs[AttachmentsDTO]
        validateAttachmentsDTO(actualAttachmentsDTO, attachmentsDTO)
      }
    }

    "return 404 response if incident not found in observed system" in {
      when(incidentRepositoryMock.getAttachments(anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(None))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.NotFound)
      }
    }

    "return 500 if error is thrown while deleting file" in {
      when(incidentRepositoryMock.getAttachments(anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.failed(new RuntimeException("Some database error")))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.InternalServerError)
      }
    }
  }

  "get events api" should {
    val request = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/incidents/events/observed-system/${testObsSysId}/${incidentDTO.id.get}")

    assertMissingHeaders(request)

    "get events linked to an incident" in {
      when(incidentRepositoryMock.getEvents(anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(eventList))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val actualEventList = entityAs[Seq[Event]]
        validateEvents(actualEventList, eventList)
      }
    }

    "return empty list if events not found for incident" in {
      when(incidentRepositoryMock.getEvents(anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.successful(Seq()))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val actualEventList = entityAs[Seq[Event]]
        validateEvents(actualEventList, Seq())
      }
    }

    "return 500 if error is thrown while deleting file" in {
      when(incidentRepositoryMock.getEvents(anyString(), anyString())(any[ExecutionContext])).thenReturn(Future.failed(new RuntimeException("Some database error")))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.InternalServerError)
      }
    }
  }

  "get categories api" should {
    val request = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/incidents/categories")

    assertMissingHeaders(request)

    "get categories list" in {
      when(incidentRepositoryMock.getCategories()(any[ExecutionContext])).thenReturn(Future.successful(categories))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val actualCategories = entityAs[Seq[Category]]
        actualCategories shouldEqual categories
      }
    }

    "return empty list if categories not found" in {
      when(incidentRepositoryMock.getCategories()(any[ExecutionContext])).thenReturn(Future.successful(Seq()))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val actualCategories = entityAs[Seq[Category]]
        actualCategories shouldEqual Seq()
      }
    }

    "return 500 if error is thrown while fetching categories" in {
      when(incidentRepositoryMock.getCategories()(any[ExecutionContext])).thenReturn(Future.failed(new RuntimeException("Some database error")))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.InternalServerError)
      }
    }
  }

  "get tactics api" should {
    val request = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/incidents/tactics")

    assertMissingHeaders(request)

    "get tactics list" in {
      when(incidentRepositoryMock.getTactics()(any[ExecutionContext])).thenReturn(Future.successful(tactics))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val actualTactics = entityAs[Seq[Tactic]]
        actualTactics shouldEqual tactics
      }
    }

    "return empty list if tactics not found" in {
      when(incidentRepositoryMock.getTactics()(any[ExecutionContext])).thenReturn(Future.successful(Seq()))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val actualTactics = entityAs[Seq[Tactic]]
        actualTactics shouldEqual Seq()
      }
    }

    "return 500 if error is thrown while fetching tactics" in {
      when(incidentRepositoryMock.getTactics()(any[ExecutionContext])).thenReturn(Future.failed(new RuntimeException("Some database error")))
      val requestWithBody = request.copy(headers = Seq(userIdH, accountIdH))
      requestWithBody ~> routes ~> check {
        status should be(StatusCodes.InternalServerError)
      }
    }
  }

  "statistics API" should {
    "get a statistics summary" in {
      val statisticsSummaryReq = HttpRequest(uri = s"/api/v1/incidents/observed-system/${testObsSysId}/statistics/summary", headers = Seq(authTokenH))

      val now = Instant.now().toEpochMilli
      val summary =
        StatisticsSummary(
          requestTimestampMillis = now,
          all = MinimalStatistics(3, 1),
          unassigned = MinimalStatistics(4, 0),
          assigned = MinimalStatistics(5, -1),
          closed = MinimalStatistics(10, 2))

      when(incidentRepositoryMock.getStatisticsSummary(anyString, anyLong)(any[ExecutionContext]))
        .thenReturn(Future.successful(summary))

      statisticsSummaryReq ~> publicRoutes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val responseBody = responseAs[StatisticsSummary]
        responseBody should be(summary)
      }
    }

    val statsDetailsUri = s"/api/v1/incidents/observed-system/${testObsSysId}/statistics?status=all&numDays=30"
    "get statistics details" in {
      val statsDetailsReq = HttpRequest(uri = statsDetailsUri, headers = Seq(authTokenH))
      val details = StatisticsDetails(Instant.now().toEpochMilli, "all", 30, "6days", Vector(4, 2, 3, 10, 1))

      when(incidentRepositoryMock.getStatisticsDetails(anyString, anyLong, any[IncidentStatusForStatistics], anyInt)(any[ExecutionContext]))
        .thenReturn(Future.successful(details))

      statsDetailsReq ~> publicRoutes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val responseBody = responseAs[StatisticsDetails]
        responseBody should be(details)
      }
    }

    val severityStatsUri = s"/api/v1/incidents/observed-system/${testObsSysId}/statistics/severity?status=all&numDays=30"
    "get severity statistics" in {
      val severityStatsReq = HttpRequest(uri = severityStatsUri, headers = Seq(authTokenH))
      val severityStats = SeverityStatistics(Instant.now().toEpochMilli, "all", 30, SeverityCounts(1, 2, 3))

      when(incidentRepositoryMock.getSeverityStatistics(anyString, anyLong, any[IncidentStatusForStatistics], anyInt)(any[ExecutionContext]))
        .thenReturn(Future.successful(severityStats))

      severityStatsReq ~> publicRoutes ~> check {
        status should be(StatusCodes.OK)
        contentType should be(ContentTypes.`application/json`)
        val responseBody = responseAs[SeverityStatistics]
        responseBody should be(severityStats)
      }
    }

    "return a 400 if the status parameter doesn't have an acceptable value" in {
      val statsDetailsReq = HttpRequest(uri = statsDetailsUri.replaceAllLiterally("status=all", "status=nope"), headers = Seq(authTokenH))
      statsDetailsReq ~> publicRoutes ~> check {
        status should be(StatusCodes.BadRequest)
      }

      val severityStatsReq = HttpRequest(uri = severityStatsUri.replaceAllLiterally("status=all", "status=nope"), headers = Seq(authTokenH))
      severityStatsReq ~> publicRoutes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }

    "return a 400 if the numDays parameter doesn't have an acceptable value" in {
      val statsDetailsReq = HttpRequest(uri = statsDetailsUri.replaceAllLiterally("numDays=30", "numDays=99"), headers = Seq(authTokenH))
      statsDetailsReq ~> publicRoutes ~> check {
        status should be(StatusCodes.BadRequest)
      }

      val severityStatsReq = HttpRequest(uri = severityStatsUri.replaceAllLiterally("numDays=30", "numDays=99"), headers = Seq(authTokenH))
      severityStatsReq ~> publicRoutes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }
  }

  private def assertMissingHeaders(request: HttpRequest) = {
    "reject request if the required headers are not present" in {
      request ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }
    "reject request if the account-id header is not present" in {
      request.copy(headers = Seq(userIdH, xApiKeyH)) ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }
    "reject request if the user-id header is not present" in {
      request.copy(headers = Seq(accountIdH, xApiKeyH)) ~> routes ~> check {
        status should be(StatusCodes.BadRequest)
      }
    }
  }

  private def validateIncidentDTO(actualIncidentDTO: IncidentDTO, incidentDTO: IncidentDTO) = {
    actualIncidentDTO.id shouldEqual incidentDTO.id
    actualIncidentDTO.title shouldEqual incidentDTO.title
    actualIncidentDTO.status shouldEqual incidentDTO.status
    actualIncidentDTO.reporterSource shouldEqual incidentDTO.reporterSource
    actualIncidentDTO.reporterId shouldEqual incidentDTO.reporterId
    actualIncidentDTO.observedSystemId shouldEqual incidentDTO.observedSystemId
    actualIncidentDTO.resolution shouldEqual incidentDTO.resolution
  }

  private def validateIncidentFile(actuaIncidentFile: IncidentFile, expectedIncidentFile: IncidentFile) = {
    actuaIncidentFile.id shouldEqual expectedIncidentFile.id
    actuaIncidentFile.name shouldEqual expectedIncidentFile.name
    actuaIncidentFile.description shouldEqual expectedIncidentFile.description
    actuaIncidentFile.s3Key shouldEqual expectedIncidentFile.s3Key
    actuaIncidentFile.incidentId shouldEqual expectedIncidentFile.incidentId
  }

  private def validateAttachmentsDTO(actualAttachmentsDTO: AttachmentsDTO, attachmentsDTO: AttachmentsDTO) = {
    actualAttachmentsDTO.incidentFiles shouldEqual attachmentsDTO.incidentFiles
    actualAttachmentsDTO.incidentURLs shouldEqual attachmentsDTO.incidentURLs
    actualAttachmentsDTO.incidentNotes shouldEqual attachmentsDTO.incidentNotes
  }

  def validateEvents(actualEventList: Seq[Event], expectedEventList: Seq[Event]) = {
    actualEventList shouldEqual expectedEventList
  }

  private def validateListResult(actualResult: GetIncidentsRes, resultCount: Int, totalCount: Int, results: Seq[IncidentDTO]) = {
    actualResult.results.length shouldEqual resultCount
    actualResult.totalCount shouldEqual totalCount
    actualResult.results shouldEqual results
  }
}
