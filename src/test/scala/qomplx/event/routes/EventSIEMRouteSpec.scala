package qomplx.event.routes

import java.util.UUID

import akka.http.scaladsl.model._
import akka.http.scaladsl.model.headers.{ HttpChallenge, RawHeader }
import akka.http.scaladsl.server.AuthenticationFailedRejection
import akka.http.scaladsl.server.AuthenticationFailedRejection.{ CredentialsMissing, CredentialsRejected }
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.stream.Materializer
import com.typesafe.scalalogging.StrictLogging
import org.mockito.ArgumentMatchers._
import org.mockito.Mockito.when
import org.scalatest.mockito.MockitoSugar
import org.scalatest.{ Matchers, WordSpec }
import qomplx.event.JsonSupport
import qomplx.event.directives.SecurityDirectivesForSIEMRoutes
import qomplx.event.repository.{ EventRepository, SIEMEventsResponse }
import qomplx.event.thirdpartyintegrations.model.ThirdPartySystem
import qomplx.event.thirdpartyintegrations.repository.ThirdPartySystemConfigRepository
import qomplx.event.thirdpartyintegrations.routes.TSConfigDTO

import scala.concurrent.{ ExecutionContext, Future }

class EventSIEMRouteSpec
  extends WordSpec
  with Matchers
  with ScalatestRouteTest
  with StrictLogging
  with JsonSupport {

  private val eventRepo = MockitoSugar.mock[EventRepository]
  private val thirdPartyConfigRepo = MockitoSugar.mock[ThirdPartySystemConfigRepository]
  private val securityDirective = new SecurityDirectivesForSIEMRoutes(thirdPartyConfigRepo)
  private val routes = new EventSIEMRoutes(eventRepo, thirdPartyConfigRepo, securityDirective).routes

  "EventSIEMRoute" should {
    "authenticate based on the token in the X-API-KEY header" in {
      val observedSystemId = UUID.randomUUID().toString
      val token = UUID.randomUUID().toString
      val response = SIEMEventsResponse(Nil, 0L)
      val tsConfig = TSConfigDTO("config-id", observedSystemId, ThirdPartySystem.JIRA, "config 1", Some("config 1 desc"), Option(List("a@b.com")), true, false)

      when(thirdPartyConfigRepo.getObservedSystemIdByToken(token))
        .thenReturn(Future.successful(Option(observedSystemId)))

      when(thirdPartyConfigRepo.getByToken(token))
        .thenReturn(Future.successful(Option(tsConfig)))

      when(thirdPartyConfigRepo.setRecentlyEnabled(token, false))
        .thenReturn(Future.successful(1))

      when(eventRepo.getNextEventBatch(anyString(), any())(any[ExecutionContext], any[Materializer]))
        .thenReturn(Future.successful(response))

      val xApiKey = RawHeader("X-API-KEY", token)
      val request = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/event/siem?last=1")

      request.copy(headers = List(xApiKey)) ~> routes ~> check {
        status should be(StatusCodes.OK)
        entityAs[SIEMEventsResponse] should be(response)
      }
    }

    "reject the request if the X-API-KEY header is missing" in {
      val request = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/event/siem?last=1")
      request ~> routes ~> check {
        rejection should be(AuthenticationFailedRejection(CredentialsMissing, HttpChallenge("QOMPLX", Some("Credentials are missing"))))
      }
    }

    "reject the request if the token is not associated with an observed system" in {
      val observedSystemId = UUID.randomUUID().toString
      val token = UUID.randomUUID().toString

      when(thirdPartyConfigRepo.getObservedSystemIdByToken(token))
        .thenReturn(Future.successful(None))

      val xApiKey = RawHeader("X-API-KEY", token)
      val request = HttpRequest(method = HttpMethods.GET, uri = s"/api/v1/event/siem?last=1")

      request.copy(headers = List(xApiKey)) ~> routes ~> check {
        rejection should be(AuthenticationFailedRejection(CredentialsRejected, HttpChallenge("QOMPLX", Some("Credentials are rejected"))))
      }
    }
  }
}
