package qomplx.event

import akka.actor.ActorSystem
import akka.stream.{ ActorMaterializer, ActorMaterializerSettings, Supervision }
import com.typesafe.scalalogging.StrictLogging
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.time.{ Millis, Seconds, Span }
import org.scalatest.{ BeforeAndAfterAll, Matchers, WordSpec }

import scala.concurrent.duration.DurationInt
import scala.concurrent.{ Await, ExecutionContext }

/**
 * Copied from cyber-orchestrator.
 */
trait BaseSpecWithMigrations extends BaseSpec with IntegrationTestContainers {

  protected implicit val system: ActorSystem = ActorSystem("TestCyberEventService")

  val decider: Supervision.Decider = {
    t =>
      logger.error("An error occurred in a stream. Dropping the element and continuing.", t)
      Supervision.Restart
  }

  protected implicit val mat: ActorMaterializer =
    ActorMaterializer(ActorMaterializerSettings(system)
      .withSupervisionStrategy(decider))

  protected implicit val ec: ExecutionContext = system.dispatcher

  implicit val defaultPatience: PatienceConfig = PatienceConfig(timeout = Span(10, Seconds), interval = Span(5, Millis))

  override protected def afterAll(): Unit = {
    Await.ready(system.terminate(), 60 seconds)
    super.afterAll()
  }
}
