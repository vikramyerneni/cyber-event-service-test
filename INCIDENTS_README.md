Incidents
---------
This part of cyber-event-service handles incidents in and out of cyber system. Data model linked with incidents are as mentioned below:


Incident Data Model
-------------------
Table Name: amp_incidents

    {
      "id": String (format: INC-<XID>), primary key, required
      "title": String, required,
      "description": String, optional,
      "categories": Array[String], optional,
      "tactics": Array[String], optional
      "resolution": String, optional
      "resolution_timestamp": Timestamp, optional
      "status": Enum (New | Open | Closed), required, default: New
      "severity": Int, required, range: 0 to 100
      "report_timestamp": Timestamp, required
      "reporter_id": Long, required
      "reporter_source": Enum (User | System), required, default: User
      "observed_system_id": String, required
      "assignee": Long, optional
      "assignee_email": String, optional
      "created_on": Timestamp,
      "created_by": Long
      "last_modified_on": Timestamp,
      "last_modified_by": Long
    }

Table Name: amp_incident_files

    {
      "id": String, <XID> primary key,
      "name" String, required
      "description": String, optional
      "s3_key": S3 key (Note: Never get exposed)
      "incident_id": Foreign key to amp_incidents
      "created_on": Timestamp,
      "created_by": Long
      "last_modified_on": Timestamp,
      "last_modified_by": Longby": Long
    }
 
Table: amp_incident_urls

    {
      "id": String, <XID> primary key
      "url": String, required
      "incident_id": Foreign key to amp_incidents
      "created_on": Timestamp,
      "created_by": Long
      "last_modified_on": Timestamp,
      "last_modified_by": Longby": Long
    }
    
Table: amp_incident_notes
    
    {
      "id": String, <XID> primary key
      "note": String, required
      "incident_id": Foreign key to amp_incidents
      "created_on": Timestamp,
      "created_by": Long
      "last_modified_on": Timestamp,
      "last_modified_by": Longby": Long
    }
    
Table: amp_event_incident
    
    {
      "amp_event_id": Foreign key to amp_events
      "amp_incident_id": Foreign key to amp_incidents      
    }
     
API
---
- List Incidents:
    - GET `http://cyber-event-service.fos/api/v1/incidents/observed-system/{system-id}`
    - Headers:

            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1

    - Params:
        - `limit` (optional) : The number of incidents to query; Default: 10
        - `offset` (optional) : The number of incidents from offset; Default: 0
        - `assignee` (optional) : The user ID (num) of the assignee to filter by
        - `search` (optional) : The search string on which to filter (performs a case-insensitive "like" on the title)


    - Response content type: `application/json`

    - Response:

            {
              "results": [
                {
                  "id": "string",
                  "title": "string",
                  "categories": [string],
                  "tactics": [string],
                  "resolution": "string",
                  "resolutionTimestamp": long,
                  "status": "string", -- enum value
                  "reportTimestamp": long,
                  "reporterId": long,
                  "reporterSource": "string", -- enum value
                  "observedSystemId": "string", 
                  "assigneeId": long,
                  "assigneeEmail": "string"
                },
                ...
              ],
              "totalCount": 120
            }

    - Note: param values for `limit` and `offset` are restricted to `100` and `500` respectively.
      if provided values are greater than them, then it will be reverted to their default values.

- Export incidents to CSV:
    - GET `http://cyber-event-service.fos/api/v1/incidents/observed-system/{system-id}/export?format=csv`
    - Headers:

            X-QOS-AUTH-TOKEN

    - Params:
        - `assignee` (optional) : The user ID (number) of the assignee on which to filter
        - `search` (optional) : The search string on which to filter (performs a case-insensitive "like" on the title)

    - Response content type: `text/csv(UTF-8)`

    - Example response:

              id,title,description,categories,tactics,resolution,resolution-timestamp,status,severity,report-timestamp,reporter-id,reporter-source,observed-system-id,assignee-id,assignee-email,event-ids
              INC-bovl9agpf44002006iq0,Tedtsdffsdfsdf,,Initial Access|Execution,Credential Forging,,,NEW,60,1581208746943,1,USER,5e275ee3c9e77c0008c77590,,,EVT-boterlbdk87g027vn94g|EVT-boterlbdk87g027vn9ag
              INC-bpbvephlr45g020s2440,Something bad happened,,Lateral Movement,Brute Force,,,NEW,30,1582823270827,115,USER,5e275ee3c9e77c0008c77590,,,EVT-boterlbdk87g027vn8qg|EVT-boterlbdk87g027vn990

- Create Incident:
    - POST `http://cyber-event-service.fos/api/v1/incidents/observed-system/{system-id}`
    - Headers:

            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1

    - Response content type: `application/json`
    - Body: Below mentioned fields are saved in database during creation.
      
            {
              "title": "Incident 1",
              "description": "Incident description" // optional
              "severity": 1, 
              "categories": ["category 1", "category 2"], // optional
              "tactics": ["tactic 1", "tactic 2"], // optional
              "reporterSource": "SYSTEM", // if source is not passed in request, default value will be assigned to incident
              "eventIds": ["EVT-4123", "EVT-5123", "EVT-1212"] // ids of event related to incident
            }

    - Response:

            {
              "categories": [
                  "category 1",
                  "category 2"
              ], 
              "description": "Incident description",
              "id": "INC-bkt901rufnf5l9nk6dpg",
              "observedSystemId": "5d2759f7e828860008d2136d",
              "reportTimestamp": 1564119987555,
              "reporterId": 1,
              "reporterSource": "SYSTEM",
              "severity": 1,
              "status": "NEW",
              "tactics": [
                  "tactic 1",
                  "tactic 2"
              ],
              "title": "Incident 1"
            }

- Update Incident:
    - POST `http://cyber-event-service.fos/api/v1/incidents/observed-system/{system-id}/{incident-id}`
    - Headers:

            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1

    - Response content type: `application/json`
    - Body:

            {
              "title": "Updated Incident Title",
              "description": "Incident description"
              "categories": ["new category 1", "new category 2"], // optional
              "tactics": ["new tactic"], // optional
              "resolution": "Found the solution", // optional
              "resolutionTimestamp": 156379269000, // optional
              "status": "CLOSED",
              "severity": 12,
              "reporterSource": "USER",
              "assigneeId": 101, // optional
              "assigneeEmail": "bob@somecompany.com" // required if "assignee" is define; otherwise it must be empty
            }

    - Response:

            {
              "assigneeId": 101,
              "assigneeEmail": "bob@somecompany.com",
              "categories": [
                  "new category 1",
                  "new category 2"
              ],
              "description": "Incident description"
              "id": "INC-bkt901rufnf5l9nk6dpg",
              "observedSystemId": "5d2759f7e828860008d2136d",
              "reportTimestamp": 1564119987555,
              "reporterId": 1,
              "reporterSource": "USER",
              "resolution": "Found the solution",
              "resolutionTimestamp": 1564119987555,
              "severity": 11,
              "status": "CLOSED",
              "tactics": [ "new tactic" ],
              "title": "Updated Incident Title"
            }

- Get Incident:
    - GET `http://cyber-event-service.fos/api/v1/incidents/observed-system/{system-id}/{incident-id}`
    - Headers:

            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1

    - Response content type: `application/json`

    - Response: 
 
            {
              "assigneeId": 101,
              "assigneeEmail": "bob@somecompany.com",
              "categories": [
                  "new category 1",
                  "new category 2"
              ],
              "description": "Incident description"
              "id": "INC-bkt901rufnf5l9nk6dpg",
              "observedSystemId": "5d2759f7e828860008d2136d",
              "reportTimestamp": 1564119987555,
              "reporterId": 1,
              "reporterSource": "USER",
              "resolution": "Found the solution",
              "resolutionTimestamp": 1564119987555,
              "severity": 11,
              "status": "CLOSED",
              "tactics": [ "new tactic" ],
              "title": "Updated Incident Title"
            }

- Add File to Incident:
    - POST `/incidents/files/observed-system/{system-id}/{incident-id}`
    - Headers:

            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1
            
    - Body:
    
          Note: Please search for its example in `IncidentRouteSpec` test spec.
    
          Multipart FormData 
            BodyPart name = "file" `application/octet-stream`
              provide filename
            BodyPart name = "data" `application/json`
              {
                "name": "File name",
                "description": "File description", //optional, defaults to name
                "s3Key": "5b895009228ddf2f3fb43386/14c1fbf9a42c-f36b-0e04-9b1b-9b9cce6a.png"
              }

    - Response: 
 
            {
              "id": "INC-bks1orbufnf2gqnvvv7g",
              "observedSystemId": "test-obs123",
              "reportTimestamp": 1563958381928,
              "reporterId": 1,
              "reporterSource": "USER",
              "severity": 11,
              "status": "NEW",
              "title": "Incident 1"
            }

- Get File of Incident:
    - GET `/incidents/files/observed-system/{system-id}/{incident-id}/{file-id}`
    - Headers:
 
            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1

    - Response: 

            {
              "createdBy": 1,
              "createdOn": 1564398845120,
              "description": "Test file",
              "id": "bkvd9vbufnf4vnnviogg",
              "incidentId": "INC-bkvb9hjufnf1v1nvvu4g",
              "lastModifiedBy": 1,
              "lastModifiedOn": 1564398845120,
              "name": "Test file",
              "s3Key": "abc/def/ghi"
            }
            
- Download File of Incident:
    - GET `/incidents/files/observed-system/{system-id}/{incident-id}/download/{file-id}`
    - Headers:
 
            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1

    - Response: 

            ContentType: application/octet-stream
            // TODO add details 

- Delete File From Incident:
    - DELETE `/incidents/files/observed-system/{system-id}/{incident-id}/{file-id}`
    - Headers:

            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1

    - Response: 

            {
              "id": "INC-bks1orbufnf2gqnvvv7g",
              "observedSystemId": "test-obs123",
              "reportTimestamp": 1563958381928,
              "reporterId": 1,
              "reporterSource": "USER",
              "severity": 11,
              "status": "NEW",
              "title": "Incident 1"
            }

- Add Note to Incident:
    - POST `/incidents/notes/observed-system/{system-id}/{incident-id}`
    - Headers:

            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1

    - Response content type: `application/json`
    - Body:

            {
              "note": "This is a sample note for incident"
            }

    - Response: 

            {
              "id": "INC-bks1orbufnf2gqnvvv7g",
              "observedSystemId": "test-obs123",
              "reportTimestamp": 1563958381928,
              "reporterId": 1,
              "reporterSource": "USER",
              "severity": 11,
              "status": "NEW",
              "title": "Incident 1"
            }

- Delete Note From Incident:
    - DELETE `/incidents/notes/observed-system/{system-id}/{incident-id}/{note-id}`
    - Headers:

            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1

    - Response: 
 
            {
              "id": "INC-bks1orbufnf2gqnvvv7g",
              "observedSystemId": "test-obs123",
              "reportTimestamp": 1563958381928,
              "reporterId": 1,
              "reporterSource": "USER",
              "severity": 11,
              "status": "NEW",
              "title": "Incident 1"
            }
 
- Add URL to Incident:
    - POST `/incidents/urls/observed-system/{system-id}/{incident-id}`
    - Headers:
 
            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1
 
    - Response content type: `application/json`
    - Body:

            {
              "url": "https://qomplx.com"
            }

    - Response: 

            {
              "id": "INC-bks1orbufnf2gqnvvv7g",
              "observedSystemId": "test-obs123",
              "reportTimestamp": 1563958381928,
              "reporterId": 1,
              "reporterSource": "USER",
              "severity": 11,
              "status": "NEW",
              "title": "Incident 1"
            }

- Delete URL From Incident:
    - DELETE `/incidents/urls/observed-system/{system-id}/{incident-id}/{url-id}`
    - Headers:
          
            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1

    - Response: 

            {
              "id": "INC-bks1orbufnf2gqnvvv7g",
              "observedSystemId": "test-obs123",
              "reportTimestamp": 1563958381928,
              "reporterId": 1,
              "reporterSource": "USER",
              "severity": 11,
              "status": "NEW",
              "title": "Incident 1"
            }
      
- Get attachments for Incident:
    - GET `/incidents/attachments/observed-system/{system-id}/{incident-id}`
    - Headers:
          
            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1

    - Response: 
 
            {
              "incidentFiles": [
                {
                    "createdBy": 1,
                    "createdOn": 1564130184545,
                    "description": "test file descripiton",
                    "id": "bktbn23ufnf3bq7vvv9g",
                    "name": "test file"
                }
              ],
              "incidentNotes": [
                {
                    "createdBy": 1,
                    "createdOn": 1564130178454,
                    "id": "bktbn0jufnf3bq7vvv8g",
                    "note": "Test note 2 for incident"
                },
                {
                    "createdBy": 1,
                    "createdOn": 1564130179475,
                    "id": "bktbn0rufnf3bq7vvv90",
                    "note": "Test note 1 for incident"
                }
              ],
              "incidentURLs": [
                {
                    "createdBy": 1,
                    "createdOn": 1564130172898,
                    "id": "bktbmv3ufnf3bq7vvv7g",
                    "url": "https://qomplx.com"
                },
                {
                    "createdBy": 1,
                    "createdOn": 1564130175024,
                    "id": "bktbmvrufnf3bq7vvv80",
                    "url": "https://qomplxos.com"
                }
              ]
            }
          
- Get list of Categories:
    - GET `/api/v1/incidents/categories`
    - Headers:

            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1

    - Response content type: `application/json`
    - Response: 

            [
              { "name": "Initial Access" }, ...
            ]

- Get list of Tactics:
    - GET `/api/v1/incidents/tactics`
    - Headers:
    
            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1

    - Response content type: `application/json` 
    - Response: 
    
            [
              { "name": "Drive-by Compromise" }, ...
            ]

- Get events linked with an Incident:
    - GET `/incidents/events/observed-system/{system-id}/{incident-id}`
    - Headers:
          
            X-QOMPLX-UserId:1
            X-QOMPLX-CustomerAccountId:1

    - Response: 

            [
              {
                "categories": [
                  "Account Manipulation"
                ],
                "description": "Event description",
                "id": "EVT-bkrjvb26ddg001nvksvg",
                "name": "Kerberos Event 3",
                "observedSystemId": "5d2759f7e828860008d2136d",
                "severity": 90,
                "source": "Kerberos Pipeline",
                "sourceId": "8ec0d7d9",
                "tactics": [
                  "Credential Access",
                  "Persistence"
                ],
                "timestamp": 1563901673969,
                "type": "Kerberos"
              },
              ...
            ]
            
- Get count of incidents by their severity for last 24 hours:   
    - GET `/incidents/observed-system/{system-id}/count/by-severity`
    - Response content type: `application/json`
    - Example response:

            {
              "high": 40,
              "low": 26,
              "medium": 34,
              "total": 100
            }

Postgres Notes
--------------
It's assumed that the `cyber_event_service` schema is already generated in Postgres.

- `cyber_event_service.amp_incident_status` enum:

        CREATE TYPE amp_incident_status AS ENUM ('NEW', 'OPEN', 'CLOSED');
        
- `cyber_event_service.amp_incident_reporter_source` enum:

        CREATE TYPE amp_incident_reporter_source AS ENUM ('USER', 'SYSTEM');
        
- `cyber_event_service.amp_incidents` table:

        CREATE TABLE IF NOT EXISTS cyber_event_service.amp_incidents (
          id text PRIMARY KEY,
          title text NOT NULL,
          resolution text,
          resolution_timestamp bigint,
          status amp_incident_status NOT NULL DEFAULT 'NEW',
          severity smallint NOT NULL DEFAULT 0,
          report_timestamp bigint NOT NULL,
          reporter_id bigint NOT NULL,
          reporter_source amp_incident_reporter_source NOT NULL DEFAULT 'USER',
          observed_system_id text NOT NULL,
          assignee bigint,
          created_on bigint NOT NULL,
          created_by bigint NOT NULL,
          last_modified_on bigint NOT NULL,
          last_modified_by bigint NOT NULL
        );

        CREATE INDEX idx_amp_incidents_observed_system_id ON cyber_event_service.amp_incidents (observed_system_id);
        CREATE INDEX idx_amp_incidents_status ON cyber_event_service.amp_incidents (status);
        CREATE INDEX idx_amp_incidents_reporter_id ON cyber_event_service.amp_incidents (reporter_id);
        CREATE INDEX idx_amp_incidents_assignee ON cyber_event_service.amp_incidents (assignee);

        postgres-cyber-events=# \d cyber_event_service.amp_incidents;
          Table "cyber_event_service.amp_incidents"
               Column        |             Type             | Collation | Nullable |               Default                
        ----------------------+------------------------------+-----------+----------+--------------------------------------
         id                   | text                         |           | not null | 
         title                | text                         |           | not null | 
         resolution           | text                         |           |          | 
         resolution_timestamp | bigint                       |           |          | 
         status               | amp_incident_status          |           | not null | 'NEW'::amp_incident_status
         severity             | smallint                     |           | not null | 0
         report_timestamp     | bigint                       |           | not null | 
         reporter_id          | bigint                       |           | not null | 
         reporter_source      | amp_incident_reporter_source |           | not null | 'USER'::amp_incident_reporter_source
         observed_system_id   | text                         |           | not null | 
         assignee             | bigint                       |           |          | 
         created_on           | bigint                       |           | not null | 
         created_by           | bigint                       |           | not null | 
         last_modified_on     | bigint                       |           | not null | 
         last_modified_by     | bigint                       |           | not null | 
        Indexes:
            "amp_incidents_pkey" PRIMARY KEY, btree (id)
            "idx_amp_incidents_assignee" btree (assignee)
            "idx_amp_incidents_observed_system_id" btree (observed_system_id)
            "idx_amp_incidents_reporter_id" btree (reporter_id)
            "idx_amp_incidents_status" btree (status)

- `cyber_event_service.amp_incident_files` table:

        CREATE TABLE IF NOT EXISTS cyber_event_service.amp_incident_files (
          id text PRIMARY KEY,
          name text NOT NULL,
          description text,
          s3_key text NOT NULL,
          incident_id text NOT NULL ,
          created_on bigint NOT NULL,
          created_by bigint NOT NULL,
          last_modified_on bigint NOT NULL,
          last_modified_by bigint NOT NULL,

          CONSTRAINT fk_amp_incident_files_incident_id FOREIGN KEY(incident_id) REFERENCES cyber_event_service.amp_incidents
        );

        CREATE INDEX idx_amp_incident_files_incident_id ON cyber_event_service.amp_incident_files (incident_id);

        postgres-cyber-events=# \d cyber_event_service.amp_incident_files;
          Table "cyber_event_service.amp_incident_files"
               Column      |  Type  | Collation | Nullable |      Default       
        ------------------+--------+-----------+----------+--------------------
         id               | text   |           | not null |
         name             | text   |           | not null | 
         description      | text   |           |          | 
         s3_key           | text   |           | not null | 
         incident_id      | text   |           | not null | 
         created_on       | bigint |           | not null | 
         created_by       | bigint |           | not null | 
         last_modified_on | bigint |           | not null | 
         last_modified_by | bigint |           | not null | 
        Indexes:
            "amp_incident_files_pkey" PRIMARY KEY, btree (id)
            "idx_amp_incident_files_incident_id" btree (incident_id)
        Foreign-key constraints:
            "fk_amp_incident_files_incident_id" FOREIGN KEY (incident_id) REFERENCES cyber_event_service.amp_incidents(id)


- `cyber_event_service.amp_incident_urls` table:

        CREATE TABLE IF NOT EXISTS cyber_event_service.amp_incident_urls (
          id text PRIMARY KEY,
          url text NOT NULL,
          incident_id text NOT NULL ,
          created_on bigint NOT NULL,
          created_by bigint NOT NULL,
          last_modified_on bigint NOT NULL,
          last_modified_by bigint NOT NULL,

          CONSTRAINT fk_amp_incident_urls_incident_id FOREIGN KEY(incident_id) REFERENCES cyber_event_service.amp_incidents
        );

        CREATE INDEX idx_amp_incident_urls_incident_id ON cyber_event_service.amp_incident_urls (incident_id);

        postgres-cyber-events=# \d cyber_event_service.amp_incident_urls
          Table "cyber_event_service.amp_incident_urls"
               Column      |  Type  | Collation | Nullable | Default 
        ------------------+--------+-----------+----------+---------
         id               | text   |           | not null |
         url              | text   |           | not null | 
         incident_id      | text   |           | not null | 
         created_on       | bigint |           | not null | 
         created_by       | bigint |           | not null | 
         last_modified_on | bigint |           | not null | 
         last_modified_by | bigint |           | not null | 
        Indexes:
            "amp_incident_urls_pkey" PRIMARY KEY, btree (id)
            "idx_amp_incident_urls_incident_id" btree (incident_id)
        Foreign-key constraints:
            "fk_amp_incident_urls_incident_id" FOREIGN KEY (incident_id) REFERENCES cyber_event_service.amp_incidents(id)

- `cyber_event_service.amp_incident_notes` table:

        CREATE TABLE IF NOT EXISTS cyber_event_service.amp_incident_notes (
          id text PRIMARY KEY,
          note text NOT NULL,
          incident_id text NOT NULL ,
          created_on bigint NOT NULL,
          created_by bigint NOT NULL,
          last_modified_on bigint NOT NULL,
          last_modified_by bigint NOT NULL,
 
          CONSTRAINT fk_amp_incident_notes_incident_id FOREIGN KEY(incident_id) REFERENCES cyber_event_service.amp_incidents
        );

        CREATE INDEX idx_amp_incident_notes_incident_id ON cyber_event_service.amp_incident_notes (incident_id);

        postgres-cyber-events=# \d cyber_event_service.amp_incident_notes
          Table "cyber_event_service.amp_incident_notes"
               Column      |  Type  | Collation | Nullable | Default 
        ------------------+--------+-----------+----------+---------
         id               | text   |           | not null |
         note             | text   |           | not null | 
         incident_id      | text   |           | not null | 
         created_on       | bigint |           | not null | 
         created_by       | bigint |           | not null | 
         last_modified_on | bigint |           | not null | 
         last_modified_by | bigint |           | not null | 
        Indexes:
            "amp_incident_notes_pkey" PRIMARY KEY, btree (id)
            "idx_amp_incident_notes_incident_id" btree (incident_id)
        Foreign-key constraints:
            "fk_amp_incident_notes_incident_id" FOREIGN KEY (incident_id) REFERENCES cyber_event_service.amp_incidents(id)

- `cyber_event_service.amp_event_incident` table:
    
        CREATE TABLE IF NOT EXISTS cyber_event_service.amp_event_incident (
          amp_event_id text NOT NULL,
          amp_incident_id text NOT NULL,

          CONSTRAINT pk_amp_event_incident PRIMARY KEY (amp_event_id, amp_incident_id),
          CONSTRAINT fk_amp_event_incident_amp_event_id FOREIGN KEY(amp_event_id) REFERENCES cyber_event_service.amp_events,
          CONSTRAINT fk_amp_event_incident_amp_incident_id FOREIGN KEY(amp_incident_id) REFERENCES cyber_event_service.amp_incidents        
        );

        postgres=# \d cyber_event_service.amp_event_incident 
          Table "cyber_event_service.amp_event_incident"
               Column      | Type | Collation | Nullable | Default 
        -----------------+------+-----------+----------+---------
         amp_event_id    | text |           | not null | 
         amp_incident_id | text |           | not null | 
        Indexes:
            "pk_amp_event_incident" PRIMARY KEY, btree (amp_event_id, amp_incident_id)
        Foreign-key constraints:
            "fk_amp_event_incident_amp_event_id" FOREIGN KEY (amp_event_id) REFERENCES cyber_event_service.amp_events(event_id)
            "fk_amp_event_incident_amp_incident_id" FOREIGN KEY (amp_incident_id) REFERENCES cyber_event_service.amp_incidents(id)