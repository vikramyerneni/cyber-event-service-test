FROM docker.fractalindustries.com/openjdk-11.0.5-jre-slim-buster

WORKDIR /

RUN apt-get update && apt-get install -y unzip curl
COPY target/universal/* ./cyber-event-service/
WORKDIR /cyber-event-service
RUN unzip cyber-event-service-*.zip