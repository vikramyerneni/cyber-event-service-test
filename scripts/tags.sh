#!/bin/bash

# $1: internal qos address
# $2: userId
# $3: customerId
# $4: input.csv

[ -n "$(tail -c1 $4)" ] && echo >> $4

exec < $4 || exit 1

while IFS=, read kind property tag; do
    echo "Storing [$kind] ($property --> $tag)"
    curl --request POST \
      --url "$1/api/v1/event/tags" \
      --header 'Content-Type: application/json' \
      --header "X-QOMPLX-UserId: $2" \
      --header "X-QOMPLX-CustomerAccountId: $3" \
      --data "{
        \"sourceId\": \"$property\",
        \"tagId\": \"$tag\",
        \"source\":\"$kind\"
       }"

    echo ""
    echo "-----------------------------------------------------"
done
