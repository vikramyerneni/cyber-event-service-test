val akkaHttpVersion      = "10.1.8"
val akkaKafkaVersion     = "1.1.0"
val akkaVersion          = "2.5.23"
val alpakkaVersion       = "1.1.1"
val slickVersion         = "3.3.1"
val reactiveMongoVersion = "0.18.3"
val kamonVersion = "2.1.4"

val ivyLocal = Resolver.file("local", file(Path.userHome.absolutePath + "/.ivy2/local"))(Resolver.ivyStylePatterns)
val artifactory = "Artifactory" at "https://artifactory.fractalindustries.com/artifactory/Fractal-Dev/"

val resolutionRepos = Seq(ivyLocal, artifactory, Resolver.sonatypeRepo("public"), Resolver.jcenterRepo)

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.fractal",
      scalaVersion := "2.12.10",
      version      := scala.io.Source.fromFile("app-version.cfg").getLines.toList.head,
    )),
    buildInfoKeys := Seq[BuildInfoKey](
      name,
      version,
      BuildInfoKey.action("commit") {
        scala.sys.process.Process("git rev-parse HEAD").!!.trim
      }
    ),
    name := "cyber-event-service",
    externalResolvers := resolutionRepos,
    credentials += Credentials(Path.userHome / ".ivy2" / ".credentials"),
    // see https://stackoverflow.com/questions/46763773/intellij-idea-cant-resolve-symbol-that-inside-scala-managed-sources-sbt-buildi/48980805#48980805
    buildInfoUsePackageAsPath := true,
    parallelExecution in Test := false,
    coverageEnabled.in(Test, test) := true,
    coverageEnabled.in(Compile, compile) := false,
    coverageMinimum := 80,
    coverageFailOnMinimum := true,
    libraryDependencies ++= Seq(
      "com.typesafe.akka"  %% "akka-actor"              % akkaVersion,
      "com.typesafe.akka"  %% "akka-http"               % akkaHttpVersion,
      "com.typesafe.akka"  %% "akka-http-spray-json"    % akkaHttpVersion,
      "com.typesafe.akka"  %% "akka-http-xml"           % akkaHttpVersion,
      "com.typesafe.akka"  %% "akka-stream"             % akkaVersion,
      "com.typesafe.akka"  %% "akka-stream-kafka"       % akkaKafkaVersion,
      "com.lightbend.akka" %% "akka-stream-alpakka-s3"  % alpakkaVersion,
      "com.lightbend.akka" %% "akka-stream-alpakka-csv" % alpakkaVersion,
      "com.enragedginger"  %% "akka-quartz-scheduler"   % "1.8.1-akka-2.5.x" exclude("com.zaxxer", "HikariCP-java6"),
      "org.squbs" %% "squbs-ext" % "0.13.0",

      "com.typesafe.akka" %% "akka-http-testkit"    % akkaHttpVersion % Test,
      "com.typesafe.akka" %% "akka-testkit"         % akkaVersion     % Test,
      "com.typesafe.akka" %% "akka-stream-testkit"  % akkaVersion     % Test,
      "org.scalatest"     %% "scalatest"            % "3.0.5"         % Test,
      "org.mockito"       %  "mockito-core"         % "2.10.0"        % Test,
      "org.mockito"       %  "mockito-inline"       % "2.10.0"        % Test,

      // logging
      "ch.qos.logback"             %  "logback-classic" % "1.2.3",
      "com.typesafe.scala-logging" %% "scala-logging"   % "3.9.2",

      // slick
      "com.typesafe.slick"  %% "slick"               % slickVersion,
      "com.typesafe.slick"  %% "slick-hikaricp"      % slickVersion,
      "org.postgresql"      %  "postgresql"          % "42.2.12",
      "org.slf4j"           %  "slf4j-nop"           % "1.7.26",
      "com.github.tminglei" %% "slick-pg"            % "0.18.0",
      "com.github.tminglei" %% "slick-pg_spray-json" % "0.18.0",
      "io.spray"            %% "spray-json"          % "1.3.5",
      "org.liquibase"       %  "liquibase-core"      % "3.6.3",

      // other
      "ch.megard"              %% "akka-http-cors"            % "0.4.1",
      "com.beachape"           %% "enumeratum"                % "1.5.15",
      "com.github.kolotaev"    %% "ride"                      % "1.0.1",
      "com.google.crypto.tink" %  "tink"                      % "1.3.0",
      "org.reactivemongo"      %% "reactivemongo"             % reactiveMongoVersion,
      "org.reactivemongo"      %% "reactivemongo-akkastream"  % reactiveMongoVersion,
      "com.github.cb372"       %% "scalacache-guava"          % "0.28.0",
      "com.bettercloud"        %  "vault-java-driver"         % "5.1.0",
      "com.amazonaws"          %  "aws-java-sdk-s3"           % "1.11.126",
      "com.fractal.auth"       %% "auth-service-client-scala" % "3.0.1",
      "org.antlr"              % "antlr4"                     % "4.8-1",
      "de.zalando"             %% "beard"                     % "0.3.3_1.8",
      "com.github.tototoshi"   %% "scala-csv"                 % "1.3.6",

      // testing
      "com.dimafeng"            %% "testcontainers-scala"     % "0.33.0" % Test,
      "io.findify"              %% "s3mock"                   % "0.2.4"  % Test,
      "io.github.embeddedkafka" %% "embedded-kafka"           % "2.3.0"  % Test,
      "org.testcontainers"      % "postgresql"                % "1.12.4" % Test,

      // monitoring
      "io.kamon" %% "kamon-core"           % kamonVersion,
      "io.kamon" %% "kamon-status-page"    % kamonVersion,
      "io.kamon" %% "kamon-prometheus"     % kamonVersion,
      "io.kamon" %% "kamon-system-metrics" % kamonVersion,
      "io.kamon" %% "kamon-akka-http"      % kamonVersion
    )
)

javaAgents += "io.kamon" % "kanela-agent" % "1.0.6"
enablePlugins(JavaAppPackaging, BuildInfoPlugin, JavaAgent)
parallelExecution := false

buildInfoOptions += BuildInfoOption.BuildTime

testOptions in Test += Tests.Argument(TestFrameworks.ScalaTest, "-oID")
