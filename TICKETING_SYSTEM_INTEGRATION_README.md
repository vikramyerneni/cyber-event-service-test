THIRD PARTY SYSTEM INTEGRATION
---------
This part of cyber-event-service handles third party system integration in and out of cyber system:

API
---
- List configurations :
    - GET `http://event-service.qweb.qos/api/v1/observed-system/{system-id}/configs`
    - Headers:

            X-QOS-AUTH-TOKEN

    - Params:
        - `enabled` (boolean, optional) : Setting status to true will only return enabled integrations.
                                          Setting it to false returns all the disabled integrations.
                                          If not provided, does not use it to filter them.
        - `q` (string, optional) : The search string of the name to filter by (case insensitive)

    - Response content type: `application/json`

    - Response:

            {
              "results": [
                {
                  "id": "string",
                  "observedSystemId": "string",
                  "thirdPartySystem": "string", // enum value
                  "name": "string",
                  "description": "string",
                  "enabled": boolean
                },
                ...
              ]
            }
      
- Create configuration:
    - POST `http://event-service.qweb.qos/api/v1/observed-system/{system-id}/configs`
        - Headers:
    
                X-QOS-AUTH-TOKEN
    
        - Response content type: `application/json`
        - Body: Below mentioned fields are saved in database during creation.
          
                {
                  "name": "string",
                  "description": "string" // optional
                  "thirdPartySystem": "string", // enum value 
                  "emails": ["string"], // optional
                }
    
        - Response Code: 201.

- Update configuration:
    - PUT `http://event-service.qweb.qos/api/v1/observed-system/{system-id}/configs/{config-id}`
        - Headers:
    
                X-QOS-AUTH-TOKEN
    
        - Response content type: `application/json`
        - Body: Below mentioned fields are saved in database during creation.
          
                {
                  "name": "string",
                  "description": "string",
                  "thirdPartySystem": "string", 
                  "emails": ["string"], // optional
                  "enabled": boolean
                }
                
        - Note: Here, every field is required. If user want to update certain fields, sent payload where
                other fields contain original values.
    
        - Response Code: 200.
                
- Get configuration:
    - GET `http://event-service.qweb.qos/api/v1/observed-system/{system-id}/configs/{config-id}`
    - Headers:

            X-QOS-AUTH-TOKEN

    - Response content type: `application/json`

    - Response: 
 
            {
              "id": "string",
              "observedSystemId": "string",
              "thirdPartySystem": "string", // enum value
              "name": "string",
              "description": "string",
              "enabled": boolean, 
              "emails": ["string"], // optional
            }

- Delete configuration:
    - DELETE `http://event-service.qweb.qos/api/v1/observed-system/{system-id}/configs/{config-id}`
    - Headers:

            X-QOS-AUTH-TOKEN

    - Response Code: 200.

- Get the token associated with a configuration:
    - Returns the decrypted token.
    - GET `http://event-service.qweb.qos/api/v1/observed-system/{system-id}/configs/{config-id}/token`
    - Headers:

            X-QOS-AUTH-TOKEN

Postgres Notes
--------------
It's assumed that the `cyber_event_service` schema is already generated in Postgres.

- `cyber_event_service.ns_ticketing_system` enum:

        CREATE TYPE cyber_event_service.ns_third_party_system AS ENUM ('JIRA', 'SERVICENOW', 'REMEDY', 'ARCHER', 'SPLUNK', 'QRADAR');
        
- `cyber_event_service.ns_ts_configurations` table:

        CREATE TABLE IF NOT EXISTS cyber_event_service.ns_third_party_integration_configs (
          id text PRIMARY KEY,
          observed_system_id text NOT NULL,
          third_party_system cyber_event_service.ns_third_party_system NOT NULL,
          name text NOT NULL,
          description text,
          emails text[],
          encrypted_token bytea NOT NULL,
          enabled boolean NOT NULL DEFAULT true,
          created_on bigint,
          created_by text NOT NULL,
          last_modified_on bigint,
          last_modified_by text NOT NULL,
          CONSTRAINT third_party_system_name_unique UNIQUE (third_party_system, name)
        );
        
        CREATE INDEX idx_ns_ts_configurations_observed_system_id ON cyber_event_service.ns_third_party_integration_configs(observed_system_id);

        postgres-cyber-events=# \d cyber_event_service.ns_third_party_integration_configs;
                         Table "cyber_event_service.ns_third_party_integration_configs"
               Column       |                   Type                    | Collation | Nullable | Default 
        --------------------+-------------------------------------------+-----------+----------+---------
         id                 | text                                      |           | not null | 
         observed_system_id | text                                      |           | not null | 
         third_party_system | cyber_event_service.ns_third_party_system |           | not null | 
         name               | text                                      |           | not null | 
         description        | text                                      |           |          | 
         emails             | text[]                                    |           |          | 
         encrypted_token    | bytea                                     |           | not null |
         enabled            | boolean                                   |           | not null | true
         created_on         | bigint                                    |           |          | 
         created_by         | text                                      |           | not null | 
         last_modified_on   | bigint                                    |           |          | 
         last_modified_by   | text                                      |           | not null | 
        Indexes:
            "ns_third_party_integration_configs_pkey" PRIMARY KEY, btree (id)
            "third_party_system_name_unique" UNIQUE CONSTRAINT, btree (third_party_system, name)
            "token_unique" UNIQUE, btree (encrypted_token)
            "idx_ns_ts_configurations_observed_system_id" btree (observed_system_id)

- `cyber_event_service.ns_mails` table:

        CREATE TABLE IF NOT EXISTS cyber_event_service.ns_ticketing_system_mails (
          id text PRIMARY KEY,
          config_id text NOT NULL,
          inc_id text NOT NULL,
          inc_title text NOT NULL,
          inc_status amp_incident_status NOT NULL,
          inc_categories text[],
          inc_tactics text[],
          inc_event_ids text[],
          inc_created_on bigint NOT NULL,
          delivered boolean NOT NULL DEFAULT false,
          created_on bigint,
          last_modified_on bigint,
          third_party_system cyber_event_service.ns_third_party_system NOT NULL,
          emails text[] NOT NULL DEFAULT '{}',
          inc_severity smallint NOT NULL
        );
        
        CREATE INDEX idx_ns_mails_integration_id ON cyber_event_service.ns_ticketing_system_mails(config_id);
        
        postgres-cyber-events=# \d cyber_event_service.ns_ticketing_system_mails;
                                Table "cyber_event_service.ns_ticketing_system_mails"
               Column       |                   Type                    | Collation | Nullable |   Default    
        --------------------+-------------------------------------------+-----------+----------+--------------
         id                 | text                                      |           | not null | 
         config_id          | text                                      |           | not null | 
         inc_id             | text                                      |           | not null | 
         inc_title          | text                                      |           | not null | 
         inc_status         | amp_incident_status                       |           | not null | 
         inc_categories     | text[]                                    |           |          | 
         inc_tactics        | text[]                                    |           |          | 
         inc_event_ids      | text[]                                    |           |          | 
         inc_created_on     | bigint                                    |           | not null | 
         delivered          | boolean                                   |           | not null | false
         created_on         | bigint                                    |           |          | 
         last_modified_on   | bigint                                    |           |          | 
         third_party_system | cyber_event_service.ns_third_party_system |           | not null | 
         emails             | text[]                                    |           | not null | '{}'::text[]
         inc_severity       | smallint                                  |           | not null | 
        Indexes:
            "ns_ticketing_system_mails_pkey" PRIMARY KEY, btree (id)
            "idx_ns_mails_integration_id" btree (config_id)

DEPLOYMENT NOTES
----------------

- For Tink, the encryption tool that we're using, we need to generate a keyset file for each environment and place the
file in Vault. This file contains a JSON object. To generate this file, do the following:

    1. In the `initialize()` method in `EncryptionUtils.scala`, enable the `generateKeyset()` method (it's normally commented out).
    2. In the `generateKeyset()` method, replace `"/somePathToFile"` with a path (including the file name) to the desired location of the generated file.
    3. In `EncryptionUtilsSpec.scala`, enable the `"print a compact keyset JSON file"` test by replacing *ignore* with *in*.
    4. In the above test, assign the path specified in step two to the `pathName` variable.
    5. Run `sbt` in the project root directory, then enter `testOnly qomplx.event.util.EncryptionUtilsSpec` in the sbt console.
    6. A compact version of the generated JSON file will be printed to the terminal.


Design Decision Notes
---------------------

1) The column `config_id` in `ns_ticketing_system_mails` no more refers to the `id` column of `ns_third_party_integration_configs`.
   We lock the rows for which we are trying to send emails, using `select for update` construct of PostgreSQL to avoid having 
   problems if multiple instances running. The semantics of `select for update` stats that the referenced rows from other tables 
   are also locked, and we don't want to lock rows of `ns_third_party_integration_configs` table while performing email sending 
   operation, and we do that periodically with small intervals.