#!groovy
@Library('DevOps')_
import groovy.json.JsonSlurper
import groovy.json.JsonOutput
import groovy.transform.Field

def scm_url = "git@bitbucket.org:fractalindustries/cyber-event-service.git"
def scm_branch = "${BRANCH}"
def scm_credentials = "dcos-jenkins"
def secret = "${SECRET}"
def ttl = "${TTL}"

def appName ='cyber-event-service'
def docker_registry_url = "https://docker.fractalindustries.com"
@Field def docker_image = 'docker.fractalindustries.com/cyber-event-service'
def docker_artifactory_credential_id = "artifactory"

def err = null
def USER = ''

try{
    node('jenkins-dind-nodejs') {
        wrap([$class: 'BuildUser']) {
            USER = "${currentBuild.rawBuild.getCause(hudson.model.Cause$UserIdCause)}" == 'null' ? "FRACWEB_BOT" : "${BUILD_USER}"
        }
        print 'DEBUG: Build Trigger Details = ' + "${scm_branch}" + " by " + "${USER}"

        stage ('Checkout') {
            // Wipe working dir, since sometimes jenkins doesn't get new version of code.
            deleteDir()
            git url: scm_url, branch: scm_branch, credentialsId: scm_credentials
        }

        stage('Build package') {
            sbtBuildPackage()
        }

        //to add git commit id as property for dockerimage
        stage('Git COMMIT ID') {
            print "DEBUG: START ENV CAPTURE"
            sh "git rev-parse HEAD > .git/head"
            COMMIT_ID_LONG = readFile('.git/head')
        }

        stage("Deployment") {
            VERSION = sbtAppVersion()
            (buildInfo, rtServer, appId) = dockerBuildAndDeploy(appName, VERSION, env.BUILD_NUMBER, scm_branch, COMMIT_ID_LONG, "build_pipeline/dev/environment.json", "build_pipeline/dev/manifest.json", ttl, false)
        }

        stage("Post Deployment Checks") {
            mesosHealthCheck(appId)
        }

        stage ('Xray scan') {
            def result = xrayScan(rtServer, buildInfo)
            xrayjson = parseJson(result)
        }
    }
} catch (caughtError) {
    err = caughtError
    currentBuild.result = "FAILURE"
} finally {
    if (currentBuild.result != "ABORTED" && currentBuild.result != "FAILURE") {
        node {
            slackSend channel: '#sbt-backend-builds',
                    color:   '#00FF00',
                    message: """Status: *Success Job* - ${env.JOB_NAME} [${env.BUILD_NUMBER}] (<${env.BUILD_URL}|Open>)
                                  |Started by: ${USER}
                                  |*Branch* : ${scm_branch}
                                  |Jfrog Report: $xrayjson.summary.total_alerts alerts were generated, (<${xrayjson.summary.more_details_url}|XrayReport>)""".stripMargin()
        }
    }

    if (err) {
        slackSend channel: '#sbt-backend-builds',
                color: '#FF0000',
                message: """Status: *FAILURE Job* - Job ${env.JOB_NAME} [${env.BUILD_NUMBER}] ] (<${env.BUILD_URL}|Open>)
                              |Build started by: ${USER}
                              |*Branch* : ${scm_branch}
                              |Failure Cause: $err""".stripMargin()
        throw err
    }
}
