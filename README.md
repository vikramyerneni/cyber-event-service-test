Cyber Event Service (Migrated to GitLab)
====================

Does the following:

1. Subscribes to AMP event Kafka topics (named `cyber-amp-events-<observed system id>`) and writes them to Postgres.
2. Exposes an API to retrieve the events associated with an observed system.
3. Exposes an API to handle incidents, which are linked to one or more events.

Data Model
----------

    {
      "id": String,
      "name": String,
      "description": String,
      "type": String,
      "severity": Int,
      "timestamp": Long,
      "source": String, // ConMon, Kerberos Pipeline, Rules Detections, MDTSDB Trigger
      "sourceId": String, // put part of the hash here, for example (see CYBER-868)
      "categories": [String],
      "tactics": [String],
      "observedSystemId": String
    }

- `"id"` field
    - Optional when the event is published to Kafka.
    - If the event does not have this field when the event is published to Kafka, the service will create an ID before
      persisting the event to Postgres.
    - A created ID will be the "EVT-" prefix followed by a [xid](https://github.com/rs/xid), which is a globally unique
      identifier. A xid has a fixed length of 20 characters and, unlike the [uniqueness counter](https://bitbucket.org/fractalindustries/uniqueness-counter/src/master/), does not require
      the use of a database.

API
---

- Retrieve all the events associated with an observed system:
    - GET `http://cyber-event-service.fos/api/v1/event/observed-system/{system-id}`
    - Response content type: `application/json`
    - Parameters:
        - `limit` (integer, optional): if `offset` is present, `limit` is the number of events per page. If `offset` is not
          present, `limit` is the maximum number of events returned. Must be at least zero and no greater than 500.
        - `offset` (integer, optional): used for pagination. If `limit` is absent, then a `limit` of 500 is used. Must be
          at least zero and no greater than 500.
        - `from-ts` (long, optional): Epoch time in milliseconds. Filters on events whose `"timestamp"` field value is greater
          than or equal to this parameter. Example: `http://cyber-event-service.fos/api/v1/event/observed-system/system123?from-ts=1562178392502`
        - `to-ts` (long, optional): Epoch time in milliseconds. Filters on events whose `"timestamp"` field value is less
          than or equal to this parameter. Example: `http://cyber-event-service.fos/api/v1/event/observed-system/system123?to-ts=1562178392502`
        - `from-severity` (integer, optional): Filters on events whose `"severity"` field value is greater
          than or equal to this parameter. Example: `http://cyber-event-service.fos/api/v1/event/observed-system/system123?from-severity=10`
        - `to-severity` (integer, optional): Filters on events whose `"severity"` field value is less
          than or equal to this parameter. Example: `http://cyber-event-service.fos/api/v1/event/observed-system/system123?to-severity=90`
        - `categories` (comma separated strings, optional): Filters on events whose `"categories"` field value contains this parameter. 
          Example: `http://cyber-event-service.fos/api/v1/event/observed-system/system123?categories=one,two,there`
        - `tactics` (comma separated strings, optional): Filters on events whose `"tactics"` field value contains this parameter. 
          Example: `http://cyber-event-service.fos/api/v1/event/observed-system/system123?tactics=one,two,there`
        - `q` (string, optional): Search on events whose `"id"`, `"name"` and `"description"` fields contains this parameter. 
          Example: `http://cyber-event-service.fos/api/v1/event/observed-system/system123?q=someString`
    - Example response:

            {
              "results": [{
                "id": "EVT-9m4e2mr0ui3e8a215n4g",
                "name": "Kerberos Golden Ticket",
                "description": "10.11.22.33 accessed 10.22.33.44",
                "type": "",
                "severity": 90,
                "timestamp": "1562176658297",
                "source": "MDTSDB trigger",
                "sourceId": "Kerberos pipeline",
                "categories": ["Credential Access", "Lateral Movement"],
                "tactics": ["Credential Forging", "Pass the Ticket"],
                "observedSystemId": "5cf81527f4428a000653ab36"
              },
              {
                "id": "EVT-9m4e2mr0ui3e8a2343ox",
                "name": "Kerberos Silver Ticket",
                "description": "10.11.22.55 accessed 10.22.33.66",
                "type": "Kerberos",
                "severity": 90,
                "timestamp": "1562176660133",
                "source": "MDTSDB trigger",
                "sourceId": "Kerberos pipeline",
                "categories": ["Credential Access", "Lateral Movement"],
                "tactics": ["Credential Forging", "Pass the Ticket"],
                "observedSystemId": "5cf81527f4428a000653ab36"
              }],
              "count": 2
            }
        
    - GET `http://cyber-event-service.fos/api/v1/event/observed-system/{system-id}/count`
        - Response content type: `application/json`
        - Parameters:
            - `days` (integer, optional): if value of param "days" is `n`, then endpoint will give total count of events for last `n` days
              if param is not provided, then default value of "days" is 90.
              if param's value is more than 90, then it will be reset to 90.
              Example: /api/v1/event/observed-system/system123/count?days=20
              Example: /api/v1/event/observed-system/system123/count (days will be 90)
              Example: /api/v1/event/observed-system/system123/count?days=120 (days will reset to 90)
        - Example response:
          
          
            {
              "count": 285
            }
            
        
   - GET `http://cyber-event-service.fos/api/v1/event/observed-system/{system-id}/count/by-severity`
        - Response content type: `application/json`
        - Note: This endpoint will fetch counts by severity for last 24 hours
        - Example response:
          
          
            {
              "high": 40,
              "low": 26,
              "medium": 34,
              "total": 100
            }


- SIEM Ingestion Routes:
    - GET `http://cyber-event-service.fos/api/v1/event/siem/<qradar or splunk>`
        - Response content type: `application/json`
        - Required Headers:
            - `X-API-KEY` authentication API key (Under `user.api_key` for an Observed System)
                - Note: The key will be generated specifically for this in the future, rather than using the Obs Sys key
        - Parameters:
            - `last` (integer, required) The last incremental ID read by the consuming service
                - Note: For the case that `last < 0`, the _most recent_ `limit` events will be returned
            - `limit` (integer, optional) Num of elements to return
        - Example Response:

                {
                    "last": 1250, <----
                    "results": [
                        {
                            "incIdx": 1250, <----
                            "categories": ["Credential Access", "Lateral Movement"],
                            "description": "187.41.140.147 presented to 214.55.175.87",
                            "id": "EVT-bp618go5qkpg021chrp0",
                            "name": "Kerberos Golden Ticket",
                            "observedSystemId": "5e29d685e7baf70fb6c7e6ba",
                            "severity": 90,
                            "source": "Kerberos Pipeline",
                            "sourceId": "Kerberos Pipeline Detection",
                            "tactics": ["Credential Forging", "Pass the Ticket"],
                            "timestamp": 1582044227047,
                            "type": "Kerberos"
                        },
                        ...
                    ]
                }


Postgres Notes
--------------

- `cyber_event_service` schema:

        CREATE SCHEMA IF NOT EXISTS cyber_event_service;

- `cyber_event_service.amp_events` table:

        CREATE TABLE IF NOT EXISTS cyber_event_service.amp_events (
          event_id text PRIMARY KEY,
          observed_system_id text NOT NULL,
          name text NOT NULL,
          description text NOT NULL,
          type text NOT NULL,
          severity int NOT NULL,
          timestamp bigint NOT NULL,
          source text NOT NULL,
          source_id text NOT NULL,
          categories text[] DEFAULT '{}',
          tactics text[] DEFAULT '{}',
          extra_info jsonb,
          tokens tsvector
        );
        
        CREATE INDEX idx_timestamp ON cyber_event_service.amp_events (timestamp);
        CREATE INDEX idx_obs_sys_id ON cyber_event_service.amp_events (observed_system_id);
        CREATE INDEX amp_events_textsearch_idx ON cyber_event_service.amp_events USING gin (tokens);
        
        CREATE TRIGGER tsvector_amp_events_tokens_update
          BEFORE INSERT OR UPDATE
          ON cyber_event_service.amp_events
          FOR EACH ROW
          EXECUTE PROCEDURE tsvector_update_trigger('tokens', 'pg_catalog.english', 'event_id', 'name', 'description');
        
        postgres-cyber-events=# \d cyber_event_service.amp_events;
                Table "cyber_event_service.amp_events"
               Column       |   Type   |      Modifiers       
        --------------------+----------+----------------------
         event_id           | text     | not null
         observed_system_id | text     | not null
         name               | text     | not null
         description        | text     | not null
         type               | text     | not null
         severity           | integer  | not null
         timestamp          | bigint   | not null
         source             | text     | not null
         source_id          | text     | not null
         categories         | text[]   | default '{}'::text[]
         tactics            | text[]   | default '{}'::text[]
         extra_info         | jsonb    | 
         tokens             | tsvector | 
        Indexes:
            "amp_events_pkey" PRIMARY KEY, btree (event_id)
            "amp_events_textsearch_idx" gin (tokens)
            "idx_obs_sys_id" btree (observed_system_id)
            "idx_timestamp" btree ("timestamp")
        Referenced by:
            TABLE "cyber_event_service.amp_event_incident" CONSTRAINT "fk_amp_event_incident_amp_event_id" FOREIGN KEY (amp_event_id) REFERENCES cyber_event_service.amp_events(event_id)
        Triggers:
            tsvector_amp_events_tokens_update BEFORE INSERT OR UPDATE ON cyber_event_service.amp_events FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger('tokens', 'pg_catalog.english', 'event_id', 'name', 'description')

- Note the unique constraint on `event_id`. The service will ignore events with duplicate IDs.
- The `extra_info` column stores arbitrary JSON. For example, it could store the original message that triggered the event.
- The `tokens` column (indexed) stores keywords from `event_id`, `name` and `description` values to facilitate search on those fields.

Environment Variables
---------------------

- KAFKA_BOOTSTRAP_SERVERS
- KAFKA_HEALTH_PING_TIMEOUT: The duration after which to consider a Kafka health ping a failure, i.e. `1.seconds`. Defaults to `5.seconds`
- POSTGRES_HEALTH_PING_TIMEOUT: The duration after which to consider a Kafka health ping a failure, i.e. `1.seconds`. Defaults to `5.seconds`
- POSTGRES_HOST
- POSTGRES_PORT
- POSTGRES_USER
- POSTGRES_PASSWORD
- POSTGRES_SLICK_NUM_THREADS: `numThreads` config in [this reference](http://slick.lightbend.com/doc/3.3.1/api/index.html#slick.jdbc.JdbcBackend$DatabaseFactoryDef@forConfig(String,Config,Driver,ClassLoader):Database)
- POSTGRES_SLICK_MAX_CONNECTIONS: `maxConnections` config in [this reference](http://slick.lightbend.com/doc/3.3.1/api/index.html#slick.jdbc.JdbcBackend$DatabaseFactoryDef@forConfig(String,Config,Driver,ClassLoader):Database)
- POSTGRES_HIKARI_MAX_LIFETIME: Hikari setting as described in [this reference](https://github.com/brettwooldridge/HikariCP)
- POSTGRES_EVENTS_PURGER_OLDER_THAN_DAYS: Defaults to `91`.
- MAX_REQUESTED_RECORDS_ALLOWED: The maximum number of records allowed in one request (def 1000)
- DEFAULT_REQUEST_LIMIT: Default limit (page size) on a request if none given (def 10)
- AKKA_SERVER_REQUEST_TIMEOUT: Request timeout. Defaults to `30s` (30 seconds). [docs](https://doc.akka.io/docs/akka-http/current/common/timeouts.html#request-timeout)
- AKKA_SERVER_IDLE_TIMEOUT: Idle timeout for connections. Should always be bigger than `AKKA_SERVER_REQUEST_TIMEOUT`. Defaults to `60s` (60 seconds)
- S3_EVENTS_DEFAULT_ENDPOINT
- S3_EVENTS_WARM_STORAGE_BUCKET_NAME
- S3_EVENTS_WARM_STORAGE_REGION_NAME: Defaults to `us-east-2`.
- SIEM_DEFAULT_BATCH_SIZE: Page size for SIEM query if none provided. Default 250

## If exposing this service publicly

- ~~USE_API_KEY_HEADER: Set this environment variable to `true` when exposing this API publicly so that the authentication can be done by checking the `X-API-KEY` header. This is a very hacky setup and will be replaced in the future.~~

  - Note: We have removed above header for reasons described in comments under CYBER-1746 ticket.

- CYBER_MONGODB_HOST: the mongo host from which observed-system collection can be queried. This is used for API-KEY checks when called externally.

## Vault Setup Environment Variables

- VAULT_API_URL: The URL of the vault instance in the current environment/
- VAULT_ROLE_ID: The vault app-role id for cyber-event-service (applicable in the current env)
- VAULT_SECRET_ID: The vault secret-id for cyber-event-service (applicable in the current env)

### SSL Setup

SSL is not enabled by default (at least in the MT environment). Some VPC deployments require an SSL cert since the API gets exposed publicly _in some cases_. The SSL gets enabled if the `VAULT_SSL_CERT_PATH` env-var is set. The required env-vars are explained below.

This is not an ideal way of doing this, and the TLS shouldn't be terminated within the service itself (even if we want end to end TLS). Since this requires the service having access to QOMPLX wildcard SSL cert (insane).

Following env-vars are required for SSL to work. Note that all of them should be adjusted for the particular environment.

- VAULT_SSL_CERT_PATH: the vault path where the cert is stored. This is a JSON object containing two values. Described in the following two env-vars.
- VAULT_SSL_PKCS_KEY: the key of actual base64 encoded cert in the JSON object obtained from vault at `VAULT_SSL_CERT_PATH`.
- VAULT_SSL_PKCS_PASSWORD: the key for the field containing the password required for this SSL cert in the JSON object obtained from vault at `VAULT_SSL_CERT_PATH`. 

**note** When deploying with SSL, make sure to change the health-check config in marathon to use HTTPS instead of HTTP.

Since we are using the qomxplxos.com certs for SSL, and the certs do not match the domain when accessing the service via an l4lb address in marathon, we need to setup a different port for http access for internal services. For ex. fw-cyber accesses this service internally using an l4lb address. And as of now, this needs to be an HTTP port. The HTTP port is 9876. So, when deploying with SSL, we'd also need to change the l4lb address used by other services. This is, hopefully, a temporary problem, and we'd replace the SSL handling by a front-proxy that handles it for all of our publicly exposed services.

Running locally
----------------

When running locally, you want to use application.local.conf. To enable that, run this service with:

```
sbt -Dqlocal
```

The `q` prefix was used to make sure it doesn't conflict with anything unintentionally.



## Events emails

### Environment Variables

 **EVENTS_MAILER_ENABLED**: Enabled true/false, default is false
 **EVENTS_MAILER_FROM_EMAIL**: from, default noreply@qomplx.com
 **EVENTS_EMAIL_RECIPIENTS**: comma separated list of recipients, default ""
 **EVENTS_MAILER_THREAD_POOL_SIZE**: sending pool size, default 1

### Metrics

**event_emails_sent_total** - total counter of sent emails.
**email_events_sent_total** - total counter of sent events.
**email_sent_errors_total** - total counter of sent errors.